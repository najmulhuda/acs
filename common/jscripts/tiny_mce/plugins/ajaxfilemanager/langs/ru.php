<?php
	/**
	 * language pack
	 * @author Logan Cai (cailongqun [at] yahoo [dot] com [dot] cn)
	 * @link www.phpletter.com
	 * @since 22/April/2007
	 *
	 */
	define('DATE_TIME_FORMAT','d/M/Y H:i:s');
	//Common
	//Menu
	
	
	
	
	define('MENU_SELECT','Выбрать');
	define('MENU_DOWNLOAD','Скачать');
	define('MENU_PREVIEW','Предпросмотр');
	define('MENU_RENAME','Переименовать');
	define('MENU_EDIT','Правка');
	define('MENU_CUT','Вырезать');
	define('MENU_COPY','Копировать');
	define('MENU_DELETE','Удалить');
	define('MENU_PLAY','Проиграть');
	define('MENU_PASTE','Вставить');
	
	//Label
		//Top Action
		define('LBL_ACTION_REFRESH','Обновить');
		define('LBL_ACTION_DELETE','Удалить');
		define('LBL_ACTION_CUT','Вырезать');
		define('LBL_ACTION_COPY','Копировать');
		define('LBL_ACTION_PASTE','Вставить');
		define('LBL_ACTION_CLOSE','Закрыть');
		define('LBL_ACTION_SELECT_ALL','Выбрать все');
		//File Listing
	define('LBL_NAME','Имя');
	define('LBL_SIZE','Размер');
	define('LBL_MODIFIED','Изменен');
		//File Information
	define('LBL_FILE_INFO','Инфо файла:');
	define('LBL_FILE_NAME','Имя:');	
	define('LBL_FILE_CREATED','Создан:');
	define('LBL_FILE_MODIFIED','Изменен:');
	define('LBL_FILE_SIZE','Размер:');
	define('LBL_FILE_TYPE','Тип:');
	define('LBL_FILE_WRITABLE','Запись?');
	define('LBL_FILE_READABLE','Чтение?');
		//Folder Information
	define('LBL_FOLDER_INFO','Инфо папки');
	define('LBL_FOLDER_PATH','Папка:');
	define('LBL_CURRENT_FOLDER_PATH','Путь текущей папки:');
	define('LBL_FOLDER_CREATED','Создана:');
	define('LBL_FOLDER_MODIFIED','Изменена:');
	define('LBL_FOLDER_SUDDIR','Подпапки:');
	define('LBL_FOLDER_FIELS','Файлы:');
	define('LBL_FOLDER_WRITABLE','Запись?');
	define('LBL_FOLDER_READABLE','Чтение?');
	define('LBL_FOLDER_ROOT','Корень');
		//Preview
	define('LBL_PREVIEW','Предпросмотр');
	define('LBL_CLICK_PREVIEW','Щелкните для предпросмотра.');
	//Buttons
 define('LBL_BTN_SELECT','Выбор');
 define('LBL_BTN_CANCEL','Отмена');
 define('LBL_BTN_UPLOAD','Загрузка');
 define('LBL_BTN_CREATE','Создать');
 define('LBL_BTN_CLOSE','Закрыть');
 define('LBL_BTN_NEW_FOLDER','Новая папка');
 define('LBL_BTN_NEW_FILE','Новый файл');
 define('LBL_BTN_EDIT_IMAGE','Правка');
 define('LBL_BTN_VIEW','Выберите вид');
 define('LBL_BTN_VIEW_TEXT','Текст');
 define( 'LBL_BTN_VIEW_DETAILS','Детали');
 define('LBL_BTN_VIEW_THUMBNAIL','Эскизы');
 define( 'LBL_BTN_VIEW_OPTIONS','Показать в:');
 //разбиение на страницы
 define('PAGINATION_NEXT','Следующая');
 define('PAGINATION_PREVIOUS','Предыдущая');
 define('PAGINATION_LAST','Последняя');
 define('PAGINATION_FIRST','Первая');
 define('PAGINATION_ITEMS_PER_PAGE','Показать %s элементов для каждой страницы');
 define('PAGINATION_GO_PARENT','В родительскую папку');
 //Система
 define('SYS_DISABLED','Нет прав доступа: система заблокирована.');
	
 //Вырезка
 define('ERR_NOT_DOC_SELECTED_FOR_CUT','Никакой документ(ы) не выбран для вырезки.');
 //Копия
 define('ERR_NOT_DOC_SELECTED_FOR_COPY','Никакой документ(ы) не выбран для копирования.');
 //Вставить
 define('ERR_NOT_DOC_SELECTED_FOR_PASTE','Никакой документ(ы) не выбран для вставки.');
 define('WARNING_CUT_PASTE',' Вы действительно хотите переместить выбранные документы в текущую папку?');
 define('WARNING_COPY_PASTE','Вы действительно хотите скопировать выбранные документы в текущую папку?');
 define('ERR_NOT_DEST_FOLDER_SPECIFIED','Папка - получатель не указана.');
 define('ERR_DEST_FOLDER_NOT_FOUND','Папка - получатель не найдена.');
 define('ERR_DEST_FOLDER_NOT_ALLOWED','Вы не можете переместить файлы в эту папку');
 define('ERR_UNABLE_TO_MOVE_TO_SAME_DEST','Сбой перемещения этого файла (%s): Оригинальный путь такой же, как путь адресата.');
 define('ERR_UNABLE_TO_MOVE_NOT_FOUND','Сбой перемещения этого файла (%s): Исходный файл не существует.');
 define('ERR_UNABLE_TO_MOVE_NOT_ALLOWED','Сбой перемещения этого файла (%s): Исходный файл не доступен.');
 
 define('ERR_NOT_FILES_PASTED','Никакой файл(ы) не был вставлен.');

 //Искать
 define('LBL_SEARCH','Поиск');
 define('LBL_SEARCH_NAME','Полное/частичное имя файла:');
 define('LBL_SEARCH_FOLDER','Искать в:');
 define('LBL_SEARCH_QUICK','Быстрый поиск');
 define('LBL_SEARCH_MTIME','Время изменения файла (диапазон):');
 define('LBL_SEARCH_SIZE','Размер файла:');
 define('LBL_SEARCH_ADV_OPTIONS','Дополнительные параметры');
 define('LBL_SEARCH_FILE_TYPES','Тип файла:');
 define('SEARCH_TYPE_EXE','Приложение');
	
 define('SEARCH_TYPE_IMG','Изображение');
 define('SEARCH_TYPE_ARCHIVE','Архив');
 define('SEARCH_TYPE_HTML','HTML');
 define('SEARCH_TYPE_VIDEO','Видео');
 define('SEARCH_TYPE_MOVIE','Фильм');
 define('SEARCH_TYPE_MUSIC','Музыка');
 define('SEARCH_TYPE_FLASH','Flash');
 define('SEARCH_TYPE_PPT','PowerPoint');
 define('SEARCH_TYPE_DOC','Документ');
 define('SEARCH_TYPE_WORD','Word');
 define('SEARCH_TYPE_PDF','PDF');
 define('SEARCH_TYPE_EXCEL','Excel');
 define('SEARCH_TYPE_TEXT','Текст');
 define('SEARCH_TYPE_UNKNOWN','Неизвестно');
 define('SEARCH_TYPE_XML','XML');
 define('SEARCH_ALL_FILE_TYPES','Все типы файлов');
 define('LBL_SEARCH_RECURSIVELY','Рекурсивно:');
 define('LBL_RECURSIVELY_YES','Да');
 define('LBL_RECURSIVELY_NO','Нет');
 define('BTN_SEARCH','Искать');
 //thickbox
 define('THICKBOX_NEXT','Следующий&gt;');
 define('THICKBOX_PREVIOUS','&lt; Предыдущий');
 define('THICKBOX_CLOSE','Закрыть');
 //Календарь
 define('CALENDAR_CLOSE','Закрыть');
 define('CALENDAR_CLEAR','Очистить');
 define('CALENDAR_PREVIOUS','&lt; Предыдущий');
 define('CALENDAR_NEXT','Следующий&gt;');
 define('CALENDAR_CURRENT','Сегодня');
 define('CALENDAR_MON','Пн');
 define('CALENDAR_TUE','Вт');
 define('CALENDAR_WED','Ср');
 define('CALENDAR_THU','Чт');
 define('CALENDAR_FRI','Пт');
 define('CALENDAR_SAT','Сб');
 define('CALENDAR_SUN','Вск');
 define('CALENDAR_JAN','Янв');
 define('CALENDAR_FEB','Фев');
 define('CALENDAR_MAR','Март');
 define('CALENDAR_APR','Апр');
 define('CALENDAR_MAY','Май');
 define('CALENDAR_JUN','Июнь');
 define('CALENDAR_JUL','Июль');
 define('CALENDAR_AUG','Август');
 define('CALENDAR_SEP','Сент');
 define('CALENDAR_OCT','Окт');
 define('CALENDAR_NOV','Ноя');
 define('CALENDAR_DEC','Дек');
 //СООБЩЕНИЯ ОБ ОШИБКАХ
  //удаление
 define('ERR_NOT_FILE_SELECTED','Пожалуйста выберите файл.');
 define('ERR_NOT_DOC_SELECTED','Никакой документ(ы) не выбран для удаления.');
 define('ERR_DELTED_FAILED','Невозможно удалить выбранный документ(ы).');
 define('ERR_FOLDER_PATH_NOT_ALLOWED','Путь папки не позволен.');
  //менеджер класса
 define('ERR_FOLDER_NOT_FOUND','Неспособный определить местонахождение определенной папки:');
  //переименовать
 define('ERR_RENAME_FORMAT','Пожалуйста дайте имя, которое содержит только буквы, цифры, пробел, дефис и символ подчеркивания.');
 define('ERR_RENAME_EXISTS','Пожалуйста дайте имя, которое уникально в папке.');
 define('ERR_RENAME_FILE_NOT_EXISTS','Файл/папка не существует.');
 define('ERR_RENAME_FAILED','Невозможно переименовать его, пожалуйста попытайтесь снова.');
 define('ERR_RENAME_EMPTY','Пожалуйста дайте имя.');
 define('ERR_NO_CHANGES_MADE','Никакие изменения не были произведены.');
 define('ERR_RENAME_FILE_TYPE_NOT_PERMITED','Вы не можете изменить файл к такому расширению.');
  //создание папки
 define('ERR_FOLDER_FORMAT','Пожалуйста дайте имя, которое содержит только буквы, цифры, пробел, дефис и символ подчеркивания.');
 define('ERR_FOLDER_EXISTS','Пожалуйста дайте имя, которое уникально в папке.');
 define('ERR_FOLDER_CREATION_FAILED','Невозможно создать папку, пожалуйста попытайтесь снова.');
 define('ERR_FOLDER_NAME_EMPTY','Пожалуйста дайте имя.');
 define('FOLDER_FORM_TITLE','Новая форма папки');
 define('FOLDER_LBL_TITLE','Заголовок папки:');
 define('FOLDER_LBL_CREATE','Создать папку');
 //Новый Файл
 define('NEW_FILE_FORM_TITLE','Новая форма файла');
 define('NEW_FILE_LBL_TITLE','Имя файла:');
 define('NEW_FILE_CREATE','Создать файл');
  //загрузка файла
 define('ERR_FILE_NAME_FORMAT','Пожалуйста дайте имя, которое содержит только буквы, цифры, пробел, дефис и символ подчеркивания.');
 define('ERR_FILE_NOT_UPLOADED','Никакой файл не был выбран для загрузки.');
 define('ERR_FILE_TYPE_NOT_ALLOWED','Вы не можете загрузить такой тип файла.');
 define('ERR_FILE_MOVE_FAILED','Сбой перемещения файла.');
 define('ERR_FILE_NOT_AVAILABLE','Файл недоступен.');
 define('ERROR_FILE_TOO_BID','Слишком большой файл. (максимально: %s)');
 define('FILE_FORM_TITLE','Форма загрузки файла');
 define('FILE_LABEL_SELECT','Выберите файл');
 define('FILE_LBL_MORE','Добавить загрузчик файла');
 define('FILE_CANCEL_UPLOAD','Отменить загрузку файла');
 define('FILE_LBL_UPLOAD','Загрузка');
 //загрузка файла
 define('ERR_DOWNLOAD_FILE_NOT_FOUND','Никакие файлы не выбраны для закачки.');
 //Переименовать
 define('RENAME_FORM_TITLE','Переименовать форму');
 define('RENAME_NEW_NAME','Новое имя');
 define('RENAME_LBL_RENAME','Переименовать');

 //Рекомендации
 define('TIP_FOLDER_GO_DOWN','Один щелчок для этой папки...');
 define('TIP_DOC_RENAME','Двойной щелчок, чтобы редактировать...');
 define('TIP_FOLDER_GO_UP','Один щелчок для родительской папки...');
 define('TIP_SELECT_ALL','Выделить все');
 define('TIP_UNSELECT_ALL','Снять все выделение');
 //ПРЕДУПРЕЖДЕНИЕ
 define('WARNING_DELETE','Вы уверены, что хотите удалить выбранный документ(ы).');
 define('WARNING_IMAGE_EDIT','Пожалуйста выберите изображение для редактирования.');
 define('WARNING_NOT_FILE_EDIT','Пожалуйста выберите файл для редактирования.');
 define('WARING_WINDOW_CLOSE','Вы действительно хотите закрыть окно?');
 //Предпросмотр
 define('PREVIEW_NOT_PREVIEW','Нет доступного предпросмотра.');
 define('PREVIEW_OPEN_FAILED','Невозможно открыть файл.');
 define('PREVIEW_IMAGE_LOAD_FAILED','Невозможно загрузить изображение');

 //Вход в систему
 define('LOGIN_PAGE_TITLE','Форма входа Ajax File Manager');
 define('LOGIN_FORM_TITLE','Форма входа');
 define('LOGIN_USERNAME','Имя пользователя:');
 define('LOGIN_PASSWORD','Пароль:');
 define('LOGIN_FAILED','Недопустимое имя пользователя и пароль.');
	
	
 //88888888888 Ниже для Редактора Изображения 888888888888888888888
  //Предупреждение 
  define('IMG_WARNING_NO_CHANGE_BEFORE_SAVE','Вы не произвели изменений в изображениях.');
		
  //Общий
  define('IMG_GEN_IMG_NOT_EXISTS','Изображение не существует');
  define('IMG_WARNING_LOST_CHANAGES','Все несохраненные изменения, произведенные в изображении, будут потерянны, Вы уверены, что хотите продолжить?');
  define('IMG_WARNING_REST','Все несохраненные изменения, произведенные в изображении, будут потеряны, Вы уверены, что хотите восстановить?');
  define('IMG_WARNING_EMPTY_RESET','Никакие изменения пока не были произведены в изображении');
  define('IMG_WARING_WIN_CLOSE','Вы уверены, что хотите закрыть окно?');
  define('IMG_WARNING_UNDO','Вы уверены, что хотите восстановить изображение к предыдущему состоянию?');
  define('IMG_WARING_FLIP_H','Вы уверены, что хотите отразить изображение горизонтально?');
  define('IMG_WARING_FLIP_V','Вы уверены, что хотите отразить изображение горизонтально?');
  define('IMG_INFO','Информация об Изображении');
		
  //Режим
   define('IMG_MODE_RESIZE','Изменить размеры:');
   define('IMG_MODE_CROP','Обрезка:');
   define('IMG_MODE_ROTATE','Поворот:');
   define('IMG_MODE_FLIP','Отражение:');  
  //Кнопка
		
   define('IMG_BTN_ROTATE_LEFT','90&deg; против часовой');
   define('IMG_BTN_ROTATE_RIGHT','90&deg; по часовой');
   define('IMG_BTN_FLIP_H','Отразить горизонтально');
   define('IMG_BTN_FLIP_V','Отразить вертикально');
   define('IMG_BTN_RESET','Сброс');
   define('IMG_BTN_UNDO','Отмена');
   define('IMG_BTN_SAVE','Сохранить');
   define('IMG_BTN_CLOSE','Закрыть');
   define('IMG_BTN_SAVE_AS','Сохранить как');
   define('IMG_BTN_CANCEL','Отмена');
  //Переключатель
   define('IMG_CHECKBOX_CONSTRAINT','Ограничить?');
  //Метка
   define('IMG_LBL_WIDTH','Ширина:');
   define('IMG_LBL_HEIGHT','Высота:');
   define('IMG_LBL_X','X:');
   define('IMG_LBL_Y','Y:');
   define('IMG_LBL_RATIO','Отношение:');
   define('IMG_LBL_ANGLE','Угол:');
   define('IMG_LBL_NEW_NAME','Новое имя:');
   define('IMG_LBL_SAVE_AS','Форма Сохранить как');
   define('IMG_LBL_SAVE_TO','Сохранить:');
   define('IMG_LBL_ROOT_FOLDER','Корневая папка');
  //Редактор
  //Сохранить как 
  define('IMG_NEW_NAME_COMMENTS','Не содержит расширение изображения.');
  define('IMG_SAVE_AS_ERR_NAME_INVALID','Пожалуйста дайте имя, которое содержит только буквы, цифры, пробел, дефис и символ подчеркивания.');
  define('IMG_SAVE_AS_NOT_FOLDER_SELECTED','Никакая целевая папка не выбрана.'); 
  define('IMG_SAVE_AS_FOLDER_NOT_FOUND','Папка - получатель не существует.');
  define('IMG_SAVE_AS_NEW_IMAGE_EXISTS','Там существует изображение с тем же именем.');

  //Сохранить
  define('IMG_SAVE_EMPTY_PATH','Пустой путь изображения.');
  define('IMG_SAVE_NOT_EXISTS','Изображение не существует.');
  define('IMG_SAVE_PATH_DISALLOWED','Вы не можете обратиться к этому файлу.');
  define('IMG_SAVE_UNKNOWN_MODE','Неожиданный режим операции изображения');
  define('IMG_SAVE_RESIZE_FAILED','Сбой изменения размера изображения.');
  define('IMG_SAVE_CROP_FAILED','Сбой подрезки изображения.');
  define('IMG_SAVE_FAILED','Сбой сохранения ричунка.');
  define('IMG_SAVE_BACKUP_FAILED','Невозможно резервировать оригинальное изображение.');
  define('IMG_SAVE_ROTATE_FAILED','Невозможно повернуть изображение.');
  define('IMG_SAVE_FLIP_FAILED','Невозможно отразить изображение.');
  define('IMG_SAVE_SESSION_IMG_OPEN_FAILED','Невозможно открыть изображение из сессии.');
  define('IMG_SAVE_IMG_OPEN_FAILED','Невозможно открыть изображение');
		
		
  //ОТМЕНА
  define('IMG_UNDO_NO_HISTORY_AVAIALBE','Недоступна история для отмены.');
  define('IMG_UNDO_COPY_FAILED','Невозможно восстановить изображение.');
  define('IMG_UNDO_DEL_FAILED','Невозможно удалить сессию изображения');
	
 //88888888888 Выше для Редактора Изображения 888888888888888888888
	
 //88888888888 Сессий 888888888888888888888
  define('SESSION_PERSONAL_DIR_NOT_FOUND','Невозможно найти определенную папку, которая должна была быть создана под папкой сессии');
  define('SESSION_COUNTER_FILE_CREATE_FAILED','Невозможно открыть файл счетчика сессии.');
  define('SESSION_COUNTER_FILE_WRITE_FAILED','Невозможно записать файл счетчика сессии.');
 //88888888888 Сессий 888888888888888888888
	
 //88888888888 Ниже для Текстового редактора 888888888888888888888
  define('TXT_FILE_NOT_FOUND','Файл не найден.');
  define('TXT_EXT_NOT_SELECTED','Пожалуйста выберите расширение имени файла');
  define('TXT_DEST_FOLDER_NOT_SELECTED','Пожалуйста выберите папку - получатель');
  define('TXT_UNKNOWN_REQUEST','Неизвестный запрос.');
  define('TXT_DISALLOWED_EXT','Вы можете редактировать/добавить такой тип файла.');
  define('TXT_FILE_EXIST','Такой файл уже существует.');
  define('TXT_FILE_NOT_EXIST','Не найден.');
  define('TXT_CREATE_FAILED','Сбой создания нового файла.');
  define('TXT_CONTENT_WRITE_FAILED','Сбой записи содержимого в файл.');
  define('TXT_FILE_OPEN_FAILED','Сбой открыия файла.');
  define('TXT_CONTENT_UPDATE_FAILED','Сбой обновления содержимого файла.');
  define('TXT_SAVE_AS_ERR_NAME_INVALID','Пожалуйста дайте имя, которые содержит только буквы, цифры, пробел, дефис и символ подчеркивания.');
 //88888888888 Выше для Текстового редактора 888888888888888888888
	

