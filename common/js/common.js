// JavaScript Document

//var j = jQuery.noConflict();

function show_popup(popID, popURL){	

	//alert("POPUP OK");

	var query= popURL.split('?');

	var dim= query[1].split('&');

	var popWidth = dim[0].split('=')[1]; //Gets the first query string value



	//Fade in the Popup and add close button

	$('#' + popID).fadeIn().css({ 'width':parseInt(popWidth-100) }).prepend('<a onclick="close_popup();" class="close"><img src="'+common_url+'images/x.png" class="btn_close" title="Close Window" alt="Close" /></a>');



	//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css

	var popMargTop = 250;

	var popMargLeft = ($('#' + popID).width() + 80) / 2;



	//Apply Margin to Popup

	$('#' + popID).css({

		'margin-top' : -popMargTop,

		'margin-left' : -popMargLeft

	});



	//Fade in Background

	$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.

	$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

	return false;	

	$("#preloader").delay(100).fadeOut(400);

}

function close_popup(){

	$('#fade , .popup_block').fadeOut(function() {

		$('#fade, a.close').remove();  //fade them both out

	});	

}

function check_decimal(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value=='' || field_id_namevar.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789.";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_integer(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value=='' || field_id_namevar.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_phone(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else if(field_id_namevar.value.length<6){

		elementmessage.innerHTML = lebelname+' should be min 6 numbers.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789.()+-, ";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_date(field_id_namevar, lebelname, erroridname){	

	var sText = field_id_namevar.value;		

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value=='' || field_id_namevar.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else if(field_id_namevar.value.length<9){

		elementmessage.innerHTML = lebelname+' should be min 9 characters.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789-";

		var IsNumber=true;

		var Char;

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	} 

	var dateext = sText.split('-');

	if(dateext[0] =='' || dateext[1]=='' || dateext[2]=='' || dateext[0].length<4 || dateext[1].length<2 || dateext[2].length<2 ){

		elementmessage.innerHTML = lebelname+' is invalid date format.';

		field_id_namevar.focus();

		return false;

	} 

}



function check_email(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

		if(!emailReg.test(frmidname.value)) {

			elementmessage.innerHTML = lebelname+' is invalid Email.';

			frmidname.focus();

			return false;

		}

	}

}



function check_url(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var url = frmidname.value;		

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	

	var returnval = true;

	var ext = url.split('://');

	var allow = new Array('http','https');

	var firstextension = ext[0].toLowerCase();

	if(jQuery.inArray(firstextension, allow) == -1) {

    	returnval = false;

	}

	//alert("firstextension:"+returnval);

	var nextstr = url.split('://').pop().toLowerCase();

	var nextnextstr = '';

	if(returnval==true && nextstr !=''){

		var ext = nextstr.split('.');

		var allow = new Array('www');

		var firstextension = ext[0].toLowerCase();

		//alert("firstextension:"+firstextension);

		

		if(jQuery.inArray(firstextension, allow) == -1) {

			nextnextstr = nextstr;

		}

		else{

			nextnextstr = url.split(firstextension+'.').pop().toLowerCase();	

		}

		//alert("nextnextstr:"+nextnextstr);

	}	

	//alert("nextnextstr:"+nextnextstr);

	

	if(returnval==true && nextnextstr !=''){

		var is_dotfound=nextnextstr.indexOf('.');

		if (is_dotfound==-1){ 

			returnval = false;

		}

		else{

			var ext = nextnextstr.split('.').pop().toLowerCase();

			

			if(ext =='' || checkwebchar(ext)==false){

				returnval = false;

			}

			else if(ext.length<2){

				returnval = false;

			}

			else{

				returnval = true;

			}

			//alert("ext:"+ext+", returnval:"+returnval);

		}

	}

	

	if(returnval == false){

		elementmessage.innerHTML = lebelname+' is invalid email format.';

		frmidname.focus();

		return false;

	} 

}



function check_letters(frmidname, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_&()/.@,$[]{} ";

		var IsNumber=true;

		var Char;

		var sText = frmidname.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				frmidname.focus();

				return false;

			}

		}

	}  

}



function check_letterswithnumber(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_&()/.@,$[]{} ";

		var IsNumber=true;

		var Char;

		var sText = frmidname.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				frmidname.focus();

				return false;

			}

		}

	}  

}



function check_redio_or_checkbox(frmName, rbGroupName, lebelname, erroridname){

	var checkcount = 0;

	var radios = document[frmName].elements[rbGroupName];

	for (var i=0; i <radios.length; i++) {

		if (radios[i].checked) {

			checkcount++;

		}

	}

	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if (checkcount == 0){

		elementmessage.innerHTML = 'You have to check at least one '+lebelname;

		return false;

	}

}



function check_imagesbyval(imagesval){

	var ext = imagesval.split('.').pop().toLowerCase();

	var allow = new Array('gif','png','jpg','jpeg');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}

function checkNumericintVal(sText){

	var ValidChars = "0123456789.";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function checkNumericTelephoneMobileVal(sText){

	var ValidChars = "0123456789.()+-, ";

	var IsNumber=true;

	var Char;

	var check = 'ok';	

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function checkNumericcharVal(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,-_ ";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function checkcharVal(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function checkmobileortelephone(sText){

	var ValidChars = "0123456789.()+- ";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function emailcheck(str) {

	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	if(!emailReg.test(str)) {

       	return false

    }

	/*

	var extat = str.split('@');

	var txtafterat = extat[1];

	var ext = txtafterat.split('.');

	

	if(checkNumericintVal(ext[0]) == true) {

    	return false;

	}

	else if(checkcharVal(ext[1]) == false) {

    	return false;

  	}	*/

	return true					

}



function checknumericintvaluewithid(textvalue, returnidname){

	

	if(checkNumericintVal(textvalue)==false) {

		document.getElementById(returnidname).value = '';

		document.getElementById(returnidname).focus();

		return false;

	}

	return true;

}



function check_frmforgot_pass(){

	var oField = document.frmforgot_pass.students_email;

	var oElement = document.getElementById('err_students_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "Email is missing for forgot Password.";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "Invalid email address.";

		oField.focus();

		return(false);

	}

	return(true);

}



function check_sitesearch_form(){

	var oField = document.frm_sitesearch_form.search_site;

	if(oField.value == "" || oField.value == 'Search here'){

		oField.focus();

		return(false);

	}

	return(true);

}

function check_images(idname){

	var ext = $('#'+idname).val().split('.').pop().toLowerCase();

	var allow = new Array('gif','png','jpg','jpeg');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}

