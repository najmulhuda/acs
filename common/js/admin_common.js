var j = jQuery.noConflict();

function show_popup(popID, popURL){	

	//alert("POPUP OK");

	var query= popURL.split('?');

	var dim= query[1].split('&');

	var popWidth = dim[0].split('=')[1]; //Gets the first query string value



	//Fade in the Popup and add close button

	j('#' + popID).fadeIn().css({ 'width':parseInt(popWidth-100) }).prepend('<a onclick="close_popup();" class="close"><img src="'+common_url+'images/x.png" class="btn_close" title="Close Window" alt="Close" /></a>');



	//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css

	var popMargTop = 250;

	var popMargLeft = (j('#' + popID).width() + 80) / 2;



	//Apply Margin to Popup 

	j('#' + popID).css({

		'margin-top' : -popMargTop,

		'margin-left' : -popMargLeft

	});



	//Fade in Background

	j('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.

	j('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

	return false;	

	j("#preloader").delay(100).fadeOut(400);

}

function check_decimal(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789.";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_integer(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value=='' || field_id_namevar.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_phone(field_id_namevar, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else if(field_id_namevar.value.length<6){

		elementmessage.innerHTML = lebelname+' should be min 6 numbers.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789.()+-, ";

		var IsNumber=true;

		var Char;

		var sText = field_id_namevar.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	}  

}



function check_date(field_id_namevar, lebelname, erroridname){	

	var sText = field_id_namevar.value;		

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(field_id_namevar.value=='' || field_id_namevar.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		field_id_namevar.focus();

		return false;

	} 

	else if(field_id_namevar.value.length<9){

		elementmessage.innerHTML = lebelname+' should be min 9 characters.';

		field_id_namevar.focus();

		return false;

	} 

	else{

		var ValidChars = "0123456789-";

		var IsNumber=true;

		var Char;

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				field_id_namevar.focus();

				return false;

			}

		}

	} 

	var dateext = sText.split('-');

	if(dateext[0] =='' || dateext[1]=='' || dateext[2]=='' || dateext[0].length<4 || dateext[1].length<2 || dateext[2].length<2 ){

		elementmessage.innerHTML = lebelname+' is invalid date format.';

		field_id_namevar.focus();

		return false;

	} 

}



function check_email(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

		if(!emailReg.test(frmidname.value)) {

			elementmessage.innerHTML = lebelname+' is invalid Email.';

			frmidname.focus();

			return false;

		}

	}

}



function check_url(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var url = frmidname.value;		

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	

	var returnval = true;

	var ext = url.split('://');

	var allow = new Array('http','https');

	var firstextension = ext[0].toLowerCase();

	if(jQuery.inArray(firstextension, allow) == -1) {

    	returnval = false;

	}

	//alert("firstextension:"+returnval);

	var nextstr = url.split('://').pop().toLowerCase();

	var nextnextstr = '';

	if(returnval==true && nextstr !=''){

		var ext = nextstr.split('.');

		var allow = new Array('www');

		var firstextension = ext[0].toLowerCase();

		//alert("firstextension:"+firstextension);

		

		if(jQuery.inArray(firstextension, allow) == -1) {

			nextnextstr = nextstr;

		}

		else{

			nextnextstr = url.split(firstextension+'.').pop().toLowerCase();	

		}

		//alert("nextnextstr:"+nextnextstr);

	}	

	//alert("nextnextstr:"+nextnextstr);

	

	if(returnval==true && nextnextstr !=''){

		var is_dotfound=nextnextstr.indexOf('.');

		if (is_dotfound==-1){ 

			returnval = false;

		}

		else{

			var ext = nextnextstr.split('.').pop().toLowerCase();

			

			if(ext =='' || checkwebchar(ext)==false){

				returnval = false;

			}

			else if(ext.length<2){

				returnval = false;

			}

			else{

				returnval = true;

			}

			//alert("ext:"+ext+", returnval:"+returnval);

		}

	}

	

	if(returnval == false){

		elementmessage.innerHTML = lebelname+' is invalid URL format.';

		frmidname.focus();

		return false;

	} 

}





function check_module_url(frmidname, lebelname, erroridname){	

	var module_url = frmidname.value;		

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 	

	else{

		var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-+_";

		var IsValid=true;

		var Char;

		var module_urlval = '';

		for (var i = 0; i < module_url.length; i++){ 

			Char = module_url.charAt(i); 

			Char = Char.replace(" ", '-');

			Char = Char.replace("'", '');

			Char = Char.replace('"', '');

			Char = Char.replace('.', '');

			Char = Char.replace(',', '');

			Char = Char.replace('!', '');

			//Char = Char.replace('_', '-');

			Char = Char.replace('/', '-or-');

			Char = Char.replace('&', '-and-');

			Char = Char.replace('&amp;', '-and-');

			Char = Char.replace('(', '');

			Char = Char.replace(')', '');

			module_urlval = module_urlval+Char;

		}

		module_url = '';

		for (i = 0; i < module_urlval.length && IsValid == true; i++){ 

			Char = module_urlval.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				IsValid = false;

				elementmessage.innerHTML = lebelname+' is invalid. Only "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-" is alowed';

				frmidname.value = module_url.toLowerCase();

				frmidname.focus();

				return false;

			}

			else{

				module_url = module_url+Char;

			}

		}



		if(IsValid == false){

			frmidname.value = module_url.toLowerCase();

			return false;

		}

		else{

			frmidname.value = module_url.toLowerCase();

			return true;

		}

	}

}



function check_letters(frmidname, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_&()/.@,$[]{}+' ";

		var IsNumber=true;

		var Char;

		var sText = frmidname.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				frmidname.focus();

				return false;

			}

		}

	}  

}



function check_letterswithnumber(frmidname, lebelname, erroridname){	

	var frmidname = frmidname;

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(frmidname.value=='' || frmidname.value==0){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		frmidname.focus();

		return false;

	} 

	else{

		var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_&()/.@,$[]{}?-='<>!+ "+'"'+'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ';

		var IsNumber=true;

		var Char;

		var sText = frmidname.value;		

		for (i = 0; i < sText.length && IsNumber == true; i++){ 

			Char = sText.charAt(i); 

			if (ValidChars.indexOf(Char) == -1){

				elementmessage.innerHTML = lebelname+' is invalid. Only '+ValidChars+' are allowed.';

				frmidname.focus();

				return false;

			}

		}

	}  

}



function check_redio_or_checkbox(frmName, rbGroupName, lebelname, erroridname){

	var checkcount = 0;

	var radios = document[frmName].elements[rbGroupName];

	for (var i=0; i <radios.length; i++) {

		if (radios[i].checked) {

			checkcount++;

		}

	}

	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if (checkcount == 0){

		elementmessage.innerHTML = 'You have to check at least one '+lebelname;

		return false;

	}

}



function check_images(idname, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(j('#'+idname).val()==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		j('#'+idname).focus();

		return false;

	} 

	else{

		var ext = j('#'+idname).val().split('.').pop().toLowerCase();

		var allow = new Array('gif','png','jpg','jpeg');

		if(jQuery.inArray(ext, allow) == -1) {

			elementmessage.innerHTML = lebelname+' is invalid. Only .gif, .png, .jpg and .jpeg file types are allowed.';

			j('#'+idname).focus();

			return false;

		}

	}  

}



function check_document_file(idname, lebelname, erroridname){	

	var elementmessage = document.getElementById(erroridname);

	elementmessage.innerHTML = '';

	if(j('#'+idname).val()==''){

		elementmessage.innerHTML = lebelname+' is mandatory.';

		j('#'+idname).focus();

		return false;

	} 

	else{

		var ext = j('#'+idname).val().split('.').pop().toLowerCase();

		var allow = new Array('doc','docx','pdf','txt', 'xlsx');

		if(jQuery.inArray(ext, allow) == -1) {

			elementmessage.innerHTML = lebelname+' is invalid. Only .doc, .docx, .pdf and .txt file types are allowed.';

			j('#'+idname).focus();

			return false;

		}

	}  

}



function check_imagesbyval(imagesval){

	var ext = imagesval.split('.').pop().toLowerCase();

	var allow = new Array('gif','png','jpg','jpeg');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}



function check_audios(idname){

	var ext = j('#'+idname).val().split('.').pop().toLowerCase();

	var allow = new Array('wav','mp3','wma','ogg','gsm','dct','au','aiff','vox','raw','aac','atrac','ra','ram','dss','msv','dvf');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}

function check_document(idname){

	var ext = j('#'+idname).val().split('.').pop().toLowerCase();

	var allow = new Array('doc','docx','pdf','txt');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}

function checkonlycharVal(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function checkonlyNumericcharVal(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function checkwebchar(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_/";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function isURL(url) {

	var returnval = true;

	var ext = url.split('://');

	var allow = new Array('http','https');

	var firstextension = ext[0].toLowerCase();

	if(jQuery.inArray(firstextension, allow) == -1) {

    	returnval = false;

	}

	//alert("firstextension:"+returnval);

	var nextstr = url.split('://').pop().toLowerCase();

	var nextnextstr = '';

	if(returnval==true && nextstr !=''){

		var ext = nextstr.split('.');

		var allow = new Array('www');

		var firstextension = ext[0].toLowerCase();

		//alert("firstextension:"+firstextension);

		

		if(jQuery.inArray(firstextension, allow) == -1) {

			nextnextstr = nextstr;

		}

		else{

			nextnextstr = url.split(firstextension+'.').pop().toLowerCase();	

		}

		//alert("nextnextstr:"+nextnextstr);

	}	

	//alert("nextnextstr:"+nextnextstr);

	

	if(returnval==true && nextnextstr !=''){

		var is_dotfound=nextnextstr.indexOf('.');

		if (is_dotfound==-1){ 

			returnval = false;

		}

		else{

			var ext = nextnextstr.split('.').pop().toLowerCase();

			

			if(ext =='' || checkwebchar(ext)==false){

				returnval = false;

			}

			else if(ext.length<2){

				returnval = false;

			}

			else{

				returnval = true;

			}

			//alert("ext:"+ext+", returnval:"+returnval);

		}

	}

	return returnval;

} 



function check_zipfile(idname){

	var ext = j('#'+idname).val().split('.').pop().toLowerCase();

	var allow = new Array('zip');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}

function check_phpfile(idname){

	var ext = j('#'+idname).val().split('.').pop().toLowerCase();

	var allow = new Array('php');

	if(jQuery.inArray(ext, allow) == -1) {

    	return false;

	}

	else{

    	return true;

  	}

}



function check_frmforgot_pass(){

	var oField = document.frmforgot_pass.students_email;

	var oElement = document.getElementById('err_students_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "Email is missing for forgot Password.";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "Invalid email address.";

		oField.focus();

		return(false);

	}

	return(true);

}

function checkNumericintVal(sText){

	var ValidChars = "0123456789.-";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function checkNumericcharVal(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,-_ ";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}

function checkmkdir(sText){

	var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_ '/";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function checkmobileortelephone(sText){

	var ValidChars = "0123456789.()+- ";

	var IsNumber=true;

	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function emailcheck(str) {

	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	if(!emailReg.test(str)) {

       	return false

    }

	

	var extat = str.split('@');

	var txtafterat = extat[1];

	var ext = txtafterat.split('.');

	

	if(checkNumericintVal(ext[0]) == true) {

    	return false;

	}

	else if(checkNumericintVal(ext[1]) == true) {

    	return false;

  	}	

	return true					

}

function checkNumericVal(sText){

	var ValidChars = "0123456789.";

	var IsNumber=true;

	var Char;

	var check = 'ok';	

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function returnonlyintVal(fieldvalue){

	var ValidChars = "0123456789";

	var IsNumber=true;

	var Char;

	var intvalue = '';	

	for (i = 0; i < fieldvalue.length && IsNumber == true; i++){ 

		Char = fieldvalue.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

		else{

			intvalue = intvalue+Char;

		}

	}

	return intvalue;

}



function checkNumericTelephoneMobileVal(sText){

	var ValidChars = "0123456789.()+-, ";

	var IsNumber=true;

	var Char;

	var check = 'ok';	

	for (i = 0; i < sText.length && IsNumber == true; i++){ 

		Char = sText.charAt(i); 

		if (ValidChars.indexOf(Char) == -1){

			IsNumber = false;

		}

	}

	return IsNumber;

}



function checknumericintvaluewithid(textvalue, returnidname){

	

	if(checkNumericintVal(textvalue)==false) {

		document.getElementById(returnidname).value = returnonlyintVal(textvalue);

		document.getElementById(returnidname).focus();

		return false;

	}

	return true;

}

function change_productquantitywithid(upordown, returnidname){

	var prev_val = parseInt(document.getElementById(returnidname).value);

	if(prev_val=='')prev_val = 0;

	if(upordown=='Up') {

		document.getElementById(returnidname).value = Math.floor(prev_val+1);

	}

	else if(upordown=='Down') {

		if(prev_val>1)

			document.getElementById(returnidname).value = Math.floor(prev_val-1);

	}

	return true;

}





function checkfrmuser_login(){

	var oField = document.frmuser_login.user_name;

	var oElement = document.getElementById('errmsg_user_name');

	oElement.innerHTML = "";

	var user_namestrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing user name";

		oField.focus();

		return(false);

	}

	else if(user_namestrlength<5){

		oElement.innerHTML = "User name should be greater than 4 letter";

		oField.focus();

		return(false);

	}

	

	var oField = document.frmuser_login.user_password;

	var oElement = document.getElementById('errmsg_user_password');

	oElement.innerHTML = "";

	var user_passwordstrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing Password";

		oField.focus();

		return(false);

	}

	else if(user_passwordstrlength<5){

		oElement.innerHTML = "Password should be greater than 4 letter";

		oField.focus();

		return(false);

	}	

	return(true);

}



function check_changepass_form(){	

	var newpassword = document.frm_changepass_form.newpassword;

	var oElement = document.getElementById('errmsg_newpassword');

	oElement.innerHTML = "";	

	if(newpassword.value==''){

		newpassword.focus();

		oElement.innerHTML = "You are missing new password.";

		return false;

	}

	

	var confirmpassword = document.frm_changepass_form.confirmpassword;

	var oElement = document.getElementById('errmsg_confirmpassword');

	oElement.innerHTML = "";	

	if(confirmpassword.value==''){

		confirmpassword.focus();

		oElement.innerHTML = "You are missing confirm password.";

		return false;

	}

	else if(confirmpassword.value !=newpassword.value){

		confirmpassword.focus();

		oElement.innerHTML = "You should type new password and confirm password same.";

		return false;

	}

	return true;	

}



function checkfrmadmin_registration(){

	var oField = document.frmadmin_registration.user_name;

	var oElement = document.getElementById('errmsg_user_name');

	oElement.innerHTML = "";

	var user_namestrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing user name";

		oField.focus();

		return(false);

	}

	else if(user_namestrlength<5){

		oElement.innerHTML = "User name should be greater than 4 letter";

		oField.focus();

		return(false);

	}

	

	var oField = document.frmadmin_registration.user_password;

	var oElement = document.getElementById('errmsg_user_password');

	oElement.innerHTML = "";

	var user_passwordstrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing Password";

		oField.focus();

		return(false);

	}

	else if(user_passwordstrlength<5){

		oElement.innerHTML = "Password should be greater than 4 letter";

		oField.focus();

		return(false);

	}

	

	var conf_oField = document.frmadmin_registration.conf_user_password;

	var oElement = document.getElementById('errmsg_conf_user_password');

	oElement.innerHTML = "";

	var conf_user_passwordstrlength=oField.value.length;

	if(conf_oField.value == ""){

		oElement.innerHTML = "You are missing Confirm Password";

		conf_oField.focus();

		return(false);

	}

	else if(conf_user_passwordstrlength<5){

		oElement.innerHTML = "Confirm Password should be greater than 4 letter";

		conf_oField.focus();

		return(false);

	}

	else if(conf_oField.value != oField.value){

		oElement.innerHTML = "Confirm Password should be same as password";

		conf_oField.focus();

		return(false);

	}

	

	var oField = document.frmadmin_registration.user_fullname;

	var oElement = document.getElementById('errmsg_user_fullname');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "You are missing full name";

		oField.focus();

		return(false);

	}

	else if(oField.value.length<5){

		oElement.innerHTML = "Full name should be greater than 4 letter";

		oField.focus();

		return(false);

	}

	else if(checkNumericcharVal(oField.value)==false) {

		oElement.innerHTML = "You given full name shouldn't be numeric.";

		oField.focus();

		return(false);

	}			

	

	var oField = document.frmadmin_registration.user_email;

	var oElement = document.getElementById('errmsg_user_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "You are missing email";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "You given email is invalid";

		oField.focus();

		return(false);

	}	

	

	var oField = document.frmadmin_registration.user_contactno;

	var oElement = document.getElementById('errmsg_user_contactno');

	oElement.innerHTML = "";

	if(oField.value.length>0){

		if(checkNumericTelephoneMobileVal(oField.value)==false){

			oElement.innerHTML = "Contact no name should be numeric";

			oField.focus();

			return(false);

		}

		else if(oField.value.length<5) {

			oElement.innerHTML = "You given contact no should be greater than 4 letter.";

			oField.focus();

			return(false);

		}

				

	}			

	var user_level_id = document.frmadmin_registration.user_level_id;

	var elementmessage = document.getElementById("errmsg_user_level_id");

	elementmessage.innerHTML = '';

	j('#user_level_id').removeClass('field400_err').addClass('field400');

	if(user_level_id.value=='' || user_level_id.value==0){

		elementmessage.innerHTML = 'You are missing User Level Type';

		user_level_id.focus();

		j('#user_level_id').removeClass('field400').addClass('field400_err');

		return false;

	}	

	

	return(true);

}





function check_frmforgot_pass(){

	var oField = document.frmforgot_pass.fuser_email;

	var oElement = document.getElementById('error_fuser_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "Email is missing.";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "Invalid email address.";

		oField.focus();

		return(false);

	}

	return(true);

}





function close_forgotpopup(){

	j('#fade , .popup_block').fadeOut(function() {

		j('#fade, a.close').remove();  //fade them both out

	});

}



function close_popup(){

	j('#fade , .popup_block').fadeOut(function() {

		j('#fade, a.close').remove();  //fade them both out

	});

}



function show_forgotpass_popup(){

	j('#show_forgotpass_message').html('');

	var popID = 'show_forgotpasspopup_form'; //Get Popup ID

	var popURL = '#?w=500'; //Get Popup href to define size

	show_popup(popID, popURL);

}



function checkfrmstudents_login(){

	var oField = document.frmstudents_login.students_email;

	var oElement = document.getElementById('err_students_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "You are missing email";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "You given email is invalid";

		oField.focus();

		return(false);

	}

	

	var oField = document.frmstudents_login.students_password;

	var oElement = document.getElementById('err_students_password');

	oElement.innerHTML = "";

	var user_passwordstrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing Password";

		oField.focus();

		return(false);

	}

	else if(user_passwordstrlength<4){

		oElement.innerHTML = "Password should be greater than or equal to 4 letter";

		oField.focus();

		return(false);

	}

	

	return(true);

}



function checkfontactfield(){

	var oField = document.frmcontact.first_name;

	var oElement = document.getElementById('errmsg_first_name');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "*";

		oField.focus();

		return(false);

	}

	var oField = document.frmcontact.email;

	var oElement = document.getElementById('errmsg_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "*";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "*";

		oField.focus();

		return(false);

	}			

	

	var oField = document.frmcontact.comments;

	var oElement = document.getElementById('errmsg_comments');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "*";

		oField.focus();

		return(false);

	}			

	return(true);

}



function checkfrmforgotpass_form(){

	var oField = document.frmforgotpass.students_email;

	var oElement = document.getElementById('err_students_email');

	oElement.innerHTML = "";

	if(oField.value == ""){

		oElement.innerHTML = "You are missing email";

		oField.focus();

		return(false);

	}

	else if(emailcheck(oField.value)==false) {

		oElement.innerHTML = "You given email is invalid";

		oField.focus();

		return(false);

	}	

	return(true);

}



function checkfrmchangepass_form(){

	var oField = document.frmchangepass.students_password;

	var oElement = document.getElementById('err_students_password');

	oElement.innerHTML = "";

	var user_passwordstrlength=oField.value.length;

	if(oField.value == ""){

		oElement.innerHTML = "You are missing Password";

		oField.focus();

		return(false);

	}

	else if(user_passwordstrlength<4){

		oElement.innerHTML = "Password should be greater than or equal to 4 letter";

		oField.focus();

		return(false);

	}

	

	var oconfField = document.frmchangepass.confstudents_password;

	var oElement = document.getElementById('err_confstudents_password');

	oElement.innerHTML = "";

	var user_passwordstrlength=oconfField.value.length;

	if(oconfField.value == ""){

		oElement.innerHTML = "You are missing confirm Password";

		oconfField.focus();

		return(false);

	}

	else if(user_passwordstrlength<4){

		oElement.innerHTML = "Confirm Password should be greater than or equal to 4 letter";

		oconfField.focus();

		return(false);

	}

	else if(oconfField.value !=oField.value){

		oElement.innerHTML = "Confirm Password should be equal to Password";

		oconfField.focus();

		return(false);

	}

	

	return(true);

}



function check_none(idname, label,errorid){

	return true;

}