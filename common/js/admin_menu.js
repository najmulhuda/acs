// JavaScript Document
var mastertabvar=new Object();
mastertabvar.baseopacity=0;
mastertabvar.browserdetect="";

function showsubmenu(masterid, id){
	if (typeof highlighting!="undefined"){
		clearInterval(highlighting);
	}

	submenuobject=document.getElementById(id);
	mastertabvar.browserdetect=submenuobject.filters? "ie" : typeof submenuobject.style.MozOpacity=="string"? "mozilla" : "";
	hidesubmenus(mastertabvar[masterid]);
	submenuobject.style.display="block";
	instantset(mastertabvar.baseopacity);
	highlighting=setInterval("gradualfade(submenuobject)",50);
}

function hidesubmenus(submenuarray){
	for (var i=0; i<submenuarray.length; i++){
		document.getElementById(submenuarray[i]).style.display="none";
	}
}

function instantset(degree){
	if (mastertabvar.browserdetect=="mozilla"){
		submenuobject.style.MozOpacity=degree/100;
	}
	else if (mastertabvar.browserdetect=="ie"){
		submenuobject.filters.alpha.opacity=degree;
	}
}


function gradualfade(cur2){
	if (mastertabvar.browserdetect=="mozilla" && cur2.style.MozOpacity<1)
		cur2.style.MozOpacity=Math.min(parseFloat(cur2.style.MozOpacity)+0.1, 0.99);
	else if (mastertabvar.browserdetect=="ie" && cur2.filters.alpha.opacity<100)
		cur2.filters.alpha.opacity+=10;
	else if (typeof highlighting!="undefined") //fading animation over
		clearInterval(highlighting);
}

function initalizetab(tabid){
	mastertabvar[tabid]=new Array();
	var menuitems=document.getElementById(tabid).getElementsByTagName("li");
	
	for (var i=0; i<menuitems.length; i++){
		if (menuitems[i].getAttribute("rel")){
			menuitems[i].setAttribute("rev", tabid); //associate this submenu with main tab
			mastertabvar[tabid][mastertabvar[tabid].length]=menuitems[i].getAttribute("rel"); //store ids of submenus of tab menu
			
			if (menuitems[i].className=="active")
				showsubmenu(tabid, menuitems[i].getAttribute("rel"));
				
			menuitems[i].getElementsByTagName("a")[0].onmouseover=function(){
				showsubmenu(this.parentNode.getAttribute("rev"), this.parentNode.getAttribute("rel"));
			}
		}
	}
}
var menu=function(){
	var t=15,z=50,s=6,a;
	function dd(n){this.n=n; this.h=[]; this.c=[]}
	dd.prototype.init=function(p,c){
		a=c; var w=document.getElementById(p), s=w.getElementsByTagName('ul'), l=s.length, i=0;
		for(i;i<l;i++){
			var h=s[i].parentNode; this.h[i]=h; this.c[i]=s[i];
			h.onmouseover=new Function(this.n+'.st('+i+',true)');
			h.onmouseout=new Function(this.n+'.st('+i+')');
		}
	}
	dd.prototype.st=function(x,f){
		var c=this.c[x], h=this.h[x], p=h.getElementsByTagName('a')[0];
		clearInterval(c.t); c.style.overflow='hidden';
		if(f){
			p.className+=' '+a;
			if(!c.mh){c.style.display='block'; c.style.height=''; c.mh=c.offsetHeight; c.style.height=0}
			if(c.mh==c.offsetHeight){c.style.overflow='visible'}
			else{c.style.zIndex=z; z++; c.t=setInterval(function(){sl(c,1)},t)}
		}else{p.className=p.className.replace(a,''); c.t=setInterval(function(){sl(c,-1)},t)}
	}
	function sl(c,f){
		var h=c.offsetHeight;
		if((h<=0&&f!=1)||(h>=c.mh&&f==1)){
			if(f==1){c.style.filter=''; c.style.opacity=1; c.style.overflow='visible'}
			clearInterval(c.t); return
		}
		var d=(f==1)?Math.ceil((c.mh-h)/s):Math.ceil(h/s), o=h/c.mh;
		c.style.opacity=o; c.style.filter='alpha(opacity='+(o*100)+')';
		c.style.height=h+(d*f)+'px'
	}
	return{dd:dd}
}();