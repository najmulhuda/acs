-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2015 at 11:41 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `n`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('eca9e68aa5cf53270c4ba68a0f4139dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko', 1439373336, ''),
('5fcee430c05afb4d2e8e90200bbf34bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko', 1439391341, ''),
('e421df6ecc1e6060a6e11b07d16ceba9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko', 1439356883, 'a:11:{s:7:"user_id";s:1:"1";s:9:"user_name";s:6:"sadmin";s:13:"user_password";s:32:"96b5a35db9990f7c6b4309a4240e803c";s:13:"user_fullname";s:19:"Super Administrator";s:10:"user_email";s:19:"shafiqswh@gmail.com";s:14:"user_contactno";s:11:"01712088033";s:11:"user_gender";s:4:"Male";s:16:"user_dateofbirth";s:10:"1980-08-21";s:13:"user_level_id";s:1:"1";s:11:"create_user";s:1:"1";s:9:"logged_in";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comments_id` int(11) NOT NULL AUTO_INCREMENT,
  `comments_user_name` varchar(250) NOT NULL,
  `comments_user_email` varchar(150) NOT NULL,
  `comments_description` text NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_id_name` varchar(100) NOT NULL,
  `table_id_val` int(11) NOT NULL,
  `client_ipaddress` varchar(50) NOT NULL,
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `comments_publish` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comments_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comments` text NOT NULL,
  `client_ipaddress` varchar(20) NOT NULL,
  `contact_us_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  PRIMARY KEY (`contact_us_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) NOT NULL,
  `country_code` char(2) NOT NULL,
  `country_publish` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=239 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_code`, `country_publish`) VALUES
(1, 'Andorra', 'AD', 1),
(2, 'United Arab Emirates', 'AE', 1),
(3, 'Afghanistan', 'AF', 1),
(4, 'Antigua and Barbuda', 'AG', 1),
(5, 'Anguilla', 'AI', 1),
(6, 'Albania', 'AL', 1),
(7, 'Armenia', 'AM', 1),
(8, 'Netherlands Antilles', 'AN', 1),
(9, 'Angola', 'AO', 1),
(10, 'Argentina', 'AR', 1),
(11, 'American Samoa', 'AS', 1),
(12, 'Austria', 'AT', 1),
(13, 'Australia', 'AU', 1),
(14, 'Aruba', 'AW', 1),
(15, 'Azerbaijan', 'AZ', 1),
(16, 'Bosnia and Herzegowina', 'BA', 1),
(17, 'Barbados', 'BB', 1),
(18, 'Bangladesh', 'BD', 1),
(19, 'Belgium', 'BE', 1),
(20, 'Burkina Faso', 'BF', 1),
(21, 'Bulgaria', 'BG', 1),
(22, 'Bahrain', 'BH', 1),
(23, 'Burundi', 'BI', 1),
(24, 'Benin', 'BJ', 1),
(25, 'Bermuda', 'BM', 1),
(26, 'Brunei Darussalam', 'BN', 1),
(27, 'Bolivia', 'BO', 1),
(28, 'Brazil', 'BR', 1),
(29, 'Bahamas', 'BS', 1),
(30, 'Bhutan', 'BT', 1),
(31, 'Bouvet Island', 'BV', 1),
(32, 'Botswana', 'BW', 1),
(33, 'Belarus', 'BY', 1),
(34, 'Belize', 'BZ', 1),
(35, 'Canada', 'CA', 1),
(36, 'Cocos (Keeling) Islands', 'CC', 1),
(37, 'Central African Republic', 'CF', 1),
(38, 'Congo', 'CG', 1),
(39, 'Switzerland', 'CH', 1),
(40, 'Cote D''Ivoire', 'CI', 1),
(41, 'Cook Islands', 'CK', 1),
(42, 'Chile', 'CL', 1),
(43, 'Cameroon', 'CM', 1),
(44, 'China', 'CN', 1),
(45, 'Colombia', 'CO', 1),
(46, 'Costa Rica', 'CR', 1),
(47, 'Cuba', 'CU', 1),
(48, 'Cape Verde', 'CV', 1),
(49, 'Christmas Island', 'CX', 1),
(50, 'Cyprus', 'CY', 1),
(51, 'Czech Republic', 'CZ', 1),
(52, 'Germany', 'DE', 1),
(53, 'Djibouti', 'DJ', 1),
(54, 'Denmark', 'DK', 1),
(55, 'Dominica', 'DM', 1),
(56, 'Dominican Republic', 'DO', 1),
(57, 'Algeria', 'DZ', 1),
(58, 'Ecuador', 'EC', 1),
(59, 'Estonia', 'EE', 1),
(60, 'Egypt', 'EG', 1),
(61, 'Western Sahara', 'EH', 1),
(62, 'Eritrea', 'ER', 1),
(63, 'Spain', 'ES', 1),
(64, 'Ethiopia', 'ET', 1),
(65, 'Finland', 'FI', 1),
(66, 'Fiji', 'FJ', 1),
(67, 'Falkland Islands (Malvinas)', 'FK', 1),
(68, 'Micronesia, Federated States of', 'FM', 1),
(69, 'Faroe Islands', 'FO', 1),
(70, 'France', 'FR', 1),
(71, 'France, Metropolitan', 'FX', 1),
(72, 'Gabon', 'GA', 1),
(73, 'Grenada', 'GD', 1),
(74, 'Georgia', 'GE', 1),
(75, 'French Guiana', 'GF', 1),
(76, 'Ghana', 'GH', 1),
(77, 'Gibraltar', 'GI', 1),
(78, 'Greenland', 'GL', 1),
(79, 'Gambia', 'GM', 1),
(80, 'Guinea', 'GN', 1),
(81, 'Guadeloupe', 'GP', 1),
(82, 'Equatorial Guinea', 'GQ', 1),
(83, 'Greece', 'GR', 1),
(84, 'South Georgia and the South Sandwich\r\n Islands', 'GS', 1),
(85, 'Guatemala', 'GT', 1),
(86, 'Guam', 'GU', 1),
(87, 'Guinea-bissau', 'GW', 1),
(88, 'Guyana', 'GY', 1),
(89, 'Hong Kong', 'HK', 1),
(90, 'Heard and Mc Donald Islands', 'HM', 1),
(91, 'Honduras', 'HN', 1),
(92, 'Croatia', 'HR', 1),
(93, 'Haiti', 'HT', 1),
(94, 'Hungary', 'HU', 1),
(95, 'Indonesia', 'ID', 1),
(96, 'Ireland', 'IE', 1),
(97, 'Israel', 'IL', 1),
(98, 'India', 'IN', 1),
(99, 'British Indian Ocean Territory', 'IO', 1),
(100, 'Iraq', 'IQ', 1),
(101, 'Iran (Islamic Republic of)', 'IR', 1),
(102, 'Iceland', 'IS', 1),
(103, 'Italy', 'IT', 1),
(104, 'Jamaica', 'JM', 1),
(105, 'Jordan', 'JO', 1),
(106, 'Japan', 'JP', 1),
(107, 'Kenya', 'KE', 1),
(108, 'Kyrgyzstan', 'KG', 1),
(109, 'Cambodia', 'KH', 1),
(110, 'Kiribati', 'KI', 1),
(111, 'Comoros', 'KM', 1),
(112, 'Saint Kitts and Nevis', 'KN', 1),
(113, 'Korea, Democratic People''s\r\n Republic of', 'KP', 1),
(114, 'Korea, Republic of', 'KR', 1),
(115, 'Kuwait', 'KW', 1),
(116, 'Cayman Islands', 'KY', 1),
(117, 'Kazakhstan', 'KZ', 1),
(118, 'Lao People''s Democratic Republic', 'LA', 1),
(119, 'Lebanon', 'LB', 1),
(120, 'Saint Lucia', 'LC', 1),
(121, 'Liechtenstein', 'LI', 1),
(122, 'Sri Lanka', 'LK', 1),
(123, 'Liberia', 'LR', 1),
(124, 'Lesotho', 'LS', 1),
(125, 'Lithuania', 'LT', 1),
(126, 'Luxembourg', 'LU', 1),
(127, 'Latvia', 'LV', 1),
(128, 'Libyan Arab Jamahiriya', 'LY', 1),
(129, 'Morocco', 'MA', 1),
(130, 'Monaco', 'MC', 1),
(131, 'Moldova, Republic of', 'MD', 1),
(132, 'Madagascar', 'MG', 1),
(133, 'Marshall Islands', 'MH', 1),
(134, 'Macedonia, The Former Yugoslav\r\n Republic of', 'MK', 1),
(135, 'Mali', 'ML', 1),
(136, 'Myanmar', 'MM', 1),
(137, 'Mongolia', 'MN', 1),
(138, 'Macau', 'MO', 1),
(139, 'Northern Mariana Islands', 'MP', 1),
(140, 'Martinique', 'MQ', 1),
(141, 'Mauritania', 'MR', 1),
(142, 'Montserrat', 'MS', 1),
(143, 'Malta', 'MT', 1),
(144, 'Mauritius', 'MU', 1),
(145, 'Maldives', 'MV', 1),
(146, 'Malawi', 'MW', 1),
(147, 'Mexico', 'MX', 1),
(148, 'Malaysia', 'MY', 1),
(149, 'Mozambique', 'MZ', 1),
(150, 'Namibia', 'NA', 1),
(151, 'New Caledonia', 'NC', 1),
(152, 'Niger', 'NE', 1),
(153, 'Norfolk Island', 'NF', 1),
(154, 'Nigeria', 'NG', 1),
(155, 'Nicaragua', 'NI', 1),
(156, 'Netherlands', 'NL', 1),
(157, 'Norway', 'NO', 1),
(158, 'Nepal', 'NP', 1),
(159, 'Nauru', 'NR', 1),
(160, 'Niue', 'NU', 1),
(161, 'New Zealand', 'NZ', 1),
(162, 'Oman', 'OM', 1),
(163, 'Panama', 'PA', 1),
(164, 'Peru', 'PE', 1),
(165, 'French Polynesia', 'PF', 1),
(166, 'Papua New Guinea', 'PG', 1),
(167, 'Philippines', 'PH', 1),
(168, 'Pakistan', 'PK', 1),
(169, 'Poland', 'PL', 1),
(170, 'St. Pierre and Miquelon', 'PM', 1),
(171, 'Pitcairn', 'PN', 1),
(172, 'Puerto Rico', 'PR', 1),
(173, 'Portugal', 'PT', 1),
(174, 'Palau', 'PW', 1),
(175, 'Paraguay', 'PY', 1),
(176, 'Qatar', 'QA', 1),
(177, 'Reunion', 'RE', 1),
(178, 'Romania', 'RO', 1),
(179, 'Russian Federation', 'RU', 1),
(180, 'Rwanda', 'RW', 1),
(181, 'Saudi Arabia', 'SA', 1),
(182, 'Solomon Islands', 'SB', 1),
(183, 'Seychelles', 'SC', 1),
(184, 'Sudan', 'SD', 1),
(185, 'Sweden', 'SE', 1),
(186, 'Singapore', 'SG', 1),
(187, 'St. Helena', 'SH', 1),
(188, 'Slovenia', 'SI', 1),
(189, 'Svalbard and Jan Mayen Islands', 'SJ', 1),
(190, 'Slovakia (Slovak Republic)', 'SK', 1),
(191, 'Sierra Leone', 'SL', 1),
(192, 'San Marino', 'SM', 1),
(193, 'Senegal', 'SN', 1),
(194, 'Somalia', 'SO', 1),
(195, 'Suriname', 'SR', 1),
(196, 'Sao Tome and Principe', 'ST', 1),
(197, 'El Salvador', 'SV', 1),
(198, 'Syrian Arab Republic', 'SY', 1),
(199, 'Swaziland', 'SZ', 1),
(200, 'Turks and Caicos Islands', 'TC', 1),
(201, 'Chad', 'TD', 1),
(202, 'French Southern Territories', 'TF', 1),
(203, 'Togo', 'TG', 1),
(204, 'Thailand', 'TH', 1),
(205, 'Tajikistan', 'TJ', 1),
(206, 'Tokelau', 'TK', 1),
(207, 'Turkmenistan', 'TM', 1),
(208, 'Tunisia', 'TN', 1),
(209, 'Tonga', 'TO', 1),
(210, 'East Timor', 'TP', 1),
(211, 'Turkey', 'TR', 1),
(212, 'Trinidad and Tobago', 'TT', 1),
(213, 'Tuvalu', 'TV', 1),
(214, 'Taiwan', 'TW', 1),
(215, 'Tanzania, United Republic of', 'TZ', 1),
(216, 'Ukraine', 'UA', 1),
(217, 'Uganda', 'UG', 1),
(218, 'United Kingdom', 'UK', 1),
(219, 'United States Minor Outlying\r\n Islands', 'UM', 1),
(220, 'United States', 'US', 1),
(221, 'Uruguay', 'UY', 1),
(222, 'Uzbekistan', 'UZ', 1),
(223, 'Vatican City State (Holy See)', 'VA', 1),
(224, 'Saint Vincent and the Grenadines', 'VC', 1),
(225, 'Venezuela', 'VE', 1),
(226, 'Virgin Islands (British)', 'VG', 1),
(227, 'Virgin Islands (U.S.)', 'VI', 1),
(228, 'Viet Nam', 'VN', 1),
(229, 'Vanuatu', 'VU', 1),
(230, 'Wallis and Futuna Islands', 'WF', 1),
(231, 'Samoa', 'WS', 1),
(232, 'Yemen', 'YE', 1),
(233, 'Mayotte', 'YT', 1),
(234, 'Yugoslavia', 'YU', 1),
(235, 'South Africa', 'ZA', 1),
(236, 'Zambia', 'ZM', 1),
(237, 'Zaire', 'ZR', 1),
(238, 'Zimbabwe', 'ZW', 1);

-- --------------------------------------------------------

--
-- Table structure for table `form_fields`
--

CREATE TABLE IF NOT EXISTS `form_fields` (
  `form_fields_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_label` varchar(150) NOT NULL,
  `front_label` varchar(150) NOT NULL,
  `form_fields_name` varchar(50) NOT NULL,
  `form_fields_type` varchar(20) NOT NULL,
  `default_value` varchar(255) NOT NULL,
  `form_fields_unique` tinyint(1) NOT NULL,
  `form_fields_value_required` tinyint(1) NOT NULL,
  `form_fields_input_validation` varchar(30) NOT NULL,
  `showing_order` tinyint(1) NOT NULL,
  `form_fields_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`form_fields_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `form_fields`
--

INSERT INTO `form_fields` (`form_fields_id`, `admin_label`, `front_label`, `form_fields_name`, `form_fields_type`, `default_value`, `form_fields_unique`, `form_fields_value_required`, `form_fields_input_validation`, `showing_order`, `form_fields_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Name', 'Name', 'name', 'textfield', '', 1, 1, 'letterswithnumber', 1, 1, 1369722857, 1369722857, 1),
(2, 'URI Name', 'URI Name', 'uri_name', 'textfield', '', 1, 1, 'module_url', 2, 1, 1369722958, 1369722958, 1),
(3, 'Description', 'Description', 'description', 'textarea', '', 0, 0, 'none', 3, 1, 1369723021, 1369723021, 1),
(4, 'URL', 'URL', 'url', 'textfield', '', 1, 1, 'url', 4, 1, 1369723084, 1369723084, 1),
(5, 'From Date', 'From Date', 'from_date', 'datecalendar', '0', 0, 0, 'date', 5, 1, 1369723155, 1369723155, 1),
(6, 'To Date', 'To Date', 'to_date', 'datecalendar', '0', 0, 0, 'date', 6, 1, 1369723187, 1369723187, 1),
(7, 'Meta Title', 'Meta Title', 'meta_title', 'textfield', '', 0, 1, 'letterswithnumber', 7, 1, 1369723249, 1369723249, 1),
(8, 'URI Table Name', 'Table Name', 'uri_table_name', 'textfield', '', 0, 1, 'module_url', 8, 1, 1369723543, 1369724037, 1),
(9, 'URI Table Field Name', 'Table Field Name', 'uri_table_field_name', 'textfield', '', 0, 1, 'module_url', 9, 1, 1369723583, 1369723583, 1),
(10, 'Meta Keyword', 'Meta Keyword', 'meta_keyword', 'textarea', '', 0, 0, 'none', 10, 1, 1369723677, 1369723677, 1),
(11, 'Meta Description', 'Meta Description', 'meta_description', 'textarea', '', 0, 0, 'none', 11, 1, 1369723749, 1369723749, 1),
(22, 'Email', 'Email', 'email', 'textfield', '', 0, 1, 'email', 22, 1, 1374229488, 1374229488, 1),
(35, 'Longitude', 'Longitude', 'lng', 'textfield', '', 1, 1, 'decimal', 30, 1, 1439100417, 1439230393, 1),
(28, 'State Code', 'State Code', 'state_code', 'textfield', '', 0, 1, 'letters', 26, 1, 1398242709, 1398242709, 1),
(29, 'Country Name', 'Country Name', 'country_id', 'foreign_key', '', 0, 1, 'integer', 27, 1, 1398242764, 1398242764, 1),
(34, 'Latitude', 'Latitude', 'lat', 'textfield', '', 1, 1, 'decimal', 29, 1, 1439100166, 1439230384, 1),
(33, 'Category Name', 'Category', 'cat_name', 'dropdown', '', 1, 1, 'decimal', 28, 1, 1439098248, 1439356588, 1);

-- --------------------------------------------------------

--
-- Table structure for table `form_fields_options`
--

CREATE TABLE IF NOT EXISTS `form_fields_options` (
  `form_fields_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_fields_id` int(11) NOT NULL,
  `label_name` varchar(255) NOT NULL,
  `label_value` varchar(255) NOT NULL,
  `showing_order` tinyint(1) NOT NULL,
  `form_fields_options_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`form_fields_options_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `form_fields_options`
--

INSERT INTO `form_fields_options` (`form_fields_options_id`, `form_fields_id`, `label_name`, `label_value`, `showing_order`, `form_fields_options_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 3, 'Editor', 'yes', 1, 1, 1369723021, 1369723021, 1),
(2, 10, 'Editor', 'no', 2, 1, 1369723677, 1369723677, 1),
(3, 11, 'Editor', 'no', 3, 1, 1369723749, 1369723749, 1),
(4, 12, '35', '35', 1, 1, 1374140418, 1374140418, 1),
(5, 13, '1000', '400', 4, 1, 1374141639, 1393942435, 1),
(6, 14, '214', '136', 5, 1, 1374142260, 1374142260, 1),
(7, 19, '85', '58', 6, 1, 1374145208, 1374145208, 1),
(8, 20, 'projects', 'name', 7, 1, 1374224916, 1374224916, 1),
(9, 21, '85', '58', 8, 1, 1374224990, 1374225031, 1),
(10, 23, 'Editor', 'yes', 9, 1, 1374229528, 1374229528, 1),
(11, 26, '80', '80', 10, 1, 1386829932, 1386829932, 1),
(12, 27, '230', '140', 11, 1, 1386850099, 1386850099, 1),
(13, 29, 'country', 'country_name', 12, 1, 1398242764, 1398242764, 1),
(14, 32, '160', '120', 13, 1, 1398253171, 1398253171, 1),
(15, 33, 'No Categoty', 'nocategory', 14, 1, 1439098248, 1439098248, 1),
(16, 33, 'Shop', 'shop', 15, 1, 1439098248, 1439098248, 1),
(17, 33, 'Masjid', 'masjid', 16, 1, 1439098248, 1439098248, 1),
(18, 33, 'Restaurant', 'Restaurant', 17, 1, 1439314020, 1439314020, 0),
(20, 33, 'Restaurant', 'Restaurant', 18, 0, 1439356588, 1439356588, 1);

-- --------------------------------------------------------

--
-- Table structure for table `front_menu`
--

CREATE TABLE IF NOT EXISTS `front_menu` (
  `front_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_menu_category_id` int(11) NOT NULL,
  `parent_front_menu` int(11) NOT NULL,
  `front_menu_name` varchar(255) NOT NULL,
  `front_menu_title` varchar(255) NOT NULL,
  `front_menu_target` varchar(50) NOT NULL,
  `front_menu_type` varchar(255) NOT NULL,
  `front_menu_link` varchar(255) NOT NULL,
  `showing_order` tinyint(1) NOT NULL,
  `front_menu_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`front_menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `front_menu`
--

INSERT INTO `front_menu` (`front_menu_id`, `front_menu_category_id`, `parent_front_menu`, `front_menu_name`, `front_menu_title`, `front_menu_target`, `front_menu_type`, `front_menu_link`, `showing_order`, `front_menu_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(7, 1, 0, 'About', 'About', '_self', 'URI', 'who-we-are', 1, 1, 1386827123, 1416391326, 1),
(11, 1, 0, 'Registration', 'Registration', '_self', 'URI', 'students/signup', 5, 0, 1386827123, 1398241768, 0);

-- --------------------------------------------------------

--
-- Table structure for table `front_menu_category`
--

CREATE TABLE IF NOT EXISTS `front_menu_category` (
  `front_menu_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_menu_category_name` varchar(255) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `front_menu_category_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`front_menu_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `front_menu_category`
--

INSERT INTO `front_menu_category` (`front_menu_category_id`, `front_menu_category_name`, `showing_order`, `front_menu_category_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Main Menu', 1, 1, 1367392979, 1367392979, 1),
(2, 'Footer menu', 2, 1, 1368108910, 1368108910, 1);

-- --------------------------------------------------------

--
-- Table structure for table `global_config`
--

CREATE TABLE IF NOT EXISTS `global_config` (
  `global_config_id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `themes_id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `author_name` varchar(250) NOT NULL,
  `author_email` varchar(255) NOT NULL,
  `author_address` text NOT NULL,
  `author_address_latitude` varchar(50) NOT NULL,
  `author_address_longitude` varchar(50) NOT NULL,
  `google_map_code` text NOT NULL,
  `author_telephone` varchar(50) NOT NULL,
  `author_mobile` varchar(200) NOT NULL,
  `author_fax` varchar(50) NOT NULL,
  `author_paypal_email` varchar(150) NOT NULL,
  `author_bank_name` varchar(250) NOT NULL,
  `author_branch_name` varchar(250) NOT NULL,
  `author_account_name` varchar(250) NOT NULL,
  `author_account_no` varchar(100) NOT NULL,
  `site_current_theme` varchar(250) NOT NULL,
  `site_seo_title` text NOT NULL,
  `site_seo_description` text NOT NULL,
  `site_seo_keywords` text NOT NULL,
  `site_email_logo` varchar(250) NOT NULL,
  `site_email_logo_thumbimage` varchar(250) NOT NULL,
  `site_visit_count` int(11) NOT NULL,
  PRIMARY KEY (`global_config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `global_config`
--

INSERT INTO `global_config` (`global_config_id`, `themes_id`, `site_name`, `author_name`, `author_email`, `author_address`, `author_address_latitude`, `author_address_longitude`, `google_map_code`, `author_telephone`, `author_mobile`, `author_fax`, `author_paypal_email`, `author_bank_name`, `author_branch_name`, `author_account_name`, `author_account_no`, `site_current_theme`, `site_seo_title`, `site_seo_description`, `site_seo_keywords`, `site_email_logo`, `site_email_logo_thumbimage`, `site_visit_count`) VALUES
(1, 0, 'Get Near by You', 'Shafiq', 'shafiqswh@gmail.com', 'Demo Address', '32.939537', '-96.731354', '', '972-231-5698', '972-231-5698 ext-115', 'NA', 'shafiqswh@gmail.com', 'None', 'None', 'Demo Account', '0001090013963', 'suffa', 'Halal Shop Near by you', 'Halal Shop Near by you', 'Halal Shop Near by you', 'logo.png', 'logo_thumb.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `location_info`
--

CREATE TABLE IF NOT EXISTS `location_info` (
  `location_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_info_publish` tinyint(1) NOT NULL DEFAULT '1',
  `showing_order` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `cat_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`location_info_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `location_info`
--

INSERT INTO `location_info` (`location_info_id`, `location_info_publish`, `showing_order`, `created_date`, `modified_date`, `created_by`, `name`, `description`, `cat_name`, `lat`, `lng`) VALUES
(1, 1, 1, 1439098815, 1439230263, 1, 'Gloria Jeans Coffees', 'Mexican &amp; Latin American specialties, including Salvadoran pupusas, served in a strip-mall setting.', 'shop', '23.7791694', '90.4163942'),
(2, 1, 2, 1439230120, 1439230120, 1, 'Nandos', '5E (F) Bir Uttam Mir Shawkat Sarak,\r\nDhaka 1212', 'shop', '23.7765065', '90.4198733'),
(3, 1, 3, 1439230359, 1439230459, 1, 'KFC', 'Road 7A,Dhanmondi,Dhaka', 'shop', '23.7435263', '90.3734159'),
(4, 1, 4, 1439293466, 1439293497, 1, 'Taqwa Masjid', 'Address:Rd No. 12A, Dhaka', 'masjid', '23.7503937', '90.3403645');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_id` int(11) NOT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_module` varchar(255) NOT NULL,
  `module_type` varchar(5) NOT NULL DEFAULT 'No',
  `showing_order` tinyint(1) NOT NULL,
  `menu_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `menu_module` (`menu_module`),
  UNIQUE KEY `menu_name` (`menu_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_category_id`, `parent_menu`, `menu_name`, `menu_module`, `module_type`, `showing_order`, `menu_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 1, 0, 'Page Information', 'page_info', 'Yes', 3, 1, 1350445641, 1367392424, 1),
(2, 1, 0, 'Contact Us', 'contact_us', 'No', 4, 1, 1352530202, 1352530202, 1),
(3, 1, 0, 'Site URI', 'site_uri', 'Yes', 1, 1, 1367391858, 1367391858, 1),
(4, 1, 0, 'Comments', 'comments', 'No', 6, 0, 1368191562, 1368261343, 1),
(5, 1, 0, 'Front Menu', 'front_menu', 'No', 2, 1, 1371616917, 1371616917, 1),
(19, 1, 0, 'State', 'state', 'Yes', 7, 1, 1398242967, 1398242967, 1),
(27, 16, 0, 'Location Info', 'location_info', 'Yes', 1, 1, 1439098726, 1439098726, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_category`
--

CREATE TABLE IF NOT EXISTS `menu_category` (
  `menu_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_name` varchar(255) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `menu_category_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`menu_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `menu_category`
--

INSERT INTO `menu_category` (`menu_category_id`, `menu_category_name`, `showing_order`, `menu_category_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Common Module', 1, 1, 1371617515, 1371617515, 1),
(8, 'FAQ''s', 8, 1, 1374230389, 1374230389, 1),
(16, 'Location Information', 9, 1, 1439098720, 1439098720, 1);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `module_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `table_name` (`table_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `module_name`, `table_name`, `showing_order`, `module_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Site URI', 'site_uri', 1, 1, 1369724087, 1369724087, 1),
(2, 'Page Information', 'page_info', 2, 1, 1369724282, 1369724282, 1),
(15, 'State', 'state', 14, 1, 1398242637, 1398242637, 1),
(18, 'Locationinfo', 'location_info', 15, 1, 1439097963, 1439097975, 1);

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE IF NOT EXISTS `module_fields` (
  `module_fields_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `form_fields_id` varchar(255) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `module_fields_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`module_fields_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`module_fields_id`, `module_id`, `form_fields_id`, `showing_order`, `module_fields_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 1, '2', 2, 1, 1369724171, 1369724171, 1),
(2, 1, '7', 1, 1, 1369724171, 1369724171, 1),
(3, 1, '8', 3, 1, 1369724171, 1369724171, 1),
(4, 1, '9', 4, 1, 1369724171, 1369724171, 1),
(5, 1, '10', 5, 1, 1369724171, 1369724171, 1),
(6, 1, '11', 6, 1, 1369724171, 1369724171, 1),
(7, 2, '1', 1, 1, 1369724314, 1369724314, 1),
(8, 2, '2', 2, 1, 1369724314, 1369724314, 1),
(9, 2, '3', 3, 1, 1369724314, 1369724314, 1),
(10, 2, '10', 4, 1, 1369724314, 1369724314, 1),
(11, 2, '11', 5, 1, 1369724314, 1369724314, 1),
(90, 18, '35', 5, 1, 1439104475, 1439104475, 1),
(71, 15, '1', 2, 1, 1398242885, 1398242885, 1),
(72, 15, '28', 3, 1, 1398242885, 1398242885, 1),
(73, 15, '29', 1, 1, 1398242885, 1398242885, 1),
(87, 18, '3', 2, 1, 1439098593, 1439104475, 1),
(86, 18, '1', 1, 1, 1439098593, 1439104475, 1),
(89, 18, '34', 4, 1, 1439104475, 1439104475, 1),
(88, 18, '33', 3, 1, 1439098593, 1439104475, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_info`
--

CREATE TABLE IF NOT EXISTS `page_info` (
  `page_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_info_publish` tinyint(1) NOT NULL DEFAULT '1',
  `showing_order` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uri_name` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_keyword` text,
  `meta_description` text,
  PRIMARY KEY (`page_info_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `page_info`
--

INSERT INTO `page_info` (`page_info_id`, `page_info_publish`, `showing_order`, `created_date`, `modified_date`, `created_by`, `name`, `uri_name`, `description`, `meta_keyword`, `meta_description`) VALUES
(1, 1, 1, 1367397151, 1438970243, 1, 'Who We Are', 'who-we-are', '<p>Comming soon...</p>', 'About', 'About'),
(2, 1, 2, 1367426223, 1368010169, 1, 'Missing URL', 'missing-url', '<p><span>Opps you are missing URL. </span></p>\r\n<p>&nbsp;</p>\r\n<p><span>Don\\''t try to visit URL without site map.</span></p>', NULL, NULL),
(3, 1, 3, 1367426274, 1368010179, 1, 'Member Permission Not Allowed Page', 'member-permission-not-allowed-page', '<p>Sorry! you don\\''t have permission of this page.</p>\r\n<p>&nbsp;</p>\r\n<p>Please contact with admin of this site for getting permission of this page.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', NULL, NULL),
(4, 1, 4, 1367426318, 1368010189, 1, 'Logout Page', 'logout-page', '<p>You have been successfully logout.</p>\r\n<p>&nbsp;</p>\r\n<p>Thank you very much for using foster child management.</p>', NULL, NULL),
(5, 1, 5, 1368263176, 1368263176, 1, 'Contact Us Client Message', 'contact-us-client-message', '<p>Thanks a lot for contacting with us. We will give feedback as soon as possible.</p>\r\n<p>&nbsp;</p>', 'Contact Us Client Message', 'Contact Us Client Message');

-- --------------------------------------------------------

--
-- Table structure for table `set_user_permission`
--

CREATE TABLE IF NOT EXISTS `set_user_permission` (
  `set_user_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_level_id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT '0',
  `menu_module` varchar(150) NOT NULL,
  `view_option` tinyint(1) DEFAULT '0',
  `add_option` tinyint(1) DEFAULT '0',
  `edit_option` tinyint(1) DEFAULT '0',
  `hide_option` tinyint(1) DEFAULT '0',
  `showing_order` int(11) NOT NULL,
  `set_user_permission_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`set_user_permission_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `set_user_permission`
--

INSERT INTO `set_user_permission` (`set_user_permission_id`, `user_level_id`, `menu_id`, `menu_module`, `view_option`, `add_option`, `edit_option`, `hide_option`, `showing_order`, `set_user_permission_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 1, 3, 'site_uri', 1, 1, 1, 1, 1, 1, 1371617595, 1398764618, 1),
(2, 1, 5, 'front_menu', 1, 1, 1, 1, 2, 1, 1371617595, 1398764618, 1),
(3, 1, 1, 'page_info', 1, 1, 1, 1, 3, 1, 1371617595, 1398764618, 1),
(4, 1, 2, 'contact_us', 1, 1, 1, 1, 4, 1, 1371617595, 1398764618, 1),
(20, 2, 27, 'location_info', 1, 1, 1, 1, 5, 1, 1439106801, 1439107046, 1),
(6, 2, 3, 'site_uri', 0, 0, 0, 0, 1, 0, 1371617616, 1439107046, 1),
(7, 2, 5, 'front_menu', 1, 1, 1, 0, 2, 0, 1371617616, 1439107046, 1),
(8, 2, 1, 'page_info', 1, 1, 1, 0, 3, 1, 1371617616, 1439107046, 1),
(9, 2, 2, 'contact_us', 1, 1, 1, 0, 4, 1, 1371617616, 1439107046, 1),
(16, 1, 19, 'state', 1, 1, 1, 1, 11, 1, 1398764618, 1398764618, 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_uri`
--

CREATE TABLE IF NOT EXISTS `site_uri` (
  `site_uri_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_uri_publish` tinyint(1) NOT NULL DEFAULT '1',
  `showing_order` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `uri_name` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `uri_table_name` varchar(255) DEFAULT NULL,
  `uri_table_field_name` varchar(255) DEFAULT NULL,
  `meta_keyword` text,
  `meta_description` text,
  PRIMARY KEY (`site_uri_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `site_uri`
--

INSERT INTO `site_uri` (`site_uri_id`, `site_uri_publish`, `showing_order`, `created_date`, `modified_date`, `created_by`, `uri_name`, `meta_title`, `uri_table_name`, `uri_table_field_name`, `meta_keyword`, `meta_description`) VALUES
(1, 1, 1, 1367397307, 1438970243, 1, 'who-we-are', 'Who We Are', 'page_info', 'uri_name', 'About', 'About'),
(2, 1, 4, 1367426223, 1368010169, 1, 'missing-url', 'Missing URL', 'page_info', 'uri_name', 'Missing URL', 'Missing URL'),
(3, 1, 5, 1367426274, 1368010179, 1, 'member-permission-not-allowed-page', 'Member Permission Not Allowed Page', 'page_info', 'uri_name', 'Member Permission Not Allowed Page', 'Member Permission Not Allowed Page'),
(4, 1, 6, 1367426318, 1368010189, 1, 'logout-page', 'Logout Page', 'page_info', 'uri_name', 'Logout Page', 'Logout Page'),
(5, 1, 7, 1367426363, 1368010201, 1, 'contact-us-client-message', 'Contact Us Client Message', 'page_info', 'uri_name', 'Contact Us Client Message', 'Contact Us Client Message'),
(6, 1, 8, 1374142142, 1374142142, 1, 'get-the-weatherguard-advantage', 'Get the Weatherguard Advantage', 'page_info', 'uri_name', 'Get the Weatherguard Advantage', 'Get the Weatherguard Advantage'),
(7, 1, 9, 1374142479, 1374142479, 1, 'roofing', 'Roofing', 'services', 'uri_name', 'Roofing', 'Roofing'),
(8, 1, 10, 1374142509, 1374142509, 1, 'siding', 'Siding', 'services', 'uri_name', 'Siding', 'Siding'),
(9, 1, 11, 1374142540, 1374142540, 1, 'stone-and-stucco', 'Stone & Stucco', 'services', 'uri_name', 'Stone & Stucco', 'Stone & Stucco'),
(10, 1, 12, 1374142577, 1374142577, 1, 'windows-and-doors', 'Windows & Doors', 'services', 'uri_name', 'Windows & Doors', 'Windows & Doors'),
(11, 1, 13, 1374142612, 1374142612, 1, 'garage-doors', 'Garage Doors', 'services', 'uri_name', 'Garage Doors', 'Garage Doors'),
(12, 1, 14, 1374142636, 1374142636, 1, 'gutters-and-leaders', 'Gutters & Leaders', 'services', 'uri_name', 'Gutters & Leaders', 'Gutters & Leaders'),
(13, 1, 15, 1374142659, 1374142715, 1, 'columns-and-railings', 'Columns & Railings', 'services', 'uri_name', 'Columns & Railings', 'Columns & Railings'),
(14, 1, 16, 1374142704, 1374142704, 1, 'architectural-millwork', 'Architectural Millwork', 'services', 'uri_name', 'Architectural Millwork', 'Architectural Millwork'),
(15, 1, 17, 1374142844, 1374142844, 1, 'sadfsaf', 'sadfsaf', 'services', 'uri_name', '', ''),
(16, 1, 18, 1374227711, 1374227711, 1, 'test', 'Test', 'projects', 'uri_name', '', ''),
(17, 1, 19, 1374227796, 1374228653, 1, 'test-final', 'Test Final', 'projects', 'uri_name', 'asd fsaf asdfas fasd', 'asd fasdf dasf'),
(18, 1, 20, 1374227917, 1374227917, 1, 'super-final', 'Super Final', 'projects', 'uri_name', '', ''),
(19, 1, 21, 1374233608, 1374234559, 1, 'test1', 'test', 'case_files', 'uri_name', NULL, NULL),
(20, 1, 22, 1386829142, 1416394385, 1, 'admission', 'Admission', 'page_info', 'uri_name', 'admission', 'admission'),
(21, 1, 23, 1386829190, 1416395403, 1, 'give', 'Give', 'page_info', 'uri_name', 'Give', 'Give'),
(22, 1, 24, 1386830211, 1416379985, 1, 'race-to-goodness--pillars-to-a-good-family', 'RACE TO GOODNESS- Pillars to a Good Family', 'latest_event', 'uri_name', 'RACE TO GOODNESS | Pillars to a Good Family', 'RACE TO GOODNESS | Pillars to a Good Family'),
(39, 1, 41, 1395901380, 1407151306, 1, 'suhbah-program', 'Suhbah Program', 'programs', 'uri_name', 'Suhbah Program', 'Suhbah Program'),
(23, 1, 25, 1386830274, 1416379887, 1, 'open-house-coming-up-on-friday-the-5th-from-maghrib-to-isha', 'Open House coming up on Friday the 5th from Maghrib to Isha', 'latest_event', 'uri_name', 'Open House coming up on Friday the 5th from Maghrib to Isha', 'Open House coming up on Friday the 5th from Maghrib to Isha'),
(24, 1, 26, 1386831402, 1386831402, 1, 'our-objective1', 'Our Objective1', 'our_objectives', 'uri_name', 'Our Objective1', 'Our Objective1'),
(25, 1, 27, 1386831430, 1386831430, 1, 'our-objective2', 'Our Objective2', 'our_objectives', 'uri_name', 'Our Objective2', 'Our Objective2'),
(26, 1, 28, 1386831454, 1386831454, 1, 'our-objective3', 'Our Objective3', 'our_objectives', 'uri_name', 'Our Objective3', 'Our Objective3'),
(27, 1, 29, 1386831467, 1386831467, 1, 'our-objective4', 'Our Objective4', 'our_objectives', 'uri_name', 'Our Objective4', 'Our Objective4'),
(28, 1, 30, 1386831490, 1386831490, 1, 'our-objective5', 'Our Objective5', 'our_objectives', 'uri_name', 'Our Objective5', 'Our Objective5'),
(29, 1, 31, 1386831505, 1386831505, 1, 'our-objective6', 'Our Objective6', 'our_objectives', 'uri_name', 'Our Objective6', 'Our Objective6'),
(30, 1, 32, 1386831519, 1386831519, 1, 'our-objective7', 'Our Objective7', 'our_objectives', 'uri_name', 'Our Objective7', 'Our Objective7'),
(31, 1, 33, 1386831625, 1416043432, 1, 'our-mission', 'Our Mission', 'page_info', 'uri_name', 'Our Mission', 'Our Mission'),
(32, 1, 34, 1386843526, 1387280274, 1, 'what-we-do', 'What we do', 'page_info', 'uri_name', 'What we do', 'What we do'),
(33, 1, 35, 1386846697, 1416393612, 1, 'programs', 'Programs', 'page_info', 'uri_name', 'programs', 'programs'),
(34, 1, 36, 1386846811, 1386846811, 1, 'upcoming-seminars', 'Upcoming Seminars', 'page_info', 'uri_name', 'Upcoming Seminars', 'Upcoming Seminars'),
(35, 1, 37, 1386850486, 1407151149, 1, 'journey-program', 'Journey Program', 'programs', 'uri_name', 'Journey Program', 'Journey Program'),
(36, 1, 38, 1386850532, 1407151229, 1, 'guidance-program', 'Guidance Program', 'programs', 'uri_name', 'Guidance Program', 'Guidance Program'),
(37, 1, 39, 1386850559, 1389943277, 3, 'nisaa-and-quranic-arabic', 'Nisaa & Quranic Arabic', 'programs', 'uri_name', NULL, NULL),
(38, 1, 40, 1387281613, 1391178012, 3, 'islamic-association-of-collin-county', 'Islamic Association of Collin County', 'page_info', 'uri_name', 'Islamic Association of Collin County', 'Islamic Association of Collin County'),
(40, 1, 42, 1398347328, 1399796067, 3, 'payment-by-cash', 'Payment by Cash', 'page_info', 'uri_name', 'Payment by Cash', 'Payment by Cash'),
(41, 1, 43, 1398347371, 1398347371, 1, 'paypal-transaction-successful', 'PayPal Transaction Successful', 'page_info', 'uri_name', 'PayPal Transaction Successful', 'PayPal Transaction Successful'),
(42, 1, 44, 1398347396, 1398347396, 1, 'paypal-transaction-cancel', 'PayPal Transaction Cancel', 'page_info', 'uri_name', 'PayPal Transaction Cancel', 'PayPal Transaction Cancel'),
(44, 1, 45, 1399999226, 1407151385, 1, 'post-hifz-program', 'Post-Hifz Program', 'programs', 'uri_name', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_publish` tinyint(1) NOT NULL DEFAULT '1',
  `showing_order` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_publish`, `showing_order`, `created_date`, `modified_date`, `created_by`, `name`, `country_id`, `state_code`) VALUES
(1, 1, 1, 1381446718, 1381446718, 1, 'Alabama', 220, 'AL'),
(2, 1, 2, 1381446749, 1381446749, 1, 'Alaska', 220, 'AK'),
(3, 1, 3, 1381446763, 1381446763, 1, 'American Samoa', 220, 'AS'),
(4, 1, 4, 1381446785, 1381446785, 1, 'Arizona', 220, 'AZ'),
(5, 1, 5, 1381446808, 1381446808, 1, 'Arkansas', 220, 'AR'),
(6, 1, 6, 1381446828, 1381446828, 1, 'Armed Forces Africa', 220, 'AF'),
(7, 1, 7, 1381446848, 1381446848, 1, 'Armed Forces Americas', 220, 'AA'),
(8, 1, 8, 1381446867, 1381446867, 1, 'Armed Forces Canada', 220, 'AC'),
(9, 1, 9, 1381446890, 1381446890, 1, 'Armed Forces Europe', 220, 'AE'),
(10, 1, 10, 1381446910, 1381446910, 1, 'Armed Forces Middle East', 220, 'AM'),
(11, 1, 11, 1381446928, 1381446928, 1, 'Armed Forces Pacific', 220, 'AP'),
(12, 1, 12, 1381446947, 1381446947, 1, 'California', 220, 'CA'),
(13, 1, 13, 1381447004, 1381447004, 1, 'Colorado', 220, 'CO'),
(14, 1, 14, 1381447027, 1381447027, 1, 'Connecticut', 220, 'CT'),
(15, 1, 15, 1381447042, 1381447042, 1, 'Delaware', 220, 'DE'),
(16, 1, 16, 1381447061, 1381447061, 1, 'District of Columbia', 220, 'DC'),
(17, 1, 17, 1381447084, 1381447084, 1, 'Federated States Of Micronesia', 220, 'FM'),
(18, 1, 18, 1381447102, 1381447102, 1, 'Florida', 220, 'FL'),
(19, 1, 19, 1381447129, 1381447129, 1, 'Georgia', 220, 'GA'),
(20, 1, 20, 1381447138, 1381447138, 1, 'Guam', 220, 'GU'),
(21, 1, 21, 1381447154, 1381447154, 1, 'Hawaii', 220, 'HI'),
(22, 1, 22, 1381447169, 1381447169, 1, 'Idaho', 220, 'ID'),
(23, 1, 23, 1381447190, 1381447190, 1, 'Illinois', 220, 'IL'),
(24, 1, 24, 1381447213, 1381447213, 1, 'Indiana', 220, 'IN'),
(25, 1, 25, 1381447243, 1381447243, 1, 'Iowa', 220, 'IA'),
(26, 1, 26, 1381447285, 1381447285, 1, 'Kansas', 220, 'KS'),
(27, 1, 27, 1381447369, 1381447369, 1, 'Kentucky', 220, 'KY'),
(28, 1, 28, 1381447449, 1381447449, 1, 'Louisiana', 220, 'LA'),
(29, 1, 29, 1381447492, 1381447492, 1, 'Maine', 220, 'ME'),
(30, 1, 30, 1381447573, 1381447573, 1, 'Marshall Islands', 220, 'MH'),
(31, 1, 31, 1381447627, 1381447627, 1, 'Maryland', 220, 'MD'),
(32, 1, 32, 1381447709, 1381447709, 1, 'Massachusett', 220, 'MA'),
(33, 1, 33, 1381447777, 1381447777, 1, 'Massachusetts', 220, 'MI'),
(34, 1, 34, 1381447839, 1381447839, 1, 'Minnesota', 220, 'MN'),
(35, 1, 35, 1381447903, 1381447903, 1, 'Mississippi', 220, 'MS'),
(36, 1, 36, 1381447967, 1381447967, 1, 'Missouri', 220, 'MO'),
(37, 1, 37, 1381448023, 1381448023, 1, 'Montana', 220, 'MT'),
(38, 1, 38, 1381448066, 1381448066, 1, 'Nebraska', 220, 'NE'),
(39, 1, 39, 1381448128, 1381448128, 1, 'Nevada', 220, 'NV'),
(40, 1, 40, 1381448341, 1381448341, 1, 'New Hampshire', 220, 'NH'),
(41, 1, 41, 1381448414, 1381448414, 1, 'New Jersey', 220, 'NJ'),
(42, 1, 42, 1381448482, 1381448482, 1, 'New Mexico', 220, 'NM'),
(43, 1, 43, 1381448520, 1381448520, 1, 'New York', 220, 'NY'),
(44, 1, 44, 1381448562, 1381448562, 1, 'North Carolina', 220, 'NC'),
(45, 1, 45, 1381448599, 1381448599, 1, 'North Dakota', 220, 'ND'),
(46, 1, 46, 1381448634, 1381448634, 1, 'Northern Mariana Islands', 220, 'MP'),
(47, 1, 47, 1381448670, 1381448670, 1, 'Ohio', 220, 'OH'),
(48, 1, 48, 1381448696, 1381448696, 1, 'Oklahoma', 220, 'OK'),
(49, 1, 49, 1381448723, 1381448723, 1, 'Oregon', 220, 'OR'),
(50, 1, 50, 1381448777, 1381448777, 1, 'Palau', 220, 'PW'),
(51, 1, 51, 1381448820, 1381448820, 1, 'Pennsylvania', 220, 'PA'),
(52, 1, 52, 1381448853, 1381448853, 1, 'Puerto Rico', 220, 'PR'),
(53, 1, 53, 1381448903, 1381448903, 1, 'Rhode Island', 220, 'RI'),
(54, 1, 54, 1381448937, 1381448937, 1, 'South Carolina', 220, 'SC'),
(55, 1, 55, 1381448966, 1381448966, 1, 'South Dakota', 220, 'SD'),
(56, 1, 56, 1381449013, 1381449013, 1, 'Tennessee', 220, 'TN'),
(57, 1, 57, 1381449043, 1381449043, 1, 'Texas', 220, 'TX'),
(58, 1, 58, 1381449070, 1381449070, 1, 'Utah', 220, 'UT'),
(59, 1, 59, 1381449094, 1381449094, 1, 'Vermont', 220, 'VT'),
(60, 1, 60, 1381449115, 1381449115, 1, 'Virgin Islands', 220, 'VI'),
(61, 1, 61, 1381449131, 1381449131, 1, 'Virginia', 220, 'VA'),
(62, 1, 62, 1381449148, 1381449148, 1, 'Washington', 220, 'WA'),
(63, 1, 63, 1381449166, 1381449166, 1, 'West Virginia', 220, 'WV'),
(64, 1, 64, 1381449183, 1381449183, 1, 'Wisconsin', 220, 'WI'),
(65, 1, 65, 1381449204, 1381449204, 1, 'Wyoming', 220, 'WY'),
(66, 1, 66, 1386578667, 1386578667, 1, 'Manitoba', 35, 'MB'),
(67, 1, 67, 1386578746, 1386578746, 1, 'Ontario', 35, 'ON'),
(68, 1, 68, 1386578939, 1386578939, 1, 'Quebec', 35, 'QC'),
(69, 1, 69, 1386578993, 1386578993, 1, 'Alberta', 35, 'AB'),
(70, 1, 70, 1386579031, 1386579031, 1, 'British Columbia', 35, 'BC'),
(71, 1, 71, 1386579327, 1386579327, 1, 'Nova Scotia', 35, 'NS'),
(72, 1, 72, 1386579426, 1386579426, 1, 'Saskatchewan', 16, 'SK');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `themes_id` int(11) NOT NULL AUTO_INCREMENT,
  `themes_folder` varchar(50) NOT NULL,
  `themes_name` varchar(255) NOT NULL,
  `themes_image` varchar(200) NOT NULL,
  `themes_thumbimage` varchar(250) NOT NULL,
  `showing_order` tinyint(1) NOT NULL,
  `themes_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`themes_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`themes_id`, `themes_folder`, `themes_name`, `themes_image`, `themes_thumbimage`, `showing_order`, `themes_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(3, 'suffa', 'suffa', 'suffa.jpg', 'suffa_thumb.jpg', 3, 1, 1386825504, 1438970491, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_fullname` varchar(250) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contactno` varchar(20) NOT NULL,
  `user_gender` varchar(20) NOT NULL,
  `user_dateofbirth` date NOT NULL,
  `user_published` tinyint(1) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  `create_user` tinyint(1) NOT NULL,
  `user_onlinestatus` tinyint(1) NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` date NOT NULL,
  `user_lastlogin` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_email`, `user_contactno`, `user_gender`, `user_dateofbirth`, `user_published`, `user_level_id`, `create_user`, `user_onlinestatus`, `created_date`, `modified_date`, `user_lastlogin`, `created_by`) VALUES
(1, 'sadmin', '96b5a35db9990f7c6b4309a4240e803c', 'Super Administrator', 'shafiqswh@gmail.com', '01712088033', 'Male', '1980-08-21', 1, 1, 1, 1, '2011-01-12', '2013-04-15', 1439356563, 1),
(3, 'ashraf', '96b5a35db9990f7c6b4309a4240e803c', 'Ashraf Ferdouse', 'ashraff@gmail.com', '112233445566', 'Male', '1981-03-25', 1, 2, 0, 0, '2012-03-25', '2014-01-17', 1439229785, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE IF NOT EXISTS `user_level` (
  `user_level_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_level_name` varchar(255) NOT NULL,
  `showing_order` tinyint(1) NOT NULL,
  `user_level_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`user_level_id`, `user_level_name`, `showing_order`, `user_level_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Administrator', 1, 1, 1347371004, 1347371019, 1),
(2, 'Editor', 2, 1, 1347371090, 1439104887, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
