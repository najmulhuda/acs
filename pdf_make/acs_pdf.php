<html>
<head>
<title>ShotDev.Com Tutorial</title>
</head>
<body>
<?php
require('fpdf.php');

class PDF extends FPDF
{
//Load data
function LoadData($file)
{
	//Read file lines
	$lines=file($file);
	$data=array();
	foreach($lines as $line)
		$data[]=explode(';',chop($line));
	return $data;
}

//Simple table
function BasicTable($header,$data)
{
	//Header
	$w=array(17,23,18,56,26,26,25);
	//Header
	for($i=0;$i<count($header);$i++)
	$this->Cell($w[$i],6,$header[$i],1,0,'C');
	$this->Ln();
	//Data
	foreach ($data as $eachResult) 
	{
		$this->Cell(17,6,$eachResult["SiteName"],1);
		$this->Cell(23,6,$eachResult["SubcentreName"],1);
		$this->Cell(18,6,$eachResult["RegionName"],1,0,'C');
		$this->Cell(56,6,$eachResult["Status"],1);
		$this->Cell(26,6,$eachResult["DSTime"],1);
		$this->Cell(26,6,$eachResult["CSTime"],1);
		$this->Cell(25,6,$eachResult["IP"],1);
		$this->Ln();
	}
}
}

$pdf=new PDF();
//Column titles
$header=array('SiteName','SubcentreName','RegionName','Status','DSTime','CSTime','IP');
//Data loading

//*** Load MySQL Data ***//
$objConnect = mysql_connect("localhost","root","") or die(mysql_error());
$objDB = mysql_select_db("2radb");
$strSQL = "SELECT * FROM acs";
$objQuery = mysql_query($strSQL);
$resultData = array();
for ($i=0;$i<mysql_num_rows($objQuery);$i++) {
	$result = mysql_fetch_array($objQuery);
	array_push($resultData,$result);
}
//************************//



$pdf->SetFont('Arial','',7);

//*** Table 1 ***//
$pdf->AddPage();
$pdf->Image('2ra.jpg',80,8,33);
$pdf->Ln(30);
$pdf->BasicTable($header,$resultData);
$pdf->Output("shotdev/acs_pdf.pdf","F");
?>

PDF Created Click <a href="shotdev/acs_pdf.pdf">here</a> to Download

</body>
</html>
<!--- This file download from www.shotdev.com -->