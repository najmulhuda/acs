<html>
<head>
<title>Employee Activity</title>
</head>
<body>
<h1>Employee ViewList</h1>
<?php
require('fpdf.php');

class PDF extends FPDF
{
//Load data
function LoadData($file)
{
	//Read file lines
	$lines=file($file);
	$data=array();
	foreach($lines as $line)
		$data[]=explode(';',chop($line));
	return $data;
}

//Simple table
function BasicTable($header,$data)
{
	//Header
	$w=array(25,30,30,56);
	//Header
	for($i=0;$i<count($header);$i++)
	$this->Cell($w[$i],6,$header[$i],1,0,'C');
	$this->Ln();
	//Data
	foreach ($data as $eachResult) 
	{
		$this->Cell(25,6,$eachResult["Employee_Id"],1);
		$this->Cell(30,6,$eachResult["Employee_Name"],1);
		$this->Cell(30,6,$eachResult["Site_Name"],1,0,'C');
		$this->Cell(56,6,$eachResult["Visit_Date"],1);
		$this->Ln();
	}
}
}

$pdf=new PDF();
//Column titles
$header=array('Employee_Id','Employee_Name','Site_Name','Visit_Date');
//Data loading

//*** Load MySQL Data ***//
$objConnect = mysql_connect("localhost","root","") or die(mysql_error());
$objDB = mysql_select_db("2radb");
$strSQL = "SELECT tbl_employee.EmployeeId AS Employee_Id,tbl_employee.EmployeeName AS Employee_Name,tbl_site.Name AS Site_Name,tbl_attendancedata.Ctime AS Visit_Date FROM tbl_employee,tbl_site,tbl_attendancedata WHERE tbl_employee.CardNumber = tbl_attendancedata.CardId AND tbl_site.Id = tbl_attendancedata.SiteId ";
$objQuery = mysql_query($strSQL);
$resultData = array();
for ($i=0;$i<mysql_num_rows($objQuery);$i++) {
	$result = mysql_fetch_array($objQuery);
	array_push($resultData,$result);
}
//************************//



$pdf->SetFont('Arial','',7);

//*** Table 1 ***//
$pdf->AddPage();
$pdf->Image('2ra.jpg',80,8,33);
$pdf->Ln(30);
$pdf->BasicTable($header,$resultData);
$pdf->Output("shotdev/employeeactivity_pdf.pdf","F");
?>

PDF Created Click <a href="shotdev/employeeactivity_pdf.pdf">here</a> to Download

</body>
</html>
<!--- This file download from www.shotdev.com -->