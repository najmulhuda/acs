<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_addemployee', 'onsubmit'=>'return check_add_device();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'employee/edit/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'employee/add/', $userformattributes); 
 }
 
?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>

<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   foreach($data as $row){
		   $id = $row->id;
		   $employeeid = $row->employeeid;
		   $employeename = $row->employeename;
		   $cardnumber = $row->cardnumber;
		   $cardstatus= $row->cardstatus;
		   $state = $row->state;
		   $subcentre = $row->subcentre;
		   $phoneno = $row->phoneno;
	   }	
	    
	}	
}else{
	$id = "";
	$employeeid = "";
	$employeename = "";
	$cardnumber = "";
	$cardstatus = "";
	$state = "";
	$subcentre = "";
    $phoneno = "";
}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New Employee

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'employee';?>" class="white14bold">Show Employee List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

				<tr>

					<td align="right" width="30%" class="black12bold">Employee Id <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'employee_id',

									  'id'       => 'employee_id',

									  'tabindex'       => '1',

									  'value'    => $employeeid,

									  'maxlength' => '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
                <tr>

					<td align="right" width="30%" class="black12bold">Employee Name <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'employee_name',

									  'id'       => 'employee_name',

									  'tabindex'       => '1',

									  'value'    => $employeename,

									  'maxlength' => '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
				<tr>

					<td align="right" class="black12bold">Employee Status <span class="red12bold">*</span>:</td>

					<td align="left">

						<select name='employee_status' id='employee_status'>
						   <option value="0">0</option>
						   <option value="1" selected>1</option>
						</select>

					   <span id="errmsg_user_email" class="red11normal"></span>

					</td>

				</tr>												
				<tr>

					<td align="right" class="black12bold">Card Number <span class="red12bold"></span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'card_id',

									  'id'       => 'card_id',

									  'tabindex'       => '2',

									  'value'    => $cardnumber,

									  'maxlength' => '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>	
                <tr>
				    <td align="right" class="black12bold">Card Status <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					   echo "<select name='card_status' id='card_status'>";
                       for($i = 1; $i < 5; $i++){
						   if($mode=="edit"){
							   if($cardstatus == $i) { 
							      echo "<option value='". $i . "' selected>" . $i . "</option>";
							   }else{
								   echo "<option value='". $i . "'>" . $i . "</option>";
							   }
						   }else{
							   
							   if($i==2){
								   echo "<option value='". $i . "' selected>" . $i . "</option>";
							   }else{
								    echo "<option value='". $i . "'>" . $i . "</option>";
							   }
							}	
					   }
					   echo "</select>";
					  ?>	
                     

					</td>				
				</tr>				
                 <tr>
				    <td align="right" class="black12bold">Sub Centre <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					   echo "<select name='sub_centre' id='sub_centre'>";
                        if(is_array($subcentredata) && count($subcentredata)>0) {
	                       foreach ($subcentredata as $row1) {
		                     echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
	                      }

                            
                        }	
					   echo "</select>";
					  ?>	
                     

					</td>				
				</tr>
			   <tr>

					<td align="right" class="black12bold">Phone # <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'phone_no',

									  'id'       => 'phone_no',

									  'tabindex'       => '2',

									  'value'    => $phoneno,

									  'maxlength' => '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												
				
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>