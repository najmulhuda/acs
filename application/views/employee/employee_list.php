<?php
 $functionname = 'Employee';
 $site_url = str_replace('/index.php','',site_url("/"));
 
 $userformattributes = array('name' => 'frm_employeeearch','method' => 'post', 'accept-charset' => 'utf-8','onsubmit'=>'return check_search_employee();');
// echo form_open(site_url("/").'employee/search_employee', $userformattributes); 
 
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">

<script language="JavaScript" type="text/javascript">

	j=jQuery.noConflict();

	j(document).ready(function($) {

		initPagination();

	});

	 function pageselectCallback(page_index, jq){

		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();

		j('#Searchresult').empty().append(new_content);

		return false;

	}

	function initPagination() {

		

		// count entries inside the hidden content

		var num_entries = j('#hiddenresult div.result').length;

		// Create content inside pagination element

		j("#Pagination").pagination(num_entries, {

			callback: pageselectCallback,

			items_per_page:1 // Show only one item per page

		});

	 }  
   /* 
	function pageselectCallback(page_index, jq){

		var form_fields_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form

		var show_by_order=document.getElementById('show_by_order').value;

		var starting_val = parseInt(page_index*form_fields_per_page);

		var searching_field=document.getElementById('searching_field').value;

		

		j.post(form_fieldsfetchurl, {"show_by_order":show_by_order, "searching_field":searching_field, "starting_val": starting_val, "form_fields_per_page": form_fields_per_page},

		function(data) {

			j('#Searchresult').html(data);			

		});

		

		return true;

	}
	
	function getOptionsFromForm(){

		var opt = {callback: pageselectCallback};

		return opt;

	}
	
	function loadpagination(){

		var optInit = getOptionsFromForm(); // Create pagination element with options from form

		//alert(optInit);

		var totalrows_count = document.getElementById('totalrowscount').value;

		var data_per_page = document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form

		j("#Pagination").pagination(parseInt(totalrows_count), optInit, data_per_page);

	}
	
	j(document).ready(function(){

		//loadpagination();

	}); */


</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	All <?php echo $functionname; ?> List

                </td>

                <td align="right">
                   <a href="<?php echo site_url("/").'employee/add';?>" class="white14bold">Add New Employee</a>
                </td>

              </tr>

            </table>			

		</td>

	</tr>
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	
	
	<tr>
     
		<td align="center">		
				<div style="width:99%; overflow:hidden;">				

					<div id="Searchresult">

						This content will be replaced when pagination inits.

					</div>

					<div style="padding:0 10px 0 20px;display:none" class="black12bold">Total <?php echo count($data);?> data or <?php echo ceil(count($data)/10);?> pages found </div>

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">
                     
						<table width="100%" cellpadding="0" cellspacing="1" border="0">
                            <tr>
							 <td colspan="9">
							 <form action="employee" method="post">
							   <table width="100%" cellpadding="0" cellspacing="1" border="0">
							     <tr>
								  <td width="15%" style="text-align: right;font-weight:bold;">Sub Centre :</td>
								 <td>
								      <select name="sub_centre" id="sub_centre">
									   <option value="-1">All</option>
									  <?php
					                      
                                             if(is_array($subcentredata) && count($subcentredata)>0) {
												 $i=0;
												 foreach ($subcentredata as $row1) {
													
		                                             echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
													 
	                                             }

                            
                                              }	
					                       
					                    ?>	
                                      </select>
								 </td>
								<!-- <td width="5%" style="text-align: right;font-weight:bold;">State :</td>
								 <td>
								      <select name='employee_status' id='employee_status'>
									     <option value="-1">All</option>
						                 <option value="0">0</option>
						                 <option value="1">1</option>
						              </select>

								 </td>
								 <td width="10%" style="text-align: right;font-weight:bold;">Employee Id :</td>
								 <td>
								      <?php 

						                 $data = array('name'     => 'employee_id',

									                   'id'       => 'employee_id',

									                   'tabindex'       => '1',

									                   'value'    => '',

									                   'maxlength' => '100',

									                   'size'    => '50',
													   
													   'class'    => 'input120x20'

									                );															

						                echo form_input($data);

						           ?>
								 </td>
								 <td width="10%" style="text-align: right;font-weight:bold;">Phone No.</td>
								 <td>
								     <?php 

						                 $data = array('name'     => 'phone_no',

									                   'id'       => 'phone_no',

									                   'tabindex'       => '1',

									                   'value'    => '',

									                   'maxlength' => '100',

									                   'size'    => '50',

									                   'class'    => 'input120x20'

									                );															

						                echo form_input($data);

						           ?>
								 </td>-->
								  <td>
								       <?php 

						                  $data = array( 'name'    => 'submit',

										                 'value'    => 'Search',

										                 'tabindex'       => '13',

									                      'class'    => 'submitbutton');															

						                           echo form_submit($data);
									  ?>			   
								  </td>
								 </tr>
							
                               </table>
							
							 </form>
							 </td>
							</tr>
							<tr style="background:#FFF;height:20px;"> <td colspan="9"></tr>
							<tr>
							    <th class="titlerow" align="center" width="5%">#</th>

								<th class="titlerow" align="center" width="15%">E. Id</th>
        
		                        <th class="titlerow" align="center" width="15%">E. Name</th>	
								
                                <th class="titlerow" align="left" width="10%">E. Status</th>
								
                                <th class="titlerow" align="left" width="10%">Card Number</th>
								
								 <th class="titlerow" align="left" width="10%">C. Status</th>
								
								<th class="titlerow" align="left" width="10%">Sub Centre</th>
								
								<th class="titlerow" align="left" width="10%">Phone No.</th>

								
                                <th class="titlerow" align="center" width="15%">Action</th>																					
							</tr>

							<?php

							$str = "";

							$i = 1;
                           
                           if(isset($employeedata) && count($employeedata)>0){ 
							 foreach($employeedata as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;

                                $employeeid = $row->employeeid;
								
								 $employeename = $row->employeename;
								 
								$cardnumber = $row->cardnumber;
								
								$estatus = $row->state;
								
								$cardstatus = $row->cardstatus;
								
								$subcentre = $row->subcentre;
								
								$phoneno = $row->phoneno;

								$modifytime = $row->modifytime;

								$createdate = $row->createdate;

								

								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';

								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';

								
								$str .= "<tr>
                                    
									<td align=\"center\" class=\"$classname\">$i</td>
									
							        <td align=\"center\" class=\"$classname\">$employeeid</td>
									
                                    <td align=\"center\" class=\"$classname\">$employeename</td>
									
									<td align=\"center\" class=\"$classname\">$estatus</td>

									<td align=\"left\" class=\"$classname\">$cardnumber</td>
									
									<td align=\"left\" class=\"$classname\">$cardstatus</td>

									<td align=\"left\" class=\"$classname\">$subcentre</td>
									
									<td align=\"left\" class=\"$classname\">$phoneno</td>";
								

									$str .= "<td class=\"$classname\" align=\"center\">";

										$str .= "
                                                 <a href=\"$site_url"."employee/edit/$id\" title=\"Edit\">
                                                 <img src=\"$editimg\" border=\"0\" alt=\"Edit\">
                                                 </a> &nbsp;&nbsp;&nbsp;
												 <a onclick=\"tablerow_remove($id, 'demployee', 'demployee', 'demployee');\" title=\"Remove\">
                                                 <img src=\"$removeimg\" border=\"0\" alt=\"Remove\">
                                                 </a>";

									
									$str .= "</td>";

								

								

								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				echo "<div class=red18bold>No device found</div>";					

			}								

			?>

		</td>

	</tr>

</table>
<?php echo form_close(); ?>