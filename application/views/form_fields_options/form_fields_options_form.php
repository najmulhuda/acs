<?php
if(isset($form_fields_id['value']) && $form_fields_id['value'] !=''){ $form_fields_idval = $form_fields_id['value'];}
else{ $form_fields_idval = set_value('form_fields_id');}

if(isset($label_name['value']) && $label_name['value'] !=''){ $label_nameval = $label_name['value'];}
else{ $label_nameval = set_value('label_name');}

if(isset($label_value['value']) && $label_value['value'] !=''){ $label_valueval = $label_value['value'];}
else{ $label_valueval = set_value('label_value');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($form_fields_options_id['value']) && $form_fields_options_id['value'] !=''){ $form_fields_options_idval = $form_fields_options_id['value'];}
else{ $form_fields_options_idval = set_value('form_fields_options_id');}	
if($form_fields_options_idval==''){$form_fields_options_idval = 0;}			
$site_url = site_url();
?>
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();	
	function checkfrmform_fields_options_form(){
		
		var form_fields_options_id = document.frmform_fields_options.form_fields_options_id;
		
		var form_fields_id = document.frmform_fields_options.form_fields_id;
		var elementmessage = document.getElementById("errmsg_form_fields_id");
		elementmessage.innerHTML = '';
		if(form_fields_id.value==0){
			elementmessage.innerHTML = 'Form Field name is mandatory';
			form_fields_id.focus();
			return false;
		}
		
		var label_name = document.frmform_fields_options.label_name;
		var elementmessage = document.getElementById("errmsg_label_name");
		elementmessage.innerHTML = '';
		if(label_name.value==''){
			elementmessage.innerHTML = 'You are missing form_fields_options name';
			label_name.focus();
			return false;
		}
		
		var label_value = document.frmform_fields_options.label_value;
		var elementmessage = document.getElementById("errmsg_label_value");
		elementmessage.innerHTML = '';
		if(label_value.value==''){
			elementmessage.innerHTML = 'You are missing form_fields_options module name';
			label_value.focus();
			return false;
		}
		return true;
	}
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	Form Field Options Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'form_fields_options/form_fields_options_list';?>" class="white14bold">Show Form Field Options List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
	<tr>
		<td align="center">
			<?php           
            $attributes = array('name' => 'frmform_fields_options', 'onsubmit'=>'return checkfrmform_fields_options_form();');
            echo form_open_multipart(site_url("/").'form_fields_options/save_form_fields_options', $attributes);
           	?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
						<td width="140" class="black11bold" align="left">Form Field Name: <font class="red14bold">*</font></td>
						<td align="left" class="black11normal">
							<select name="form_fields_id" id="form_fields_id" class="input200x20">
                                <option value="0"<?php if($form_fields_idval=='0'){echo ' selected="selected"';}?>>Select Form Field</option>
                                <?php
                                $result = $commonmodel->getallrowbysqlquery("select * from  form_fields where form_fields_publish=1 order by showing_order asc");
                                if(count($result)>0 && $result !=''){
                                    foreach($result as $row){
                                        $table_id = $row->form_fields_id;
                                        $table_field = $row->admin_label;
                                        $selected = '';
                                        if($table_id==$form_fields_idval){$selected = ' selected="selected"';}
                                        ?>
                                        <option value="<?php echo $table_id;?>" <?php echo $selected;?>><?php echo $table_field;?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>	
                            <span id="errmsg_form_fields_id" class="errormsg"></span>	
						</td>
					</tr>	
                    <tr>
                        <td class="black11bold" align="left">Label Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'label_name',
                                          'id'       => 'label_name',
                                          'tabindex'       => '2',
                                          'value'    => $label_nameval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_label_name" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="black11bold" align="left">Option Value: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'label_value',
                                          'id'       => 'label_value',
                                          'tabindex'       => '2',
                                          'value'    => $label_valueval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_label_value" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal" id="finalsaverow">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            <input type="hidden" name="form_fields_options_id" id="form_fields_options_id" value="<?php echo $form_fields_options_idval;?>" />
                        </td>
                    </tr>									
                </table>
            <?php echo form_close(); ?>
       	</td>
   	</tr>
</table>