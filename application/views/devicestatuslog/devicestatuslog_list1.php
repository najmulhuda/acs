<?php
 $functionname = 'ACS Alarm View';
 $site_url = str_replace('/index.php','',site_url("/"));
 
 $alarmlog_fetchingurl = site_url("/").'devicestatuslog/';
?>
<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">
    <link rel="stylesheet" href="<?php echo base_url("common/css/bootstrap.css"); ?>">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 well">
        <?php 
        $attr = array("class" => "form-horizontal", "role" => "form", "id" => "form1", "SitName" => "form1");
        echo form_open("devicestatuslog/search", $attr);?>
            <div class="form-group">
                <div class="col-md-6">
                    <input class="form-control" id="book_name" name="book_name" placeholder="Search for Book Name..." type="text" value="<?php echo set_value('book_name'); ?>" />
                </div>
                <div class="col-md-6">
                    <input id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="Search" />
                    <a href="<?php echo base_url(). "index.php/devicestatuslog/index"; ?>" class="btn btn-primary">Show All</a>
                </div>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2 bg-border">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>SiteName</th>
                    <th>SubcentreName</th>
                    <th>Region</th>
					<th>AlarmName</th>
					<th>Time</th>
					<th>IP</th>
                    </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($booklist); ++$i) { ?>
                <tr>
                    <td><?php echo ($page+$i+1); ?></td>
                    <td><?php echo $booklist[$i]->SiteName; ?></td>
                    <td><?php echo $booklist[$i]->SubcentreName; ?></td>
                    <td><?php echo $booklist[$i]->RegionName; ?></td>
					<td><?php echo $booklist[$i]->Status; ?></td>
					<td><?php echo $booklist[$i]->DSTime; ?></td>
					<td><?php echo $booklist[$i]->IP; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        </div>
    </div>
</div>
</body>
</html>