<?php
$site_url = str_replace('/index.php','',site_url("/"));

$moreimagefieldname = $more_image_table_name.'[]';
?>
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();

	function check_<?php echo $more_image_table_name;?>(){
		var more_imageid = document.getElementsByName('<?php echo $moreimagefieldname;?>');
		var more_image_count = more_imageid.length;
			
		var error_messageid = document.getElementById('errmsg_<?php echo $more_image_table_name;?>');
		error_messageid.innerHTML = '';
		
		for(i = 0; i < more_image_count; i++) {
															
			if(more_imageid[i].value==''){
				error_messageid.innerHTML = 'Missing More Image #'+parseInt(i+1);
				more_imageid[i].focus();
				return false;
			}
			else if(check_imagesbyval(more_imageid[i].value)==false){
				error_messageid.innerHTML = 'More Image #'+parseInt(i+1)+ ' should be only .png, .jpg and .jpeg';
				return false;
			}
		}
		return true;
	}
	
	function addnew_<?php echo $more_image_table_name;?>row(animate) {
		if(check_<?php echo $more_image_table_name;?>()==false){return false;}
		else{
			j("#errmsg_<?php echo $more_image_table_name;?>").html('');
			var more_image_rowcount = j("ul#<?php echo $more_image_table_name;?>_list").children().length+1;
			
			var newrow = "<table id=\"<?php echo $more_image_table_name;?>_listrowno"+more_image_rowcount+"\" align=\"left\" width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">"+
							"<tr>"+
								"<td align=\"left\" width=\"130\" class=\"evenrow\">Product More Image  #"+more_image_rowcount+":</td>"+
								'<td align="left" class="evenrow">'+
									"<input name=\"<?php echo $moreimagefieldname;?>\" id=\"<?php echo $moreimagefieldname;?>\" type=\"file\" tabindex=\"6\" maxlength=\"8\" class=\"input60x20\" />"+
								'</td>'+
							"</tr>"+
						"</table>";
						
			var $newmore_image_list = j("<li></li>").html(newrow);
										
			j("#<?php echo $more_image_table_name;?>_list").append($newmore_image_list.hide());
			if(animate) 
				$newmore_image_list.slideDown(500); 
			else 
			   $newmore_image_list.show();
		}
	}
</script>