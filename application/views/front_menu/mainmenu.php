<?php

$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";

if ($_SERVER["SERVER_PORT"] != "80"){

	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];

}

else{

	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

}



if ($this->uri->segment(1) === FALSE){$segment1name = '';}

else{$segment1name = $this->uri->segment(1);} 

$front_menustr = '';					

$sqlfront_menu = "select * from front_menu where front_menu_publish = 1 and front_menu_category_id = $front_menu_category_id and parent_front_menu = 0 order by showing_order";						

$queryfront_menu = $commonmodel->getallrowbysqlquery($sqlfront_menu);

if($queryfront_menu !='' && count($queryfront_menu)>0){

	$k = 1;

	$front_menustr = '<ul>';

	foreach($queryfront_menu as $rowfront_menu){

		$subfront_menu_id = $rowfront_menu->front_menu_id;

		$subfront_menu_name = $rowfront_menu->front_menu_name;

		$href = '';

		if ($rowfront_menu->front_menu_type=='URL'){

			$href = $rows->url;

		}

		else{

			$href= site_url("/").$rowfront_menu->front_menu_link;

		}

		

		$subsubfront_menustr = '';

		$sqlsubsubmenu = "select * from front_menu where front_menu_publish = 1 and parent_front_menu = $subfront_menu_id order by showing_order asc";						

		$querysubsubmenu = $commonmodel->getallrowbysqlquery($sqlsubsubmenu);

		if($querysubsubmenu !='' && count($querysubsubmenu)>0){

			$subsubfront_menustr = '<ul>';

			foreach($querysubsubmenu as $rowsubsubmenu){

				$subsubmenu_id = $rowsubsubmenu->front_menu_id;

				$subsubmenu_name = $rowsubsubmenu->front_menu_name;

				$subsubmenu_module = $rowsubsubmenu->front_menu_link;

				$submenulistlink = site_url("/").$subsubmenu_module."/";

				$subsubfront_menustr .= "<li><a href=\"$submenulistlink\">$subsubmenu_name</a></li>";

			}

			$subsubfront_menustr .= '</ul>';

		}

		$subclass = '';

		if($subsubfront_menustr !=''){$subclass = ' class="sub"';}

		

		$selectclass = '';

		if($href == "$pageURL"){

			$selectclass = 'id="current"';

		}

		$front_menustr .= "<li $selectclass>

				<a href=\"$href\"$subclass>$subfront_menu_name</a>

				$subsubfront_menustr

		</li>";

	}
	if(!$this->session->userdata('email')){

		$loginurl = site_url('/').'students/students_login/';
		$teacher_loginurl = site_url('/').'teacher/teacher_login/';			

		$selectclass = '';

		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}

		$front_menustr .= "<li $selectclass><a>Member Sign In</a>
					<ul>
						<li><a href=\"$loginurl\">As student</a></li>
						<li><a href=\"$teacher_loginurl\">As Teacher</a></li>
					</ul>					
				</li>";					

		$registrationurl = site_url('/').'students/signup';
		$selectclass = '';

		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}

		$front_menustr .= "<li $selectclass>
					<a href=\"$registrationurl\">Member Signup</a>
				</li>";				

	}
	else{
		if($this->session->userdata('students_id')){
			$loginurl = site_url('/').'students';	
			$registrationurl = site_url('/').'students/logout';			
		}
		if($this->session->userdata('teacher_id')){
			$loginurl = site_url('/').'teacher';
			$registrationurl = site_url('/').'teacher/logout';				
		}
		$selectclass = '';
		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$front_menustr .= "<li $selectclass>
					<a href=\"$loginurl\">My Account</a>
				</li>";	
		
		$selectclass = '';
		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$front_menustr .= "<li $selectclass>
					<a href=\"$registrationurl\">Log Out</a>
				</li>";
	}	 

	$front_menustr .= '</ul>';

}

		

echo $front_menustr;

?>