<ul id="cssmenu1" style="z-index:10;">
    <?php
   $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";

    if ($_SERVER["SERVER_PORT"] != "80"){

        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];

        }
    else{
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }

        if ($this->uri->segment(1) === FALSE){$segment1name = '';}

        else{$segment1name = $this->uri->segment(1);} 
  

    $query = $this->Common_model->getallrowsbyconditionwithorder('front_menu', array('front_menu_category_id' => $front_menu_category_id, 'front_menu_publish'=>1), 'showing_order', 'asc');

    $str = '';

    if(count($query)>0){
        $i=0;

        foreach($query as $rows){
            $i++;
            $href = '';
            if ($rows->front_menu_type=='URL'){
                $href = $rows->front_menu_link;
            }
            else{
                $href= site_url("/").$rows->front_menu_link;
            }           

			$targettype = '';
            if($rows->front_menu_target == "_blank"){
                $targettype = ' target="_blank"';
            }            

			$selectclass = '';

            if($href == "$pageURL"){
                $selectclass = ' class="current"';
            }

            if(site_url("/") == "$pageURL" && $rows->front_menu_link=='home'){$selectclass = 'class="current"';}

            $front_menu_name = $rows->front_menu_name;

            $lastclass = '';

            if($i==count($query)){
                $lastclass = ' class="last"';
            }

            $str .= "<li $selectclass>
                        <a$targettype href=\"$href\">$front_menu_name</a>
                    </li>"; 
        }

    } 
//echo "<pre>";print_r($this->session->userdata);echo "</pre>";	

	if(!$this->session->userdata('email')){

		$loginurl = site_url('/').'students/students_login/';
		$teacher_loginurl = site_url('/').'teacher/teacher_login/';			

		$selectclass = '';

		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		if($front_menu_category_id==2){
			$signin_menu = '<a href="'.$loginurl.'">Member Sign In</a>';
		}else {$signin_menu = '<a>Member Sign In</a>';}
		$str .= "<li $selectclass>$signin_menu
					<ul>
						<li><a href=\"$loginurl\">As student</a></li>
						<li><a href=\"$teacher_loginurl\">As Teacher</a></li>
					</ul>					
				</li>";					

		$registrationurl = site_url('/').'students/signup';
		$selectclass = '';

		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}

		$str .= "<li $selectclass>
					<a href=\"$registrationurl\">Member Signup</a>
				</li>";				

	}
	else{
		if($this->session->userdata('students_id')){
			$loginurl = site_url('/').'students';	
			$registrationurl = site_url('/').'students/logout';			
		}
		if($this->session->userdata('teacher_id')){
			$loginurl = site_url('/').'teacher';
			$registrationurl = site_url('/').'teacher/logout';				
		}
		$selectclass = '';
		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$loginurl\">My Account</a>
				</li>";	
		
		$selectclass = '';
		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$registrationurl\">Log Out</a>
				</li>";
	}
	echo $str;
    ?>
</ul>