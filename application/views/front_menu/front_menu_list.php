<?php
$site_url = str_replace('/index.php','',site_url("/"));

if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}
$functionname = 'Front Menu';

$front_menu_totalrows = $commonmodel->getcount_all_resultsrows('front_menu', '', 'modified_date');
$front_menu_fatchingurl = site_url("/").'front_menu/fetching_front_menu_list/';
?>
<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $front_menu_totalrows;?>" />
<style type="text/css">
.pagination {font-size: 80%; width:30%; float:left; margin-bottom:10px;}
.pagination a {text-decoration: none;border: solid 1px #AAE;color: #F7FFF3;}
.pagination a, .pagination span {display: block;float: left;padding: 0.3em 0.5em;margin-right: 5px;margin-bottom: 5px; min-width:16px; 
	text-align:center;}
.pagination .current {background: #26B;color: #fff;border: solid 1px #AAE;}
.pagination .current.prev, .pagination .current.next{color:#999;border-color:#999;background:#fff;}
</style>

<script type="text/javascript">

   	var j = jQuery.noConflict();
   
   	var front_menufetchurl = '<?php echo $front_menu_fatchingurl;?>';
	var firstval = 1;
		

   function pageselectCallback(page_index, jq){
		var front_menu_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var show_by_order=document.getElementById('show_by_order').value;
		var starting_val = parseInt(page_index*front_menu_per_page);
		var searching_field=document.getElementById('searching_field').value;
		
		j.post(front_menufetchurl, {"show_by_order":show_by_order, "searching_field":searching_field, "starting_val": starting_val, "front_menu_per_page": front_menu_per_page},
		function(data) {
			j('#Searchresult').html(data);			
		});
		
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		var optInit = getOptionsFromForm(); // Create pagination element with options from form
		//alert(optInit);
		var totalrows_count = document.getElementById('totalrowscount').value;
		var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit,data_per_page);
	}
	
	function seachingwithpagination(){
		var searching_field=document.getElementById('searching_field').value;
		j.post('<?php echo site_url("/").'front_menu/fetching_front_menu_listcountwithsearch/';?>', {"searching_field":searching_field},
		function(data) {
			//alert(data);
			document.getElementById('totalrowscount').value = data;	
			loadpagination();
		});
	}
	function show_by_orderlist(show_by_orderval){
		//alert(show_by_orderval);
		document.getElementById('show_by_order').value = show_by_orderval;
		loadpagination();        
	}
	j(document).ready(function(){
		loadpagination();
	});
	
</script>

<!--<script src="<?php //echo $site_url?>common/js/topPagination.js" type="text/javascript" language="javascript"></script>-->
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/toppagination.css" type="text/css" />
<script src="<?php echo $site_url?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>


<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%">
                	All <?php echo $functionname; ?> List
                </td>
                
                <td width="50%" align="left">
                	<div style="width:540px; float:left; overflow:hidden;">
						<style type="text/css">
                            .pagination a {border: 1px solid #f7fff3;color: #F7FFF3;text-decoration: none;}
                            .pagination .current {background:#2266BB;border: 1px solid #f7fff3;color: #FFFFFF;}
                        </style>
                    <div id="Pagination" class="pagination"></div>
                   	<div style="float:left; margin-top:2px;">
                        <div style="float:left; width:50px; line-height:22px;">Limits:</div>
                        <div style="float:left; width:130px; height:22px;">
                        <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20" selected="selected">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <select id="show_by_order" class="searchselect80x20" name="show_by_order" onchange="seachingwithpagination();">
                            <option value="front_menu_category_id asc">Category Wise</option>
                            <option value="showing_order asc">Showing Order ASC</option>
                            <option value="showing_order desc">Showing Order DESC</option>
                            <option value="created_date desc">New First</option>
                            <option value="created_date asc">Old First</option>
                            <option value="front_menu_name asc">Name ASC</option>
                            <option value="front_menu_name desc">Name DESC</option>
                        </select>
                        <input type="hidden" name="show_by_order" id="show_by_order" value="" />
                        </div>
                  	</div>
                    <div style="width:185px; float:left; margin-left:10px;">
                        <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" />
                        <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
                    </div>
                    <?php //echo $front_menu_totalrows;?>
                </td>
                
                <td align="right">
                	<a href="<?php echo site_url("/").'front_menu/front_menu_form';?>" class="white14bold">Add New Front Menu</a>
                </td>
              </tr>
            </table>			
		</td>
	</tr>    
	<tr>
		<td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="50%" align="left" style="padding-left:10px;" class="errormessage">
                	<?php 
					if ($this->uri->segment(3) === FALSE){
						$msg = '';
					}
					else{
						$msg = $this->uri->segment(3);
					}
					if(isset($msg) && $msg !='' ){
						if($msg=='add_success')
							echo "<div class=red18bold>Front Menu added successfully</div>";
						elseif($msg=='update_success')
							echo "<div class=red18bold>Front Menu updated successfully</div>";							
					}
					?>
                </td>
              </tr>
            <tr>
                <td align="center">
                	<div id="Pagination"></div>
                </td>
            </tr>
    
            <tr>
            	<td align="center" id="Searchresult">&nbsp;</td>
            </tr>
            </table>
		</td>
	</tr>
    

</table>
