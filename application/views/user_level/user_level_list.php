<?php
$site_url = str_replace('/index.php','',site_url("/"));

if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}
$functionname = 'User Level';
$user_leveltotalrows = $commonmodel->getcount_all_resultsrows('user_level', '', 'modified_date');
$user_level_fatchingurl = site_url("/").'user_level/fetchinguser_level_list/';
?>
<style type="text/css">
.pagination {font-size: 80%; width:100%; float:right; margin-bottom:10px;}
.pagination a {text-decoration: none;border: solid 1px #AAE;color: #15B;}
.pagination a, .pagination span {display: block;float: left;padding: 0.3em 0.5em;margin-right: 5px;margin-bottom: 5px; min-width:16px; 
	text-align:center;}
.pagination .current {background: #26B;color: #fff;border: solid 1px #AAE;}
.pagination .current.prev, .pagination .current.next{color:#999;border-color:#999;background:#fff;}
</style>

<script type="text/javascript">
   var j = jQuery.noConflict();
   var user_levelfetchurl = '<?php echo $user_level_fatchingurl;?>';
   function pageselectCallback(page_index, jq){
		var publishval = 1;
		var user_level_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var show_by_order=document.getElementById('show_by_order').value;
		var starting_val = parseInt(page_index*user_level_per_page);
		j.post(user_levelfetchurl, {publishval:publishval, show_by_order:show_by_order, starting_val: starting_val, user_level_per_page: user_level_per_page},
	   	function(data) {
			
			j('#Searchresult').html(data);
	   	});
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		var optInit = getOptionsFromForm(); // Create pagination element with options from form
		var totalrows_count = '<?php echo $user_leveltotalrows;?>';
		
		var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit,data_per_page);
	}

	function show_by_orderlist(show_by_orderval){
		//alert(show_by_orderval);
		document.getElementById('show_by_order').value = show_by_orderval;
		loadpagination();        
	}
	j(document).ready(function(){               
		loadpagination();
	});
</script>
<script src="<?php echo $site_url?>common/pagination/topPagination.js" type="text/javascript" language="javascript"></script>
<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20%">
                	All <?php echo $functionname; ?> List
                </td>
                <td width="55%">
                	<div style="width:400px; float:left; height:22px; overflow:hidden;">
                    	<style type="text/css">
							.pagination a {border: 1px solid #f7fff3;color: #1155BB;text-decoration: none;}
							.pagination .current {background:#2266BB;border: 1px solid #f7fff3;color: #FFFFFF;}
						</style>
                    	<div id="Pagination" class="pagination"></div>
                    </div>
                   	<div style="width:90px; float:left;">
                        <div style="float:left; width:50px; line-height:22px;">Limits:</div>
                        <div style="float:left; width:40px; height:22px;">
                        <select id="list_per_page" class="pagination_tr_middleselect" name="list_per_page" onchange="loadpagination();">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                        </select>
                        <input type="hidden" name="show_by_order" id="show_by_order" value="showing_order asc" />
                        </div>
                  	</div>
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'user_level/user_level_form';?>" class="white14bold">Add New User Level</a>
                </td>
              </tr>
            </table>			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			if ($this->uri->segment(3) === FALSE){
				$msg = '';
			}
			else{
				$msg = $this->uri->segment(3);
			}
			if(isset($msg) && $msg !='' ){
				if($msg=='add_success')
					echo "<div class=red18bold>User Level added successfully</div>";
				elseif($msg=='update_success')
					echo "<div class=red18bold>User Level updated successfully</div>";							
			}
			?>
		</td>
	</tr>
	<tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
	<tr>
		<td align="center" id="Searchresult">&nbsp;</td>
	</tr>
</table>