<?php
if(isset($user_level_name['value']) && $user_level_name['value'] !=''){ $user_level_nameval = $user_level_name['value'];}
else{ $user_level_nameval = set_value('user_level_name');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($user_level_id['value']) && $user_level_id['value'] !=''){ $user_level_idval = $user_level_id['value'];}
else{ $user_level_idval =set_value('user_level_id');}
if($user_level_idval==''){$user_level_idval = 0;}
?>

<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();
	function checkfrmuser_level_form(){
		var user_level_name = document.frmuser_level.user_level_name;
		var elementmessage = document.getElementById("errmsg_user_level_name");
		elementmessage.innerHTML = '';
		j('#user_level_name').removeClass('field400_err').addClass('field400');
		if(user_level_name.value==''){
			elementmessage.innerHTML = 'You are missing user level name';
			user_level_name.focus();
			j('#user_level_name').removeClass('field400').addClass('field400_err');
			return false;
		}
		return true;
	}
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	User level Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'user_level/user_level_list';?>" class="white14bold">Show User Level List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmuser_level', 'onsubmit'=>'return checkfrmuser_level_form();');
            echo form_open_multipart(site_url("/").'user_level/save_user_level', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
                        <td class="black11bold" width="150" align="left">User Level Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'user_level_name',
                                          'id'       => 'user_level_name',
                                          'tabindex' => '1',
                                          'value'    => $user_level_nameval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_user_level_name" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            <input type="hidden" name="user_level_id" id="user_level_id" value="<?php echo $user_level_idval;?>" />
                        </td>
                    </tr>								
                </table>
            <?php echo form_close(); ?>
		</td>
	</tr>
</table>




