<?php
if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}
$functionname = 'User';

$site_url = str_replace('/index.php','',site_url("/"));

?>
<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">
<script language="JavaScript" type="text/javascript">
	j=jQuery.noConflict();
	j(document).ready(function($) {
		initPagination();
	});
	 function pageselectCallback(page_index, jq){
		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();
		j('#Searchresult').empty().append(new_content);
		return false;
	}
	function initPagination() {
		
		// count entries inside the hidden content
		var num_entries = j('#hiddenresult div.result').length;
		// Create content inside pagination element
		j("#Pagination").pagination(num_entries, {
			callback: pageselectCallback,
			items_per_page:1 // Show only one item per page
		});
	 }  
	
</script>
<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	All <?php echo $functionname; ?> List
                </td>
                <td align="right">
					<?php
					if($this->session->userdata('create_user')>0){
					?>
					<a href="<?php echo site_url("/").'admin/user_form/';?>" class="white14bold">Add New User</a>
					<?php
					}
					?>
                </td>
              </tr>
            </table>			
		</td>
	</tr>
	<tr>
		<td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="50%" align="left" style="padding-left:10px;" class="errormessage">
                	<?php 
					if ($this->uri->segment(3) === FALSE){
						$msg = '';
					}
					else{
						$msg = $this->uri->segment(3);
					}
					if(isset($msg) && $msg !='' ){
						if($msg=='add_success')
							echo "<div class=red18bold>User added successfully</div>";
						elseif($msg=='update_success')
							echo "<div class=red18bold>User updated successfully</div>";							
					}
					?>
                </td>
                <td align="right">
                	<div id="Pagination"></div>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center">			
			<?php
			if(isset($user_list) && count($user_list)>0){
			?>	
				<div style="width:99%; overflow:hidden;">				
					<div id="Searchresult">
						This content will be replaced when pagination inits.
					</div>
					<div style="padding:0 10px 0 20px;" class="black12bold">Total <?php echo count($user_list);?> data or <?php echo ceil(count($user_list)/10);?> pages found </div>
				</div>
				
				<div id="hiddenresult" style="display:none;">
					<div class="result">
						<table width="100%" cellpadding="0" cellspacing="1" border="0">
							<tr>
								<th class="titlerow" align="center" width="5%">#</th>
								<th class="titlerow" align="left" width="20%">User Name</th>
								<th class="titlerow" align="left" width="15%">User Type</th>
								<th class="titlerow" align="left" width="20%">Email</th>
								<th class="titlerow" align="left" width="10%">Contact No</th>
								<th class="titlerow" align="center" width="10%">Last Login</th>
								<th class="titlerow" align="center" width="10%">Action</th>													
							</tr>
							<?php
							$str = "";
							$i = 1;
							foreach($user_list as $row){
								
								if($i%2==0)	$classname = 'evenrow';
								else		$classname = 'oddrow';
								
								$login_by = $this->session->userdata('user_id');
								$user_id1 = $row->user_id;
								$user_name1 = $row->user_name;
								$user_password = $row->user_password;
								$user_fullname = $row->user_fullname;
								$user_email = $row->user_email;
								$user_contactno = $row->user_contactno;
								$user_gender = $row->user_gender;
								$user_dateofbirth = $row->user_dateofbirth;
								$user_level_id = $row->user_level_id;
								if($user_level_id>0){
									$user_level_name =  $this->Common_model->get_onefieldnamebyid('user_level', 'user_level_id', $user_level_id, 'user_level_name');									
								}
								else{
									$user_level_name = '';
								}	
								
								$user_onlinestatus = $row->user_onlinestatus;
								$user_lastlogin = date('Y-m-d H:i:s',$row->user_lastlogin);
								
								if($user_onlinestatus==1){ $user_onlinestatus = 'Online';}
								else{ $user_onlinestatus = 'Offline';}
								
								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';
								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';
								
								$user_published = $row->user_published;
								$unpublished = str_replace('/index.php','',site_url("/")).'common/pagination/images/published.png';
								$publishedimg = str_replace('/index.php','',site_url("/")).'common/images/minus20x21.png';		
								if($user_published>0){
									$published = "<a href=\"".site_url("/")."admin/update_onetablefield/user/user_id/$user_id1/user_published/0/admin/user_list\"><img src=\"$publishedimg\" border=\"0\" alt=\"Unpublished\"></a>";
								}
								else{
									$published = "<a href=\"".site_url("/")."admin/update_onetablefield/user/user_id/$user_id1/user_published/1/admin/user_list\"><img src=\"$unpublished\" border=\"0\" alt=\"Published\"></a>";
								}
								$user_fullname1 = $user_fullname;
								if($user_id1==$this->session->userdata('user_id')){
									$user_fullname1 = "<a href=\"".site_url("/")."admin/edit_user/\">$user_fullname</a>";
								}
								elseif($user_id1 !=1 && $login_by != $user_id1){
									$user_fullname1 = "<a href=\"".site_url("/")."admin/edit_user/$user_id1/\">$user_fullname</a>";
								}
								
									
								$str .= "<tr>
									<td align=\"center\" class=\"$classname\">$i</td>
									<td align=\"left\" class=\"$classname\">$user_fullname1</td>
									<td align=\"left\" class=\"$classname\">$user_level_name</td>
									<td align=\"left\" class=\"$classname\">$user_email</td>
									<td align=\"left\" class=\"$classname\">$user_contactno</td>
									<td align=\"center\" class=\"$classname\">$user_lastlogin</td>";
								
									$str .= "<td class=\"$classname\" align=\"center\">";
									
									if($user_id1 !=1 && $login_by != $user_id1){
										$str .= "$published
												<a onclick=\"tablerow_remove('user', 'user_id', '$user_id1', '$user_fullname', 'admin', 'user_list');\" title=\"Remove\">
												<img src=\"$removeimg\" border=\"0\" alt=\"Remove\">
											</a>";
									}
											
									$str .= "</td>";
								
								
								$str .= "</tr>";
								
								if($i%10==0 && count($user_list)>$i){
									$str .= '</table></div>
									<div class="result">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<th class="titlerow" align="center" width="5%">#</th>
											<th class="titlerow" align="left" width="20%">User Name</th>
											<th class="titlerow" align="left" width="15%">User Type</th>
											<th class="titlerow" align="left" width="20%">Email</th>
											<th class="titlerow" align="left" width="10%">Contact No</th>
											<th class="titlerow" align="center" width="10%">Last Login</th>
											<th class="titlerow" align="center" width="10%">Action</th>
										</tr>
									';
								}		
								$i++;
							}
							echo $str;
						?>
						</table>
						</div>
					</div>
			<?php
			}
			else{
				echo "<div class=red18bold>There are currently no site visitor found</div>";					
			}								
			?>
		</td>
	</tr>
</table>