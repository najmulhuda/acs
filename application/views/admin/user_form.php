<?php
if(isset($user_id['value']) && $user_id['value'] !=''){ $user_idval = $user_id['value'];}
else{ $user_idval = set_value('user_id');}
if($user_idval==''){$user_idval = 0;}

if(isset($user_level_id['value']) && $user_level_id['value'] !=''){ $user_level_idval = $user_level_id['value'];}
else{ $user_level_idval = set_value('user_level_id');}

if(isset($user_name['value']) && $user_name['value'] !=''){ $user_nameval = $user_name['value'];}
else{ $user_nameval = set_value('user_name');}
	
if(isset($user_password['value']) && $user_password['value'] !=''){ $user_passwordval = $user_password['value'];}
else{ $user_passwordval = set_value('user_password');}
	
if(isset($conf_user_password['value']) && $conf_user_password['value'] !=''){ $conf_user_passwordval = $conf_user_password['value'];}
else{ $conf_user_passwordval = set_value('conf_user_password');}

if(isset($user_fullname['value']) && $user_fullname['value'] !=''){ $user_fullnameval = $user_fullname['value'];}
else{ $user_fullnameval = set_value('user_fullname');}

if(isset($user_email['value']) && $user_email['value'] !=''){ $user_emailval = $user_email['value'];}
else{ $user_emailval = set_value('user_email');}

if(isset($user_contactno['value']) && $user_contactno['value'] !=''){ $user_contactnoval = $user_contactno['value'];}
else{ $user_contactnoval = set_value('user_contactno');}

if(isset($user_gender['value']) && $user_gender['value'] !=''){ 
	$user_genderval = $user_gender['value'];
	if($user_genderval=='Male'){
		$user_gendermaleval = set_radio('user_gender', 'Male', true);
		$user_genderfemaleval = set_radio('user_gender', 'Female', false);			
	}
	else{
		$user_gendermaleval = set_radio('user_gender', 'Male', false);
		$user_genderfemaleval = set_radio('user_gender', 'Female', true);
	}
}
else{ 
	$user_gendermaleval = set_radio('user_gender', 'Male', true);
	$user_genderfemaleval = set_radio('user_gender', 'Female', false);		
}
	
if(isset($user_dateofbirth['value']) && $user_dateofbirth['value'] !=''){ $user_dateofbirthval = $user_dateofbirth['value'];}	
else{ 
	if(!set_value('user_dateofbirth')){$user_dateofbirthval = date('Y-m-d',mktime(0,0,0,date('m'),date('d'),date('Y')-25));}
	else{$user_dateofbirthval = set_value('user_dateofbirth');}
}

if(isset($user_level_id['value']) && $user_level_id['value'] !=''){ $user_level_idval = $user_level_id['value'];}
else{$user_level_idval = set_select('user_level_id');}
if($user_level_idval==''){$user_level_idval=0;}

if(isset($user_published['value']) && $user_published['value'] !=''){ 
	$user_publishedval = $user_published['value'];
	if($user_publishedval>0){
		$user_publishedval = set_checkbox('user_published', '1', true);
	}
	else{
		$user_publishedval = set_checkbox('user_published', '1', false);
	}
}
else{ $user_publishedval = set_checkbox('user_published', '1', true);}

if(isset($create_user['value']) && $create_user['value'] !=''){ 
	$create_userval = $create_user['value'];
	$create_usercheck = '';
	if($create_userval>0){
		$create_usercheck = ' checked="checked"';
	}
}
else{ $create_usercheck = '';
$create_userval =1;
}
	
if(isset($created_by['value']) && $created_by['value'] !=''){ $created_byval = $created_by['value'];}
else{ $created_byval = set_value('created_by');}
if($created_byval==''){$created_byval = 0;}
		
if(isset($action_type['value']) && $action_type['value'] !=''){	$action_typeval = $action_type['value'];}
else{ $action_typeval = set_value('action_type');}
if($action_typeval==''){$action_typeval = 0;}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Add New User ';}				
			
$site_url = str_replace('/index.php','',site_url("/"));
$userformattributes = array('name' => 'frmadmin_registration', 'onsubmit'=>'return checkfrmadmin_registration();');
echo form_open(site_url("/").'admin/save_user_form/', $userformattributes);
?>
<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />
<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	Admin Panel New User Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'admin/user_list';?>" class="white14bold">Show User List</a>
                </td>
              </tr>
            </table>
			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="right" width="30%" class="black12bold">User Name <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'user_name',
									  'id'       => 'user_name',
									  'tabindex'       => '1',
									  'value'    => $user_nameval,
									  'maxlength'=> '100',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_input($data);
						?>
						<span id="errmsg_user_name" class="red11normal"></span>
					</td>
				</tr>
				<tr>
					<td align="right" class="black12bold">Password <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'user_password',
									  'id'       => 'user_password',
									  'tabindex'       => '2',
									  'value'    => $user_passwordval,
									  'maxlength'=> '100',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_password($data);
						?>
						<span id="errmsg_user_password" class="red11normal"></span>
					</td>
				</tr>
				<tr>
					<td align="right" class="black12bold">Confirm Password <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'conf_user_password',
									  'id'       => 'conf_user_password',
									  'tabindex'       => '3',
									  'value'    => $conf_user_passwordval,
									  'maxlength'=> '100',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_password($data);
						?>
						<span id="errmsg_conf_user_password" class="red11normal"></span>
					</td>
				</tr>
				<tr>
					<td align="right" class="black12bold">Full Name <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'user_fullname',
									  'id'       => 'user_fullname',
									  'tabindex'       => '4',
									  'value'    => $user_fullnameval,
									  'maxlength'=> '250',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_input($data);
						?>
						<span id="errmsg_user_fullname" class="red11normal"></span>
					</td>
				</tr>												
				<tr>
					<td align="right" class="black12bold">Email <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'user_email',
									  'id'       => 'user_email',
									  'tabindex'       => '5',
									  'value'    => $user_emailval,
									  'maxlength'=> '150',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_input($data);
						?>
						<span id="errmsg_user_email" class="red11normal"></span>
					</td>
				</tr>												
				<tr>
					<td align="right" class="black12bold">Contact Number:</td>
					<td align="left">
						<?php 
						$data = array('name'     => 'user_contactno',
									  'id'       => 'user_contactno',
									  'tabindex'       => '6',
									  'value'    => $user_contactnoval,
									  'maxlength'=> '20',
									  'size'    => '50',
									  'class'    => 'input200x20'
									);															
						echo form_input($data);
						?>
						<span id="errmsg_user_contactno" class="red11normal"></span>
					</td>
				</tr>												
				<tr>
					<td align="right" class="black12bold">Gender:</td>
					<td align="left">
						<input tabindex="7" type="radio" name="user_gender" value="Male" <?php echo $user_gendermaleval; ?> /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input tabindex="8" type="radio" name="user_gender" value="Female" <?php echo $user_genderfemaleval; ?> /> Female 
					</td>
				</tr>											
				<tr>
					<td align="right" class="black12bold">Date of Birth <span class="red12bold">*</span>:</td>
					<td align="left">
						<?php						
						$data = array('name'     => 'user_dateofbirth',
									  'id'       => 'user_dateofbirth',
									  'tabindex'       => '9',
									  'value'    => $user_dateofbirthval,
									  'readonly'=> 'readonly',								  
									  'maxlength'=> '20',								  
									  'size'    => '40',
									  'class'    => 'input120x20'
									);															
						echo form_input($data);
						?>
						<input tabindex="9" type="button" class="calendarbut" value="" onClick="displayCalendar(document.frmadmin_registration.user_dateofbirth,'yyyy-mm-dd',this);">
					</td>
				</tr>
                <?php
                if($user_idval>0 && $this->session->userdata('user_id') !=1){
					echo "<input tabindex=\"10\" type=\"hidden\" id=\"user_level_id\" name=\"user_level_id\" value=\"$user_level_idval\" />";
				}
				else{?>
                    <tr>
                        <td align="right" class="black12bold">User Level Type:</td>
                        <td align="left">
                            <select tabindex="10" name="user_level_id" id="user_level_id" class="input200x20">
                                <option value="0">Select User Level</option>
                                <?php
                                $conditionarray = array('user_level_publish'=>1);
                                
                                echo $commonmodel->get_onetableallfieldvalueforselect('user_level', 'user_level_id', $user_level_idval, 'user_level_name', $conditionarray);
                                ?>
                            </select>
                            <span id="errmsg_user_level_id" class="red11normal"></span>
                        </td>
                    </tr>				
				<?php
				}?>
				<tr>
					<td align="right" class="black12bold">Published:</td>
					<td align="left">
						<input tabindex="11" type="checkbox" name="user_published" value="1" <?php echo $user_publishedval; ?> />
					</td>
				</tr>
				<?php 
				if($this->session->userdata('user_level_id') =='ADMINISTRATOR' || ($created_byval != $user_idval && $create_userval==1)){
				?>							
				<tr>
					<td align="right" class="black12bold">User Create:</td>
					<td align="left">
						<input tabindex="12" type="checkbox" name="create_user" value="1" <?php echo $create_usercheck; ?> />
					</td>
				</tr>							
				<?php
				}
				else{
					echo "<input tabindex=\"12\" type=\"hidden\" name=\"create_user\" value=\"$create_userval\" />";
				}
				?>
				<tr>
					<td align="right" class="black12bold">&nbsp;</td>
					<td align="left">
						<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_idval;?>" />
						<input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />
						<?php 
						$data = array( 'name'    => 'submit',
										'value'    => $submitval,
										'tabindex'       => '13',
									  'class'    => 'submitbutton');															
						echo form_submit($data);
						$data = array( 'name'    => 'reset',
										'value'    => ' Reset ',
										'tabindex'       => '14',
									  'class'    => 'submitbutton');															
						echo '&nbsp;&nbsp;'.form_reset($data);
						?>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>						
			</table>
		</td>
	</tr>
</table>
<?php echo form_close(); ?>