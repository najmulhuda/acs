<?php

if ($this->uri->segment(3) === FALSE){
    $segment3name = '';
}
else{
   $segment3name = $this->uri->segment(3);
}

$site_url = str_replace('/index.php','',site_url("/"));

if($segment3name =='forgot_pass' || $segment3name == 'forgotpass' || $segment3name == 'forgot_successmail'){
	?>
  	<table id="blueborder" width="60%" align="center" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td id="bluebgtd">
                Forgot Password Information:
            </td>
        </tr>
        <tr>
            <td align="left">  
                <?php
                $forgotpassattributes = array('name' => 'frmforgot_pass', 'onsubmit'=>'return check_frmforgot_pass();');
                echo form_open(site_url("/").'admin/forgotpass', $forgotpassattributes);
                ?>
                <table align="center" width="100%" border="0" cellspacing="5" cellpadding="0">			
                    <tr>
                        <td align="center">					
                            <?php
                            if($segment3name == 'forgot_successmail'){
							?>
                             <table width="100%" border="0" cellspacing="12" cellpadding="0">
                             <tr>
                                <td align="left" class="black12normal">
                                	We have sent a mail with login information to your email address. Please check your mail then try to login by given username and password.<br />
									<br />
									 <a href="<?php echo site_url("/");?>admin/" class="blue11anchor">Click Here</a> for login
                                </td>
                               </tr>
                              </table>
                            <?php	
							}
							else{
							?>
                            <table width="100%" border="0" cellspacing="12" cellpadding="0">
                             <tr>
                                <td width="20%" class="black12bold" align="left">Email *:</td>
                                <td width="80%" class="black12normal" align="left">
                                    <?php 
                                    $data = array('name'     => 'fuser_email',
                                                  'id'       => 'fuser_email',
                                                  'tabindex'       => '111',
                                                  'value'    => '',
                                                  'class'=> 'input200x20',
                                                  'maxlength'=> '150',
                                                  'size'    => '50'
                                                );															
                                    echo form_input($data);
                                    ?>
                                    <span id="error_fuser_email" class="red12bold"></span>
                                </td>
                              </tr>
                              <tr>
                                <td class="black12bold"></td>
                                <td class="black12normal">&nbsp;
                                    
                                </td>
                              </tr>
                              <tr>
                                <td class="black12bold"></td>
                                <td class="black12normal" align="left">
                                    <?php 
									$data = array( 'name'    => 'forgotsubmit',
													'id'    => 'forgotsubmit',
													'value'    => ' Send ',
													'tabindex'       => '112',
													'class'    => 'submitbutton');															
									echo form_submit($data);
									$data = array( 'name'    => 'reset',
													'value'    => ' Reset ',
													'tabindex'       => '113',
												  'class'    => 'submitbutton');															
									echo '&nbsp;&nbsp;'.form_reset($data);
									?>
                                </td>
                              </tr>
                            </table>
                            <?php
							}?>
                        </td>
                    </tr>
                </table>
            <?php
            echo form_close();
            ?>
        	</td>
        </tr>
	</table>    
<?php
}
else if(!$this->session->userdata('user_id')){
	if(set_value('user_name')){ $user_nameval =set_value('user_name');}
	else{ $user_nameval = '';}
	
	if(set_value('user_password')){ $user_passwordval = set_value('user_password');}
	else{ $user_passwordval = '';}

	$attributes = array('name' => 'frmuser_login', 'onsubmit'=>'return checkfrmuser_login();');
	echo form_open(site_url("/").'admin/user_login', $attributes);
	?>
		<table id="blueborder" width="60%" align="center" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td id="bluebgtd">
					User Login Information
				</td>
			</tr>
			<tr>
				<td align="left">
					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td align="left" colspan="2">
								&nbsp;
								<?php 
									echo validation_errors();
									if(isset($err_message) && $err_message!=''){
										echo $err_message;
									}
								?>
							</td>
						</tr>
						<tr>
							<td align="right" width="30%" class="black12bold">User&nbsp;Name:</td>
							<td align="left">
								<?php 
								$data = array('name'     => 'user_name',
											  'id'       => 'user_name',
											  'tabindex'       => '1',
											  'value'    => $user_nameval,
											  'class'=> 'input200x20',
											  'maxlength'=> '100',
											  'size'    => '50'
											);															
								echo form_input($data);
								?>
								<span id="errmsg_user_name" class="red11normal"></span>
							</td>
						</tr>
						<tr>
							<td align="right" class="black12bold">Password:</td>
							<td align="left">
								<?php 
								$data = array('name'     => 'user_password',
											'id'       => 'user_password',
											'tabindex'       => '2',
											'value'    => $user_passwordval,
											'class'=> 'input200x20',
											'maxlength'=> '100',
											'size'    => '50'
											);															
								echo form_password($data);
								?>
								<span id="errmsg_user_password" class="red11normal"></span>
							</td>
						</tr>						
						<tr>
							<td align="right" class="black12normal">&nbsp;</td>
							<td align="left">
								<?php 
								$data = array( 'name'    => 'submit',
												'value'    => ' Login ',
												'value'    => ' Login ',
												'tabindex'       => '4',
											 	'class'    => 'submitbutton');															
								echo form_submit($data);
								$data = array( 'name'    => 'reset',
												'value'    => ' Reset ',
												'tabindex'       => '5',
											  'class'    => 'submitbutton');															
								echo '&nbsp;&nbsp;'.form_reset($data);
								?>
							</td>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td align="right">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" align="center" class="black12normal">Forgot your password? <a href="<?php echo site_url("/");?>admin/index/forgot_pass" class="blue11anchor">Click Here</a></td>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td align="right">&nbsp;</td>
						</tr>						
					</table>
				</td>
			</tr>
		</table>
	<?php 
	echo form_close(); 								
}
else{
	$user_id = $this->session->userdata('user_id');
	$user_name = $this->session->userdata('user_name');
	$user_fullname = $this->session->userdata('user_fullname');
	?>
		<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
		<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">
		<script language="JavaScript" type="text/javascript">
			j=jQuery.noConflict();
			j(document).ready(function($) {
				initPagination();
			});
			 function pageselectCallback(page_index, jq){
				var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();
				j('#Searchresult').empty().append(new_content);
				return false;
			}
			function initPagination() {
				
				// count entries inside the hidden content
				var num_entries = j('#hiddenresult div.result').length;
				// Create content inside pagination element
				j("#Pagination").pagination(num_entries, {
					callback: pageselectCallback,
					items_per_page:1 // Show only one item per page
				});
			 }  
			
		</script>
		
		<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td id="bluebgtd">
					Last 100 visitor list
				</td>
			</tr>
			<tr>
				<td align="center">
					<div style="width:99%; overflow:hidden;">					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">						
						<tr>
							<td colspan="3" style="padding:2px 0px 0px 10px; text-align:center;">

								<div id="Pagination"></div>
							</td>
						</tr>
					</table>
				</div>
				
				<div style="width:99%; overflow:hidden;">				
					<div id="Searchresult">
						This content will be replaced when pagination inits.
					</div>
					<div style="padding:0 10px 0 20px;" class="black12bold">Total <?php echo count($queryrows);?> data or <?php echo ceil(count($queryrows)/20);?> pages found </div>
				</div>
				
					<?php
					if(isset($queryrows) && count($queryrows)>0){
					?>
					
						
						<div id="hiddenresult" style="display:none;">
							<div class="result">
								<table width="100%" cellpadding="0" cellspacing="1" border="0">
									<tr>
										<th align="center" class="titlerow" width="10%">#</th>
										<th align="left" class="titlerow" width="40%">User IP Address</th>
										<th align="left" class="titlerow" width="40%">Last Activity</th>
										<th align="center" class="titlerow" width="10%">Remove</th>															
									</tr>
									<?php
									$str = "";
									$i = 1;
									foreach($queryrows as $row){
										
										$session_id = $row->session_id;
										$ip_address = $row->ip_address;
										$user_agent = $row->user_agent;
										$last_activity = date('Y M d D h:i:s',$row->last_activity);
										$user_data = $row->user_data;																						
										$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';
										if($i%2==0)	$classname = 'evenrow';
										else		$classname = 'oddrow';
											
										$str .= "<tr>
											<td align=\"center\" class=\"$classname\">$i</td>
											<td align=\"left\" class=\"$classname\">$ip_address</td>
											<td align=\"left\" class=\"$classname\">$last_activity</td>";
										$returnpage = 'index';
										if($this->session->userdata('user_level_id')==1){
											$str .= "<td align=\"center\" class=\"$classname\"><a onclick=\"tablerow_remove('ci_sessions', 'session_id', '$session_id', '$ip_address', 'admin', 'index');\">
														<img src=\"$removeimg\" border=\"0\" alt=\" Remove page_info \">
													</a></td>";
										}
										
										$str .= "</tr>";
										
										if($i%10==0 && count($queryrows)>$i){
											$str .= '</table></div>
											<div class="result">
											<table width="100%" cellpadding="0" cellspacing="0" border="0">
												<tr>
													<th align="center" class="titlerow" width="10%">#</th>
													<th align="left" class="titlerow" width="40%">Usre IP Address</th>
													<th align="left" class="titlerow" width="40%">Last Activity</th>
													<th align="center" class="titlerow" width="10%">Remove</th>																
												</tr>
											';
										}		
										$i++;
									}
									echo $str;
								?>
								</table>
								</div>
							</div>
					<?php
					}
					else{
						echo "<div class=red18bold>There are currently no site visitor found</div>";					
					}								
					?>
				</td>
			</tr>
		</table>
	<?php 
}
?>