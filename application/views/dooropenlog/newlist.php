<?php
 $functionname = 'Door Open Log';
 $site_url = str_replace('/index.php','',site_url("/"));
 
 $alarmlog_fetchingurl = site_url("/").'devicestatuslog/';
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">

<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $data['Count']; ?>" />
<script language="JavaScript" type="text/javascript">
var j = jQuery.noConflict();

   	var data_fieldsfetchurl = '<?php echo $alarmlog_fetchingurl;?>';

	var firstval = 1;
	function getOptionsFromForm(){

		var opt = {callback: pageselectCallback, items_per_page:20};

		return opt;

	}

	function loadpagination(){

		var optInit = getOptionsFromForm(); //Create pagination element with options from form

		//alert(optInit);

		var totalrows_count = document.getElementById('totalrowscount').value;

		var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form

		j("#Pagination").pagination(totalrows_count, optInit);

	}

	

	function seachingwithpagination(){

		var searching_field=document.getElementById('searching_field').value;

		j.post('<?php echo site_url("/").'dooropenlog/dooropenloglist_count_with_search/';?>', {"searching_field":searching_field},

		function(data) {

			//alert(data);

			document.getElementById('totalrowscount').value = data;	

			loadpagination();

		});

	}

	function show_by_orderlist(show_by_orderval){

		//alert(show_by_orderval);

		document.getElementById('show_by_order').value = show_by_orderval;

		loadpagination();        

	}

	j(document).ready(function(){

		loadpagination();

	});
    
	
</script>
<table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">
 <tr>
     <th class=\"titlerow\" align=\"left\" width=\"10%\">Id</th>
	 <th class=\"titlerow\" align=\"left\" width=\"10%\">Site Name</th>
	 <th class=\"titlerow\" align=\"left\" width=\"20%\">Employee Name</th>
</tr>
 <tr>
                    <td><?php echo ($page+$i+1); ?></td>
                    <td><?php echo $booklist[$i]->SiteName; ?></td>
                    <td><?php echo $booklist[$i]->SubcentreName; ?></td>
                    <td><?php echo $booklist[$i]->RegionName; ?></td>
					<td><?php echo $booklist[$i]->Status; ?></td>
					<td><?php echo $booklist[$i]->DSTime; ?></td>
					<td><?php echo $booklist[$i]->IP; ?></td>
                </tr>
                <?php } ?>
</table>
<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
    <tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td>

                	

                   	<div style="width:275px; float:left; margin-top:2px;">

                        <div style="float:left; width:140px; line-height:22px;">Display Per Page:</div>

                        <div style="float:left; width:125px; height:22px;">

                           <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();" disabled>

                               <option value="5">5</option>

                               <option value="10">10</option>

                               <option value="20" selected="selected">20</option>

                               <option value="30">30</option>

                               <option value="50">50</option>

                               <option value="70">70</option>

                               <option value="100">100</option>

                            </select>

                        

                        </div>

                  	</div>
                   
				    <div style="width:185px; float:right; margin-left:10px;">
                       <table style="width:100%">
					      <tr>
						      <td style="text-align:right;"> <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" /></td>
							  <td>
							     <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
							  </td>
						  </tr>
					   </table>                       
					  

                    </div>     
                   

                </td>

                <td align="right" width="1">

                	               

                </td>

              </tr>

            </table>			

		</td>

	</tr>
	
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	<tr>
     
		<td align="center">			

			<?php

			if(isset($data) && count($data)>0){
			 
			?>	

				<div style="width:99%; overflow:hidden;">				
                    <div id="searchall" style="width:100%; overflow:hidden;">
					  <div style="width:100%; overflow:hidden;">
					   <table style="width:100%;">
					    <tr>
							 <td colspan="6">
							 <form action="dooropenlog" method="post">
							   <table width="100%" cellpadding="0" cellspacing="1" border="0">
							     <tr>
								  <td width="15%" style="text-align: right;font-weight:bold;">Site Name :</td>
								 <td>
								      <select name="site_name" id="site_name">
									   <option value="-1">All</option>
									  <?php
					                      
                                             if(is_array($sitedata) && count($sitedata)>0) {
												 $i=0;
												 foreach ($sitedata as $row1) {
													if($searchedsite == $row1->id){
														 echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";
												    }else{
														echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
													}
	                                             }

                                               }	
					                       
					                    ?>	
                                      </select>
								 </td>
								 
								 <td width="15%" style="text-align: right;font-weight:bold;">Date between :</td>
								 <td>
								      <?php 

						                 $data1 = array('name'     => 'datefrom',

									                   'id'       => 'datefrom',

									                   'tabindex'       => '1',

									                   'value'    => '',

									                   'maxlength' => '100',

									                   'size'    => '50',
													   
													   'class'    => 'input120x20'

									                );															

						                echo form_input($data1);

						           ?>
								 </td>
								 <td width="3%" style="text-align: center;font-weight:bold;">to</td>
								 <td>
								     <?php 

						                 $data1 = array('name'     => 'dateto',

									                   'id'       => 'dateto',

									                   'tabindex'       => '1',

									                   'value'    => '',

									                   'maxlength' => '100',

									                   'size'    => '50',

									                   'class'    => 'input120x20'

									                );															

						                echo form_input($data1);

						           ?>
								 </td>
								  <td>
								       <?php 

						                  $data1 = array( 'name'    => 'submit',

										                 'value'    => 'Search',

										                 'tabindex'       => '13',

									                      'class'    => 'submitbutton');															

						                           echo form_submit($data1);
									  ?>			   
								  </td>
								 </tr>
							
                               </table
							</td>
							 </form>    
							</tr>
					   </table>
					</div>
					<div id="Searchresult" style="width:100%; overflow:hidden;">

						This content will be replaced when pagination inits.

					</div>
                  </div>
					<div style="padding:0 10px 0 20px;" class="black12bold">Total <?php echo $data['Count'];?> data or <?php echo ceil($data['Count']/20);?> pages found </div>

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">

						<table width="100%" cellpadding="0" cellspacing="1" border="0">
                            
							<tr>

								<th class="titlerow" align="left" width="10%">Id</th>
								<th class="titlerow" align="left" width="10%">User</th>
								
								<th class="titlerow" align="left" width="10%">Site Name</th>
								
								<th class="titlerow" align="left" width="15%">Employee Name</th>
								
                                <th class="titlerow" align="left" width="10%">Employee Id</th>

								<th class="titlerow" align="left" width="10%">Phone</th>

								<th class="titlerow" align="left" width="35%">Purpose</th>
								
								<th class="titlerow" align="left" width="35%">Status</th>
								
                               
							</tr>

							<?php

							$str = "";

							$i = 1;

							foreach($data as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;
								$sitename = $row->sitename;

								$employeename = $row->employeename;
								
								$employeeid = $row->employeeid;

								$phoneno = $row->phoneno;

								$purpose = $row->purpose;
								
								$status = $row->purpose;
								
								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';

								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';

								
								$str .= "<tr>

									

									<td align=\"left\" class=\"$classname\">$i</td>
									<td align=\"left\" class=\"$classname\">$user</td>
                                     <td align=\"left\" class=\"$classname\">$sitename</td>
									
									<td align=\"left\" class=\"$classname\">$employeename</td>	
									
									<td align=\"left\" class=\"$classname\">$employeeid</td>
									
									<td align=\"left\" class=\"$classname\">$phoneno</td>
									
									<td align=\"left\" class=\"$classname\">$purpose</td>
									
									<td align=\"left\" class=\"$classname\">$status</td>";
								
									
								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				echo "<div class=red18bold>No log found</div>";					

			}								

			?>

		</td>

	</tr>

</table>