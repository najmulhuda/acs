<?php
if($this->session->userdata('first_name') !=''){ $first_nameval = $this->session->userdata('first_name');}
else{ $first_nameval = set_value('first_name');}
if($first_nameval=='')$first_nameval = 'Name';

if($this->session->userdata('students_cell') !=''){ $phoneval = $this->session->userdata('students_cell');}
else{ $phoneval = set_value('phone');}
if($phoneval=='')$phoneval = 'Phone Number';

if($this->session->userdata('students_email') !=''){ $emailval = $this->session->userdata('students_email');}
else{ $emailval = set_value('email');}
if($emailval=='')$emailval = 'Email Address';

if(set_value('comments') !=''){ $commentsval = set_value('comments');}
else{ $commentsval = 'Type your Comments';}

if($this->session->userdata('subject') !=''){ $subjectval = $this->session->userdata('subject');}
else{ $subjectval = 'Students wants to quick contact with you';}


?>

<script type="text/javascript" language="javascript">			
	var j = jQuery.noConflict();
	function checkfontactfield(){
		var oField = document.frmcontact1.first_name;
		j("#first_name").removeClass("error_inputText").addClass("inputText");
		if(oField.value == "" || oField.value == "Name"){
			j("#first_name").removeClass("inputText").addClass("error_inputText");
			oField.focus();
			return(false);
		}
		
		var oField = document.frmcontact1.phone;
		j("#phone").removeClass("error_inputText").addClass("inputText");
		if(oField.value == "" || oField.value == "Phone Number"){
			j("#phone").removeClass("inputText").addClass("error_inputText");
			oField.focus();
			return(false);
		}
		else if(checkNumericintVal(oField.value)==false) {
			j("#phone").removeClass("inputText").addClass("error_inputText");
			oField.focus();
			return(false);
		}
		
		var oField = document.frmcontact1.email;
		j("#email").removeClass("error_inputText").addClass("inputText");
		if(oField.value == "" || oField.value == "Email Address"){
			j("#email").removeClass("inputText").addClass("error_inputText");
			oField.focus();
			return(false);
		}
		else if(emailcheck(oField.value)==false) {
			j("#email").removeClass("inputText").addClass("error_inputText");
			oField.focus();
			return(false);
		}
		
		j("#comments").removeClass("error_commentsInput").addClass("commentsInput");
		var oField = document.frmcontact1.comments;
		if(oField.value == "" || oField.value == "Type your Comments"){
			j("#comments").removeClass("commentsInput").addClass("error_commentsInput");
			oField.focus();
			return(false);
		}
		return(true);
	}

</script>


<?php
$attributes = array('name' => 'frmcontact1','id' => 'frmcontact1','onsubmit'=>'return checkfontactfield();');
echo form_open(site_url("/").'contact_us/send_mail', $attributes);
	
	echo validation_errors();
	if(isset($err_message) && $err_message!=''){
		echo $err_message;
	}
	?>
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
		<label>name:</label>
		<span class="textBox">
			<?php 
			$data = array('name'     => 'first_name',
						  'id'       => 'first_name',
						  'tabindex'       => '1',
						  'value'    => $first_nameval,
						  'maxlength'=> '190',
						  'onfocus'=> "if(this.value=='Name'){this.value='';}",
						  'onblur'=> "if(this.value==''){this.value='Name';}",
						  'class'    => 'inputText'
						);															
			echo form_input($data);
			?>
		</span>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
		<label>Phone</label>
		<span class="textBox">
			<?php 
			$data = array('name'     => 'phone',
						  'id'       => 'phone',
						  'tabindex'       => '2',
						  'value'    => $phoneval,
						  'maxlength'=> '190',
						  'onfocus'=> "if(this.value=='Phone Number'){this.value='';}",
						  'onblur'=> "if(this.value==''){this.value='Phone Number';}",
						  'class'    => 'inputText'
						);															
			echo form_input($data);
			?>
		</span>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
		<label>Email</label>
		<span class="textBox">
			<?php 
			$data = array('name'     => 'email',
						  'id'       => 'email',
						  'tabindex'       => '3',
						  'value'    => $emailval,
						  'maxlength'=> '190',
						  'onfocus'=> "if(this.value=='Email Address'){this.value='';}",
						  'onblur'=> "if(this.value==''){this.value='Email Address';}",
						  'class'    => 'inputText'
						);															
			echo form_input($data);
			?>
		</span>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
		<em>How May I help you?</em>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
		<span class="commentsBox">
			<?php 
			$data = array('name'     => 'comments',
						  'id'       => 'comments',
						  'value'    => $commentsval,
						  'tabindex'       => '4',
						  'onfocus'=> "if(this.value=='Type your Comments'){this.value='';}",
						  'onblur'=> "if(this.value==''){this.value='Type your Comments';}",
						  'rows'=> '4',
						  'cols'=> '4',
						  'class'    => 'commentsInput'
						);															
			echo form_textarea($data);
			?>
		</span>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	<div class="innerRow_0"><!-- Begin:innerRow_0 -->
	
		<input type="hidden" name="subject" id="subject" value="<?php echo $subjectval;?>" />
		<?php 
		$data = array( 'name'    => 'submit',
						'value'    => '',
						'tabindex'       => '5',
						'class'    => 'btn_submit');															
		echo '&nbsp;&nbsp;'.form_submit($data);
		
		?>
		<span class="clear"><!-- Instead of overflow hidden --></span>
	</div><!-- End:innerRow_0 -->
	
	
<?php echo form_close(); ?>

