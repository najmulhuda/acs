<?php
if ($this->uri->segment(1) === FALSE){$segment1name = '';}
else{$segment1name = $this->uri->segment(1);} 

if(isset($first_name['value']) && $first_name['value'] !=''){ $first_nameval = $first_name['value'];}
else{ $first_nameval = set_value('first_name');}

if(isset($last_name['value']) && $last_name['value'] !=''){ $last_nameval = $first_name['value'];}
else{ $last_nameval = set_value('last_name');}

if(isset($phone['value']) && $phone['value'] !=''){ $phoneval = $phone['value'];}
else{ $phoneval = set_value('phone');}

if(isset($email['value']) && $email['value'] !=''){ $emailval = $email['value'];}
else{ $emailval = set_value('email');}

if(isset($comments['value']) && $comments['value'] !=''){ $commentsval = $comments['value'];}
else{ $commentsval = set_value('comments');}
							
if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($contact_us_id['value']) && $contact_us_id['value'] !=''){ $contact_us_idval = $contact_us_id['value'];}
else{ $contact_us_idval = set_value('contact_us_id');}
if($contact_us_idval==''){$contact_us_idval = 0;}

$site_url = str_replace('/index.php','',site_url("/"));
?>
<script type="text/javascript" src="<?php echo $site_url?>common/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();
	function checkfrmcontact_us_us_form(){
		var oField = document.frmcontact_us.first_name;
		var oElement = document.getElementById('errmsg_first_name');
		oElement.innerHTML = "";
		if(oField.value == ""){
			oElement.innerHTML = "You are missing First name.";
			oField.focus();
			return(false);
		}
		else if(oField.value.length<2){
			oElement.innerHTML = "First Name should be more than 2 character";
			oField.focus();
			return(false);
		}
		
		
		var oField = document.frmcontact_us.phone;
		var oElement = document.getElementById('errmsg_phone');
		oElement.innerHTML = "";
		if(oField.value != ""){
			if(checkmobileortelephone(oField.value)==false){
				oElement.innerHTML = "Phone is invalid.";
				oField.focus();
				return(false);
			}
			else if(oField.value.length<5){
				oElement.innerHTML = "Phone should be more than 5 character";
				oField.focus();
				return(false);
			}
		}
		
		var oField = document.frmcontact_us.email;
		var oElement = document.getElementById('errmsg_email');
		oElement.innerHTML = "";
		if(oField.value == ""){
			oElement.innerHTML = "You are missing email.";
			oField.focus();
			return(false);
		}
		else if(emailcheck(oField.value)==false) {
			oElement.innerHTML = "Invalid Email!. Please type valid email.";
			oField.focus();
			return(false);
		}
		
		return true;
	}
	tinyMCE.init({
		mode : "exact",
		elements : "comments",
		theme : "advanced",
		plugins : "advimage,advlink,media,contextmenu",
		theme_advanced_buttons1_add_before : "newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,separator,",
		theme_advanced_buttons3_add_before : "",
		theme_advanced_buttons3_add : "media",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "comments",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : true,
		apply_source_formatting : true,
		force_br_newlines : true,
		force_p_newlines : false,	
		relative_urls : true
	});

	function contact_us_description(field_name, url, type, win) {
		var contact_us_descriptionurl = "<?php echo $site_url?>common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
		}
		tinyMCE.activeEditor.windowManager.open({
			url: "<?php echo $site_url?>common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
			width: 800,
			height: 300,
			inline : "yes",
			close_previous : "no"
		},{
			window : win,
			input : field_name
		});
	}
</script>
<table id="blueborder" width="750" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	<?php echo $title;?>
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").$segment1name.'/'.$segment1name.'_list';?>" class="white14bold">Show Contact Us List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
				<?php				
				$attributes = array('name' => 'frmcontact_us_us', 'onsubmit'=>'return checkfrmcontact_us_us_form();');
				echo form_open_multipart(site_url("/").'contact_us/save_contact_us', $attributes);
				?>
					<table width="100%" border="0" cellspacing="7" cellpadding="0">										
						<tr>
							<td class="black11bold" align="left">First Name: <font class="red14bold">*</font></td>
							<td align="left" class="black11normal">
								<?php 
								if($contact_us_idval>0){
									$data = array('name'     => 'first_name',
												  'id'       => 'first_name',
												  'tabindex' => '1',
												  'value'    => $first_nameval,
												  'readonly'=> '250',
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								else{
									$data = array('name'     => 'first_name',
												  'id'       => 'first_name',
												  'tabindex' => '1',
												  'value'    => $first_nameval,
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								echo form_input($data);
								?>
								<span id="errmsg_first_name" class="errormsg"></span>
							</td>
						</tr>
                        <tr>
							<td class="black11bold" align="left">Last Name:</td>
							<td align="left" class="black11normal">
								<?php 
								if($contact_us_idval>0){
									$data = array('name'     => 'last_name',
												  'id'       => 'last_name',
												  'tabindex' => '1',
												  'value'    => $last_nameval,
												  'readonly'=> '250',
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								else{
									$data = array('name'     => 'last_name',
												  'id'       => 'last_name',
												  'tabindex' => '1',
												  'value'    => $last_nameval,
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								echo form_input($data);
								?>
							</td>
						</tr>
						<tr>
							<td class="black11bold" align="left">Phone:</td>
							<td align="left" class="black11normal">
								<?php 
								if($contact_us_idval>0){
									$data = array('name'     => 'phone',
												  'id'       => 'phone',
												  'tabindex' => '1',
												  'value'    => $phoneval,
												  'readonly'=> '250',
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								else{
									$data = array('name'     => 'phone',
												  'id'       => 'phone',
												  'tabindex' => '1',
												  'value'    => $phoneval,
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								echo form_input($data);
								?>
								<span id="errmsg_phone" class="errormsg"></span>
							</td>
						</tr>
						<tr>
							<td class="black11bold" align="left">E-mail: <font class="red14bold">*</font></td>
							<td align="left" class="black11normal">
								<?php 
								if($contact_us_idval>0){
									$data = array('name'     => 'email',
												  'id'       => 'email',
												  'tabindex' => '1',
												  'value'    => $emailval,
												  'readonly'=> '250',
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								else{
									$data = array('name'     => 'email',
												  'id'       => 'email',
												  'tabindex' => '1',
												  'value'    => $emailval,
												  'maxlength'=> '250',
												  'class'    => 'input300x20'
												);
								}
								echo form_input($data);
								?>
								<span id="errmsg_email" class="errormsg"></span>
							</td>
						</tr>	
                        			
						<tr>
							<td class="black11bold" align="left">Comments:</td>
							<td align="left">
								<textarea tabindex="6" name="comments" style="width:500px; height: 200px"><?php echo $commentsval; ?></textarea>
							</td>
						</tr>
						<tr>
							<td align="left" class="black11bold"></td>
							<td align="left" class="black11normal">
								<?php 
								$data = array( 'name'    => 'submit',
											   'value'   => $submitval,
											   'tabindex'=> '7',
											   'class'   => 'submitbutton');															
								
								echo form_submit($data);
								$data = array( 'name'    => 'reset',
											   'value'   => ' Reset ',
											   'tabindex'=> '8',
											   'class'   => 'submitbutton');
											   															
								echo '&nbsp;&nbsp;'.form_reset($data);
								?>
								<input type="hidden" name="contact_us_id" id="contact_us_id" value="<?php echo $contact_us_idval;?>" />
							</td>
						</tr>									
					</table>
				<?php echo form_close(); ?> 	
		</td>
	</tr>
</table>
		