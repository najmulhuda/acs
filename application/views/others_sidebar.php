<?php
$site_url = str_replace('/index.php','',site_url("/"));
?>
<style type="text/css">
.product_special{
	width:252px;
	margin:0;
	padding:0;
}
.product_special img{
	text-align:center;
}
.product_specialdetails{
	margin:10px 0;
	padding:0;
}
.product_special h4{
	color:#fff;
	text-align:center;
	font-size:16px;
	line-height:20px;
	font-weight:normal;
}
.product_special a{
	color:#fff;
	text-align:center;
	width:90px;
	height:28px;
	float:left;
	font-size:14px;
	color:#FFF;
	line-height:28px;
	background:#3272c0;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	-ms-border-radius:5px;
    -o-border-radius:5px;
	margin:5px 0 20px 80px;
}
</style>
<div class="productcategory"><!-- Begin: sidebar -->    
	<h3 class="sideHead_new">Product Special</h3>
    <div class="product_special">
    	<?php
		$queryproduct_special = $commonmodel->getallrowbysqlquery("select * from product where product_publish = 1 and product_special = 1 order by created_date asc limit 0,1");
		if(count($queryproduct_special)>0){
			foreach($queryproduct_special as $rowspecial_product){
				$product_id = $rowspecial_product->product_id;	
				$product_name = substr($rowspecial_product->product_name, 0, 20);	
				$product_price = $rowspecial_product->product_price;
				$product_discount = $rowspecial_product->product_discount;
				
				$price = $product_price - $product_discount;
				if(floor($product_discount)>0){
					$product_pricestr = $price;	
				}
				else{
					$product_pricestr = $product_price;
				}
				
				$product_image 	= $rowspecial_product->product_image;
				$product_imageUrl = $site_url.'application/views/module/product_image/'.$product_image;
				$product_Url = site_url("/").'product/product_details/'.$product_id;
				echo "<div class=\"product_specialimg\">
            			<img src=\"$product_imageUrl\" alt=\"$product_name\" width=\"250\" height=\"230\" />
            		</div>
					<div class=\"product_specialdetails\">
	                <h4>$product_name</h4>
					<h4>$ $product_pricestr</h4>
					<a href=\"$product_Url\">Details</a>
					</div>";
			}
		}
		?>
    </div>
    
    <h3 class="sideHead_new">Product Category</h3>
    <ul>
	<?php
	$queryproduct_category = $commonmodel->getallrowbysqlquery("select * from product_category where product_category_publish = 1 order by showing_order asc limit 0,10");
	if(count($queryproduct_category)>0){
		foreach($queryproduct_category as $rowproduct_category){	
			$product_category_id = $rowproduct_category->product_category_id;	
			$product_category_name = stripslashes($rowproduct_category->product_category_name);			
			$product_category_url = site_url("/").'product/front_product_show_category_wise/'.$product_category_id;
			echo "<li><a href=\"$product_category_url\">$product_category_name</a></li>";
		}
	}
	else{
		echo "<li>Sorry, no product category found.</li>";
	}
	?>
    </ul>
</div><!-- End: sidebar -->