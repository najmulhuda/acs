<?php
$site_url = str_replace('/index.php','',site_url("/"));
?>
<script type="text/javascript" language="javascript">
	var j = jQuery.noConflict();
	//===========================For Front Menu Category insert ===================================//
	function show_front_menu_category_name_list(){
		j("#finalsaverow").fadeIn(100);
		j.post("<?php echo site_url("/");?>front_menu_category/show_property_front_menu_category_name_list/",{}, function(data){
			if(data !=''){
				j("#show_front_menu_category_name_list").html('<select name="front_menu_category_id" id="front_menu_category_id" onchange="show_parent_front_menu(this.value);" class="input200x20"><option value="0">Select Front Menu Category</option>'+data+'</select>');
				j("#addnew_front_menu_category_button").html("<a onclick=\"addnew_front_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Front Menu Category\" /></a>").fadeIn(100);
			}
			else{
				j("#show_front_menu_category_name_list").html('<select name="front_menu_category_id" id="front_menu_category_id" onchange="show_parent_front_menu(this.value);" class="input200x20"><option value="0">Select Front Menu Category</option></select>');
				j("#addnew_front_menu_category_button").html("<a onclick=\"addnew_front_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Front Menu Category\" /></a>").fadeIn(100);
			}
		});
	}	

	function addnew_front_menu_category_form(){
		j("#show_front_menu_category_name_list").fadeOut(100);
		j("#addnew_front_menu_category_button").fadeOut(100);
		j("#finalsaverow").fadeOut(100);
		j("#show_front_menu_category_name_list").html("<input name=\"front_menu_category_name\" id=\"front_menu_category_name\" class=\"input200x20\">").fadeIn(100);		
		j("#addnew_front_menu_category_button").html("<a onclick=\"savenew_front_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/savebutton.png\" alt=\"Save Front Menu Category\" /></a><a onclick=\"show_front_menu_category_name_list();\"><img src=\"<?php echo $site_url;?>common/images/cancelsave.png\" alt=\"Cancel Save Front Menu Category\" /></a>").fadeIn(100);
		document.frmfront_menu.front_menu_category_name.focus();
	}	

	function savenew_front_menu_category_form(){
		var front_menu_category_name = document.frmfront_menu.front_menu_category_name;
		var elementmessage = document.getElementById("errmsg_front_menu_category_id");
		elementmessage.innerHTML = '';
		j('#front_menu_category_name').removeClass('error_input200x20').addClass('input200x20');
		if(front_menu_category_name.value==''){
			elementmessage.innerHTML = 'English Front Menu Category name is mandatory';
			front_menu_category_name.focus();
			j('#front_menu_category_name').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		if(front_menu_category_name.value !=''){
			j.post("<?php echo site_url("/");?>front_menu_category/save_front_menu_category_form/",{ front_menu_category_name:front_menu_category_name.value}, function(data){
				//alert(data);
				if(data !='ERROR'){
					j("#show_front_menu_category_name_list").fadeOut(100);
					j("#addnew_front_menu_category_button").fadeOut(100);
					j("#finalsaverow").fadeIn(100);					
					j("#show_front_menu_category_name_list").slideDown(500).html('<select name="front_menu_category_id" id="front_menu_category_id" onchange="show_parent_front_menu(this.value);" class="input200x20" onchange="show_front_menu_category_name_list(this.value);"><option value="0">Select Front Menu Category</option>'+data+'</select>');
					j("#addnew_front_menu_category_button").html("<a onclick=\"addnew_front_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Front Menu Category\" /></a>").fadeIn(100);
					
					show_parent_front_menu(document.getElementById("front_menu_category_id").value);
				}
				else{
					elementmessage.innerHTML = 'Error occured while saving front_menu_category name.';
					front_menu_category_name.focus();
				}
			});	
		}
	}
	
	function show_parent_front_menu(front_menu_category_id){
		j.post("<?php echo site_url("/");?>front_menu_category/show_parent_front_menu_listbyjquery/",{front_menu_category_id:front_menu_category_id}, function(data){
			if(data !=''){
				j("#parent_front_menu").html('<option value="0">Root</option>'+data);
			}
			else{
				j("#parent_front_menu").html('<option value="0">Root</option>');
			}
		});
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding:0px 5px 0px 0px;" width="200" valign="middle" align="left">
        <div id="show_front_menu_category_name_list">
        <select tabindex="4" name="front_menu_category_id" id="front_menu_category_id" onchange="show_parent_front_menu(this.value);" class="input200x20">
            <option value="0">Select Front Menu Category</option>
            <?php
            $conditionarray = array('front_menu_category_publish'=>1);
            echo $commonmodel->get_onetableallfieldvalueforselect('front_menu_category', 'front_menu_category_id', $front_menu_category_idval, 'front_menu_category_name', $conditionarray);
            ?>
        </select>
        </div>
    </td>
    <td width="50" style="padding:0px 5px 0px 0px;" align="center" valign="middle">
        <div id="addnew_front_menu_category_button" style="height:20px; width:80px;">
            <a onclick="addnew_front_menu_category_form();">
                <img src="<?php echo $site_url;?>common/images/addnewbutton.png" alt="Add New Front Menu Category" />
            </a>											
        </div>
    </td>
    <td><span id="errmsg_front_menu_category_id" class="red11normal"></span></td>
  </tr>
</table>