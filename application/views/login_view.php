<?php
  $site_url = str_replace('/index.php','',site_url("/"));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $title;?></title>

<link rel="shortcut icon" href="<?php echo $site_url?>common/images/favicon.ico" />

<link rel="stylesheet" href="<?php echo $site_url?>common/css/admin_template.css" type="text/css" />

<script src="<?php echo $site_url?>common/datetimepicker/jquery.js" type="text/javascript"></script>
<script src="<?php echo $site_url?>common/datetimepicker/jquery.datetimepicker.js" type="text/javascript"></script>
<script src="<?php echo $site_url?>common/js/admin_menu.js" type="text/javascript"></script>
<script src="<?php echo $site_url?>common/js/admin_common.js" type="text/javascript"></script>

<link type="text/css" href="<?php echo $site_url;?>common/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />

</head>
<body onload="deleteloading();">

<div id="pageloadingbar" style="display:none; position:absolute; top:75px; left:600px; z-index:999;">

    <img src="<?php echo str_replace('/index.php','',site_url("/"));?>common/images/032.gif" alt="Page Loading.."/>

</div> 
<center>

<table align="center" id="fullpagetable" width="1160" border="0" cellspacing="0" cellpadding="0">

  	<tr>

    	<td align="center" valign="top">

			<div id="topheaderdiv">

				<div id="toplinkmenu">					

				</div>

				<div id="toplogodiv">

					<div class="topheaderleft"></div>

					<div class="topheaderbg">

						<div id="div550x41leftmar14">

							<div class="logo"></div>

							<div class="topusernameshow">

								

								<div id="show_message" class="red12bold">

									
								</div>

							</div>

						</div>

					</div>

					<div class="topheaderright">

                    	<div class="site_preview">

                    	

                        </div>

						<div id="div159x50headerright" >

							

						</div>

                        

					</div>

				</div>	

				<div id="topmainmenu">

					<div class="mainmenuleft"></div>

					<div class="mainmenubg" id="mainmenu">

                		<ul class="menu" id="menu">

                           
					    </ul>		
					</div>

					<div class="mainmenuright"></div>

				</div>

			</div>

			<script type="text/javascript">

				//initalizetab("menu");

			</script>

		</td>

  	</tr>

  	<tr>

    	<td bgcolor="#FFFFFF" height="100" valign="top" class="rowtopbotpadding15">

			<center>

				<?php
				$site_url = str_replace('/index.php','',site_url("/"));
				$attributes = array('name' => 'frmuser_login', 'onsubmit'=>'return checkfrmuser_login();');
				echo form_open(site_url("/").'login/check_user_login', $attributes);

				?>

		<table id="blueborder" width="60%" align="center" border="0" cellspacing="0" cellpadding="5">

			<tr>

				<td id="bluebgtd">

					User Login Information

				</td>

			</tr>

			<tr>

				<td align="left">

					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

						<tr>

							<td align="left" colspan="2">

								&nbsp;

								<?php 

									echo validation_errors();

									if(isset($err_message) && $err_message!=''){

										echo $err_message;

									}

								?>

							</td>

						</tr>

						<tr>

							<td align="right" width="30%" class="black12bold">User&nbsp;Name:</td>

							<td align="left">

								<?php 

								$data = array('name'     => 'user_name',

											  'id'       => 'user_name',

											  'tabindex'       => '1',

											  'value'    => '',

											  'class'=> 'input200x20',

											  'maxlength'=> '100',

											  'size'    => '50'

											);															

								echo form_input($data);

								?>

								<span id="errmsg_user_name" class="red11normal"></span>

							</td>

						</tr>

						<tr>

							<td align="right" class="black12bold">Password:</td>

							<td align="left">

								<?php 

								$data = array('name'     => 'user_password',

											'id'       => 'user_password',

											'tabindex'       => '2',

											'value'    => '',

											'class'=> 'input200x20',

											'maxlength'=> '100',

											'size'    => '50'

											);															

								echo form_password($data);

								?>

								<span id="errmsg_user_password" class="red11normal"></span>

							</td>

						</tr>						

						<tr>

							<td align="right" class="black12normal">&nbsp;</td>

							<td align="left">

								<?php 

								$data = array( 'name'    => 'submit',

												'value'    => ' Login ',

												'value'    => ' Login ',

												'tabindex'       => '4',

											 	'class'    => 'submitbutton');															

								echo form_submit($data);

								$data = array( 'name'    => 'reset',

												'value'    => ' Reset ',

												'tabindex'       => '5',

											  'class'    => 'submitbutton');															

								echo '&nbsp;&nbsp;'.form_reset($data);

								?>

							</td>

						</tr>

						<tr>

							<td align="right">&nbsp;</td>

							<td align="right">&nbsp;</td>

						</tr>

						<tr>

							<td colspan="2" align="center" class="black12normal">Forgot your password? <a href="<?php echo site_url("/");?>login/index/forgot_pass" class="blue11anchor">Click Here</a></td>

						</tr>

						<tr>

							<td align="right">&nbsp;</td>

							<td align="right">&nbsp;</td>

						</tr>						

					</table>

				</td>

			</tr>

		</table>

	<?php 

	echo form_close(); 								
    ?>

			</center>

		</td>

  	</tr>

  	<tr>

    	<td>

			<div id="footerdiv">

				<div class="footertop1160x4div">

					<div class="footertopleft"></div>

					<div class="footertopbg"></div>

					<div class="footertopright"></div>

				</div>

				<div class="footertop1160x34div">

					&copy;2015 picolight.net, All rights reserved.<br />

					The Local Time <?php echo date('Y M d D h:i A');?> Page rendered in {elapsed_time} seconds

				</div>

			</div>

		</td>

  	</tr>

</table>

</center>

<script type="text/javascript">

   jQuery(document).ready(function($){
			$('#datetimepicker').datetimepicker({
	             datepicker:false,
	             allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00','21:00','22:00','23:00'],
	             step:5
             });
		});

   		
	var menu=new menu.dd("menu");

	menu.init("menu","menuhover");

	//deleteloading();	
	
	 
		

</script>

</body>

</html>


