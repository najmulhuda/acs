<?php
 $functionname = 'BTS External Alarm';
 $site_url = str_replace('/index.php','',site_url("/"));
 
 $alarmlog_fetchingurl = site_url("/").'activity/activitylist/';
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">



<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
    <tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td>

                	

                   	<div style="width:275px; float:left; margin-top:2px;">

                        <div style="float:left; width:140px; line-height:22px;">Display Per Page:</div>

                        <div style="float:left; width:125px; height:22px;">

                           <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();" disabled>

                               <option value="5">5</option>

                               <option value="10">10</option>

                               <option value="20" selected="selected">20</option>

                               <option value="30">30</option>

                               <option value="50">50</option>

                               <option value="70">70</option>

                               <option value="100">100</option>

                            </select>

                        

                        </div>

                  	</div>
                   
				    <div style="width:225px; float:right; margin-left:10px;">

                       <table style="width:100%">
					      <tr>
						      <td style="text-align:right;"> <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" /></td>
							  <td>
							     <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
							  </td>
						  </tr>
					   </table>                       
					  

                       

                    </div>     
                   

                </td>

                <td align="right" width="1">

                	               

                </td>

              </tr>

            </table>			

		</td>

	</tr>
	<div style="width:100%; overflow:hidden;">
					   <table style="width:100%;">
					    <tr>
							 <td colspan="6">
							   <table width="100%" cellpadding="0" cellspacing="1" border="0">
							     <tr>
								 <div class="row">
								<div class="col-md-8 col-md-offset-2 well">
								<h2>BTS External Alarm Activity Log</h2>
								<?php
							     echo form_open('activity/select_by_date_range');
								echo form_label('SiteName: ');
								$data = array(
								'name' => 'name',
								'placeholder' => 'Please Enter Name'
								);
								echo form_input($data);
								
								echo form_label('Date: ');
								echo "From : ";

								$data = array(
								'type' => 'date',
								'name' => 'date_from',
								'placeholder' => 'yyyy-mm-dd'
								);
								echo form_input($data);
								echo " To : ";

								$data = array(
								'type' => 'date',
								'name' => 'date_to',
								'placeholder' => 'yyyy-mm-dd'
								);
								echo form_input($data);
								
							    echo form_submit('submit', 'Search');
								echo "<div class='error_msg'>";
								if (isset($date_range_error_message)) {
								echo $date_range_error_message;
								}								
								echo "</div>";

								echo form_close();
							   ?>
								</div>
							</div>
							<td width="94%" style="text-align: right;font-weight:bold;"><a href="download/btsxls.php">Excel</a></td>
							<td width="10%" style="text-align: right;font-weight:bold;"><a href="pdf_make/bts_pdf.php">Pdf</a></td>
								 </tr>
							
                               </table
							</td>
							 </form>    
							</tr>
					   </table>
					</div>
					<br/>
			    <div id="Searchresult" style="width:95%; overflow:hidden;">
				 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">
				 
				
				<tr>
				<table width="100%" cellspacing="1" cellpadding="0" border="0">
				<tr>

				
				<th class="titlerow" align="left" width="10%">Site Name</th>
				
				<th class="titlerow" align="left" width="15%">SubcentreName</th>
				
				<th class="titlerow" align="left" width="10%">RegionName</th>

				<th class="titlerow" align="left" width="10%">Alarm</th>

				<th class="titlerow" align="left" width="25%">ATime</th>
				
				<th class="titlerow" align="left" width="35%">CTime</th>
				
				<th class="titlerow" align="left" width="35%">IP</th>
				</tr>
				<tbody>
				<?php
					if (isset($result_display)) {
					if ($result_display == 'No record found !') {
					echo $result_display;
					} else {
	
					foreach ($result_display as $value) {?>
			
					<tr>
						<td align="left" class="evenrow"><?php echo $value->SiteName; ?></td>
						<td align="left" class="evenrow"><?php echo $value->SubcentreName; ?></td>
						<td align="left" class="evenrow"><?php echo $value->RegionName; ?></td>
						<td align="left" class="evenrow"><?php echo $value->Alarm; ?></td>
						<td align="left" class="evenrow"><?php echo $value->ATime; ?></td>
						<td align="left" class="evenrow"><?php echo $value->CTime; ?></td>
						<td align="left" class="evenrow"><?php echo $value->IP; ?></td>
				
			     	</tr>
			     	<?php
					}
					}
					}
					?>
				</tr></tbody></table>
				
           </table>
        </div> 
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	<tr>
     
		<td align="center">			

			<?php

			if(isset($data) && count($data)>0){
			 
			?>	

				<div style="width:99%; overflow:hidden;">				

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">

						<table width="100%" cellpadding="0" cellspacing="1" border="0">

							<tr>

								
                                <th class="titlerow" align="left" width="10%">Device IP</th>

								<th class="titlerow" align="left" width="15%">Time</th>
								<th class="titlerow" align="left" width="15%">CSTime</th>

								<th class="titlerow" align="left" width="45%">Alarm</th>
								
                                <th class="titlerow" align="center" width="10%">Action</th>																					
							</tr>

							<?php

							$str = "";

							$i = 1;

							foreach($data as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;

								$deviceip = $row->deviceip;

								$alarm = $row->alarm;

								$atime = $row->atime;

								
								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';

								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';

								
								$str .= "<tr>

									

									<td align=\"left\" class=\"$classname\">$deviceip</td>
									
                                     <td align=\"left\" class=\"$classname\">$atime</td>
									 
									<td align=\"left\" class=\"$classname\">$alarm</td>";
								
									$str .= "<td class=\"$classname\" align=\"center\">";

										$str .= "
                                                 <a href=\"$site_url"."alarmlog/edit/$id\" title=\"Edit\">
                                                 <img src=\"$editimg\" border=\"0\" alt=\"Edit\">
                                                 </a> &nbsp;&nbsp;&nbsp;
												 <a onclick=\"tablerow_remove($id, 'alarmlog', 'alarmlog', 'alarmlog');\" title=\"Remove\">
                                                 <img src=\"$removeimg\" border=\"0\" alt=\"Remove\">
                                                 </a>";

									
									$str .= "</td>";

								

								

								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				
			}								

			?>

		</td>

	</tr>

</table>