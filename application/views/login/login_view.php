<?php
$site_url = str_replace('/index.php','',site_url("/"));
$attributes = array('name' => 'frmuser_login', 'onsubmit'=>'return checkfrmuser_login();');
echo form_open(site_url("/").'admin/user_login', $attributes);

?>

		<table id="blueborder" width="60%" align="center" border="0" cellspacing="0" cellpadding="5">

			<tr>

				<td id="bluebgtd">

					User Login Information

				</td>

			</tr>

			<tr>

				<td align="left">

					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

						<tr>

							<td align="left" colspan="2">

								&nbsp;

								<?php 

									echo validation_errors();

									if(isset($err_message) && $err_message!=''){

										echo $err_message;

									}

								?>

							</td>

						</tr>

						<tr>

							<td align="right" width="30%" class="black12bold">User&nbsp;Name:</td>

							<td align="left">

								<?php 

								$data = array('name'     => 'user_name',

											  'id'       => 'user_name',

											  'tabindex'       => '1',

											  'value'    => '',

											  'class'=> 'input200x20',

											  'maxlength'=> '100',

											  'size'    => '50'

											);															

								echo form_input($data);

								?>

								<span id="errmsg_user_name" class="red11normal"></span>

							</td>

						</tr>

						<tr>

							<td align="right" class="black12bold">Password:</td>

							<td align="left">

								<?php 

								$data = array('name'     => 'user_password',

											'id'       => 'user_password',

											'tabindex'       => '2',

											'value'    => '',

											'class'=> 'input200x20',

											'maxlength'=> '100',

											'size'    => '50'

											);															

								echo form_password($data);

								?>

								<span id="errmsg_user_password" class="red11normal"></span>

							</td>

						</tr>						

						<tr>

							<td align="right" class="black12normal">&nbsp;</td>

							<td align="left">

								<?php 

								$data = array( 'name'    => 'submit',

												'value'    => ' Login ',

												'value'    => ' Login ',

												'tabindex'       => '4',

											 	'class'    => 'submitbutton');															

								echo form_submit($data);

								$data = array( 'name'    => 'reset',

												'value'    => ' Reset ',

												'tabindex'       => '5',

											  'class'    => 'submitbutton');															

								echo '&nbsp;&nbsp;'.form_reset($data);

								?>

							</td>

						</tr>

						<tr>

							<td align="right">&nbsp;</td>

							<td align="right">&nbsp;</td>

						</tr>

						<tr>

							<td colspan="2" align="center" class="black12normal">Forgot your password? <a href="<?php echo site_url("/");?>admin/index/forgot_pass" class="blue11anchor">Click Here</a></td>

						</tr>

						<tr>

							<td align="right">&nbsp;</td>

							<td align="right">&nbsp;</td>

						</tr>						

					</table>

				</td>

			</tr>

		</table>

	<?php 

	echo form_close(); 								
    ?>