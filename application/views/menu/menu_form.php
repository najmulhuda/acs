<?php

if(isset($menu_publish['value']) && $menu_publish['value'] !=''){ 

	$menu_publishval = $menu_publish['value'];

	if($menu_publishval>0){

		$menu_publishval = set_checkbox('menu_publish', '1', true);

	}

	else{

		$menu_publishval = set_checkbox('menu_publish', '1', false);

	}

}

else{ $menu_publishval = set_checkbox('menu_publish', '1', true);}



if(isset($menu_category_id['value']) && $menu_category_id['value'] !=''){ $menu_category_idval = $menu_category_id['value'];}

else{ $menu_category_idval = set_value('menu_category_id');}



if(isset($parent_menu['value']) && $parent_menu['value'] !=''){ $parent_menuval = $parent_menu['value'];}

else{ $parent_menuval = set_value('parent_menu');}



if(isset($menu_name['value']) && $menu_name['value'] !=''){ $menu_nameval = $menu_name['value'];}

else{ $menu_nameval = set_value('menu_name');}



if(isset($menu_module['value']) && $menu_module['value'] !=''){ $menu_moduleval = $menu_module['value'];}

else{ $menu_moduleval = set_value('menu_module');}



if(isset($module_type['value']) && $module_type['value'] !=''){ $module_typeval = $module_type['value'];}

else{ $module_typeval = set_value('module_type');}

if($module_typeval==''){$module_typeval='No';}



if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}

else{ $submitval = ' Save ';}



if(isset($menu_id['value']) && $menu_id['value'] !=''){ $menu_idval = $menu_id['value'];}

else{ $menu_idval = set_value('menu_id');}	

if($menu_idval==''){$menu_idval = 0;}			

				

if(isset($action_type['value']) && $action_type['value'] !=''){	$action_typeval = $action_type['value'];}

else{ $action_typeval = set_value('action_type');}

if($action_typeval==''){$action_typeval = 0;}	

$site_url = site_url();

?>

<script language="JavaScript" type="text/javascript">

	var j = jQuery.noConflict();	

	function checkfrmmenu_form(){

		

		var menu_id = document.frmmenu.menu_id;

		

		var menu_category_id = document.frmmenu.menu_category_id;

		var elementmessage = document.getElementById("errmsg_menu_category_id");

		elementmessage.innerHTML = '';

		j('#menu_category_id').removeClass('error_input200x20').addClass('input200x20');

		if(menu_category_id.value==0){

			elementmessage.innerHTML = 'Menu Category name is mandatory';

			menu_category_id.focus();

			j('#menu_category_id').removeClass('input200x20').addClass('error_input200x20');

			return false;

		}

		

		var menu_name = document.frmmenu.menu_name;

		var elementmessage = document.getElementById("errmsg_menu_name");

		elementmessage.innerHTML = '';

		j('#menu_name').removeClass('field400_err').addClass('field400');

		if(menu_name.value==''){

			elementmessage.innerHTML = 'You are missing menu name';

			menu_name.focus();

			j('#menu_name').removeClass('field400').addClass('field400_err');

			return false;

		}

		

		var menu_module = document.frmmenu.menu_module;

		var elementmessage = document.getElementById("errmsg_menu_module");

		elementmessage.innerHTML = '';

		j('#menu_module').removeClass('field400_err').addClass('field400');

		if(menu_module.value==''){

			elementmessage.innerHTML = 'You are missing menu Table name';

			menu_module.focus();

			j('#menu_module').removeClass('field400').addClass('field400_err');

			return false;

		}

		else if(checkonlyNumericcharVal(menu_module.value)==false){

			elementmessage.innerHTML = 'Invalid Table name. Only Numeric and character allowed.';

			menu_module.focus();

			j('#menu_module').removeClass('field400').addClass('field400_err');

			return false;

		}

		return true;

	}

</script>



<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Menu Information Form

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'menu/menu_list';?>" class="white14bold">Show Menu List</a>

                </td>

              </tr>

            </table>

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}

			?>

		</td>

	</tr>

	<tr>

		<td align="center">

			<?php           

            $attributes = array('name' => 'frmmenu', 'onsubmit'=>'return checkfrmmenu_form();');

            echo form_open_multipart(site_url("/").'menu/save_menu', $attributes);

            ?>

                <table width="100%" border="0" cellspacing="7" cellpadding="0">										

                    <tr>

                        <td class="black11bold" width="180" align="left">Publish:</td>

                        <td align="left">

                            <input type="checkbox" name="menu_publish" value="1" <?php echo $menu_publishval; ?> />

                        </td>

                    </tr>

                    <tr>

						<td class="black11bold" align="left">Menu Category Name: <font class="red14bold">*</font></td>

						<td align="left" class="black11normal">

							<?php 

							$admin_data['menu_category_idval'] = $menu_category_idval;

							$this->load->view('menu_category/addnew_byjquery',$admin_data);?>									

						</td>

					</tr>					

                    <tr>

                        <td class="black11bold" align="left">Parent Menu: <font class="red14bold">*</font></td>

                        <td align="left" class="black11normal">

                            <select name="parent_menu" id="parent_menu" class="input200x20">

                                <option value="0"<?php if($parent_menuval=='0'){echo ' selected="selected"';}?>>Root</option>

                                <?php

                                $result = $commonmodel->getallrowbysqlquery("select * from menu where menu_publish=1 and parent_menu = 0");

                                

                                if(count($result)>0 && $result !=''){

                                    foreach($result as $row){

                                        $table_id = $row->menu_id;

                                        $table_field = $row->menu_name;

                                        $selected = '';

                                        if($table_id==$parent_menuval){$selected = ' selected="selected"';}

                                        ?>

                                        <option value="<?php echo $table_id;?>" <?php echo $selected;?>><?php echo $table_field;?></option>

                                        <?php

                                    }

                                }

                                ?>

                            </select>

                        </td>

                    </tr>

                    <tr>

                        <td class="black11bold" align="left">Menu Name: <font class="red14bold">*</font></td>

                        <td align="left" class="black11normal">

                            <?php 

                            $data = array('name'     => 'menu_name',

                                          'id'       => 'menu_name',

                                          'tabindex'       => '2',

                                          'value'    => $menu_nameval,

                                          'maxlength'=> '250',

                                          'class'    => 'input300x20'

                                        );															

                            echo form_input($data);

                            ?>

                            <span id="errmsg_menu_name" class="errormsg"></span>

                        </td>

                    </tr>

                    <tr>

                        <td class="black11bold" align="left">Table Name: <font class="red14bold">*</font></td>

                        <td align="left" class="black11normal">

                            <?php 

                            $data = array('name'     => 'menu_module',

                                          'id'       => 'menu_module',

                                          'tabindex'       => '2',

                                          'value'    => $menu_moduleval,

                                          'maxlength'=> '250',

                                          'class'    => 'input300x20'

                                        );															

                            echo form_input($data);

                            ?>

                            <span id="errmsg_menu_module" class="errormsg"></span>

                        </td>

                    </tr>

                    <tr>

                        <td class="black11bold" width="180" align="left">Module Type:</td>

                        <td align="left">

                            <label><input type="radio" name="module_type" value="Yes" <?php if($module_typeval=='Yes'){echo ' checked="checked"';}?> /> Dynamic</label>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <label><input type="radio" name="module_type" value="No" <?php if($module_typeval=='No'){echo ' checked="checked"';}?> /> Manual</label>

                        </td>

                    </tr>

                    

                    <tr>

                        <td align="left" class="black11bold"></td>

                        <td align="left" class="black11normal" id="finalsaverow">

                            <?php 

                            $data = array( 'name'    => 'submit',

                                           'value'   => $submitval,

                                           'tabindex'=> '4',

                                           'class'   => 'submitbutton');															

                            

                            echo form_submit($data);

                            $data = array( 'name'    => 'reset',

                                           'value'   => ' Reset ',

                                           'tabindex'=> '5',

                                           'class'   => 'submitbutton');

                                                                                                    

                            echo '&nbsp;&nbsp;'.form_reset($data);

                            ?>

                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_idval;?>" />

                            <input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />

                        </td>

                    </tr>									

                </table>

            <?php echo form_close(); ?>

       	</td>

   	</tr>

</table>