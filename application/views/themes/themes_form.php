<?php
if(isset($themes_publish['value']) && $themes_publish['value'] !=''){ 
	$themes_publishcheckedval = $themes_publish['value'];
	if($themes_publishcheckedval>0){
		$themes_publishcheckedval = ' checked="checked"';
	}
	else{
		$themes_publishcheckedval = '';
	}
}
else{ $themes_publishcheckedval = ' checked="checked"';}

if(isset($themes_name['value']) && $themes_name['value'] !=''){ $themes_nameval = $themes_name['value'];}
else{ $themes_nameval = set_value('themes_name');}

if(isset($themes_folder['value']) && $themes_folder['value'] !=''){ $themes_folderval = $themes_folder['value'];}
else{ $themes_folderval = set_value('themes_folder');}

if(isset($themes_id['value']) && $themes_id['value'] !=''){ $themes_idval = $themes_id['value'];}
else{ $themes_idval = set_value('themes_id');}
if($themes_idval==''){$themes_idval = 0;}

if(isset($themes_id['value']) && $themes_id['value'] !=''){ $themes_idval = $themes_id['value'];}
else{ $themes_idval = set_value('themes_id');}
if($themes_idval==''){$themes_idval = 0;}

if(isset($themes_thumbimage['value']) && $themes_thumbimage['value'] !=''){ $themes_thumbimageval = $themes_thumbimage['value'];}
else{ $themes_thumbimageval = set_value('themes_thumbimage');}
if($themes_thumbimageval==''){$themes_thumbimageval = 'no_images_120x120.png';}
$thumbimagesrc = str_replace('/index.php','',site_url("/")).'application/views/themes/themes_images/'.$themes_thumbimageval;

if(isset($action_type['value']) && $action_type['value'] !=''){	$action_typeval = $action_type['value'];}
else{ $action_typeval = set_value('action_type');}
if($action_typeval == ''){$action_typeval = 0;}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

?>

<script language="JavaScript" type="text/javascript">
var j = jQuery.noConflict();
function checkfrmthemes_form(){
	var themes_folder = document.frmthemes.themes_folder;
	var elementmessage = document.getElementById("errmsg_themes_folder");
	elementmessage.innerHTML = '';
	j('#themes_folder').removeClass('error_input300x20').addClass('input300x20');
	if(themes_folder.value==''){
		elementmessage.innerHTML = 'You are missing Folder name';
		themes_folder.focus();
		j('#themes_folder').removeClass('input300x20').addClass('error_input300x20');
		return false;
	}
	else if(checkonlyNumericcharVal(themes_folder.value)==false){
		elementmessage.innerHTML = 'Invalid  folder name. Only Numeric and character allowed.';
		themes_folder.focus();
		j('#themes_folder').removeClass('field400').addClass('field400_err');
		return false;
	}
	
	var themes_name = document.frmthemes.themes_name;
	var elementmessage = document.getElementById("errmsg_themes_name");
	elementmessage.innerHTML = '';
	j('#themes_name').removeClass('error_input300x20').addClass('input300x20');
	if(themes_name.value==''){
		elementmessage.innerHTML = 'You are missing photo name';
		themes_name.focus();
		j('#themes_name').removeClass('input300x20').addClass('error_input300x20');
		return false;
	}
	
	var themes_id = document.frmthemes.themes_id.value;
	if(themes_id==0){
		var elementmessage = document.getElementById("errmsg_themes_image");
		elementmessage.innerHTML = '';
		if(j('#themes_image').val()==''){
			elementmessage.innerHTML = 'You are missing image.';
			return false;
		}
		else if(check_images('themes_image')==false){
			elementmessage.innerHTML = 'You selected image is invalid.';
			return false;
		}
	}
	else if(j('#themes_image').val() !=''){
		if(check_images('themes_image')==false){
			elementmessage.innerHTML = 'You selected image is invalid.';
			return false;
		}
	}
	return true;
}
</script>
<table id="blueborder" width="650" align="center" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="50%">
					Themes Information Form
				</td>
				<td align="right">
					<a href="<?php echo site_url("/").'themes/themes_list';?>" class="white14bold">Show Themes List</a>
				</td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
			<?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
	<tr>
		<td align="center">	
			<?php
			$attributes = array('name' => 'frmthemes', 'onsubmit'=>'return checkfrmthemes_form();');
			echo form_open_multipart(site_url("/").'themes/save_themes', $attributes);
			?>
				<table width="100%" border="0" cellspacing="7" cellpadding="0">										
					<tr>
						<td class="black11bold" width="130" align="left">Publish:</td>
						<td align="left">
							<input type="checkbox" tabindex="1" name="themes_publish" value="1" <?php echo $themes_publishcheckedval; ?> />
						</td>
					</tr>
					<tr>
						<td class="black11bold" align="left">Folder Name: <font class="red14bold">*</font></td>
						<td align="left" class="black11normal">
							<?php 
							$data = array('name'     => 'themes_folder',
										  'id'       => 'themes_folder',
										  'tabindex'       => '2',
										  'value'    => $themes_folderval,
										  'maxlength'=> '250',
										  'class'    => 'input300x20'
										);															
							echo form_input($data);
							?>
							<span id="errmsg_themes_folder" class="errormsg"></span>
						</td>
					</tr>
					<tr>
						<td class="black11bold" align="left">Themes Name: <font class="red14bold">*</font></td>
						<td align="left" class="black11normal">
							<?php 
							$data = array('name'     => 'themes_name',
										  'id'       => 'themes_name',
										  'tabindex'       => '2',
										  'value'    => $themes_nameval,
										  'maxlength'=> '250',
										  'class'    => 'input300x20'
										);															
							echo form_input($data);
							?>
							<span id="errmsg_themes_name" class="errormsg"></span>
						</td>
					</tr>
					<tr>
						<td class="black11bold" align="left">Themes Image: <font class="red14bold">*</font></td>
						<td align="left" class="black11normal">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td>
									<input type="file" name="themes_image" id="themes_image" class="input300x18" /><br />
									<span id="errmsg_themes_image" class="red11normal"></span>
								</td>
								<td>
									<?php echo "<img src=\"$thumbimagesrc\" border=\"0\" alt=\"$themes_nameval\" height=\"100\">";?>
								</td>
							  </tr>
							</table> 
						</td>
					</tr>
					<tr>
						<td align="left" class="black11bold"></td>
						<td align="left" class="black11normal">
							<?php 
							$data = array( 'name'    => 'submit',
										   'value'   => $submitval,
										   'tabindex'=> '4',
										   'class'   => 'submitbutton');															
							
							echo form_submit($data);
							$data = array( 'name'    => 'reset',
										   'value'   => ' Reset ',
										   'tabindex'=> '5',
										   'class'   => 'submitbutton');
																									
							echo '&nbsp;&nbsp;'.form_reset($data);
							?>
							<input type="hidden" name="themes_id" id="themes_id" value="<?php echo $themes_idval;?>" />
							<input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />
						</td>
					</tr>							
					<tr>									
				</table>
			<?php echo form_close(); ?>
		</td>
	</tr>	
</table>