<?php

$site_url = str_replace('/index.php','',site_url("/"));

?>
<div class="inner_page">
    <h1 class="conHead"><?php echo $heading_name;?></h1>
    <?php 
    if(isset($table_name) && $table_name=='latest_event'){
   ?> 
   <?php 

        if($event_image == ''){$image_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$image_src = $site_url.'application/views/module/event_image/'.$event_image;}    

        if($event_image_thumb == ''){$thumbimage_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$thumbimage_src = $site_url.'application/views/module/event_image/'.$event_image_thumb;}   

        ?>
        <a class="show_largeImge" href="<?php echo $image_src;?>" title="<?php echo $heading_name;?>">
            <img src="<?php echo $thumbimage_src;?>" style="float:left;margin-top:5px; margin-right:10px;" alt="<?php echo $heading_name;?>" />
        </a>
        <!--<span style="position:relative;top:85px;left:-65px;font-size:10px;font-style:italic;font-weight:bold;color:#4B8A45;"><?php echo $event_date = date("d M Y", $event_date);  ?></span>-->
        <?php
        echo $heading_content; ?> 
        <?php
    } 
	else if(isset($table_name) && $table_name=='news'){
   ?> 
   <?php 

        if($news_image == ''){$image_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$image_src = $site_url.'application/views/module/news_image/'.$news_image;}    

        if($news_image_thumb == ''){$thumbimage_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$thumbimage_src = $site_url.'application/views/module/news_image/'.$news_image_thumb;}   

        ?>
        <a class="show_largeImge" href="<?php echo $image_src;?>" title="<?php echo $heading_name;?>">
            <img src="<?php echo $thumbimage_src;?>" style="float:left;margin-top:5px; margin-right:10px;" alt="<?php echo $heading_name;?>" />
        </a>
        <?php
        echo $heading_content; ?> 
        <?php
    }     
    else if(isset($table_name) && $table_name=='programs'){
        ?>        
        <script language="JavaScript" type="text/javascript">			

			function showpopupformessage(message) {

				if(message !=''){			

					$('#likemessage').html(message);
					var popID = 'show_showfacebox'; //Get Popup ID
					var popURL = '#?w=400'; //Get Popup href to define size

					show_popup(popID, popURL);				

					var myInterval = window.setInterval(function (a,b) {
					  //myNumber++;
					},1000);

					window.setTimeout(function (a,b) {
					  clearInterval(myInterval);
					  close_popup();
					},5000);
				}
			}

        </script> 
        <?php 

        if($programs_image == ''){$image_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$image_src = $site_url.'application/views/module/programs_image/'.$programs_image;}    

        if($programs_image_thumb == ''){$thumbimage_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}
        else{$thumbimage_src = $site_url.'application/views/module/programs_image/'.$programs_image_thumb;}           

        ?>

        <a class="show_largeImge" href="<?php echo $image_src;?>" title="<?php echo $heading_name;?>">
            <img src="<?php echo $thumbimage_src;?>" style="float:left;" alt="<?php echo $heading_name;?>" />
        </a>       

        <div id="show_showfacebox" class="popup_block" style="color:#000">
            <center>
                <div style="width:100%;overflow:hidden;">
                    <div id="likemessage"></div>
                </div>
            </center>
        </div>       

        <?php echo $heading_content; ?>
<?php 
    }
    else{
?> 
<?php echo $heading_content; ?>
<?php
    }

    if(isset($table_name) && $table_name=='blog'){
        $front_data['blog_id'] = $table_idvalue;
        $front_data['table_name'] = $table_name;
        $this->load->view('common_front_module/front_blog_comments', $front_data);
    } 
?>
    <!--<a href="<?php echo site_url("/");?>" class="back_to_home">Back to Home</a>-->
</div><!-- end:page_Inner -->