<?php

if ($this->uri->segment(3) === FALSE){$segment3name = '';}

else{$segment3name = $this->uri->segment(3);}



if ($this->uri->segment(2) === FALSE){$segment2name = '';}

else{$segment2name = $this->uri->segment(2);} 



if ($this->uri->segment(1) === FALSE){$segment1name = '';}

else{$segment1name = $this->uri->segment(1);} 



if($this->session->userdata('site_seo_title')){

	$site_name = $this->session->userdata('site_name');

	$site_seo_title = $this->session->userdata('site_seo_title');

	$site_seo_description = $this->session->userdata('site_seo_description');

	$site_seo_keywords = $this->session->userdata('site_seo_keywords');	

	$site_current_theme = $this->session->userdata('site_current_theme');

	$author_telephone = $this->session->userdata('author_telephone');

	$author_address = $this->session->userdata('author_address');

}

else{

	if($this->Common_model->get_global_config()){

		$site_name = $this->session->userdata('site_name');

		$site_seo_title = $this->session->userdata('site_seo_title');

		$site_seo_description = $this->session->userdata('site_seo_description');

		$site_seo_keywords = $this->session->userdata('site_seo_keywords');	

		$site_current_theme = $this->session->userdata('site_current_theme');

		$author_telephone = $this->session->userdata('author_telephone');

		$author_address = $this->session->userdata('author_address');

	}

	else{

		$global_data = $this->Common_model->get_global_configdata();

		$site_name = $global_data['site_name'];

		$site_seo_title = $global_data['site_seo_title'];

		$site_seo_description = $global_data['site_seo_description'];

		$site_seo_keywords = $global_data['site_seo_keywords'];

		$site_current_theme = $global_data['site_current_theme'];

		$author_telephone = $global_data['author_telephone'];

		$author_address = $global_data['author_address'];

	}

}



if(isset($meta_title) && $meta_title !=''){$site_seo_title = $meta_title;}

if(isset($meta_keyword) && $meta_keyword !=''){$site_seo_keywords = $meta_keyword;}

if(isset($meta_description) && $meta_description !=''){$site_seo_description = $meta_description;}



$site_url = str_replace('/index.php','',site_url("/"));

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo $site_seo_description;?>" />
<meta name="keywords" content="<?php echo $site_seo_keywords;?>" />
<title><?php echo $site_name;?></title>
<link rel="shortcut icon" href="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/favicon.ico" />
<link href="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $site_url;?>common/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/css/main.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/css/dropdown-menu.css" rel="stylesheet" type="text/css" />

   	<script type="text/javascript" src="<?php echo $site_url?>common/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/js/bootstrap.min.js"></script>
	<script src="<?php echo $site_url?>common/autocomplete/jquery.ui.core.min.js"></script>

	<script src="<?php echo $site_url?>common/autocomplete/jquery.ui.widget.min.js"></script>

    <script src="<?php echo $site_url?>common/autocomplete/jquery.ui.position.min.js"></script>

    <script src="<?php echo $site_url?>common/autocomplete/jquery.ui.autocomplete.min.js"></script>

    <link rel="stylesheet" href="<?php echo $site_url?>common/autocomplete/jquery-ui-1.8.16.custom.css"/>

    <link rel="stylesheet" href="<?php echo $site_url?>common/autocomplete/autocomplete.css"/>

	<script language="JavaScript" type="text/javascript" src="<?php echo $site_url;?>common/js/common.js"></script>
<!--[if IE 6]><script src="<?php echo $site_url;?>common/js/ie6PngFix.js"></script><![endif]-->
<script language="JavaScript" type="text/javascript">
	//var j = jQuery.noConflict();	
	var common_url = '<?php echo $site_url?>common/';
	var pathToImages = '<?php echo $site_url?>common/calendar/';
	var afterloadmovemiddle = 0;
	var modify_url = '<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/';
	//var j = jQuery.noConflict();	
	$(document).ready(function(){
		onloadbody();
	});

	function onloadbody(){	
		$("#pageloadingbar").fadeIn(100);	
	}

	function deleteloading(){
		$("#pageloadingbar").fadeOut(100);
		$("#pageloadingbar").hide();
	}
</script>
<script type="text/javascript" src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/js/jqueryslidemenu.js"></script>	
<script src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/js/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo $site_url;?>common/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site_url;?>common/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

<script language="JavaScript" type="text/javascript">   

    $(document).ready(function(){
		
        $("a.show_largeImge").fancybox({
            'overlayShow'	: false,
            'transitionIn'	: 'elastic',
            'transitionOut'	: 'elastic'
        });
    });          

</script>  
<script type="text/javascript">
	$(function() {
		$('#carousel').carouFredSel({
			width: '100%',
			items: {
				visible: 3,
				start: -1
			},
			scroll: {
				items: 1,
				duration: 1000,
				timeoutDuration: 4000
			},
			prev: '#prev',
			next: '#next',
			pagination: {
				container: '#pager',
				deviation: 1
			}
		});
	});
</script>		
<link href="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/css/carousel.css" rel="stylesheet" type="text/css" />
</head>
<body onunload="deleteloading()">

  <div id="pageloadingbar" style="display:none; position:absolute; top:75px; left:550px; z-index:999;">

        <img src="<?php echo $site_url;?>common/images/032.gif" alt="Page Loading.."/>

    </div> 
<div id="wrapper00">
<div id="wrapper0">
<div id="wrapper">
<!--start top-->
<div id="top">                       
<a class="logo" href="<?php echo site_url("/");?>" title="SUFFA"></a>

<div class="top_search">
    <?php $this->load->view('search/front_site_search_form');?>
</div>
</div>
<div class="navbar" role="navigation">
        
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>            
    <a class="navbar-brand" href="<?php echo site_url("/");?>" title="SUFFA">Suffa</a>
  </div>
  <div class="navbar-collapse collapse">
    <div id="myslidemenu" class="jqueryslidemenu">
        <?php 
            $front_data['front_menu_category_id'] = 1;
            $this->load->view('front_menu/mainmenu', $front_data);
        ?>    
    </div>
    
  </div><!--/.nav-collapse -->        
</div>
<!--stop top-->                    
    

<?php
if($segment1name=='' || $segment1name=='home'){

 $this->load->view('common_front_module/front_banner');
}
?> 
             
                    
<div class="content">
    
<?php
if($segment1name !='students' && $segment1name !='teacher'){?>   
	<div class="con_left0">
         <?php $this->load->view($front_pagemiddle);?>
        
    </div> 
    <div class="con_right">
        <h2 class="h2class2"><a href="<?php echo $site_url?>admission">Admissions</a></h2>
        <a title="Apply Now" href="<?php echo $site_url?>students/signup"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/apply-now.gif" alt="Apply Now" class="float_right"></a>
        <h2 class="h2class2"><a href="<?php echo str_replace('index.php/','',$site_url);?>common/uploaded/suffa-student-application-form.pdf" target="_blank">Download Registration form</a></h2>
        
        
        <a title="Give to SUFFA" href="<?php echo str_replace('index.php/','',$site_url);?>donate"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/give-to-Suffa.gif" alt="Give to SUFFA" class="float_right"></a>
        
        <div id="demo" style="margin-bottom:20px;float:left;width:100%;">
        
            <div class="academic_calendar"><a href="<?php echo $site_url?>common/uploaded/suffa-spring-courses-2015.jpg" title="Download the spring semester 2015 schedule" target="_blank">DOWNLOAD <br /> The spring semester 2015 schedule</a></div>
    
    </div>

    <div style="float:left;padding-top:20px;">
    
    <?php $this->load->view('common_front_module/latest_event.php');?>
    
    </div>
    <div class="mailing" style="display:none;">
        <h2 class="h2class2">Mailing List</h2>        
        
        <div class="mailing2">
            <img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/mailing.gif" alt="Mailing List" class="float_left" />
            <p>To receive email updates enter your email address: <br />
                <input name="ctl00$ctl05$txtEmail" type="text" id="ctl00_ctl05_txtEmail" title="Your E-mail address" class="form2" />
               
    <span id="ctl00_ctl05_RequiredFieldValidator6" class="float_left" style="color:Red;display:none;">* Enter Email address</span>
    <span id="ctl00_ctl05_regEmail" class="float_left" style="color:Red;display:none;">* Invalid Email address</span>
               
                <input type="submit" name="ctl00$ctl05$Button1" value="Subscribe" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl05$Button1&quot;, &quot;&quot;, true, &quot;SubscriptionForm&quot;, &quot;&quot;, false, false))" id="ctl00_ctl05_Button1" title="Subscribe" class="btn2" />
    
            </p>
        </div>
    </div>
    <div class="social">
            <h2 class="h2class2">Follow Us</h2>                    
    
            <a title="Facebook" href="https://www.facebook.com/SuffaIslamicSeminary" target="_blank"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/Facebook.png" /></a>
        
            <a title="Twitter" href="" target="_blank"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/Twitter.png" /></a>
        
            <a title="Youtube" href="" target="_blank"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/Youtube.png" /></a>
            <a title="Instagram" href="" target="_blank"><img src="<?php echo $site_url?>application/views/themes/<?php echo $site_current_theme?>/images/Instagram.png" /></a>
        </div>   
                
                
    </div>
<?php 
  } else{ ?>
  
   <?php $this->load->view($front_pagemiddle);?>

<?php	  
	  }
?>           
   </div><!-- End .content-->
                    
   </div>
   </div>
        
</div>

<div id="footer00">                   

<div id="footer0">
        <div id="footer">
            <div id="footer1">
            
                <div class="ft_left">
                
                    <div class="row0">
                     <!--<div style="float:left;"><img src="<?php echo $site_url?>application/views/themes/suffa/images/NCA.png" alt="NCA" class="float_left" style="padding-top:10px;" /></div>-->
                     <!-- Begin: footer_menu -->

                    <?php                    
					$front_data['front_menu_category_id'] = 2;		
					$this->load->view('front_menu/menubycategory', $front_data);?>
                    </div>
                    
                    <div class="float_left">Copyright &copy; 2014 SUFFA. All Rights Reserved.<br />
                        <p>Website designed & developed by <a href="http://intrigueit.com/" class="plink" title="Intrigue IT" target="_blank">Intrigue IT.</a></p>
                    </div>
                </div>
                
                <div class="ft_right">
                    
                    <div class="ft_td2"><a href="../https@maps.google.com/maps@q=IANT+Quranic+Academy,+Abrams+Road,+Richardson,+TX&hl=en&ll=32.949335,-96.730843&spn=0.036157,0.072956&s08C86EC4A5" target="_blank">840 Abrams Road<br />Richardson, TX 75081</a></div>
                    <div class="ft_td2">972-231-5698</div>
                    <div class="ft_td2"><a href="mailto:suffaislamicseminary@gmail.com">suffaislamicseminary@gmail.com</a></div>                    
                </div>      
            
            </div>
        </div>

    </div>
</div>    
<script language="JavaScript" type="text/javascript">	

        $(document).ready(function(){

			deleteloading(); 

		});

    </script>
</body>
</html>