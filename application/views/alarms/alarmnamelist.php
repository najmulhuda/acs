<?php
 $functionname = 'Alarm';
 $site_url = str_replace('/index.php','',site_url("/"));
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">

<script language="JavaScript" type="text/javascript">

	j=jQuery.noConflict();

	j(document).ready(function($) {

		initPagination();

	});

	 function pageselectCallback(page_index, jq){

		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();

		j('#Searchresult').empty().append(new_content);

		return false;

	}

	function initPagination() {

		

		// count entries inside the hidden content

		var num_entries = j('#hiddenresult div.result tr').length-1;
       
		// Create content inside pagination element

		j("#Pagination").pagination(num_entries, {

			callback: pageselectCallback,

			items_per_page:20 // Show only one item per page

		});

	 }  
    
	
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                 <?php echo $functionname; ?> List

                </td>

                <td align="right">
                   <a href="<?php echo site_url("/").'alarms/addalarm';?>" class="white14bold">Add New Alarm</a>
                </td>

              </tr>

            </table>			

		</td>

	</tr>
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	<tr>
     
		<td align="center">			

			<?php

			if(isset($data) && count($data)>0){
			 
			?>	

				<div style="width:99%; overflow:hidden;">				

					<div id="Searchresult">

						This content will be replaced when pagination inits.

					</div>

					<div style="padding:0 10px 0 20px;display:none" class="black12bold">Total <?php echo count($data);?> data or <?php echo ceil(count($data)/10);?> pages found </div>

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">

						<table width="100%" cellpadding="0" cellspacing="1" border="0">

							<tr>

								<th class="titlerow" align="center" width="5%">#</th>

								<th class="titlerow" align="left" width="10%">Name</th>
								
								<th class="titlerow" align="left" width="10%">Alarm Type</th>

                                <th class="titlerow" align="center" width="15%">Action</th>																					
							</tr>

							<?php

							$str = "";

							$i = 1;

							foreach($data as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;

								$name = $row->name;
                                
								$alarmTypeName = $row->alarmTypeName;
								
								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';

								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';

								
								$str .= "<tr>

									<td align=\"center\" class=\"$classname\">$id</td>
									
									<td align=\"left\" class=\"$classname\">$name</td>

									<td align=\"left\" class=\"$classname\">$alarmTypeName</td>";
								
									$str .= "<td class=\"$classname\" align=\"center\">";

										$str .= "
                                                 <!--<a href=\"$site_url"."alarms/editalarm/$id\" title=\"Edit\">
                                                 <img src=\"$editimg\" border=\"0\" alt=\"Edit\">-->
                                                 </a> &nbsp;&nbsp;&nbsp;
												 <a onclick=\"tablerow_remove($id, 'dfalarms', 'dfalarms', 'dfalarms');\" title=\"Remove\">
                                                 <img src=\"$removeimg\" border=\"0\" alt=\"Remove\">
                                                 </a>";

									
									$str .= "</td>";

								

								

								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				echo "<div class=red18bold>No record found</div>";					

			}								

			?>

		</td>

	</tr>

</table>