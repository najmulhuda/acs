<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_addalarmstatus', 'onsubmit'=>'return check_addalarmstatus();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'alarms/editalarmstatus/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'alarms/addalarmstatus/', $userformattributes); 
 }
  $alarmbytype_fetchurl = site_url("/").'alarms/alarmbytype/';
?>
<script type="text/javascript">
   var j = jQuery.noConflict();
   var alarm_fetchurl = '<?php echo $alarmbytype_fetchurl;?>';
   
   function getAlarmName(){
        var alarm_TypeId = j('#alarm_type').val();
		//alert(alarm_fetchurl);
		
		j.post(alarm_fetchurl, {"alarm_TypeId":alarm_TypeId},

		function(data) {
            j('#alarm_name').html("");
			j('#alarm_name').append(data);			

		});

		return true;

	}
</script>
<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   /*foreach($data as $row){
		 $id =   $item->id;
		 $alarmid  = $item->alarmid;
		 $name = $item->name; 
		 $alarmstatus =	$item->alarmstatus;
		 $alarmtypename = $item->alarmtypename;
		 $alarmtypeid =	$item->alarmtypeid;
		 $description =	$item->description; 
	   }*/	
	}	
}else{
	 $id = "";
	 $alarmid = "";
	 $name = "";
	 $alarmstatus = "";
	 $alarmtypename = "";
	 $alarmtypeid = "";
	 $description = "";
}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add Alarm Status

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'alarms/alarmstatuslist';?>" class="white14bold">Show Alarm Status List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>
    
    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>

					<td align="right" class="black12bold">Alarm Type <span class="red12bold">*</span>:</td>

					<td align="left">

						 <?php
					   echo "<select name='alarm_type' id='alarm_type' onclick='getAlarmName()'>";
					   echo "<option value=''>--Select--</option>";
                        if(is_array($alarmtypedata) && count($alarmtypedata)>0) {
	                       foreach ($alarmtypedata as $row1) {
							 if($alarmtypeid==$row1->id){
								 echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";
							 }else{
								echo "<option value='". $row1->id . "'>" . $row1->name . "</option>"; 
							 }  
		                      
	                      }

                         }	
					   echo "</select>";
					  ?>	
                     


						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												
			   
			   
			   
			   <tr>

					<td align="right" width="30%" class="black12bold">Alarm Name <span class="red12bold">*</span>:</td>

					<td align="left">

						 <?php
						 
					     echo "<select name='alarm_name' id='alarm_name'></select>";
                         
					     ?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

			     <!--<tr>

					<td align="right" width="30%" class="black12bold">Alarm Status <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'alarm_status',

									  'id'       => 'alarm_status',

									  'tabindex'       => '1',

									  'value'    => '',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>-->

				<tr>

					<td align="right" class="black12bold">Description <span class="red12bold"></span>:</td>

					<td align="left">

						<?php 
                              $data = array(
                                  'name'        => 'description',
                                  'id'          => 'description',
                                  'value'       => '',
                                  'rows'        => '5',
                                  'cols'        => '10',
                                  'style'       => 'width:50%',
                                );

                       echo form_textarea($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												
			
			   
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>