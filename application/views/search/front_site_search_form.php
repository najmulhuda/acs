<?php

$results = '';	

$tablearray = array('page_info', 'services', 'latest_event', 'our_objectives', 'programs');

foreach($tablearray as $tablename){

	

	$tablepublishname = $tablename.'_publish';

	$sql = "SELECT name, uri_name FROM $tablename where $tablepublishname = 1 order by name asc";

	$productlist = $this->Common_model->getallrowbysqlquery($sql);	

	if($productlist){

		$i=1;

		foreach($productlist as $onerow){

			$name = str_replace('"','',str_replace("'",'',stripslashes($onerow->name)));			

			$uri_name = $onerow->uri_name;			

			$shortname = $name;

			if(strlen($name)>80){

				$shortname = $this->Common_model->limit_words( $name, 8, $append_str='...' );

			}

			

			$results[] = array( 'label' => $name,

								'pn'=>$shortname,

								'ur'=>$uri_name

								);

			$i++;

		}			

	}

	

}

$results = json_encode($results);

?>

<style type="text/css">

.search_row{ width:270px;border-bottom:1px dotted #CCC; padding:5px 0; text-align:left; float:left;}

.searchproductcount{ float:left; width:65px; text-align:right;}

.seachname{ float:left; width:190px;}

</style>

<script type="application/javascript">

	

	

	function check_sitesearch_form(){

		var oField = document.getElementById("search_site");

		if(oField.value == "Search..." || oField.value == ''){

			oField.focus();

			return(false);

		}

		if(oField.value.length<2){

			oField.focus();

			return(false);

		}

		return(true);

	}

	var data = <?php echo $results;?>;

	var siteurl = '<?php echo site_url("/");?>';

	

	$(function() {

		$( "#search_site" ).autocomplete({

			source:data,

			select: function( event, ui ) {

				$( "#search_site" ).val( ui.item.ur );

				window.location=siteurl+ui.item.ur;

				return false;

			}

		}).data( "autocomplete" )._renderItem = function( ul, item ) {

			return $( "<li></li>" )

			.data( "item.autocomplete", item )

			.append("<a title=\""+item.label+"\">"+

						"<div class=\"search_row\">"+

							"<div class=\"seachname\">"+item.pn+"</div>"+

						"</div>"+

					"</a>")

			.appendTo(ul);			

		};

	});

</script>

<?php

$sitesearchformattributes = array('name' => 'frm_sitesearch_form', 'method' => 'get', 'onsubmit'=>'return check_sitesearch_form();');

echo form_open(site_url("/").'search/site_search/', $sitesearchformattributes);

?>

    <input id="search_site" name="s" class="srch_form" maxlength="100" type="text" placeholder="Search..." />

    <input type="submit" class="btn3" value=" GO " />

<?php 

echo form_close();

?>