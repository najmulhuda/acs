<div class="offer_item"><!-- Begin: offer_item -->
	<h3 class="conHead">JDocs Blog</h3>
	<?php
	$site_url = str_replace('/index.php','',site_url("/"));
	if ($this->uri->segment(2) === FALSE){
		$segment2name = '';
	}
	else{
	   $segment2name = $this->uri->segment(2);
	}
	if($segment2name !='' && $segment2name=='blog_hidelist'){
		$functionname = 'Unpublished Blog';
		$publishval = 0;
	}
	else{
		$functionname = 'Blog';
		$publishval = 1;
	}
	$blogtotalrows = $commonmodel->getcount_all_resultsrows('blog', array('blog_publish'=>$publishval), 'modified_date');
	$blog_fatchingurl = site_url("/").'common_controller/front_blog_list/';
	?>

	<style type="text/css">
    .pagination {font-size: 80%; width:100%; float:right; margin-bottom:10px;}
    .pagination a {text-decoration: none;border: solid 1px #AAE;color: #15B;}
    .pagination a, .pagination span {display: block;float: left;padding: 0.3em 0.5em;margin-right: 5px;margin-bottom: 5px; min-width:16px; 
        text-align:center;}
    .pagination .current {background: #26B;color: #fff;border: solid 1px #AAE;}
    .pagination .current.prev, .pagination .current.next{color:#999;border-color:#999;background:#fff;}
    </style>
    <script type="text/javascript">
       var j = jQuery.noConflict();
       var blogfetchurl = '<?php echo $blog_fatchingurl;?>';
       function pageselectCallback(page_index, jq){
            var publishval = '<?php echo $publishval;?>';
            var blog_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
            var show_by_order=document.getElementById('show_by_order').value;
            var starting_val = parseInt(page_index*blog_per_page);
            j.post(blogfetchurl, {publishval:publishval, show_by_order:show_by_order, starting_val: starting_val, blog_per_page: blog_per_page},
            function(data) {
                j('#Searchresult').html(data);
            });
            return true;
        }
    
        function getOptionsFromForm(){
            var opt = {callback: pageselectCallback};
            return opt;
        }
        function loadpagination(){
            var optInit = getOptionsFromForm(); // Create pagination element with options from form
            var totalrows_count = '<?php echo $blogtotalrows;?>';
            
            var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
            j("#Pagination").pagination(parseInt(totalrows_count), optInit,data_per_page);
        }
        function show_by_orderlist(show_by_orderval){
            document.getElementById('show_by_order').value = show_by_orderval;
            loadpagination();        
        }
        j(document).ready(function(){               
            loadpagination();
        });
        
    </script>
    <script src="<?php echo $site_url?>common/js/topPagination.js" type="text/javascript" language="javascript"></script>
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr class="pagination_tr">
        <td width="30%">
            <!--Here shows pagination============================================================-->
            <div style="width:90%; height:20px;">
               <div id="Pagination" class="pagination"></div>
            </div>
        </td>
        <td class="pagination_tr_middle" width="40%">
            Select your limits:
            <select id="list_per_page" class="pagination_tr_middleselect" name="list_per_page" onchange="loadpagination();">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
            </select>
            <input type="hidden" name="show_by_order" id="show_by_order" value="showing_order asc" />
        </td>
        <td width="30%">&nbsp;</td>
      </tr>
    </table>

	<div class="product_details">
   		<div class="rightContentAreaCon" id="Searchresult"></div>
	</div>
    <?php $this->load->view('others_sidebar')?>
</div>




