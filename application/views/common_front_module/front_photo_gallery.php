<?php

$site_url = str_replace('/index.php','',site_url("/"));

?>

<div class="mainPageCon"><div class="mainPageConIn"><div class="mainPageConInner">  

	<div class="inner_page">

        <h1 class="conHead">Photo Gallery</h1>



        <div class="photo_gallery1">

            <ul>

                <?php

                $queryphoto_gallery = $commonmodel->getallrowbysqlquery("select * from gallery where gallery_publish = 1 order by showing_order desc");

                if(count($queryphoto_gallery)>0){

                    $i = 0;

                    foreach($queryphoto_gallery as $photo_galleryrow){

                        $photo_gallery_id = $photo_galleryrow->gallery_id;

                        $photo_gallery_name =$photo_galleryrow->name;

                        

                        $gallery_image = $photo_galleryrow->gallery_images;

                        if($gallery_image == ''){$big_image_src = str_replace('/index.php','',site_url("/")).'common/images/picturecomingsoon.jpg';}

                        else{$big_image_src = $site_url.'application/views/module/gallery_images/'.$gallery_image;}

                        

                        $gallery_image_thumb = $photo_galleryrow->gallery_images_thumb;

                        if($gallery_image_thumb !=''){

                            $photo_gallery_image_src = $site_url.'application/views/module/gallery_images/'.$gallery_image_thumb;

                            ?>

                            <li>

                                <a class="show_largeImge" href="<?php echo $big_image_src;?>" title="<?php echo $photo_gallery_name;?>">

                                    <img src="<?php echo $photo_gallery_image_src;?>" alt="<?php echo $photo_gallery_name;?>" />

                                </a>

                                <h3><?php //echo $photo_gallery_name;?></h3>

                            </li>

                            <?php

                        }

                    }

                }

                else{

                    ?>

                    <li>

                        There is no gallery image found.

                    </li>

                    <?php 

                }?>

            </ul>

        </div>


    </div>



</div></div></div>