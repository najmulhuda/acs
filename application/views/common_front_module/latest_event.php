<h2 class="h2class2">Upcoming Events</h2>
<div class="mailing2">
                
  <marquee direction="up" height="200" scrollamount="3">                     
    <ul>
<?php

$site_url = str_replace('/index.php','',site_url("/"));

	$querywirealess = $commonmodel->getallrowbysqlquery("select * from latest_event where latest_event_publish = 1 order by showing_order asc");

	if(count($querywirealess)>0){

		foreach($querywirealess as $font_querywirealess){

			$latest_event_id = $font_querywirealess->latest_event_id;

			$name =stripcslashes( $font_querywirealess->name);
			$event_date = date("d M Y", $font_querywirealess->event_date);

			$uri_name = $font_querywirealess->uri_name;

			$description = $this->Common_model->limit_words(nl2br(strip_tags($font_querywirealess->description)),8, '').'..';

			$details_url = site_url("/").$uri_name;

			$event_image_thumb = $font_querywirealess->event_image_thumb;

				if($event_image_thumb==''){
					$event_image_thumburl = $site_url.'common/images/no_images_120x120.png';
				}

				else{
					$event_image_thumburl = $site_url.'application/views/module/event_image/'.$event_image_thumb;	
				}   

			?>
                
                    
                        <li><span class='date2'><?php echo $event_date;?></span><br />
                        <a href="<?php echo $details_url?>"><?php echo $name;?></a><br />
                        <?php echo $description;?></li>
                    
<?php }}?>


    </ul>
  </marquee>
    
</div>
<script type="text/javascript">
$('marquee').hover(function(){
        $(this).attr('scrollamount',0);
    },function(){
        $(this).attr('scrollamount',5);
    });
</script>




        