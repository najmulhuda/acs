<?php 
if($blog_id>0){	
	if($this->session->userdata('first_name') !=''){ $comments_user_nameval = $this->session->userdata('first_name');}
	else{ $comments_user_nameval = set_value('comments_user_name');}
	
	if($this->session->userdata('students_email') !=''){ $comments_user_emailval = $this->session->userdata('students_email');}
	else{ $comments_user_emailval = set_value('comments_user_email');}
		
	if(set_value('comments_description') !=''){ $comments_descriptionval = set_value('comments_description');}
	else{ $comments_descriptionval = '';}
?>
			
<h3 class="conHead2">
    User Comments
</h3>
<script type="text/javascript" language="javascript">

    function check_comments_fields(){
        var oField = document.frmcomments.comments_user_name;
        var oElement = document.getElementById('err_comments_user_name');
        oElement.innerHTML = "";
        if(oField.value == ""){
            oElement.innerHTML = "You are missing name.";
            oField.focus();
            return(false);
        }
        else if(oField.value.length<2){
            oElement.innerHTML = "Name should be more than 2 characters";
            oField.focus();
            return(false);
        }
        
        var oField = document.frmcomments.comments_user_email;
        var oElement = document.getElementById('err_comments_user_email');
        oElement.innerHTML = "";
        if(oField.value == ""){
            oElement.innerHTML = "You are missing email.";
            oField.focus();
            return(false);
        }
        else if(emailcheck(oField.value)==false) {
            oElement.innerHTML = "You are missing valid email.";
            oField.focus();
            return(false);
        }
                    
        var oField = document.frmcomments.comments_description;
        var oElement = document.getElementById('err_comments_description');
        oElement.innerHTML = "";
        if(oField.value == ""){
            oElement.innerHTML = "*";
            oField.focus();
            return(false);
        }
        else if(oField.value.length<10){
            oElement.innerHTML = "Comments should be more than 10 characters";
            oField.focus();
            return(false);
        }
                    
        return(true);
    }

</script>             
<?php 
$i = 0;
$sqlcomments = "select * from comments where comments_publish = 1 and table_name = 'blog' and table_id_name = 'blog_id' and table_id_val = '$blog_id' order by modified_date desc";
$querycomments = $commonmodel->getallrowbysqlquery($sqlcomments);
if(count($querycomments)>0){
    echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    foreach($querycomments as $rowcomments){
        
        if($i%2==0){$classname = 'evenrow';}
        else{$classname = 'oddrow';}
        
        $comments_id = $rowcomments->comments_id;
        $comments_user_name = $rowcomments->comments_user_name;
        $comments_description = $rowcomments->comments_description;
        $created_date = date('j F Y',$rowcomments->created_date);
        ?>
        <tr>									
            <td class="<?php echo $classname;?>" align="left" valign="middle">
                <table border="0" cellspacing="5" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="black12normal" align="left">
                                <?php echo $comments_description;?>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <span class="black12bold"><?php echo $comments_user_name;?></span><br />
                                <span class="black11normal"><?php echo $created_date;?></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div style="height:2px;"></div></td>
        </tr>                    
        <?php 
        $i++;
    }
    echo '</table>';
}
else{
    echo "<p>There is no comments added.</p>";
}?>
<h3 class="conHead">
    Add Your Comments
</h3>   
<?php
$attributes = array('name' => 'frmcomments', 'id' => 'frmcomments','onsubmit'=>'return check_comments_fields();');
echo form_open(site_url("/").'comments/save_clients_comments', $attributes);
?>
    <table width="100%" border="0" cellspacing="4" cellpadding="0">										
            <tr>
                <td align="left" class="red11bold" colspan="2">
                    <?php 
                    echo validation_errors();
                    if(isset($err_message) && $err_message!=''){
                        echo $err_message;
                    }
                    if ($this->uri->segment(4) === FALSE){$segment4name = '';}
                    else{$segment4name = $this->uri->segment(4);}
                    if($segment4name !='' && $segment4name=='success'){
                        echo 'Your comments has been successfully submitted. After reviewing your comments, it will be published.';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="black12bold" width="20%" align="left">Name *:</td>
                <td class="black12normal" width="80%" align="left">
                    <span class="inputbox300x25bg">
                        <?php 
                        $data = array('name'     => 'comments_user_name',
                                      'id'       => 'comments_user_name',
                                      'tabindex'       => '1',
                                      'value'    => $comments_user_nameval,
                                      'maxlength'=> '100',
                                      'class'    => 'input300x25none'
                                    );															
                        echo form_input($data);
                        ?>
                    </span>
                    <span class="red11bold" id="err_comments_user_name"></span>
                </td>
            </tr>
            <tr>
                <td class="black12bold" align="left">Email *:</td>
                <td class="black12normal" align="left">
                    <span class="inputbox300x25bg">
                        <?php 
                        $data = array('name'     => 'comments_user_email',
                                      'id'       => 'comments_user_email',
                                      'tabindex'       => '3',
                                      'value'    => $comments_user_emailval,
                                      'maxlength'=> '100',
                                      'size'    => '50',
                                      'class'    => 'input300x25none'
                                    );															
                        echo form_input($data);
                        ?>
                    </span>
                    <span class="red11bold" id="err_comments_user_email"></span>
                </td>
            </tr>								
            <tr>
                <td class="black12bold" align="left" valign="top">Comments *</td>
                <td align="left" class="black11normal">
                    <?php 
                    $data = array('name'     => 'comments_description',
                                  'id'       => 'comments_description',
                                  'value'    => $comments_descriptionval,
                                  'tabindex'       => '5',
                                  'rows'=> '5',
                                  'cols'=> '40',
                                  'class'    => 'textarea400x100css'
                                );															
                    echo form_textarea($data);
                    ?>
                    <span id="err_comments_description" class="red11bold"></span>
                </td>
            </tr>								
            <tr>
                <td align="left" class="black11bold"></td>
                <td align="left" class="black11normal">
                    <input type="hidden" name="comments_error_viewpage" id="comments_error_viewpage" value="blog/front_blog" />
                    <input type="hidden" name="comments_return_page" id="comments_return_page" value="blog/information/<?php echo $blog_id;?>" />
                    <input type="hidden" name="table_name" id="table_name" value="blog" />
                    <input type="hidden" name="table_id_name" id="table_id_name" value="blog_id" />
                    <input type="hidden" name="table_id_val" id="table_id_val" value="<?php echo $blog_id;?>" />
                    <?php 
                    $data = array( 'name'    => 'submit',
                                    'value'    => ' Submit ',
                                    'tabindex'       => '6',
                                    'class'    => 'submitbutton');															
                    echo form_submit($data);
                    $data = array( 'name'    => 'reset',
                                    'value'    => ' Reset ',
                                    'tabindex'       => '7',
                                    'class'    => 'submitbutton');															
                    echo '&nbsp;&nbsp;'.form_reset($data);
                    ?>
                </td>
            </tr>
        </table>
<?php echo form_close(); 
	}
?>