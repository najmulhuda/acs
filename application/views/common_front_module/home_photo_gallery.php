<ul>
	<?php
	$site_url = str_replace('/index.php','',site_url("/"));
	
    $queryphoto_gallery = $commonmodel->getallrowbysqlquery("select * from photo_gallery where photo_gallery_publish = 1 order by showing_order desc limit 0,3");
    if(count($queryphoto_gallery)>0){
        $i = 0;
        foreach($queryphoto_gallery as $photo_galleryrow){
            $photo_gallery_id = $photo_galleryrow->photo_gallery_id;
            $photo_gallery_name =$photo_galleryrow->name;
            $gallery_image_thumb = $photo_galleryrow->gallery_image_thumb;
            if($gallery_image_thumb !=''){
                $photo_gallery_image_src = $site_url.'application/views/module/gallery_image/'.$gallery_image_thumb;
                ?>
                <li>
                    <span class="galleryImageFrame"><img src="<?php echo $photo_gallery_image_src;?>" alt="<?php echo $photo_gallery_name;?>" /></span>
                    <div class="imageCaption"><strong><?php echo $photo_gallery_name;?></strong></div>
                </li>
                <?php
            }
        }
    }
    else{
		?>
        <li>
            There is no Social Media found.
        </li>
    	<?php 
    }?>
</ul>