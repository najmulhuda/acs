<?php
$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
if ($_SERVER["SERVER_PORT"] != "80"){
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
}
else{
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
}

if ($this->uri->segment(1) === FALSE){$segment1name = '';}
else{$segment1name = $this->uri->segment(1);} 

$servicesstr = '';					
$sqlservices = "select * from services where services_publish = 1 order by showing_order asc";						
$queryservices = $commonmodel->getallrowbysqlquery($sqlservices);
if($queryservices !='' && count($queryservices)>0){
	$k = 1;
	foreach($queryservices as $rowservices){
		$subservices_id = $rowservices->services_id;
		$name = $rowservices->name;
		$href= site_url("/").$rowservices->uri_name;
		
		$selectclass = '';
		if($href == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$servicesstr .= "<li$selectclass>
				<a href=\"$href\">$name</a>
		</li>";
	}
}		
echo $servicesstr;
?>