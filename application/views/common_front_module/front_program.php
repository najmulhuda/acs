<?php

$site_url = str_replace('/index.php','',site_url("/"));

$querywirealess = $commonmodel->getallrowbysqlquery("select * from   programs where  programs_publish = 1 order by showing_order asc limit 0,3");

if(count($querywirealess)>0){

	foreach($querywirealess as $font_querywirealess){

		$programs_id = $font_querywirealess->programs_id;

		$name =stripcslashes( $font_querywirealess->name);

		$uri_name = $font_querywirealess->uri_name;

		$description = $this->Common_model->limit_words(nl2br(strip_tags($font_querywirealess->description)),8, '').'..';

		//$details_url = site_url("/").$uri_name;
		$details_url = site_url("/")."programs";

		$programs_image_thumb = $font_querywirealess->programs_image_thumb;

		if($programs_image_thumb==''){

			$programs_image_thumburl = $site_url.'common/images/no_images_120x120.png';	

		}

		else{

			$programs_image_thumburl = $site_url.'application/views/module/programs_image/'.$programs_image_thumb;	

		}   

		?>

		<div class="program_box">

			<img src="<?php echo $programs_image_thumburl ?>" width="100%" height="120" alt="<?php echo $name;?>">

			<h3><a href="<?php echo $details_url;?>"><?php echo $name;?></a></h3>
			
		</div>

	<?php 

	}

}?>