<?php
$site_url = str_replace('/index.php','',site_url("/"));

$query = $commonmodel->getallrowbysqlquery("select * from social_media where social_media_publish = 1 order by showing_order asc");
if(count($query)>0){
	$i = 0;
	foreach($query as $onerow){
		$name = $onerow->name;
		$url =$onerow->url;
		$social_icon_thumb = $onerow->social_icon_thumb;
		if($social_icon_thumb !=''){
			$image_src = $site_url.'application/views/module/social_icon/'.$social_icon_thumb;
			?>
			<li>
				<a target="_blank" href="<?php echo $url;?>" title="<?php echo $name;?>"><img src="<?php echo $image_src;?>" width="35" alt="<?php echo $name;?>" /></a>
            </li>
			<?php
		}
	}
}
else{?>
    <p class="black12normal">
    There is no Social Media found.
    </p>
<?php 
}?>