<div class="contentRight"><!-- Begin: contentRight -->
    <h3 class="pageHead">Site Map</h3>     
    <div class="contentAreaMid">   
         <table width="100%" border="0" cellspacing="8" cellpadding="0">
                <tr>
                    <td align="left">
                        <a class="black12bold" href="<?php echo site_url("/")?>home/">Home</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a class="black12bold" href="<?php echo site_url("/")?>site_map/">Sitemap</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a class="black12bold" href="<?php echo site_url("/")?>faq/">FAQs</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a class="black12bold" href="<?php echo site_url("/")?>contact_us/">Contact Us</a>
                    </td>
                </tr>
                <?php
                $queryfaq = $commonmodel->getallrowbysqlquery("select uri_name, meta_title from site_uri where site_uri_publish = 1 order by created_date desc");
				if(count($queryfaq)>0){
					foreach($queryfaq as $faqrow){
						$uri_name = $faqrow->uri_name;
						$meta_title = $faqrow->meta_title;
						?>
						<tr>
                            <td align="left">
                                <a class="black12bold" href="<?php echo site_url("/").$uri_name?>" title="<?php echo $meta_title;?>"><?php echo $meta_title;?></a>
                            </td>
                        </tr>
					<?php
					}
				}
				?>
            </table>
	</div>            
</div>
