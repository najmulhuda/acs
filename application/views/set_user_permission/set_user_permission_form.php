<?php
if(isset($user_level_id['value']) && $user_level_id['value'] !=''){ $user_level_idval = $user_level_id['value'];}
else{ $user_level_idval = set_value('user_level_id');}
if($user_level_idval==''){$user_level_idval = 0;}

if(isset($action_type['value']) && $action_type['value'] !=''){ $action_typeval = $action_type['value'];}
else{ $action_typeval = set_value('action_type');}
if($action_typeval==''){$action_typeval = 0;}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}
?>
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();
	j(document).ready(function(){
		//showmodulelistbyuser_level_id();
	});

	function checkfrmset_user_permission_form(){
		var user_level_id = document.frmset_user_permission.user_level_id;
		var elementmessage = document.getElementById("errmsg_user_level_id");
		elementmessage.innerHTML = '';
		j('#user_level_id').removeClass('error_input200x20').addClass('input200x20');
		if(user_level_id.value=='' || user_level_id.value==0){
			elementmessage.innerHTML = 'You are missing User Level Name';
			user_level_id.focus();
			j('#user_level_id').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		
		var menu_id_list = document.getElementsByName("menu_id[]");		
		var elementmessage = document.getElementById("errmsg_menu_id");
		elementmessage.innerHTML = '';
		var onecheck = 0;
		if(menu_id_list.length>0){
			for(var i=0; i<menu_id_list.length; i++){				
				if(menu_id_list[i].checked==true){
					onecheck++;					
				}
			}
		}
		
		if(onecheck==0){
			elementmessage.innerHTML = 'You have to checked at least one module';
			menu_id_list[0].focus();
			return false;
		}
		
		return true;
	}
		
	function deletethisformfieldrow(level_nameidname){
		j("#"+level_nameidname).parent("li").slideUp(500, function() {
			j(this).remove();
		});
	}
	
	function checkmenucheckbox(selectmenu_id, menuidval){
		var menu_id_list = document.getElementsByName("menu_id[]");
		var onecheck = 0;
		
		if(menu_id_list.length>0){
			var view_option_list = document.getElementById("view_option_"+menuidval);
			if(view_option_list.checked==true){onecheck++;}
			
			var view_option_list = document.getElementById("add_option_"+menuidval);
			if(view_option_list.checked==true){onecheck++;}
			
			var view_option_list = document.getElementById("edit_option_"+menuidval);
			if(view_option_list.checked==true){onecheck++;}
			
			var view_option_list = document.getElementById("hide_option_"+menuidval);
			if(view_option_list.checked==true){onecheck++;}
			
			if(onecheck>0)
				menu_id_list[selectmenu_id].checked=true;
			else
				menu_id_list[selectmenu_id].checked=false;
		}
	}
	
	function checkallmenucheckbox(selectmenu_id, menuidval){
		var menu_id_list = document.getElementsByName("menu_id[]");
		var view_option_list = document.getElementById("view_option_"+menuidval);		
		var add_option_list = document.getElementById("add_option_"+menuidval);
		var edit_option_list = document.getElementById("edit_option_"+menuidval);
		var hide_option_list = document.getElementById("hide_option_"+menuidval);
						
		if(menu_id_list[selectmenu_id].checked==true){
			view_option_list.checked=true;
			add_option_list.checked=true;
			edit_option_list.checked=true;
			hide_option_list.checked=true;
		}
		else{
			view_option_list.checked=false;
			add_option_list.checked=false;
			edit_option_list.checked=false;
			hide_option_list.checked=false;
		}
	}
	
	function checkorderintvalue(selectpositionval){
		var showing_order = document.getElementsByName("showing_order[]");
						
		showing_order[selectpositionval].value = returnonlyintVal(showing_order[selectpositionval].value);
	}
	
	function showmodulelistbyuser_level_id(){
		var user_level_id = document.frmset_user_permission.user_level_id;
		var elementmessage = document.getElementById("errmsg_user_level_id");
		elementmessage.innerHTML = '';
		j('#user_level_id').removeClass('error_input200x20').addClass('input200x20');
		if(user_level_id.value=='' || user_level_id.value==0){
			elementmessage.innerHTML = 'You are missing User Level Name';
			user_level_id.focus();
			j('#user_level_id').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		else{
			j.post("<?php echo site_url("/");?>set_user_permission/showmodulelistbyuser_level_id/",{user_level_id:user_level_id.value}, function(data){
				if(data !=''){
					j("#menu_id_list").html(data).hide();
					j("#menu_id_list").slideDown('slow');
				}
			});
		}
	}
</script>
<table id="blueborder" width="80%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
               		User Level Permission Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'set_user_permission/set_user_permission_list';?>" class="white14bold">Show All User Level Permission List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
           	<?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmset_user_permission', 'onsubmit'=>'return checkfrmset_user_permission_form();');
            echo form_open_multipart(site_url("/").'set_user_permission/save_set_user_permission/', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
                        <td class="black12bold" width="160" align="left">User Level Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black12bold">
                           	<?php
                            $menu_idarray = '';
							if($user_level_idval==0){		
								?>
								<select name="user_level_id" id="user_level_id" class="input200x20">
									<option value="0">Select User Level</option>
									<?php
									echo $commonmodel->get_onetableallfieldvalueforselect('user_level','user_level_id', $user_level_idval, 'user_level_name', array('user_level_publish'=>1));
									?>
								</select>
								<?php
							}
							else{
								$allset_user_permissionidlist = $commonmodel->getallrow('set_user_permission', array('user_level_id'=>$user_level_idval));
								if(count($allset_user_permissionidlist)>0){
									foreach($allset_user_permissionidlist as $set_user_permission_idrow){
										$menu_idarray[] = $set_user_permission_idrow->menu_id;
									}
								}
								echo '<input type="hidden" name="user_level_id" id="user_level_id" value="'.$user_level_idval.'">';								
								
								echo $commonmodel->get_onefieldnamebyid('user_level', 'user_level_id', $user_level_idval, 'user_level_name');
							}
							?>
                            <span id="errmsg_user_level_id" class="red11normal"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                        	<table width="100%" bgcolor="#C0D2FF" cellspacing="0" cellpadding="5" border="0" align="left" >
                                <tr>
                                    <th width="30%" align="left" class="titlerow">
                                        Module Name
                                    </th>
                                    <th class="titlerow" width="15%" align="left">
                                        View Permission
                                    </th>
                                    <th class="titlerow" width="15%" align="left">
                                        Add Permission
                                    </th>
                                    <th class="titlerow" width="15%" align="left">
                                        Edit Permission
                                    </th>
                                    <th class="titlerow" align="left">
                                        Hide/Delete Permission
                                    </th>
                                </tr>
								<?php
                                $menusql = "select * from menu where menu_publish = 1 order by menu_category_id asc, showing_order asc";
								$menuquery = $commonmodel->getallrowbysqlquery($menusql);
								if(count($menuquery)>0){
									$i=0;
									foreach($menuquery as $rowmenu){
										if($i%2==0)	$classname = 'evenrow';
										else		$classname = 'oddrow';
										
										$menu_id = $rowmenu->menu_id;
										$menu_module = $rowmenu->menu_module;
										$menu_name = $rowmenu->menu_name;	
										$checked = '';
										$checkview_option = '';
										$checkadd_option = '';
										$checkedit_option = '';
										$checkhide_option = '';
										$set_user_permission_id = 0;
										$set_user_permission_idforedit = '';
										if(is_array($menu_idarray) && in_array($menu_id, $menu_idarray)){
											$set_user_permissionrow = $this->Common_model->getonerowallvaluebycondition('set_user_permission', array('user_level_id'=>$user_level_idval, 'menu_id'=>$menu_id));
											if($set_user_permissionrow){												
												$set_user_permission_id = $set_user_permissionrow->set_user_permission_id;
												
												$set_user_permission_publish = $set_user_permissionrow->set_user_permission_publish;
												if($set_user_permission_publish>0){
													$checked = ' checked="checked"';
													$view_option = $set_user_permissionrow->view_option;
													if($view_option>0){$checkview_option = ' checked="checked"';}
													
													$add_option = $set_user_permissionrow->add_option;
													if($add_option>0){$checkadd_option = ' checked="checked"';}
													
													$edit_option = $set_user_permissionrow->edit_option;
													if($edit_option>0){$checkedit_option = ' checked="checked"';}
													
													$hide_option = $set_user_permissionrow->hide_option;
													if($hide_option>0){$checkhide_option = ' checked="checked"';}													
												}
												$set_user_permission_idforedit = '<input type="hidden" name="set_user_permission_idforedit[]" value="'.$menu_id.'" />';
											}                                                       
										}																		
										?>                                        
                                        <tr id="menu_id_listrowno<?php echo $menu_id;?>">
                                            <td align="left" class="<?php echo $classname;?> black11bold">
                                                <?php echo $set_user_permission_idforedit;?>
                                                <input type="hidden" name="set_user_permission_id_<?php echo $menu_id;?>" value="<?php echo $set_user_permission_id;?>" />
                                                <input type="hidden" name="menu_module_<?php echo $menu_id;?>" value="<?php echo $menu_module;?>" /> 
                                                <input type="checkbox" name="menu_id[]" onclick="checkallmenucheckbox(<?php echo $i;?>, <?php echo $menu_id;?>);" value="<?php echo $menu_id;?>"<?php echo $checked;?> /> 
                                                <?php echo $menu_name;?>
                                            </td>
                                            <td class="<?php echo $classname;?>" align="left">
                                                <input type="checkbox" name="view_option_<?php echo $menu_id;?>" id="view_option_<?php echo $menu_id;?>" onclick="checkmenucheckbox(<?php echo $i;?>, <?php echo $menu_id;?>);" value="1"<?php echo $checkview_option;?> />View
                                            </td>
                                            <td class="<?php echo $classname;?>" align="left">
                                                <input type="checkbox" name="add_option_<?php echo $menu_id;?>" id="add_option_<?php echo $menu_id;?>" onclick="checkmenucheckbox(<?php echo $i;?>, <?php echo $menu_id;?>);" value="1"<?php echo $checkadd_option;?> />Add
                                            </td>
                                            <td class="<?php echo $classname;?>" align="left">
                                                <input type="checkbox" name="edit_option_<?php echo $menu_id;?>" id="edit_option_<?php echo $menu_id;?>" onclick="checkmenucheckbox(<?php echo $i;?>, <?php echo $menu_id;?>);" value="1"<?php echo $checkedit_option;?> />Edit
                                            </td>
                                            <td class="<?php echo $classname;?>" align="left">
                                                <input type="checkbox" name="hide_option_<?php echo $menu_id;?>" id="hide_option_<?php echo $menu_id;?>" onclick="checkmenucheckbox(<?php echo $i;?>, <?php echo $menu_id;?>);" value="1"<?php echo $checkhide_option;?> />Hide
                                            </td>
                                        </tr>
                                        <?php
										$i++;																				
									}
								}
								?>
                               </table>
                            <span id="errmsg_menu_id" class="red11normal"></span>
                        </td>
                   	</tr>
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal" id="finalsaverow"> 
                            <input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />                           
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submit1');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submit1');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                        </td>
                    </tr>									
                </table>
            <?php echo form_close(); ?>
		</td>
	</tr>
</table>