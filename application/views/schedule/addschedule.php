<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_addevice', 'onsubmit'=>'return check_add_device();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'schedule/edit/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'schedule/add/', $userformattributes); 
 }
 
?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>

<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   foreach($data as $row){
		   $id = $row->id;
		   $description = $row->description;
	       $time = $row->time;
		   $updatetime = $row->updatetime;
		   $cleardata = $row->cleardata;
		   $readdata = $row->readdata;
		   $updatecard = $row->updatecard;
	   }	
	    
	}	
}else{
	$id="";
	$description = "";
	$time = "";
    $updatetime = "";
    $cleardata = "";
    $readdata = "";
    $updatecard = "";
}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New Schedule

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'schedule';?>" class="white14bold">Show Schedules</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

				<tr>

					<td align="right" width="30%" class="black12bold">Description <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'description',

									  'id'       => 'description',

									  'tabindex'       => '1',

									  'value'    => $description,

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

				<tr>

					<td align="right" class="black12bold">Time <span class="red12bold">*</span>:</td>

					<td align="left">
                      
						<?php 

						$data = array('name'     => 'datetimepicker',

									  'id'       => 'datetimepicker',

									  'tabindex'       => '2',

									  'value'    => $time,

									  'maxlength'=> '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

					 
					    echo form_input($data);
					  

						?>
                     
						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												

				<tr>

					<td align="right" class="black12bold"></td>

					<td align="left" colspan="2">
                        <table>
						     <tr>
							     <td align="right" class="black12bold">Update Time <span class="red12bold"></span></td>
								 <td>
								     <?php 

						               $data = array(
									                'type'        => 'checkbox',
                                                    'name'        => 'updatetime',
                                                    'id'          => 'updatetime',
                                                    'value'       => $updatetime,
													'checked'     => $updatetime,
                                                    'style'       => 'margin:10px',
                                                 );

                                             echo form_checkbox($data);

						             ?>
								 </td>
								 <td align="right" class="black12bold">Clear Data:</td>
								 <td>
								        <?php 

						                   $data = array(
										            'type'        => 'checkbox',
                                                    'name'        => 'cleardata',
                                                    'id'          => 'cleardata',
                                                    'value'       => $cleardata,
													'checked'     => $cleardata,
                                                    'style'       => 'margin:10px',
                                                );

                                           echo form_checkbox($data);

						                ?>
								 </td>
							 </tr>
							 <tr>
							      <td align="right" class="black12bold">Read Data:</td>
								  <td>
								         <?php 

						                   $data = array(
										            'type'        => 'checkbox',
                                                    'name'        => 'readdata',
                                                    'id'          => 'readdata',
                                                    'value'       => $readdata,
                                                    'checked'     => $readdata,
                                                    'style'       => 'margin:10px',
                                                );

                                           echo form_checkbox($data);

						                ?>
								  </td>
								  <td align="right" class="black12bold">Update Card:</td>
								  <td>
								         <?php 

						                   $data = array(
										            'type'        => 'checkbox',
                                                    'name'        => 'updatecard',
                                                    'id'          => 'updatecard',
                                                    'value'       => $updatecard,
													'checked'     => $updatecard,
                                                    'style'       => 'margin:10px',
                                                );

                                           echo form_checkbox($data);

						                ?>
								  </td>
							 </tr>
						</table
						

					</td>

				</tr>												

				
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>