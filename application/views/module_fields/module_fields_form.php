<?php
$site_url = str_replace('/index.php','',site_url("/"));

if(isset($module_id['value']) && $module_id['value'] !=''){ $module_idval = $module_id['value'];}
else{ $module_idval = set_value('module_id');}
if($module_idval==''){$module_idval = 0;}

if(isset($form_fields_id['value']) && $form_fields_id['value'] !=''){ $form_fields_idval = $form_fields_id['value'];}
else{ $form_fields_idval = set_value('form_fields_id[]');}
//print_r($form_fields_idval);

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}
?>
<script type="text/javascript" src="<?php echo $site_url;?>common/calendar/calendar.js?random=20051112"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $site_url;?>common/calendar/calendar.css" />
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();

	function checkfrmmodule_fields_form(){
		var module_id = document.frmmodule_fields.module_id;
		var elementmessage = document.getElementById("errmsg_module_id");
		elementmessage.innerHTML = '';
		j('#module_id').removeClass('error_input200x20').addClass('input200x20');
		if(module_id.value==''){
			elementmessage.innerHTML = 'You are missing category name';
			module_id.focus();
			j('#module_id').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		
		var form_category_id_list = document.getElementsByName("form_fields_id[]");
		var elementmessage = document.getElementById("errmsg_form_fields_id");
		elementmessage.innerHTML = '';
		var onecheck = 0;
		if(form_category_id_list.length>0){
			for(var i=0; i<form_category_id_list.length; i++){
				if(form_category_id_list[i].checked==true)
					onecheck++;
			}
		}
		if(onecheck==0){
			elementmessage.innerHTML = 'You have to checked at least one field';
			form_category_id_list[0].focus();
			return false;
		}
		
		return true;
	}
	
	function createurl(textvalue, showintoidname){
		if(textvalue.length>0){
			document.getElementById(showintoidname).value = textvalue;
		}
	}
	
	function deletethisformfieldrow(label_nameidname){
		j("#"+label_nameidname).parent("li").slideUp(500, function() {
			j(this).remove();
		});
	}
	
</script>
<table id="blueborder" width="100%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
               		Module Fields Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'module_fields/module_fields_list';?>" class="white14bold">Show All Module Fields List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
           	<?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmmodule_fields', 'onsubmit'=>'return checkfrmmodule_fields_form();');
            echo form_open_multipart(site_url("/").'module_fields/save_module_fields/', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
                        <td class="black11bold" width="140" align="left">Module Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
							<select name="module_id" id="module_id" class="input200x20">
                                <option value="">Select Module Name</option>
                                <?php
                                $result = $commonmodel->getallrowbysqlquery("select * from  module where module_publish=1 order by showing_order asc");
                                if(count($result)>0 && $result !=''){
                                    foreach($result as $row){
                                        $table_id = $row->module_id;
                                        $table_field = $row->module_name;
                                        $selected = '';
                                        if($table_id==$module_idval){$selected = ' selected="selected"';}
                                        ?>
                                        <option value="<?php echo $table_id;?>" <?php echo $selected;?>><?php echo $table_field;?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <span id="errmsg_module_id" class="red11normal"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                        	<ul class="pretenderlist" id="product_imagelist">                                
                                <?php
                                $form_fieldssql = "select * from form_fields where form_fields_publish = 1 order by showing_order asc";
								$form_fieldsquery = $commonmodel->getallrowbysqlquery($form_fieldssql);
								if(count($form_fieldsquery)>0){
									$i=0;
									foreach($form_fieldsquery as $rowform_fields){
										$i++;
										if($i%2==0)	$classname = 'evenrow';
										else		$classname = 'oddrow';
										
										$form_fields_id = $rowform_fields->form_fields_id;
										$admin_label = $rowform_fields->admin_label;
										$front_label = $rowform_fields->front_label;
										$form_fields_name = $rowform_fields->form_fields_name;
										$form_fields_type = $rowform_fields->form_fields_type;
										$default_value = $rowform_fields->default_value;
										
										$form_fields_unique = $rowform_fields->form_fields_unique;
										if($form_fields_unique == 1){$form_fields_unique = 'Unique';}else{$form_fields_unique = 'Normal';}
										$form_fields_value_required = $rowform_fields->form_fields_value_required;
										if($form_fields_value_required == 1){$form_fields_value_required = '<font class="red14bold">*</font>';}
										else{$form_fields_value_required = '';}
										$form_fields_input_validation = $rowform_fields->form_fields_input_validation;
										
										$checked = '';
										if(is_array($form_fields_idval) && in_array($form_fields_id, $form_fields_idval)){$checked = ' checked="checked"';}
										?>                                        
                                        <li>
                                            <table width="500" cellspacing="1" cellpadding="0" border="0" align="left" id="product_imagelistrowno<?php echo $form_fields_id;?>">
                                                <tr>
                                                    <td width="140" align="left" class="black11bold <?php echo $classname;?>">
                                                    	<label>
                                                            <input type="checkbox" name="form_fields_id[]" value="<?php echo $form_fields_id;?>"<?php echo $checked;?> /> 
                                                            <?php echo $admin_label.$form_fields_value_required;?> 
                                                        </label>
                                                    </td>
                                                    <td align="left" class="<?php echo $classname;?>">
                                                       	<?php
														$default_valuestr = '';
                                                       	if($form_fields_type == 'checkbox'){
															$default_valuestr = '<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																$j=0;
																foreach($form_fields_optionsquery as $rowform_fields_options){
																	$j++;
																	$label_name = $rowform_fields_options->label_name;
																	$label_value = $rowform_fields_options->label_value;
																	if($j==4){
																		$j=0;
																		$default_valuestr .= '</tr><tr>';
																	}																	
																	$default_valuestr .= '<td width="33%" align="left"><input type="checkbox" name="'.$form_fields_name.'" value="'.$label_value.'" />'.$label_name.'</td>';
																}	
															}
															else{
																$default_valuestr .= '<td>There is no data found</td>';
															}
															$default_valuestr .= '</tr></table>';
													   	}
														elseif($form_fields_type == 'radiobutton'){
															$default_valuestr = '<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																$j=0;
																foreach($form_fields_optionsquery as $rowform_fields_options){
																	$j++;
																	$label_name = $rowform_fields_options->label_name;
																	$label_value = $rowform_fields_options->label_value;
																	if($j==4){
																		$j=0;
																		$default_valuestr .= '</tr><tr>';
																	}																	
																	$default_valuestr .= '<td width="33%" align="left"><input type="radio" name="'.$form_fields_name.'" value="'.$label_value.'" />'.$label_name.'</td>';
																}	
															}
															else{
																$default_valuestr .= '<td>There is no data found</td>';
															}
															$default_valuestr .= '</tr></table>';
													   	}
														elseif($form_fields_type == 'dropdown'){
															$default_valuestr = '<select name="'.$form_fields_name.'" class="input200x20" >';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																foreach($form_fields_optionsquery as $rowform_fields_options){
																	$label_name = $rowform_fields_options->label_name;
																	$label_value = $rowform_fields_options->label_value;
																	$default_valuestr .= '<option value="'.$label_value.'">'.$label_name.'</option>';
																}	
															}
															else{
																$default_valuestr .= '<option value="">There is no data found</option>';
															}
															$default_valuestr .= '</select>';
													   	}
														elseif($form_fields_type == 'multipleselect'){
															$default_valuestr = '<select multiple="multiple" name="'.$form_fields_name.'" class="input200x80" >';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																foreach($form_fields_optionsquery as $rowform_fields_options){
																	$label_name = $rowform_fields_options->label_name;
																	$label_value = $rowform_fields_options->label_value;
																	$default_valuestr .= '<option value="'.$label_value.'">'.$label_name.'</option>';
																}	
															}
															else{
																$default_valuestr .= '<option value="">There is no data found</option>';
															}
															$default_valuestr .= '</select>';
													   	}
														elseif($form_fields_type == 'textfield' || $form_fields_type == 'price'){
															$default_valuestr = '<input type="text" class="input200x20" name="'.$form_fields_name.'" value="'.$default_value.'" />';															
													   	}
														elseif($form_fields_type == 'textarea'){
															$default_valuestr = '<textarea name="'.$form_fields_name.'" id="'.$form_fields_name.'" class="textarea1" rows="3" cols="50"></textarea>';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																foreach($form_fields_optionsquery as $form_fieldrow_options){
																	$label_name = $form_fieldrow_options->label_name;
																	$label_value = $form_fieldrow_options->label_value;
																	$default_valuestr .= '&nbsp;&nbsp;&nbsp;'.$label_name.'&raquo;'.$label_value;
																}	
															}
													   	}
														elseif($form_fields_type == 'image'){
															$default_valuestr = '<input type="file" name="'.$form_fields_name.'" id="'.$form_fields_name.'" />';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																foreach($form_fields_optionsquery as $form_fieldrow_options){
																	$label_name = $form_fieldrow_options->label_name;
																	$label_value = $form_fieldrow_options->label_value;
																	$default_valuestr .= '&nbsp;&nbsp;&nbsp;Width: '.$label_name.' and Height: '.$label_value;
																}	
															}
													   	}
														elseif($form_fields_type == 'datecalendar'){
															$default_value = date('Y-m-d',$default_value);
															$default_valuestr = '<input name="'.$form_fields_name.'" type="text" value="'.$default_value.'" class="input120x20" />'
																				."<input type=\"button\" class=\"calendarbut\" value=\"\" onClick=\"displayCalendar(document.frmmodule_fields.".$form_fields_name.", 'yyyy-mm-dd', this);\">";
													   	}
														elseif($form_fields_type == 'yesno'){
															$default_valuestr = '<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
															$default_valuestr .= '<td width="33%" align="left"><input type="radio" name="'.$form_fields_name.'"';
															if($default_value=='yes'){$default_valuestr .= ' checked="checked"';}															
															 $default_valuestr .= 'value="Yes" /> Yes </td>';
															$default_valuestr .= '<td width="33%" align="left"><input type="radio" name="'.$form_fields_name.'"';
															if($default_value=='no'){$default_valuestr .= ' checked="checked"';}															
															 $default_valuestr .= ' value="No" /> No </td>';
															$default_valuestr .= '<td width="33%" align="left">&nbsp;</td>';
															$default_valuestr .= '</tr></table>';
													   	}	
														elseif($form_fields_type == 'foreign_key'){
															$default_valuestr = '<select name="'.$form_fields_name.'" class="input200x20" >
																				<option value="">Select '.$front_label.'</option>';
															$label_name = '';
															$label_value = '';
															$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
															if(count($form_fields_optionsquery)>0){
																foreach($form_fields_optionsquery as $rowform_fields_options){
																	$label_name = $rowform_fields_options->label_name;
																	$label_value = $rowform_fields_options->label_value;
																}	
															}
															
															if($label_name != '' && $label_value !=''){
																$primaryidname = $label_name.'_id';
																$publishname = $label_name.'_publish';
																$default_valuestr .= $commonmodel->get_onetableallfieldvalueforselect($label_name, $primaryidname, 0, $label_value, array($publishname=>1));
															}
															$default_valuestr .= '</select>';
													   	}
														elseif($form_fields_type == 'file'){
															$default_valuestr = '<input type="file" name="'.$form_fields_name.'" id="'.$form_fields_name.'" /><br />[Only txt|pdf|doc|docx|excell|accdb file type allowed]';
														}													
														echo $default_valuestr;
														?>
                                                    </td>
                                                    <td valign="top" nowrap="nowrap" class="<?php echo $classname;?>" align="left" width="85">&nbsp;
                                                        <a onclick="deletethisformfieldrow('product_imagelistrowno<?php echo $form_fields_id;?>');"><img src="<?php echo $site_url?>common/images/x.png" ></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                        
                                        <?php
										
									}
								}
								?>
                            </ul>
                            <span id="errmsg_form_fields_id" class="red11normal"></span>
                        </td>
                   	</tr>
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal" id="finalsaverow">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submit1');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submit1');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                        </td>
                    </tr>									
                </table>
            <?php echo form_close(); ?>
		</td>
	</tr>
</table>