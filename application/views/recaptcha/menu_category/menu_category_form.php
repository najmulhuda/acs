<?php
$site_url = str_replace('/index.php','',site_url("/"));

if(isset($menu_category_publish['value']) && $menu_category_publish['value'] !=''){ 
	$menu_category_publishcheckedval = $menu_category_publish['value'];
	if($menu_category_publishcheckedval>0){
		$menu_category_publishcheckedval = ' checked="checked"';
	}
	else{
		$menu_category_publishcheckedval = '';
	}
}
else{ $menu_category_publishcheckedval = ' checked="checked"';}

if(isset($menu_category_name['value']) && $menu_category_name['value'] !=''){ $menu_category_nameval = $menu_category_name['value'];}
else{ $menu_category_nameval = set_value('menu_category_name');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($menu_category_id['value']) && $menu_category_id['value'] !=''){ $menu_category_idval = $menu_category_id['value'];}
else{ $menu_category_idval = set_value('menu_category_id');}
if($menu_category_idval==''){$menu_category_idval=0;}

if(isset($action_type['value']) && $action_type['value'] !=''){	$action_typeval = $action_type['value'];}
else{ $action_typeval = set_value('action_type');}
if($action_typeval==''){$action_typeval=0;}
?>
<script type="text/javascript" src="<?php echo $site_url;?>common/calendar/calendar.js?random=20051112"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $site_url;?>common/calendar/calendar.css" />
<script language="JavaScript" type="text/javascript">
var j = jQuery.noConflict();

function checkfrmmenu_category_form(){
	var menu_category_name = document.frmmenu_category.menu_category_name;
	var elementmessage = document.getElementById("errmsg_menu_category_name");
	elementmessage.innerHTML = '';
	j('#menu_category_name').removeClass('error_input300x20').addClass('input300x20');
	if(menu_category_name.value==''){
		elementmessage.innerHTML = 'You are missing English Menu Category Name';
		menu_category_name.focus();
		j('#menu_category_name').removeClass('input300x20').addClass('error_input300x20');
		return false;
	}	
	else if(menu_category_name.value.length<2){
		elementmessage.innerHTML = 'English Menu Category Name should be more than 2 character';
		menu_category_name.focus();
		j('#menu_category_name').removeClass('input300x20').addClass('error_input300x20');
		return false;
	}	
	return true;
}
</script>
<table id="blueborder" width="850" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
               		Menu Category Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'menu_category/menu_category_list';?>" class="white14bold">Show All Menu Category List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
           	<?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmmenu_category', 'onsubmit'=>'return checkfrmmenu_category_form();');
            echo form_open_multipart(site_url("/").'menu_category/save_menu_category/', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
                        <td class="black11bold" width="200" align="left">Publish:</td>
                        <td align="left">
                            <input type="checkbox" name="menu_category_publish" value="1" <?php echo $menu_category_publishcheckedval; ?> />
                        </td>
                    </tr>
                   <tr>
                        <td class="black11bold" align="left">Menu Category: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'menu_category_name',
                                          'id'       => 'menu_category_name',
                                          'tabindex'       => '2',
                                          'value'    => $menu_category_nameval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_menu_category_name" class="red11normal"></span>
                        </td>
                    </tr>		
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal" id="finalsaverow">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            <input type="hidden" name="menu_category_id" id="menu_category_id" value="<?php echo $menu_category_idval;?>" />
                            <input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />
                        </td>
                    </tr>									
                </table>
            <?php echo form_close(); ?>
		</td>
	</tr>
</table>