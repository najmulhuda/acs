<?php
$site_url = str_replace('/index.php','',site_url("/"));
?>
<link href="<?php echo $site_url?>common/pagination/paignationcss.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $site_url?>common/pagination/paginationjs.js" type="text/javascript" language="javascript"></script>

<script language="JavaScript" type="text/javascript">
	j=jQuery.noConflict();
	j(document).ready(function($) {
		initPagination();
	});
	
	function pageselectCallback(page_index, jq){
		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();
		j('#Searchresult').empty().append(new_content);
		return false;
	}
	
	function initPagination() {
		
		// count entries inside the hidden content
		var num_entries = j('#hiddenresult div.result').length;
		// Create content inside pagination element
		j("#Pagination").pagination(num_entries, {
			callback: pageselectCallback,
			items_per_page:1 // Show only one item per page
		});
	 }  
	
</script>
<h3 class="articleHead">All Front Menu Category List</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
	<tr>
		<td align="center">	
			<?php
            if(isset($front_menu_categoryquery) && count($front_menu_categoryquery)>0){
            ?>	
                <div style="width:99%; overflow:hidden;">				
                    <div id="Searchresult">
                        This content will be replaced when pagination inits.
                    </div>
                </div>
                <div id="hiddenresult" style="display:none;">
                    <div class="result">
                        <table width="100%" cellpadding="0" cellspacing="1" border="0">
                            <tr>
                                <th class="titlerow" align="center" width="10%">#</th>
                                <th class="titlerow" align="left">Front Menu Category Name</th>
                            </tr>
                            <?php
                            $str = "";
                            $i = 1;
                            foreach($front_menu_categoryquery as $rowfront_menu_category){                                
                                
                               if($i%2==0){$classname = 'evenrow';$selectedeven = 'selectedeven';}
								else{$classname = 'oddrow';$selectedeven = 'selectedodd';}
						
								$front_menu_category_id = $rowfront_menu_category->front_menu_category_id;
								$front_menu_category_name = $rowfront_menu_category->front_menu_category_name;
								
								$str .= "<tr>
									<td align=\"center\" class=\"$selectedeven\">$i</td>
									<td align=\"left\" class=\"$classname\">$front_menu_category_name</td>
								</tr>";
                                        
								if($i%20==0){
									$str .= '</table></div>
									<div class="result">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<th class="titlerow" align="center" width="10%">#</th>
											<th class="titlerow" align="left">Name</th>
										</tr>';
								}      
								$i++;
                            }
                            echo $str;
                        ?>
                        </table>
                        </div>
                    </div>
            <?php
            }
            else{
                echo "<div class=red18bold>There are currently no $functionname found</div>";					
            }								
            ?>
    	</td>
	</tr>
    <tr>
		<td align="right">
			<a href="<?php echo site_url("/")?>" class="readmore">Back To Home</a>
		</td>
	</tr>
</table>
            