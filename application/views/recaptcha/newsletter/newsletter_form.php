<?php
$site_url = str_replace('/index.php','',site_url("/"));

if(isset($newsletter_publish['value']) && $newsletter_publish['value'] !=''){ 
	$newsletter_publishval = $newsletter_publish['value'];
	if($newsletter_publishval>0){
		$newsletter_publishval = set_checkbox('newsletter_publish', '1', true);
	}
	else{
		$newsletter_publishval = set_checkbox('newsletter_publish', '1', false);
	}
}
else{ $newsletter_publishval = set_checkbox('newsletter_publish', '1', true);}

if(isset($newsletter_name['value']) && $newsletter_name['value'] !=''){ $newsletter_nameval = $newsletter_name['value'];}
else{ $newsletter_nameval = set_value('newsletter_name');}

if(isset($newsletter_email['value']) && $newsletter_email['value'] !=''){ $newsletter_emailval = $newsletter_email['value'];}
else{ $newsletter_emailval = set_value('newsletter_email');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($newsletter_id['value']) && $newsletter_id['value'] !=''){ $newsletter_idval = $newsletter_id['value'];}
else{ $newsletter_idval = 0;}

if(isset($action_type['value']) && $action_type['value'] !=''){	$action_typeval = $action_type['value'];}
else{$action_typeval = 0;}
?>

<script language="JavaScript" type="text/javascript">
var j = jQuery.noConflict();
function checkfrmnewsletter_form(){	
	
	var newsletter_email = document.frmnewsletter.newsletter_email;
	var elementmessage = document.getElementById("errmsg_newsletter_email");
	elementmessage.innerHTML = '';
	j('#newsletter_email').removeClass('field400_err').addClass('field400');
	if(newsletter_email.value==''){
		elementmessage.innerHTML = 'You are missing email';
		newsletter_email.focus();
		j('#newsletter_email').removeClass('field400').addClass('field400_err');
		return false;
	}
	if(emailcheck(newsletter_email.value)==false){
		elementmessage.innerHTML = 'You are missing valid email';
		newsletter_email.focus();
		j('#newsletter_email').removeClass('field400').addClass('field400_err');
		return false;
	}
	
	return true;
}
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			Add New News Letter From
		</td>
	</tr>
	<tr>
		<td align="center">

			<div id="main"><!-- Begin: main -->	
    <div class="page">		
		<center>
		<table width="98%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td class="errormessage">
				<?php 
					echo validation_errors();
					if(isset($err_message) && $err_message!=''){
						echo $err_message;
					}
				?>
			</td>
		  </tr>
		  <tr>
			<td align="right" nowrap="nowrap" width="20%">
				<a href="<?php echo site_url("/").'newsletter/newsletter_list';?>" class="greendeep16bold">Show All New Letter List</a>
			</td>
		  </tr>
		</table>
							
		<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td class="tdtopleft12x13"></td>
			<td class="tdtopautox13"></td>
			<td class="tdtopright14x13"></td>
		  </tr>
		  <tr>
			<td class="tdleft12xauto"></td>
			<td align="center" valign="top">	
				<?php
				$attributes = array('name' => 'frmnewsletter', 'onsubmit'=>'return checkfrmnewsletter_form();');
				echo form_open_multipart(site_url("/").'newsletter/save_newsletter', $attributes);
				?>
					<table width="100%" border="0" cellspacing="7" cellpadding="0">										
						<tr>
							<td align="left" colspan="2">
								<span class="black16bold">News Letter Information</span>
							</td>
						</tr>
						<tr>
							<td class="black11bold" width="180" align="left">Publish:</td>
							<td align="left">
								<input type="checkbox" tabindex="1" name="newsletter_publish" value="1" <?php echo $newsletter_publishval; ?> />
							</td>
						</tr>
						<tr>
							<td class="black11bold" align="left">Name: <font class="red14bold">*</font></td>
							<td align="left" class="black11normal">
								<?php 
								$data = array('name'     => 'newsletter_name',
											  'id'       => 'newsletter_name',
											  'tabindex'       => '2',
											  'value'    => $newsletter_nameval,
											  'maxlength'=> '250',
											  'class'    => 'input300x18'
											);															
								echo form_input($data);
								?>
								<span id="errmsg_newsletter_name" class="errormsg"></span>
							</td>
						</tr>
						<tr>
							<td class="black11bold" align="left">Email: <font class="red14bold">*</font></td>
							<td align="left" class="black11normal">
								<?php 
								$data = array('name'     => 'newsletter_email',
											  'id'       => 'newsletter_email',
											  'tabindex'       => '2',
											  'value'    => $newsletter_emailval,
											  'maxlength'=> '250',
											  'class'    => 'input300x18'
											);															
								echo form_input($data);
								?>
								<span id="errmsg_newsletter_email" class="errormsg"></span>
							</td>
						</tr>
						<tr>
							<td align="left" class="black11bold"></td>
							<td align="left" class="black11normal">
								<?php 
								$data = array( 'name'    => 'submit',
											   'value'   => $submitval,
											   'tabindex'=> '4',
											   'class'   => 'submitbutton');															
								
								echo form_submit($data);
								$data = array( 'name'    => 'reset',
											   'value'   => ' Reset ',
											   'tabindex'=> '5',
											   'class'   => 'submitbutton');
											   															
								echo '&nbsp;&nbsp;'.form_reset($data);
								?>
								<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $newsletter_idval;?>" />
								<input type="hidden" name="action_type" id="action_type" value="<?php echo $action_typeval;?>" />
							</td>
						</tr>							
						<tr>									
					</table>
				<?php echo form_close(); ?>
			</td>
			<td class="tdright14xauto"></td>
		  </tr>
		  <tr>
			<td class="tdbottomleft12x15"></td>
			<td class="tdbottomautox15"></td>
			<td class="tdbottomright14x15"></td>
		  </tr>
		</table>
		</center>		   
    </div><!-- End: page --> 
</div><!-- End: main -->

		</td>
	</tr>
</table>


