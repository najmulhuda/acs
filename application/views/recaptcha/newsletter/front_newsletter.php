<script type="text/javascript" language="javascript">
function check_newsletter_form(){
	var oField = document.frm_newsletter_form.newsletter_email;
	if(oField.value == "" || oField.value == "Enter Email Address"){
		oField.focus();
		alert("Email is mandatory.");
		return(false);
	}
	else if(emailcheck(oField.value)==false) {
		oField.focus();
		alert("Email is invalid");
		return(false);
	}
	return(true);
}
</script>
<div class="newslatter">
    <h3 class="sideHead">Newsletter</h3>
<?php
$newsletterformattributes = array('name' => 'frm_newsletter_form', 'onsubmit'=>'return check_newsletter_form();');
echo form_open(site_url("/").'newsletter/frontsave_newsletter/', $newsletterformattributes);
	if(isset($newsletter_message) && $newsletter_message !='' && $newsletter_message =='newsletter_added'){
		echo "<span class=green12bold>Email Added successfully.</span>";
	}
	if(isset($newsletter_message) && $newsletter_message !='' && $newsletter_message =='newsletter_couldnotadded'){
		echo "<span class=green12bold>Error! Email already exist.</span>";
	}
	?>
        <div class="news_row">
            <label>Name:</label>
            <?php 
			$data = array('name'     => 'newsletter_name',
						  'id'       => 'newsletter_name',
						  'onfocus'       => "if(this.value=='Name'){this.value=''}",
						  'onblur'       => "if(this.value==''){this.value='Name'}",
						  'tabindex'       => '33',
						  'value'    => 'Name',
						  'maxlength'=> '100',
						  'class'    => 'txt_input'
						);
			echo form_input($data);
			?>
        </div>
        <div class="news_row">
            <label>E-mail * :</label>
            <?php 
			$data = array('name'     => 'newsletter_email',
						  'id'       => 'newsletter_email',
						  'onfocus'       => "if(this.value=='Email Address'){this.value=''}",
						  'onblur'       => "if(this.value==''){this.value='Email Address'}",
						  'tabindex'       => '34',
						  'value'    => 'Email Address',
						  'maxlength'=> '100',
						  'class'    => 'txt_input'
						);
			echo form_input($data);
			?>
        </div>
        <div class="news_row">
            <label>State :</label>
            <?php 
			$data = array('name'     => 'newsletter_state',
						  'id'       => 'newsletter_state',
						  'onfocus'       => "if(this.value=='State'){this.value=''}",
						  'onblur'       => "if(this.value==''){this.value='State'}",
						  'tabindex'       => '35',
						  'value'    => 'State',
						  'maxlength'=> '100',
						  'class'    => 'txt_input'
						);
			echo form_input($data);
			?>
        </div>
	    <div class="news_row2">
			<input type="submit" name="newslettersave" value="subscribe" class="btn_submit" />
		</div>
<?php echo form_close(); ?>
</div>


