<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $totalrowscount;?>" />
<script type="text/javascript">
   	var j = jQuery.noConflict();   
   	
   	function pageselectCallback(page_index, jq){
		var searching_start_date=document.getElementById('searching_start_date').value;
		var searching_end_date=document.getElementById('searching_end_date').value;
		var payment_getway=document.getElementById('payment_getway').value;
		var order_status=document.getElementById('order_status').value;
		var returnvalue='fulldata';
		
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var starting_val = parseInt(page_index*list_per_page);
		j.post('<?php echo site_url("/").'program_report/fetching_current_order_data/';?>', {"searching_start_date":searching_start_date, "searching_end_date":searching_end_date, "payment_getway":payment_getway, "order_status":order_status, "starting_val": starting_val, "list_per_page": list_per_page, "returnvalue":returnvalue},
		function(data){
			//alert(data);
			j('#Searchresult').html(data);			
		});		
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		
		var optInit = getOptionsFromForm(); // Create pagination element with options from form
		var totalrows_count = document.getElementById('totalrowscount').value;
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit, list_per_page);
	}
	
	function seachingwithpagination(){
		
		var searching_start_date=document.getElementById('searching_start_date').value;
		var searching_end_date=document.getElementById('searching_end_date').value;
		var payment_getway=document.getElementById('payment_getway').value;
		var order_status=document.getElementById('order_status').value;
		var returnvalue='datacount';
		
		j.post('<?php echo site_url("/").'program_report/fetching_current_order_data/';?>', {"order_status":order_status,"payment_getway":payment_getway, "searching_start_date":searching_start_date, "searching_end_date":searching_end_date, "returnvalue":returnvalue},
		function(data) {
			document.getElementById('totalrowscount').value = data;	
			loadpagination();
		});
	}
	
	j(document).ready(function(){
		loadpagination();
	});
	
	function change_order_status(order_status, program_order_id){
		j("#order_statusrow").slideDown("fast");
		j("#submitrow").slideDown("fast");
		if(order_status !=''){
			j('#popupmessage').html('Do you sure want to change this order status from "'+order_status+'" <br />to');	
		}
		document.getElementById("program_order_id").value = program_order_id;
		var optionstr = '<option value="Pending">Pending</option>';
		if(order_status=='Processing'){optionstr = optionstr+'<option selected="selected" value="Processing">Processing</option>';}
		else{optionstr = optionstr+'<option value="Processing">Processing</option>';}
		
		if(order_status=='Completed'){optionstr = optionstr+'<option selected="selected" value="Completed">Completed</option>';}
		else{optionstr = optionstr+'<option value="Completed">Completed</option>';}
		
		if(order_status=='Cancel'){optionstr = optionstr+'<option selected="selected" value="Cancel">Cancel</option>';}
		else{optionstr = optionstr+'<option value="Cancel">Cancel</option>';}
					
		j("#program_order_status").html(optionstr);
		var popID = 'show_change_order_status'; //Get Popup ID
		var popURL = '#?w=500'; //Get Popup href to define size
		show_popup(popID, popURL);
	}
	
	function save_change_order_status(){
		var program_order_id = document.getElementById("program_order_id").value;
		var order_status = document.getElementById("program_order_status").value;
		
		j.post("<?php echo site_url("/");?>program_report/save_change_order_status", {"program_order_id": program_order_id,"order_status":order_status},
		function(data){
			
			if(data=='OK'){
				j('#popupmessage').html("This order status updated successfully.");
			}
			else{
				j('#popupmessage').html("Sorry! This order status could not updated");
			}
			j("#order_statusrow").slideUp("fast");
			j("#submitrow").slideUp("fast");
			
			j("#orderstatus"+program_order_id).html("Status: "+order_status+'<br />'+
								"<a href=\"javascript:void(0);\" onclick=\"change_order_status('"+order_status+"', "+program_order_id+");\">Change Status</a>");
			
			if(order_status=='Cancel'){
				window.location='<?php echo site_url("/")?>program_report/current_order_list/cancel-order';
			}
			else if(order_status=='Completed'){
				window.location='<?php echo site_url("/")?>program_report/current_order_list/completed-order';
			}
			else{
				var myInterval = window.setInterval(function (a,b) {
				  //myNumber++;
				},2000);
				window.setTimeout(function (a,b) {
				  clearInterval(myInterval);
				  close_popup();
				},3000);
			}
		});
	}
</script>
<link rel="stylesheet" href="<?php echo site_url("/")?>common/pagination/toppagination.css" type="text/css" />
<script src="<?php echo site_url("/")?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="<?php echo site_url("/");?>common/anytime/anytimec.js"></script> 
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo site_url("/");?>common/anytime/anytimec.css">

<!--==================================For show_change_order_status form==================================-->
<div id="show_change_order_status" class="popup_block">
    <center>
    <div style="height:200px; width:95%; overflow:auto;">		
    <form name="frm_deletefromcart_form" method="post" action="#">			
        <table align="center" width="100%" border="0" cellspacing="5" cellpadding="0">			
            <tr>
                <td class="oddrow" colspan="2" align="center">
                    <div id="popupmessage" class="red11bold"></div>
                </td>
            </tr>
            <tr id="order_statusrow"><td align="center" colspan="2" class="oddrow">
            	<select name="program_order_status" id="program_order_status" class="input200x20">
                	<option value="Pending">Pending</option>
                	<option value="Completed">Completed</option>
                	<option value="Cancel">Cancel</option>
                </select>
            </td></tr>
           <tr><td align="center" colspan="2">&nbsp;</td></tr>  
             <tr id="submitrow">
                <td align="center">					
                    <input type="hidden" name="program_order_id" id="program_order_id" value="" />
                    <input type="button" style="border:none;" class="submitbutton" id="removeprogram" value=" Change " onclick="save_change_order_status();" />
                </td>
                <td align="center">					
                    <input type="button" style="border:none;" class="submitbutton" onclick="close_popup();" value=" Cancel " />
                </td>
            </tr>
        </table>
        </form>
    </div>
    </center>
</div>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                	<div id="Pagination" class="pagination"></div>
                </td>
                <td width="70" align="left">
                	<select id="list_per_page" class="searchinput60x20" name="list_per_page" onchange="seachingwithpagination();">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20" selected="selected">20</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                        <option value="70">70</option>
                        <option value="100">100</option>
                    </select>
               	</td>
                <td width="90" align="left">
                    <?php						
                    $searching_start_date = date('Y-m-d',mktime(0,0,0,date('m'),date('d'),date('Y')-1));
                    $data = array('name'     => 'searching_start_date',
                                  'id'       => 'searching_start_date',
                                  'tabindex' => '1',
                                  'value'    => $searching_start_date,
                                  'maxlength'=> '20',					  
                                  'class'    => 'calendartext'
                                );															
                    echo form_input($data);
                    ?>
                </td>
                <td width="90" align="left">
                    <?php	
					$searching_end_date = date('Y-m-d');					
                    $data = array('name'     => 'searching_end_date',
                                  'id'       => 'searching_end_date',
                                  'tabindex'       => '2',
                                  'value'    => $searching_end_date,
                                  'maxlength'=> '20',					  
                                  'class'    => 'calendartext'
                                );															
                    echo form_input($data);
                    ?>
                </td>
                <td class="black11bold" width="80" align="left">    
                    <select onchange="seachingwithpagination();" name="payment_getway" class="searchinput120x20" id="payment_getway">
                        <option value="">Payment Type</option>
                        <option value="Paypal">Paypal</option>
                        <option value="Payment by Cash">Payment by Cash</option>
                    </select>
                </td>
                  <td class="black11bold" width="80" align="left">
                      <select onchange="seachingwithpagination();" name="order_status" class="searchinput120x20" id="order_status">
                          <option value="">Pending/Processing</option>
                          <option value="Pending">Pending</option>
                          <option value="Processing">Processing</option>
                      </select>
                  </td>
                  <td class="black11bold" width="40" align="left">
                	<input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
                </td>
              </tr>
            </table>
            
            <script language="javascript" type="application/javascript">
				var j = jQuery.noConflict();
				
				var oneDay = 24*60*60*1000;
				var oneDay1 = 0;
				var rangeDemoFormat = "%Y-%m-%d";
				
				var startdate = new Date(2012,0,1);				
				var currentdate = new Date('<?php echo $searching_start_date;?>');
				
				var d=new Date();
				var toDayTime=d.getTime();
				
				var endday = parseInt(d.getDate() - 1);
				var endmonth = d.getMonth();
				var endyear = d.getFullYear();
				var enddate = new Date(endyear,endmonth,endday);
				
			
				var rangeDemoConv = new AnyTime.Converter({format:rangeDemoFormat});
				var dayLater = new Date(toDayTime);
				
				var oneYearLater = new Date(toDayTime+(365*oneDay*5));
								
				j("#searching_start_date").AnyTime_noPicker().removeAttr("disabled").val(rangeDemoConv.format(currentdate)).AnyTime_picker({
					earliest: startdate,
					format: rangeDemoFormat,
					latest: enddate
				});				
				
				j("#searching_start_date").change( function(e) { try {
					var fromDay = rangeDemoConv.parse(j("#searching_start_date").val()).getTime();
					var dayLater = new Date(fromDay+(oneDay*365));
					var ndayLater = new Date(fromDay);
					var ninetyDaysLater = new Date(fromDay+(365*oneDay*5));
					
					j("#searching_end_date").AnyTime_noPicker().removeAttr("disabled").val(rangeDemoConv.format(dayLater)).AnyTime_picker({ 
							earliest: ndayLater,
							format: rangeDemoFormat,
							latest: ninetyDaysLater
						});
					} catch(e){ j("#searching_end_date").val("").attr("disabled","disabled"); 	
				} } );	
			</script>			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			if ($this->uri->segment(3) === FALSE){
				$msg = '';
			}
			else{
				$msg = $this->uri->segment(3);
			}
			if(isset($msg) && $msg !='' ){
				if($msg=='cancel-order')
					echo "<div class=red18bold>Order status updated successfully and sent to Cancel list</div>";
				if($msg=='completed-order')
					echo "<div class=red18bold>Order status updated successfully and sent to completed list</div>";						
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
    
	<tr>
		<td align="center" id="Searchresult">&nbsp;</td>
	</tr>
</table>