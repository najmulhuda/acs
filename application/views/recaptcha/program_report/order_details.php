<?php
if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}

$global_data = $this->Common_model->get_global_configdata();
$site_name = $global_data['site_name'];
$author_email = $global_data['author_email'];
$author_address = $global_data['author_address'];
$author_telephone = $global_data['author_telephone'];
$author_mobile = $global_data['author_mobile'];	
$author_fax = $global_data['author_fax'];	
$site_email_logo = $global_data['site_email_logo'];

if($site_email_logo ==''){
	$sitelogo = site_url('/').'common/images/logo.png';
}
else{
	$sitelogo = site_url('/').'application/views/admin/global_images/'.$site_email_logo;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo site_url("/")?>common/images/favicon.ico" />
<title><?php echo $title;?></title>
<style type="text/css">
*{ margin:0; padding:0;}
.tablelist td{ padding:10px;}
.black12normal{font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000;}
.black14bold{font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#000;}
.black14normal{font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:normal; color:#000;}
.titlerow{ background:#C0D2FF;color:#0A0A09; line-height:20px; padding:2px 5px 2px 10px; font-weight:bold;}
.oddrow{ background:#ECEFF5;color:#0A0A09; line-height:20px; padding:2px 5px 2px 10px;}
.evenrow{ background:#FBF6F6;color:#0F0E0E; line-height:20px; padding:2px 5px 2px 10px;}

</style>
</head>

<body>
<div style="width:800px; overflow:hidden; margin:0 auto;">
    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td bgcolor="#d59244">
                <table class="tablelist" width="100%" border="0" cellspacing="1">
                  <tr>
                    <td><img src="<?php echo $sitelogo;?>" alt="<?php echo $site_name;?>" /></td>
                    <td width="300">
                        <span class="black14bold">
                            <?php echo $site_name;?>
                        </span><br />
                        <span class="black12normal">
                            Address: <?php echo $author_address;?><br />
                            Phone: <?php echo $author_telephone;?><br />
                            Email: <?php echo $author_email;?>
                        </span><br />
                    </td>
                  </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" style="padding:20px;" class="evenrow black14normal">
                Order # 
                <?php 
                $charcount = strlen($program_order_id);					
                $totalchar = floor(8-$charcount);			
                $extrcharstr = '1';			
                if($totalchar>0){			
                    for($i=0;$i<$totalchar;$i++){			
                        $extrcharstr .= '0';			
                    }
                }
                echo $extrcharstr.$program_order_id;
                ?><br />
                <?php 
                if($program_orderrow !=''){
                    echo date('M d, Y',$program_orderrow->created_date);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <table class="tablelist" width="100%" border="0" cellspacing="2">
                  <tr>
                    <td align="left" class="evenrow" valign="top">
                        <p style="font-weight:bold; font-size:16px;">Payment Information</p>
                        <p>
                            <?php 
                            if($program_orderrow !=''){
                                echo 'Order Status : '.$program_orderrow->order_status;
								echo '<br />Payment Method : '.$program_orderrow->payment_getway;							
                            }
                            ?>
                        </p>
                    </td>
                    <td align="left" class="evenrow" width="50%" valign="top">
                        <p style="font-weight:bold; font-size:16px;">Students Information</p>
                        <p>
                            <?php 
                            if($program_orderrow !=''){
                               	$first_name = stripslashes($program_orderrow->students_name);
								$last_name = '';
								$students_id=$program_orderrow->students_id;
								$street_address = '';
								$city = '';
								$state_id = 0;
								$country_id = 0;
								$zip = '';
								$company = '';
								$telephone = '';
								$students_address_id = 0;
								
								$shipping_address_id = $this->Common_model->get_onefieldnamebyid('students', 'students_id', $students_id, 'shipping_address_id');
								if($shipping_address_id){
									$students_address_id = $shipping_address_id;
									
									$addressrow = $this->Common_model->getonerowallvaluebycondition('students_address', array('students_address_id'=>$students_address_id));
									if($addressrow){
										$first_name = $addressrow->first_name;
										$last_name = $addressrow->last_name;
										$street_address = $addressrow->street_address;
										$city = $addressrow->city;
										$state_id = $addressrow->state_id;
										$country_id = $addressrow->country_id;
										$zip = $addressrow->zip;
										$company = $addressrow->company;
										$telephone = $addressrow->telephone;
									}
								}			
								if($first_name !='' || $last_name !=''){
									echo "$first_name $last_name<br />";
								}
								
								$address = $street_address;
								if($city !=''){$address .= ', '.$city;}
								
								$state = '';
								if($state_id>0)
									$state = $this->Common_model->get_onefieldnamebyid('state', 'state_id', $state_id, 'name');
								if($state !=''){$address .= ', '.$state;}
								if($zip !=''){$address .= ' - '.$zip;}
								
								$country = '';
								if($country_id>0)
									$country = $this->Common_model->get_onefieldnamebyid('country', 'country_id', $country_id, 'country_name');
								if($country !=''){$address .= ', '.$country;}
								
								if($address !=''){
									echo "$address<br />";
								}
								
								$email = $program_orderrow->email;
								
								if($email !=''){
									echo "$email<br />";
								}
								
								if($telephone !=''){
									echo "$telephone";
								}
                            }
                            ?>
                        </p>
                    </td>
                  </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td align="left" class="black14normal">
                <table width="100%" cellpadding="0" cellspacing="1" border="0">
                    <tr>
                        <th class="titlerow" align="center" width="5%">#</th>
                        <th class="titlerow" align="left" width="15%">Picture</th>
                        <th class="titlerow" align="left">Name</th>
                        <th class="titlerow" align="center" width="15%">Program Fee</th>
                    </tr>
                    <?php
                    $str = "";
                    $i = 1;
                    if($query){
                        
                        foreach($query as $program_order_list_row){
                            
                            if($i%2==0){$classname = 'evenrow';$selectedeven = 'selectedeven';}
                            else{$classname = 'oddrow';$selectedeven = 'selectedodd';}
                                                                        
                            $program_order_id = $program_order_list_row->program_order_id;
                            
                            $program_thumbimage = $program_order_list_row->programs_image_thumb;
                            $program_thumbimagesrc = site_url('/').'common/images/no_images_120x120.png';
                            if($program_thumbimage !=''){
                                $program_thumbimagesrc = site_url('/').'application/views/module/programs_image/'.$program_thumbimage;
                            }
                            
                            $program_name = $program_order_list_row->name;
                            $program_fee = $program_order_list_row->program_fee;
                            
                            $str .= "<tr>
                                <td class=\"$classname\" align=\"center\">$i</td>
                                <td class=\"$classname\" align=\"center\"><img src=\"$program_thumbimagesrc\" width=\"100\" alt=\"$program_name\" /></td>
                                <td class=\"$classname\" align=\"left\">$program_name</td>
                                <td class=\"$classname\" align=\"center\">$ $program_fee</td>
                            </tr>";
                                    
                            $i++;
                        }
                        
                        if($i%2==0){$classname = 'evenrow';$selectedeven = 'selectedeven';}
                        else{$classname = 'oddrow';$selectedeven = 'selectedodd';}
                            
                        $total_amount = $program_orderrow->total_amount;
                        if($total_amount>0){						
                            $str .="<tr>
                                        <td colspan=\"4\" align=\"right\">
                                            <div style=\" border-bottom:1px solid #d1d6dc;height:1px;\"></div>
                                        </td>					
                                    </tr>
                                    <tr>
                                        <td class=\"$classname\" colspan=\"3\" align=\"right\">
                                           <b>Total Amount :</b>
                                        </td>
                                        <td class=\"$classname\" align=\"center\">
                                            $ ".$total_amount."
                                        </td>
                                    </tr>";
                        }                       
                    }
                    else{
                        $str .= "<tr>
                                    <td colspan=\"4\" class=\"oddrow\" align=\"center\">There is no program found.</td>
                                </tr>";
                    }
                    echo $str;
                ?>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>