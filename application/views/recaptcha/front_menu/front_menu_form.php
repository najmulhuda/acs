<?php
if(isset($front_menu_category_id['value']) && $front_menu_category_id['value'] !=''){ $front_menu_category_idval = $front_menu_category_id['value'];}
else{ $front_menu_category_idval = set_value('front_menu_category_id');}

if(isset($page_info_id['value']) && $page_info_id['value'] !=''){ $page_info_idval = $page_info_id['value'];}
else{ $page_info_idval = set_value('page_info_id');}

if(isset($page_info_content['value']) && $page_info_content['value'] !=''){ $page_info_contentval = $page_info_content['value'];}
else{ $page_info_contentval = set_value('page_info_content');}

if(isset($parent_front_menu['value']) && $parent_front_menu['value'] !=''){ $parent_front_menuval = $parent_front_menu['value'];}
else{ $parent_front_menuval = set_value('parent_front_menu');}

if(isset($front_menu_name['value']) && $front_menu_name['value'] !=''){ $front_menu_nameval = $front_menu_name['value'];}
else{ $front_menu_nameval = set_value('front_menu_name');}

if(isset($front_menu_title['value']) && $front_menu_title['value'] !=''){ $front_menu_titleval = $front_menu_title['value'];}
else{ $front_menu_titleval = set_value('front_menu_title');}

if(isset($front_menu_target['value']) && $front_menu_target['value'] !=''){ $front_menu_targetval = $front_menu_target['value'];}
else{ $front_menu_targetval = set_value('front_menu_target');}
if($front_menu_targetval==''){$front_menu_targetval = '_self';}

if(isset($front_menu_type['value']) && $front_menu_type['value'] !=''){ $front_menu_typeval = $front_menu_type['value'];}
else{ $front_menu_typeval = set_value('front_menu_type');}
if($front_menu_typeval==''){$front_menu_typeval = 'Page';}

if(isset($front_menu_link['value']) && $front_menu_link['value'] !=''){ $front_menu_linkval = $front_menu_link['value'];}
else{ $front_menu_linkval = set_value('front_menu_link');}

if(isset($link_uri['value']) && $link_uri['value'] !=''){ $link_urival = $link_uri['value'];}
else{ $link_urival = set_value('link_uri');}

if(isset($link_url['value']) && $link_url['value'] !=''){ $link_urlval = $link_url['value'];}
else{ $link_urlval = set_value('link_url');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($front_menu_id['value']) && $front_menu_id['value'] !=''){ $front_menu_idval = $front_menu_id['value'];}
else{ $front_menu_idval = set_value('front_menu_id');}	
if($front_menu_idval==''){$front_menu_idval = 0;}		
	
$site_url = str_replace('/index.php','',site_url("/"));
?>
<script type="text/javascript" src="<?php echo $site_url?>common/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();	
	function checkfrmfront_menu_form(){
		
		var front_menu_id = document.frmfront_menu.front_menu_id;
		
		var front_menu_category_id = document.frmfront_menu.front_menu_category_id;
		var elementmessage = document.getElementById("errmsg_front_menu_category_id");
		elementmessage.innerHTML = '';
		j('#front_menu_category_id').removeClass('error_input200x20').addClass('input200x20');
		if(front_menu_category_id.value==0){
			elementmessage.innerHTML = 'Front Menu Category name is mandatory';
			front_menu_category_id.focus();
			j('#front_menu_category_id').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		
		var front_menu_name = document.frmfront_menu.front_menu_name;
		var elementmessage = document.getElementById("errmsg_front_menu_name");
		elementmessage.innerHTML = '';
		j('#front_menu_name').removeClass('field400_err').addClass('field400');
		if(front_menu_name.value==''){
			elementmessage.innerHTML = 'You are missing menu name';
			front_menu_name.focus();
			j('#front_menu_name').removeClass('field400').addClass('field400_err');
			return false;
		}
		var front_menu_title = document.frmfront_menu.front_menu_title;
		var elementmessage = document.getElementById("errmsg_front_menu_title");
		elementmessage.innerHTML = '';
		j('#front_menu_title').removeClass('field400_err').addClass('field400');
		if(front_menu_title.value==''){
			elementmessage.innerHTML = 'You are missing Menu Title name';
			front_menu_title.focus();
			j('#front_menu_title').removeClass('field400').addClass('field400_err');
			return false;
		}
		
		var menu_typeid = document.getElementsByName('front_menu_type');
		for(var i=0;i<menu_typeid.length; i++){
			if(menu_typeid[i].checked==true){
				if(menu_typeid[i].value=='Page'){}
				else if(menu_typeid[i].value=='Old Page'){
					var fieldidname = document.getElementById("link_uri");
					var elementmessage = document.getElementById("errmsg_link_uri");
					elementmessage.innerHTML = '';
					if(fieldidname.value=='' || fieldidname.value==0){
						elementmessage.innerHTML = 'Old Page is mandatory';
						fieldidname.focus();
						return false;
					}
					else if(fieldidname.value.length<3){
						elementmessage.innerHTML = 'Old Page should be at least 3 characters';
						fieldidname.focus();
						return false;
					}
					else if(checkwebchar(fieldidname.value)==false){
						elementmessage.innerHTML = 'Invalid! Only characters are allowed.';
						fieldidname.focus();
						return false;
					}
				}
				else if(menu_typeid[i].value=='URI'){
					var fieldidname = document.getElementById("link_uri");
					var elementmessage = document.getElementById("errmsg_link_uri");
					elementmessage.innerHTML = '';
					if(fieldidname.value !=''){
						if(fieldidname.value.length<3){
							elementmessage.innerHTML = 'URI should be at least 3 characters';
							fieldidname.focus();
							return false;
						}
						else if(checkwebchar(fieldidname.value)==false){
							elementmessage.innerHTML = 'Invalid! Only characters are allowed.';
							fieldidname.focus();
							return false;
						}
					}
				}
				else{
					var fieldidname = document.getElementById("link_url");
					var elementmessage = document.getElementById("errmsg_link_url");
					elementmessage.innerHTML = '';
					if(fieldidname.value=='' || fieldidname.value==0){
						elementmessage.innerHTML = 'URL is mandatory';
						fieldidname.focus();
						return false;
					}
					else if(fieldidname.value.length<10){
						elementmessage.innerHTML = 'URL should be at least 10 characters';
						fieldidname.focus();
						return false;
					}
					else if(isURL(fieldidname.value)==false){
						elementmessage.innerHTML = 'Invalid URL!. It should be like http://bditsoft.com/';
						fieldidname.focus();
						return false;
					}
				}
			}
		}
		return true;
	}
	
	tinyMCE.init({
		mode : "exact",
		elements : "page_info_content",
		theme : "advanced",
		plugins : "advimage,advlink,media,contextmenu",
		theme_advanced_buttons1_add_before : "newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,separator,",
		theme_advanced_buttons3_add_before : "",
		theme_advanced_buttons3_add : "media",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "page_info_content",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : true,
		apply_source_formatting : true,
		force_br_newlines : true,
		force_p_newlines : false,	
		relative_urls : true
	});

	function page_info_content(field_name, url, type, win) {
		var page_info_contenturl = "<?php echo $site_url?>common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
		}
		tinyMCE.activeEditor.windowManager.open({
			url: "<?php echo $site_url?>common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
			width: 800,
			height: 300,
			inline : "yes",
			close_previous : "no"
		},{
			window : win,
			input : field_name
		});
	}

	function showmenutype(){
		var menu_typeid = document.getElementsByName('front_menu_type');
        for(var k=0;k<menu_typeid.length;k++){
			if(menu_typeid[k].checked==true){
				var menu_typeval = menu_typeid[k].value;
				if(menu_typeval == 'Page'){
					j('#Page').fadeIn('fast');
					j('#URI').hide();
					j('#URL').hide();
				}
				else if (menu_typeval == "Old Page" || menu_typeval == "URI"){
					j('#Page').hide();
					j('#URI').fadeIn('fast');
					j('#URL').hide();
				}
				else if (menu_typeval == "URL"){
					j('#Page').hide();
					j('#URI').hide();
					j('#URL').fadeIn('fast');
				}
			}
		}
	}

</script>
<style type="text/css">
.show_hide_row{
    background: none repeat scroll 0 0 #f3f3f3;
    color: #0A0A09;
    line-height: 20px;
    padding: 2px 5px 2px 10px;
}
label{ cursor:pointer;}
</style>
<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	Front Menu Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'front_menu/front_menu_list';?>" class="white14bold">Show Front Menu List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
	<tr>
		<td align="center">
			<?php           
            $attributes = array('name' => 'frmfront_menu', 'onsubmit'=>'return checkfrmfront_menu_form();');
            echo form_open_multipart(site_url("/").'front_menu/save_front_menu', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="2" cellpadding="0">										
                    <tr>
                        <td class="black11bold evenrow" width="180" align="left">Front Menu Category Name: <font class="red14bold">*</font></td>
						<td align="left" class="black11normal oddrow">
							<?php 
							$admin_data['front_menu_category_idval'] = $front_menu_category_idval;
							$this->load->view('front_menu_category/addnew_byjquery',$admin_data);?>									
						</td>
					</tr>					
                    <tr>
                        <td class="black11bold evenrow" align="left">Parent Front Menu: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal evenrow">
                            <select name="parent_front_menu" id="parent_front_menu" class="input200x20">
                                <option value="0"<?php if($parent_front_menuval=='0'){echo ' selected="selected"';}?>>Root</option>
                                <?php
                                $result = $commonmodel->getallrowbysqlquery("select * from front_menu where front_menu_publish=1 and parent_front_menu = 0");
                                
                                if(count($result)>0 && $result !=''){
                                    foreach($result as $row){
                                        $table_id = $row->front_menu_id;
                                        $table_field = $row->front_menu_name;
                                        $selected = '';
                                        if($table_id==$parent_front_menuval){$selected = ' selected="selected"';}
                                        ?>
                                        <option value="<?php echo $table_id;?>" <?php echo $selected;?>><?php echo $table_field;?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="black11bold oddrow" align="left">Front Menu Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal oddrow">
                            <?php 
                            $data = array('name'     => 'front_menu_name',
                                          'id'       => 'front_menu_name',
                                          'tabindex'       => '2',
                                          'value'    => $front_menu_nameval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_front_menu_name" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="black11bold evenrow" align="left">Menu Title: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal evenrow">
                            <?php 
                            $data = array('name'     => 'front_menu_title',
                                          'id'       => 'front_menu_title',
                                          'tabindex'       => '2',
                                          'value'    => $front_menu_titleval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_front_menu_title" class="errormsg"></span>
                        </td>
                    </tr>
					<tr>
					 <td class="black12bold oddrow" align="left" valign="top">Menu Target Link *:</td>
						 <td class="black12normal oddrow" align="left">
							 <input tabindex="6" type="radio" name="front_menu_target" value="_self" <?php if($front_menu_targetval=='_self'){echo ' checked="checked"';}?> />
							   Self &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							 <input tabindex="7" type="radio" name="front_menu_target" value="_blank" <?php if($front_menu_targetval=='_blank'){echo ' checked="checked"';}?> />
							   Blank
						 </td>
				   </tr>
				   <tr>
					 <td class="black12bold evenrow" align="left" valign="top">Menu Type *:</td>
						 <td class="black12normal evenrow" align="left">
						  	<label>
                          		<input tabindex="6" type="radio" name="front_menu_type" onclick="showmenutype();" value="Page" <?php if($front_menu_typeval=='Page'){echo ' checked="checked"';}?> />
						  		&nbsp;New Page &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           	</label>
                           	<label>
						   		<input tabindex="6" type="radio" name="front_menu_type" onclick="showmenutype();" value="Old Page" <?php if($front_menu_typeval=='Old Page'){echo ' checked="checked"';}?> />
						   		&nbsp;Old Page &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           	</label>
                            <label>
						   		<input tabindex="6" type="radio" name="front_menu_type" onclick="showmenutype();" value="URI" <?php if($front_menu_typeval=='URI'){echo ' checked="checked"';}?> />
						   		&nbsp;URI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           	</label>
                            <label>
						   		<input tabindex="7" type="radio" name="front_menu_type" onclick="showmenutype();" value="URL" <?php if($front_menu_typeval=='URL'){echo ' checked="checked"';}?> />
						   		&nbsp;URL
                           	</label>
						</td>
				   </tr>
				    <tr id="Page"<?php if($front_menu_typeval !='Page'){echo ' style="display:none"';}?>>
                        <td class="black12bold show_hide_row" align="left">Page Description: <font class="red14bold">*</font></td>
                        <td align="left" class="show_hide_row">
                            <textarea tabindex="3" name="page_info_content" id="page_info_content" style="width:700px; height: 200px"><?php echo $page_info_contentval; ?></textarea>
                        	<br />
                            <span id="errmsg_page_info_content" class="errormsg"></span>
						</td>
                    </tr>
					<tr id="URI"<?php if($front_menu_typeval !='URI'){echo ' style="display:none"';}?>>
                        <td class="black11bold oddrow" align="left">URI : <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal oddrow">
                            <?php echo site_url("/");?>
							<?php 
                            $data = array('name'     => 'link_uri',
                                          'id'       => 'link_uri',
                                          'tabindex'       => '2',
                                          'value'    => $link_urival,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_link_uri" class="errormsg"></span>
                        </td>
                    </tr>
					
					<tr id="URL"<?php if($front_menu_typeval !='URL'){echo ' style="display:none"';}?>>
                        <td class="black11bold oddrow" align="left">URL : <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal oddrow">
                            <?php 
                           	$data = array('name'     => 'link_url',
                                          'id'       => 'link_url',
                                          'tabindex'       => '2',
                                          'value'    => $link_urlval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>[Ex: http://bditsoft.com]
                            <span id="errmsg_link_url" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="black11bold evenrow"></td>
                        <td align="left" class="black11normal evenrow" id="finalsaverow">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            <input type="hidden" name="front_menu_id" id="front_menu_id" value="<?php echo $front_menu_idval;?>" />
                        </td>
                    </tr>									
                </table>
            <?php echo form_close(); ?>
       	</td>
   	</tr>
</table>