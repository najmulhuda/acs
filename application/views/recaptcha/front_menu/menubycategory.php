
<ul>
    <?php
   $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80"){
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }
    else{
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        if ($this->uri->segment(1) === FALSE){$segment1name = '';}
        else{$segment1name = $this->uri->segment(1);} 
    
    $query = $this->Common_model->getallrowsbyconditionwithorder('front_menu', array('front_menu_category_id' => $front_menu_category_id, 'front_menu_publish'=>1), 'showing_order', 'asc');
    $str = '';
    if(count($query)>0){
        $i=0;
        foreach($query as $rows){
            $i++;
            $href = '';
            if ($rows->front_menu_type=='URL'){
                $href = $rows->front_menu_link;
            }
            else{
                $href= site_url("/").$rows->front_menu_link;
            }
            
			$targettype = '';
            if($rows->front_menu_target == "_blank"){
                $targettype = ' target="_blank"';
            }
            
			$selectclass = '';
            if($href == "$pageURL"){
                $selectclass = ' class="current"';
            }
            if(site_url("/") == "$pageURL" && $rows->front_menu_link=='home'){$selectclass = 'class="current"';}
            $front_menu_name = $rows->front_menu_name;
            $lastclass = '';
            if($i==count($query)){
                $lastclass = ' class="last"';
            }
            $str .= "<li $selectclass>
                        <a$targettype href=\"$href\">$front_menu_name</a>
                    </li>";
            
        }
    }
    
	
	if(!$this->session->userdata('students_id')){
		$loginurl = site_url('/').'students/students_login/';				
		$selectclass = '';
		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$loginurl\">Sign In</a>
				</li>";
					
		$registrationurl = site_url('/').'students/signup';
		$selectclass = '';
		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$registrationurl\">Registration</a>
				</li>";
				
	}
	else{
		$loginurl = site_url('/').'students';				
		$selectclass = '';
		if($loginurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$loginurl\">My Account</a>
				</li>";
					
		$registrationurl = site_url('/').'students/logout';
		$selectclass = '';
		if($registrationurl == "$pageURL"){
			$selectclass = ' class="current"';
		}
		$str .= "<li $selectclass>
					<a href=\"$registrationurl\">Log Out</a>
				</li>";
	}
	
	echo $str;
	
    ?>
</ul>