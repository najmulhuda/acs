<script type="text/javascript" src="<?php echo site_url("/");?>fancylightbox/fancybox-1.3.4.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo site_url("/");?>fancylightbox/fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
	var j = jQuery.noConflict();
	j(document).ready(function() {
		j("a.show_big_image").fancybox({
			'overlayShow'	: false,
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic'
		});
	});
</script>
<div class="article">
	<div class="articleBox"><div class="articleBoxIn"><div class="articleBoxInner">
		<div class="articleInner">	
			
			<h3>
				<?php
				echo $title;
				?>
			</h3>
			<div class="productlist"><!-- Begin: productlist -->
				<ul>
					<?php
					if(count($queryproperty)>0){
						foreach($queryproperty as $featurepropertyrow){
							$property_id = $featurepropertyrow->property_id;
							$property_name = $featurepropertyrow->property_name;
							$property_image = $featurepropertyrow->property_image;
							$property_thumbimage = $featurepropertyrow->property_thumbimage;
							$property_imagesrc = str_replace('/index.php','',site_url("/")).'application/views/admin/property_images/'.$property_image;
							$property_thumbimagesrc = str_replace('/index.php','',site_url("/")).'application/views/admin/property_images/'.$property_thumbimage;
							?>
							<li>
								<img src="<?php echo $property_thumbimagesrc;?>" width="170" height="105" alt="<?php echo $property_name;?>" />
								<div class="productlisDetail">
									<span><?php echo $property_name;?></span>
									<a href="<?php echo site_url("/")?>property/property_details/<?php echo $property_id;?>">Details</a>
								</div>
							</li>
							<?php
						}
					}
					else{
						echo "<li>No property found.</li>";
					}
					?>						
				</ul>
			</div><!-- End: productlist -->
			<div class="clear"></div>               
		</div>
		<div class="clear"></div>		
	</div></div></div>
</div>
