<?php
$site_url = str_replace('/index.php','',site_url("/"));

if(isset($dynamic_data) && $dynamic_data !=''){ $dynamic_data = $dynamic_data;}
else{$dynamic_data = '';}

if(isset($module_name) && $module_name !=''){ $module_nameval = $module_name;}
else{ $module_nameval = set_value('module_name');}

if(isset($table_name) && $table_name !=''){ $table_nameval = $table_name;}
else{ $table_nameval = set_value('table_name');}

if(isset($module_id) && $module_id !=''){ $module_idval = $module_id;}
else{ $module_idval = set_value('module_id');}
if($module_idval==''){$module_idval = 0;}

if(isset($table_id) && $table_id !=''){ $table_idval = $table_id;}
else{ $table_idval =set_value('table_id');}
if($table_idval==''){$table_idval = 0;}

if(isset($submit) && $submit !=''){ $submitval = $submit;}
else{ $submitval = ' Save ';}
?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	<?php echo $module_nameval;?> Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'module/dynamic_list/'.$table_nameval;?>" class="white14bold">Show <?php echo $module_nameval;?> List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmdynamicmodule', 'onsubmit'=>'return checkfrmdynamic_form();');
            echo form_open_multipart(site_url("/").'module/save_dynamic', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <?php
					$textarea_yes_or_no = 'no';
					$textarea_namearray = '';
					$validatestring = "";
					$module_fields_liststr = '';
					$isFromDate = '';
					$isToDate = '';
					$ismodule_urlpresent = '';
					$sqlquery = "select * from module_fields where module_id = $module_idval and module_fields_publish = 1 order by showing_order asc";
					$querydata = $this->Common_model->getallrowbysqlquery($sqlquery);
					if($querydata){
						foreach($querydata as $module_fieldsrow){
							$form_fields_id = $module_fieldsrow->form_fields_id;
							$form_fields_row = $this->Common_model->getonerowallvaluebycondition('form_fields', array('form_fields_id'=>"$form_fields_id"));
							if($form_fields_row){
								$front_label= $form_fields_row->front_label;
								$form_fields_name= $form_fields_row->form_fields_name;
								
								$form_fields_type = $form_fields_row->form_fields_type;
								$default_value = $form_fields_row->default_value;
							
								$form_fields_value_required = $form_fields_row->form_fields_value_required;
								$form_fields_value_requiredstr = '';
								if($form_fields_value_required == 1){$form_fields_value_requiredstr = '<font class="red14bold">*</font>';}
								
								$form_fields_input_validation = $form_fields_row->form_fields_input_validation;
							
								$error_form_fields_name = 'error_'.$form_fields_name;
							
								$default_valuestr = '';
								if($form_fields_type == 'checkbox'){
									$arrayname = $form_fields_name."[]";
									$form_fields_nameval = '';
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
										$form_fields_nameval = explode(', ',$form_fields_nameval);
									}
									else{$form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<table cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
									$form_fields_optionsquery = $commonmodel->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										$j=0;
										foreach($form_fields_optionsquery as $form_fields_row_multiple){
											$j++;
											$label_name = $form_fields_row_multiple->label_name;
											$label_value = $form_fields_row_multiple->label_value;
											if($j==4){
												$j=0;
												$default_valuestr .= '</tr><tr>';
											}
											$checked = '';
											if(is_array($form_fields_nameval) && in_array($label_value, $form_fields_nameval)){
												$checked = ' checked="checked"';
											}
											else{
												$checked = set_checkbox($form_fields_name.'[]', $label_value);
											}
											$default_valuestr .= '<td width="200" class="black12normal" align="left">
																	<label><input type="checkbox" name="'.$form_fields_name.'[]" value="'.$label_value.'"'.$checked.' /> '.$label_name.'	</label>																					
																</td>';
										}	
									}
									else{
										$default_valuestr .= '<td class="black12normal">There is no data found</td>';
									}
									$default_valuestr .= '</tr>
														</table>
														';
								}
								elseif($form_fields_type == 'radiobutton'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{$form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<table cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
									$form_fields_optionsquery = $commonmodel->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if($form_fields_optionsquery){
										$j=0;
										foreach($form_fields_optionsquery as $form_fields_row_multiple){
											$j++;
											$label_name = $form_fields_row_multiple->label_name;
											$label_value = $form_fields_row_multiple->label_value;
											if($j==4){
												$j=0;
												$default_valuestr .= '</tr><tr>';
											}																	
											$checked = '';
											if($label_value==$form_fields_nameval){
												$checked = ' checked="checked"';
											}
											
											$default_valuestr .= '<td width="200" align="left"><label><input type="radio" name="'.$form_fields_name.'" value="'.$label_value.'"'.$checked.' /> '.$label_name.'</label></td>';
										}	
									}
									else{
										$default_valuestr .= '<td>There is no data found</td>';
									}
									$default_valuestr .= '</tr>
														</table>
														';
								}
								elseif($form_fields_type == 'dropdown'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									if($form_fields_nameval==''){$form_fields_nameval = $default_value;}														
									
									$default_valuestr = '<select name="'.$form_fields_name.'" class="input200x20" >';
									$form_fields_optionsquery = $commonmodel->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										foreach($form_fields_optionsquery as $form_fields_row_multiple){
											$label_name = $form_fields_row_multiple->label_name;
											$label_value = $form_fields_row_multiple->label_value;
											$selected = '';
											if($form_fields_nameval==$label_value){$selected = ' selected="selected"';}
											$default_valuestr .= '<option value="'.$label_value.'"'.$selected.'>'.$label_name.'</option>';
										}	
									}
									else{
										$default_valuestr .= '<option value="">There is no data found</option>';
									}
									$default_valuestr .= '</select>';
								}
								elseif($form_fields_type == 'multipleselect'){
									$default_valuestr = '<select multiple="multiple" name="'.$form_fields_name.'" class="input200x20" >';
									$form_fields_optionsquery = $commonmodel->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										foreach($form_fields_optionsquery as $form_fields_row_multiple){
											$label_name = $form_fields_row_multiple->label_name;
											$label_value = $form_fields_row_multiple->label_value;
											$default_valuestr .= '<option value="'.$label_value.'">'.$label_name.'</option>';
										}	
									}
									else{
										$default_valuestr .= '<option value="">There is no data found</option>';
									}
									$default_valuestr .= '</select>';
								}
								elseif($form_fields_type == 'textfield' || $form_fields_type == 'price'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										if($form_fields_input_validation=='integer'){
											$form_fields_nameval = floor($dynamic_data[$form_fields_name]);
										}
										else{
											$form_fields_nameval = $dynamic_data[$form_fields_name];
										}
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									
									$onkeyattribute = '';
									if($form_fields_input_validation=='module_url'){
										$ismodule_urlpresent = 'Yes';
										$fieldidname = "document.frmdynamicmodule.".$form_fields_name;
										$onkeyattribute = " onkeyup=\"check_".$form_fields_input_validation."(".$fieldidname.", '".$front_label."', '".$error_form_fields_name."')\"";
									}
									
									if($form_fields_nameval==''){$form_fields_nameval = $default_value;}
									$onkeyupattribute = '';
									
									//==============================================Add new row for showing value into URI field==============================//
									if($form_fields_name=='name'){$onkeyattribute = " onkeyup=\"showthisurivalue(document.frmdynamicmodule.$form_fields_name, document.frmdynamicmodule.uri_name, '$front_label', 'error_$form_fields_name')\"";}
									//elseif($form_fields_name=='faq_name'){$onkeyupattribute = " onkeyup=\"showthisurivalue(document.frmdynamicmodule.$form_fields_name, document.frmdynamicmodule.faq_uri, '$front_label', 'error_$form_fields_name')\"";}
									//elseif($form_fields_name=='product_name'){$onkeyupattribute = " onkeyup=\"showthisurivalue(document.frmdynamicmodule.$form_fields_name, document.frmdynamicmodule.product_uri, '$front_label', 'error_$form_fields_name')\"";}
									
									$default_valuestr = '<input type="text" class="input200x20"'.$onkeyupattribute.' name="'.$form_fields_name.'" value="'.$form_fields_nameval.'" '.$onkeyattribute.' />';
								}
								elseif($form_fields_type == 'textarea'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<textarea rows="3" cols="50" name="'.$form_fields_name.'" class="textarea1">'.$form_fields_nameval.'</textarea>';
									
									$label_value = '';
									$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										foreach($form_fields_optionsquery as $form_fieldrow_options){
											$label_name = $form_fieldrow_options->label_name;
											$label_value = $form_fieldrow_options->label_value;
										}	
									}
									if($label_value != '' && $label_value == 'yes'){
										$textarea_yes_or_no = 'yes';
										$textarea_namearray[] = $form_fields_name;
									}
								}
								elseif($form_fields_type == 'image'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<input type="file" name="'.$form_fields_name.'" id="'.$form_fields_name.'" />';
									$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										foreach($form_fields_optionsquery as $form_fieldrow_options){
											$label_name = $form_fieldrow_options->label_name;
											$label_value = $form_fieldrow_options->label_value;
											$default_valuestr .= '&nbsp;&nbsp;&nbsp;Width: '.$label_name.' and Height: '.$label_value;
											$default_valuestr .= $form_fields_nameval;
										}	
									}
								}
								elseif($form_fields_type == 'datecalendar'){
									
									if(isset($form_fields_name) && $form_fields_name>0){$form_fields_nameval = date('Y-m-d',$form_fields_name);}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									if($form_fields_nameval==''){$form_fields_nameval = $default_value;}
									
									if($form_fields_nameval !='' && is_numeric($form_fields_nameval)){$form_fields_nameval = date('Y-m-d', $form_fields_nameval);}
									
									if($form_fields_name=='from_date'){
										$isFromDate = 'Yes';
										if($table_idval==0)
											$form_fields_nameval = date('Y-m-d');
											
										$default_valuestr = '<input name="'.$form_fields_name.'" id="'.$form_fields_name.'" type="text" value="'.$form_fields_nameval.'" class="input100x20 calendartext" />';
									}
									elseif($form_fields_name=='to_date'){
										$isToDate = 'Yes';
										if($table_idval==0)
											$form_fields_nameval = date('Y-m-d',strtotime(date('Y-m-d'))+86400);
										$default_valuestr = '<input name="'.$form_fields_name.'" id="'.$form_fields_name.'" type="text" value="'.$form_fields_nameval.'" class="input100x20 calendartext" />';
									}
									else{
										$form_fields_nameval = date('Y-m-d');
										$default_valuestr ='<script type="text/javascript" src="'.$site_url.'common/calendar/calendar.js?random=20051112"></script>
															<link type="text/css" rel="stylesheet" href="'.$site_url.'common/calendar/calendar.css" />'.
															"<input name=\"$form_fields_name\" type=\"text\" value=\"$form_fields_nameval\" class=\"input120x20\" />
															<input type=\"button\" class=\"calendarbut\" value=\"\" onClick=\"displayCalendar(document.frmdynamicmodule.".$form_fields_name.", 'yyyy-mm-dd', this);\">";
									}
								}
								elseif($form_fields_type == 'yesno'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									if($form_fields_nameval==''){$form_fields_nameval = $default_value;}
									
									$default_valuestr = '<table cellspacing="1" cellpadding="0" border="0" align="left"><tr>';
									$default_valuestr .= '<td width="200" align="left" class="black12normal"><label><input type="radio" name="'.$form_fields_name.'"';
									if($form_fields_nameval=='Yes'){$default_valuestr .= ' checked="checked"';}															
									 $default_valuestr .= 'value="Yes" /> Yes </label></td>';
									$default_valuestr .= '<td width="200" align="left" class="black12normal"><label><input type="radio" name="'.$form_fields_name.'"';
									if($form_fields_nameval=='No'){$default_valuestr .= ' checked="checked"';}															
									 $default_valuestr .= ' value="No" /> No </label></td>';
									$default_valuestr .= '<td width="200" align="left">&nbsp;</td>';
									$default_valuestr .= '</tr></table>';
								}	
								elseif($form_fields_type == 'foreign_key'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<select name="'.$form_fields_name.'" class="input200x20" >
														<option value="">Select '.$front_label.'</option>';
									$label_name = '';
									$label_value = '';
									$form_fields_optionsquery = $this->Common_model->getallrow('form_fields_options', array('form_fields_id'=>$form_fields_id,  'form_fields_options_publish'=> 1));
									if(count($form_fields_optionsquery)>0){
										foreach($form_fields_optionsquery as $rowform_fields_options){
											$label_name = $rowform_fields_options->label_name;
											$label_value = $rowform_fields_options->label_value;
										}	
									}
									
									if($label_name != '' && $label_value !=''){
										$primaryidname = $label_name.'_id';
										$publishname = $label_name.'_publish';
										$default_valuestr .= $commonmodel->get_onetableallfieldvalueforselect($label_name, $primaryidname, $form_fields_nameval, $label_value, array($publishname=>1));
									}
									$default_valuestr .= '</select>';
								}
								elseif($form_fields_type == 'file'){
									if($dynamic_data !='' && is_array($dynamic_data)){
										$form_fields_nameval = $dynamic_data[$form_fields_name];
									}
									else{ $form_fields_nameval = set_value($form_fields_name);}
									
									$default_valuestr = '<input type="file" name="'.$form_fields_name.'" id="'.$form_fields_name.'" /><br />[Only txt|pdf|doc|docx|excell|accdb file type allowed]';
								}
								
								$default_valuestr .= '&nbsp;&nbsp;<span id="'.$error_form_fields_name.'" class="red12bold"></span>';
								$module_fields_liststr .= '<tr>
																	<td nowrap="nowrap" class="black12bold" width="120" align="left">
																		'.$front_label.$form_fields_value_requiredstr.'
																	</td>
																	<td align="left" class="black12normal">'.$default_valuestr.'</td>
																</tr>';
								
								if($table_nameval=='projects'){
									if($form_fields_name == 'project_picture'){
										
										$admin_data['form_fields_name'] = $form_fields_name;										
										$admin_data['foreign_fieldval'] = $foreign_fieldval = $table_idval;										
										$admin_data['more_image_table_name'] = $more_image_table_name = 'project_more_image';
										$admin_data['foreign_fieldname'] = $foreign_fieldname = 'projects_id';
										$module_fields_liststr .= $commonmodel->more_image_form($more_image_table_name, $foreign_fieldname, $foreign_fieldval);
										$this->load->view('module/dynamicmoreimagejs',$admin_data);
										
									}
								}
								
								if($form_fields_value_required>0 ){
									if($form_fields_input_validation=='images' || $form_fields_input_validation=='document_file'){
										if($table_idval>0){}
										else{
	$validatestring .= "var returnval = check_".$form_fields_input_validation."('".$form_fields_name."', '".$front_label."', '".$error_form_fields_name."');
	if(returnval==false){return false;}
	";
										}
									}
									elseif($form_fields_input_validation=='redio_or_checkbox'){
										if($form_fields_type == 'checkbox'){
											$form_fields_name = $form_fields_name.'[]';
										}
	$validatestring .= "var returnval = check_redio_or_checkbox('frmdynamicmodule', '".$form_fields_name."', '".$front_label."', '".$error_form_fields_name."');
	if(returnval==false){return false;}
	";
									}
									else{
										$fieldidname = "document.frmdynamicmodule.".$form_fields_name;
	$validatestring .= "var returnval = check_".$form_fields_input_validation."(".$fieldidname.", '".$front_label."', '".$error_form_fields_name."');
	if(returnval==false){return false;}
	";
									}
								}
							}
						}
					}									
					echo $module_fields_liststr;
					
					if($table_nameval=='product'){
						$admin_data['table_id'] = $table_idval;
						$this->load->view('product/extra_form',$admin_data);
					}
					?>
                    <tr>
                        <td align="left" width="140" class="black11bold"></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');                           
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            
                            <input type="hidden" name="module_id" id="module_id" value="<?php echo $module_idval;?>" />
                            <input type="hidden" name="module_name" id="module_name" value="<?php echo $module_nameval;?>" />
                            <input type="hidden" name="table_name" id="table_name" value="<?php echo $table_nameval;?>" />
                            <input type="hidden" name="table_id" id="table_id" value="<?php echo $table_idval;?>" />
                        </td>
                    </tr>								
				</table>
            <?php 
			echo form_close();
			?>
			
            <?php
			if($isToDate !='' && $isFromDate !='' ){
			?>
            	<link type="text/css" href="<?php echo $site_url?>common/From_and_To_Date_Calendar/anytimec.css" rel="stylesheet" />
				<script type="text/javascript" src="<?php echo $site_url?>common/From_and_To_Date_Calendar/anytimec.js"></script>
				<script type="text/javascript">				
					var j = jQuery.noConflict();					
					var oneDay = 24*60*60*1000;
					var rangeDemoFormat = "%Y-%m-%d";
					var d=new Date();
					var toDayTime=d.getTime();
					var rangeDemoConv = new AnyTime.Converter({format:rangeDemoFormat});
					
					var today = new Date();
					var oneYearLater = new Date(today.getTime() + 31536000000);
					j("#from_date").AnyTime_noPicker().removeAttr("disabled").AnyTime_picker({
						earliest: today,
						format: rangeDemoFormat,
						latest: oneYearLater
					});
					
					j("#from_date").change( function(e) { try {
						var fromDay = rangeDemoConv.parse(j("#from_date").val()).getTime();
						var dayLater = new Date(fromDay+oneDay);
						dayLater.setHours(0,0,0,0);
						var oneYearLater = new Date(fromDay+(365*oneDay));
						oneYearLater.setHours(23,59,59,999);
						
						j("#to_date").AnyTime_noPicker().removeAttr("disabled").val(rangeDemoConv.format(dayLater)).AnyTime_picker({ 
								earliest: dayLater,
								format: rangeDemoFormat,
								latest: oneYearLater
							});
						} catch(e){ j("#to_date").val("").attr("disabled","disabled"); 	
					} } );
						
				</script>
            <?php
			}
			$textarealoadstr = '';
			if($textarea_yes_or_no == 'yes'){
				echo "<script type=\"text/javascript\" src=\"".$site_url."common/jscripts/tiny_mce/tiny_mce.js\"></script>";
				if($textarea_namearray !='' && is_array($textarea_namearray)){
					foreach($textarea_namearray as $textareaform_fields_name){
$textarealoadstr = 'tinyMCE.init({
		mode : "exact",
		elements : "'.$textareaform_fields_name.'",
		theme : "advanced",
		plugins : "advimage,advlink,media,contextmenu",
		theme_advanced_buttons1_add_before : "newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,separator,",
		theme_advanced_buttons3_add_before : "",
		theme_advanced_buttons3_add : "media",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "'.$textareaform_fields_name.'",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : true,
		apply_source_formatting : true,
		force_br_newlines : true,
		force_p_newlines : false,	
		relative_urls : true
	});

	function '.$textareaform_fields_name.'(field_name, url, type, win) {
		var '.$textareaform_fields_name.'url = "'.$site_url.'common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
		}
		tinyMCE.activeEditor.windowManager.open({
			url: "'.$site_url.'common/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
			width: 800,
			height: 300,
			inline : "yes",
			close_previous : "no"
		},{
			window : win,
			input : field_name
		});
	}';
					}
				}				
			}
			?>
			<script language="JavaScript" type="text/javascript">
                var j = jQuery.noConflict();
                function checkfrmdynamic_form(){		
                    <?php echo $validatestring;
					if($table_nameval=='product' && $table_idval==0){?>						
						if(check_packing_type()==false){return false;}
					<?php
					}
					?>
					return true;
                }
				<?php
				if($ismodule_urlpresent !=''){
					?>
					function showthisurivalue(frmidname, tofieldidname, lebelname, erroridname){
						var module_url = frmidname.value;		
						//alert(module_url);
						var elementmessage = document.getElementById(erroridname);
						elementmessage.innerHTML = '';
						if(frmidname.value==''){
							elementmessage.innerHTML = lebelname+' is mandatory.';
							frmidname.focus();
							return false;
						} 	
						else{
							var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-+_";
							var IsValid=true;
							var Char;
							var module_urlval = '';
							for (var i = 0; i < module_url.length; i++){ 
								Char = module_url.charAt(i); 
								Char = Char.replace(" ", '-');
								Char = Char.replace("'", '');
								Char = Char.replace('"', '');
								Char = Char.replace('.', '');
								Char = Char.replace(',', '');
								Char = Char.replace('!', '');
								Char = Char.replace('/', '-or-');
								Char = Char.replace('?', '');
								Char = Char.replace('&', 'and');								
								Char = Char.replace('$', 'dollar');
								Char = Char.replace('+', '-plus-');
								Char = Char.replace('&amp;', 'and');
								Char = Char.replace('(', '');
								Char = Char.replace(')', '');
								module_urlval = module_urlval+Char;
							}
							module_url = '';
							for (i = 0; i < module_urlval.length && IsValid == true; i++){ 
								Char = module_urlval.charAt(i); 
								if (ValidChars.indexOf(Char) == -1){
									IsValid = false;
									elementmessage.innerHTML = lebelname+' is invalid. Only "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-" is alowed';
									tofieldidname.value = module_url.toLowerCase();
									frmidname.focus();
									return false;
								}
								else{
									module_url = module_url+Char;
								}
							}
					
							if(IsValid == false){
								tofieldidname.value = module_url.toLowerCase();
								return false;
							}
							else{
								tofieldidname.value = module_url.toLowerCase();
								return true;
							}
						}
					}
					
					<?php 
				}
				echo $textarealoadstr;?>
				
				function deleteonerowwithattach(table_name, table_id_value, attachedfieldname, tableidname){					
					j.post('<?php echo site_url("/");?>admin/removerowwithattachedbyjquery', {"table_name":table_name, "table_id_value":table_id_value, "attachedfieldname": attachedfieldname},
					function(data) {
						if(data=='OK'){
							j("#"+tableidname).parent("li").slideUp(500, function() {
								j(this).remove();			  	
							});
						}
					});					
				}
            </script>
		</td>
	</tr>
</table>




