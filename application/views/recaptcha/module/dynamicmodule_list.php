<?php
$site_url = str_replace('/index.php','',site_url("/"));

if ($this->uri->segment(2) === FALSE){$segment2name = '';}
else{$segment2name = $this->uri->segment(2);}
$module_totalrows = $commonmodel->getcount_all_resultsrows($table_name, '', 'modified_date');
$module_fatchingurl = site_url("/").'module/fetching_dynamic_list/';
?>
<input type="hidden" name="module_id" id="module_id" value="<?php echo $module_id;?>" />
<input type="hidden" name="table_name" id="table_name" value="<?php echo $table_name;?>" />
<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $module_totalrows;?>" />
<script type="text/javascript">

   	var j = jQuery.noConflict();
   
   	var modulefetchurl = '<?php echo $module_fatchingurl;?>';
	var firstval = 1;
		

   function pageselectCallback(page_index, jq){
		var module_id=document.getElementById('module_id').value;
		var table_name=document.getElementById('table_name').value;
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var show_by_order=document.getElementById('show_by_order').value;
		var starting_val = parseInt(page_index*list_per_page);
		var searching_field=document.getElementById('searching_field').value;
		
		j.post(modulefetchurl, {"module_id":module_id, "table_name":table_name, "show_by_order":show_by_order, "searching_field":searching_field, "starting_val": starting_val, "list_per_page": list_per_page},
		function(data) {
			j('#Searchresult').html(data);			
		});
		
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		var optInit = getOptionsFromForm(); // Create pagination element with options from form
		//alert(optInit);
		var totalrows_count = document.getElementById('totalrowscount').value;
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit, list_per_page);
	}
	
	function seachingwithpagination(){
		var module_id=document.getElementById('module_id').value;
		var table_name=document.getElementById('table_name').value;
		var searching_field=document.getElementById('searching_field').value;
		j.post('<?php echo site_url("/").'module/fetching_dynamic_listcountwithsearch/';?>', {"module_id":module_id, "table_name":table_name, "searching_field":searching_field},
		function(data) {
			document.getElementById('totalrowscount').value = data;	
			loadpagination();
		});
	}
	
	function show_by_orderlist(show_by_orderval){
		document.getElementById('show_by_order').value = show_by_orderval;
		loadpagination();        
	}
	j(document).ready(function(){
		loadpagination();
	});
	
</script>

<!--<script src="<?php //echo $site_url?>common/js/topPagination.js" type="text/javascript" language="javascript"></script>-->
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/toppagination.css" type="text/css" />
<script src="<?php echo $site_url?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                	<div style="width:540px; float:left; overflow:hidden;">
                    	<div id="Pagination" class="pagination"></div>
                    </div>
                   	<div style="width:175px; float:left; margin-top:2px;">
                        <div style="float:left; width:50px; line-height:22px;">Limits:</div>
                        <div style="float:left; width:125px; height:22px;">
                        <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20" selected="selected">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="70">70</option>
                            <option value="100">100</option>
                        </select>
                        <select id="show_by_order" class="searchselect80x20" name="show_by_order" onchange="seachingwithpagination();">
                            <option value="created_date desc">New First</option>
                            <option value="created_date asc">Old First</option>
                            <option value="showing_order asc" selected="selected">Showing ASC</option>
                            <option value="showing_order desc">Showing DESC</option>
                        </select>
                        </div>
                  	</div>
                    <div style="width:185px; float:left; margin-left:10px;">
                        <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" />
                        <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
                    </div>
                </td>
                <td align="right" width="200">
                	<a href="<?php echo site_url("/").'module/form/'.$table_name;?>" class="white14bold">Add New <?php echo $module_name;?></a>                    
                </td> 
              </tr>
            </table>			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			if ($this->uri->segment(3) === FALSE){
				$msg = '';
			}
			else{
				$msg = $this->uri->segment(3);
			}
			if(isset($msg) && $msg !='' ){
				if($msg=='add_success')
					echo "<div class=red18bold>$module_name added successfully</div>";
				elseif($msg=='update_success')
					echo "<div class=red18bold>$module_name updated successfully</div>";							
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
    
	<tr>
		<td align="center" id="Searchresult">&nbsp;</td>
	</tr>
</table>