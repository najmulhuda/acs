<?php
$site_url = str_replace('/index.php','',site_url("/"));

if(isset($module_name['value']) && $module_name['value'] !=''){ $module_nameval = $module_name['value'];}
else{ $module_nameval = set_value('module_name');}

if(isset($table_name['value']) && $table_name['value'] !=''){ $table_nameval = $table_name['value'];}
else{ $table_nameval = set_value('table_name');}

if(isset($submit['value']) && $submit['value'] !=''){ $submitval = $submit['value'];}
else{ $submitval = ' Save ';}

if(isset($module_id['value']) && $module_id['value'] !=''){ $module_idval = $module_id['value'];}
else{ $module_idval =set_value('module_id');}
if($module_idval==''){$module_idval = 0;}
?>

<script language="JavaScript" type="text/javascript">
	var j = jQuery.noConflict();
	function checktable_nameval(){
		var table_name = document.getElementById("table_name");
		var elementmessage = document.getElementById("errmsg_table_name");
		elementmessage.innerHTML = '';
		j('#table_name').removeClass('error_input300x20').addClass('input300x20');
		if(table_name.value==''){
			elementmessage.innerHTML = 'You are missing Table Name';
			table_name.focus();
			j('#table_name').removeClass('input300x20').addClass('error_input300x20');
			return false;
		}	
		else{
			var ValidChars = "abcdefghijklmnopqrstuvwxyz_";
			var IsValid=true;
			var Char;
			var fields_nameval = '';
			for (var i = 0; i < table_name.value.length && IsValid == true; i++){ 
				Char = table_name.value.charAt(i); 
				if (ValidChars.indexOf(Char) == -1){
					IsValid = false;
					elementmessage.innerHTML = 'Table Name should be only (abcdefghijklmnopqrstuvwxyz_)';
					table_name.focus();
					j('#table_name').removeClass('input300x20').addClass('error_input300x20');
				}
				else{
					fields_nameval = fields_nameval+Char;
				}
			}
			if(IsValid == false){
				table_name.value = fields_nameval;
				return false;
			}
		}
	}
	
	function checkfrmmodule_form(){
		var module_name = document.getElementById("module_name");
		var elementmessage = document.getElementById("errmsg_module_name");
		elementmessage.innerHTML = '';
		j('#module_name').removeClass('error_input300x20').addClass('input300x20');
		if(module_name.value==''){
			elementmessage.innerHTML = 'You are missing Module Name';
			module_name.focus();
			j('#module_name').removeClass('input300x20').addClass('error_input300x20');
			return false;
		}
		
		var table_name = document.getElementById("table_name");
		var elementmessage = document.getElementById("errmsg_table_name");
		elementmessage.innerHTML = '';
		j('#table_name').removeClass('error_input300x20').addClass('input300x20');
		if(table_name.value==''){
			elementmessage.innerHTML = 'You are missing Table Name';
			table_name.focus();
			j('#table_name').removeClass('input300x20').addClass('error_input300x20');
			return false;
		}	
		else{
			var ValidChars = "abcdefghijklmnopqrstuvwxyz_";
			var IsValid=true;
			var Char;
			var fields_nameval = '';
			for (var i = 0; i < table_name.value.length && IsValid == true; i++){ 
				Char = table_name.value.charAt(i); 
				if (ValidChars.indexOf(Char) == -1){
					IsValid = false;
					elementmessage.innerHTML = 'Table Name should be only (abcdefghijklmnopqrstuvwxyz_)';
					table_name.focus();
					j('#table_name').removeClass('input300x20').addClass('error_input300x20');
				}
				else{
					fields_nameval = fields_nameval+Char;
				}
			}
			if(IsValid == false){
				table_name.value = fields_nameval;
				return false;
			}
		}
		
		return true;
	}
	
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                	Module Information Form
                </td>
                <td align="right">
                	<a href="<?php echo site_url("/").'module/module_list';?>" class="white14bold">Show Module List</a>
                </td>
              </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			echo validation_errors();
			if(isset($err_message) && $err_message!=''){
				echo $err_message;
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<?php
            $attributes = array('name' => 'frmmodule', 'onsubmit'=>'return checkfrmmodule_form();');
            echo form_open_multipart(site_url("/").'module/save_module', $attributes);
            ?>
                <table width="100%" border="0" cellspacing="7" cellpadding="0">										
                    <tr>
                        <td class="black11bold" width="120" align="left">Module Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'module_name',
                                          'id'       => 'module_name',
                                          'tabindex' => '1',
                                          'value'    => $module_nameval,
                                          'maxlength'=> '250',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_module_name" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="black11bold" width="120" align="left">Table Name: <font class="red14bold">*</font></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array('name'     => 'table_name',
                                          'id'       => 'table_name',
                                          'tabindex' => '1',
                                          'value'    => $table_nameval,
										  'onkeyup'  =>"checktable_nameval();",
                                          'maxlength'=> '100',
                                          'class'    => 'input300x20'
                                        );															
                            echo form_input($data);
                            ?>
                            <span id="errmsg_table_name" class="errormsg"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="black11bold"></td>
                        <td align="left" class="black11normal">
                            <?php 
                            $data = array( 'name'    => 'submit',
                                           'value'   => $submitval,
                                           'tabindex'=> '4',
                                           'class'   => 'submitbutton');															
                            
                            echo form_submit($data);
                            $data = array( 'name'    => 'reset',
                                           'value'   => ' Reset ',
                                           'tabindex'=> '5',
                                           'class'   => 'submitbutton');
                                                                                                    
                            echo '&nbsp;&nbsp;'.form_reset($data);
                            ?>
                            <input type="hidden" name="module_id" id="module_id" value="<?php echo $module_idval;?>" />
                        </td>
                    </tr>								
                </table>
            <?php echo form_close(); ?>
		</td>
	</tr>
</table>




