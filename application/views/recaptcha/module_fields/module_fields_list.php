<?php
$site_url = str_replace('/index.php','',site_url("/"));

if ($this->uri->segment(2) === FALSE){$segment2name = '';}
else{$segment2name = $this->uri->segment(2);}

$functionname = 'Module Fields';
$module_fields_totalrows = $commonmodel->getcount_all_resultsrows('module_fields', '', 'modified_date');
$module_fields_fatchingurl = site_url("/").'module_fields/fetching_module_fields_list/';
?>
<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $module_fields_totalrows;?>" />
<script type="text/javascript">

   	var j = jQuery.noConflict();
   
   	var module_fieldsfetchurl = '<?php echo $module_fields_fatchingurl;?>';
	var firstval = 1;
		

   function pageselectCallback(page_index, jq){
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var show_by_order=document.getElementById('show_by_order').value;
		var starting_val = parseInt(page_index*list_per_page);
		var searching_field=document.getElementById('searching_field').value;
		
		j.post(module_fieldsfetchurl, {"show_by_order":show_by_order, "searching_field":searching_field, "starting_val": starting_val, "list_per_page": list_per_page},
		function(data) {
			j('#Searchresult').html(data);			
		});
		
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		var optInit = getOptionsFromForm(); // Create pagination element with options from form
		//alert(optInit);
		var totalrows_count = document.getElementById('totalrowscount').value;
		var list_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit, list_per_page);
	}
	
	function seachingwithpagination(){
		var searching_field=document.getElementById('searching_field').value;
		j.post('<?php echo site_url("/").'module_fields/fetching_module_fields_listcountwithsearch/';?>', {"searching_field":searching_field},
		function(data) {
			document.getElementById('totalrowscount').value = data;	
			loadpagination();
		});
	}
	function show_by_orderlist(show_by_orderval){
		document.getElementById('show_by_order').value = show_by_orderval;
		loadpagination();        
	}
	j(document).ready(function(){
		loadpagination();
	});
	
</script>

<!--<script src="<?php //echo $site_url?>common/js/topPagination.js" type="text/javascript" language="javascript"></script>-->
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/toppagination.css" type="text/css" />
<script src="<?php echo $site_url?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                	<div style="width:540px; float:left; overflow:hidden;">
                    	<div id="Pagination" class="pagination"></div>
                    </div>
                   	<div style="width:175px; float:left; margin-top:2px;">
                        <div style="float:left; width:50px; line-height:22px;">Limits:</div>
                        <div style="float:left; width:125px; height:22px;">
                        <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20" selected="selected">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="70">70</option>
                            <option value="100">100</option>
                        </select>
                        <select id="show_by_order" class="searchselect80x20" name="show_by_order" onchange="seachingwithpagination();">
                            <option value="created_date desc">New First</option>
                            <option value="created_date asc">Old First</option>
                            <option value="module_id asc, showing_order asc" selected="selected">Module ID ASC</option>
                            <option value="module_id desc, showing_order asc">Module ID DESC</option>
                        </select>
                        </div>
                  	</div>
                    <div style="width:185px; float:left; margin-left:10px;">
                        <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" />
                        <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
                    </div>
                </td>
                <td align="right" width="200">
                	<a href="<?php echo site_url("/").'module_fields/module_fields_form';?>" class="white14bold">Add New <?php echo $functionname;?></a>                    
                </td> 
              </tr>
            </table>			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			if ($this->uri->segment(3) === FALSE){
				$msg = '';
			}
			else{
				$msg = $this->uri->segment(3);
			}
			if(isset($msg) && $msg !='' ){
				if($msg=='add_success')
					echo "<div class=red18bold>$functionname added successfully</div>";
				elseif($msg=='update_success')
					echo "<div class=red18bold>$functionname updated successfully</div>";							
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
    
	<tr>
		<td align="center" id="Searchresult">&nbsp;</td>
	</tr>
</table>

<?php
/*$str = "";
$i = 0;
$t = 0;
foreach($query as $rowset_module_fields){
	$i++;
	if($t>20){
		$t=1;
		$str .= '</table>
				</div>
				<div class="result">
					<table width="100%" cellpadding="0" cellspacing="1" border="0">
						<tr>
							<th class="titlerow" align="center" width="5%">#</th>
							<th class="titlerow" align="left"  width="20%">Form Category</th>
							<th class="titlerow" align="left" width="75%">Field Name</th></tr>';
	}
	
	if($i%2==0)	$classname = 'evenrow';
	else		$classname = 'oddrow';											
	$set_module_fields_id = $rowset_module_fields->set_module_fields_id;
	$product_category_id = $rowset_module_fields->product_category_id;
	$product_category_id_name = '';
	if($product_category_id>0){
		$product_category_id_name = $commonmodel->get_onefieldnamebyid('product_category', 'product_category_id', $product_category_id, 'product_category_name');									
	}
	
	$set_module_fields_liststr = '';
	if($product_category_id>0){																						
		$set_module_fields_id_list = $commonmodel->getallrowsbyconditionwithorder('set_module_fields', array('product_category_id'=>$product_category_id), 'showing_order', 'asc');
		if($set_module_fields_id_list && count($set_module_fields_id_list)>0){
			$set_module_fields_liststr .= '<ul class="pretenderlist">';
			foreach($set_module_fields_id_list as $set_module_fieldsrow){
				$set_module_fields_id = $set_module_fieldsrow->set_module_fields_id;
				$form_fields_id = $set_module_fieldsrow->set_module_fields_id_list;
				$showing_order = $set_module_fieldsrow->showing_order;
				$set_module_fields_publish = $set_module_fieldsrow->set_module_fields_publish;
	
				$publishedimg = str_replace('/index.php','',site_url("/")).'common/images/minus20x21.png';	
				$unpublished = str_replace('/index.php','',site_url("/")).'common/pagination/images/published.png';
				$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';	
				if($set_module_fields_publish>0){
					$published = "<a href=\"".site_url("/")."admin/update_onetablefield/set_module_fields/set_module_fields_id/$set_module_fields_id/set_module_fields_publish/0/set_module_fields/set_module_fields_list\"><img src=\"$publishedimg\" border=\"0\" alt=\"Unpublished\"></a>";
				}
				else{
					$published = "<a href=\"".site_url("/")."admin/update_onetablefield/set_module_fields/set_module_fields_id/$set_module_fields_id/set_module_fields_publish/1/set_module_fields/set_module_fields_list\"><img src=\"$unpublished\" border=\"0\" alt=\"Published\"></a>";
				}
				
				$ListOrder = $commonmodel->listOrderbycategory('set_module_fields', 'product_category_id', $product_category_id, 'set_module_fields_id', $set_module_fields_id, $showing_order, 'set_module_fields', 'set_module_fields_list');

				$set_module_fields_liststr .= '<li>
													<table width="100%" cellpadding="0" cellspacing="1" border="0">
														<tr>
															<td align="left">';
				$frmset_category_module_name = $commonmodel->formfieldtag_byidandfrmname($form_fields_id, 'frmset_category_module');											
				$set_module_fields_liststr .= $frmset_category_module_name;
				$set_module_fields_liststr .= '</td>
												<td align="left" width="40">
												'.$ListOrder.'
												</td>';
				$set_module_fields_liststr .= "<td class=\"left\" width=\"40\">$published
				<a onclick=\"tablerow_remove('set_module_fields', 'set_module_fields_id', '$set_module_fields_id', 'Field', 'set_module_fields', 'set_module_fields_list');\" title=\"Remove\">
				<img src=\"$removeimg\" border=\"0\" alt=\"Remove\">
				</a></td>";
				$set_module_fields_liststr .= '</tr>
												</table>
												</li>';
				$t++;									
			}
			$set_module_fields_liststr .= '</ul>';
		}													
	}
	
	$showing_order = $rowset_module_fields->showing_order;
	
	$ListOrder = $commonmodel->listOrderbycategory('set_module_fields', 'product_category_id', $product_category_id, 'set_module_fields_id', $set_module_fields_id, $showing_order, 'set_module_fields', 'set_module_fields_list');
	
	$set_module_fields_publish = $rowset_module_fields->set_module_fields_publish;
	
	$publishedimg = str_replace('/index.php','',site_url("/")).'common/images/minus20x21.png';	
	$unpublished = str_replace('/index.php','',site_url("/")).'common/pagination/images/published.png';
	$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';	
	if($set_module_fields_publish>0){
		$published = "<a href=\"".site_url("/")."admin/update_onetablefield/set_module_fields/set_module_fields_id/$set_module_fields_id/set_module_fields_publish/0/set_module_fields/set_module_fields_list\"><img src=\"$publishedimg\" border=\"0\" alt=\"Unpublished\"></a>";
	}
	else{
		$published = "<a href=\"".site_url("/")."admin/update_onetablefield/set_module_fields/set_module_fields_id/$set_module_fields_id/set_module_fields_publish/1/set_module_fields/set_module_fields_list\"><img src=\"$unpublished\" border=\"0\" alt=\"Published\"></a>";
	}
	
	$classname = 'firstrow';
	$firstparentstr = "<tr>
		<td class=\"$classname\" align=\"center\">$i</td>
		<td class=\"$classname\" align=\"left\" >
			<a href=\"".site_url("/")."set_module_fields/set_module_fields_form/$set_module_fields_id\">$product_category_id_name</a>
		</td>
		<td class=\"$classname\" align=\"left\">$set_module_fields_liststr</td></tr>";									
	$str .= $firstparentstr;
	
}
echo $str;

*/
?>