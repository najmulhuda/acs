<?php
if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}
if($segment2name !='' && $segment2name=='comments_hidelist')$functionname = 'Unpublished Comments';
else $functionname = 'Comments';

$site_url = str_replace('/index.php','',site_url("/"));

?>
<style type="text/css">
.pagination {font-size: 80%; width:100%; float:right; margin-bottom:10px;}
.pagination a {text-decoration: none;border: solid 1px #AAE;color: #15B;}
.pagination a, .pagination span {display: block;float: left;padding: 0.3em 0.5em;margin-right: 5px;margin-bottom: 5px; min-width:16px; 
	text-align:center;}
.pagination .current {background: #26B;color: #fff;border: solid 1px #AAE;}
.pagination .current.prev, .pagination .current.next{color:#999;border-color:#999;background:#fff;}
</style>
<script src="<?php echo $site_url?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>

<script language="JavaScript" type="text/javascript">
	j=jQuery.noConflict();
	j(document).ready(function($) {
		initPagination();
	});
	 function pageselectCallback(page_index, jq){
		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();
		j('#Searchresult').empty().append(new_content);
		return false;
	}

	function initPagination() {
		// count entries inside the hidden content
		var num_entries = j('#hiddenresult div.result').length;
		// Create content inside pagination element
		j("#Pagination").pagination(num_entries, {
			callback: pageselectCallback,
			items_per_page:1 // Show only one item per page
		});
	 }  

</script>


<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<?php echo $functionname; ?> list
		</td>
	</tr>
	<tr>
		<td align="center">
			<div style="width:99%; overflow:hidden;">					
				<table width="100%" border="0" cellspacing="0" cellpadding="0">						
					<tr>
						<td width="60%">
							<table width="98%" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left">&nbsp;
								</td>
								<td>
									<?php 
									if ($this->uri->segment(3) === FALSE){
										$msg = '';
									}
									else{
										$msg = $this->uri->segment(3);
									}
									if(isset($msg) && $msg !='' ){
										if($msg=='add_success')
											echo "<span class=red14bold>Comments added successfully</span>";
										elseif($msg=='update_success')
											echo "<span class=red14bold>Comments updated successfully</span>";							
									}

									?>
								</td>
							  </tr>
							</table>
						</td>
						<td style="padding:2px 0px 0px 10px; text-align:center;">

							<div id="Pagination" class="pagination"></div>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:99%; overflow:hidden;">				
				<div id="Searchresult">
					This content will be replaced when pagination inits.
				</div>
				<div style="padding:0 10px 0 20px;" class="black12bold">Total <?php echo count($query);?> data or <?php echo ceil(count($query)/10);?> pages found </div>
			</div>
			<?php
			if(isset($query) && count($query)>0){
			?>	
				<div id="hiddenresult" style="display:none;">
					<div class="result">
						<table width="100%" cellpadding="0" cellspacing="1" border="0">
							<tr>
                            	<th class="titlerow" align="center" width="5%">#</th>
                                <th class="titlerow" align="left" nowrap="nowrap" width="15%">Name</th>
                                <th class="titlerow" align="left" nowrap="nowrap" width="15%">Email</th>
                                <th class="titlerow" align="center" width="30%">Comments</th>
                                <th class="titlerow" align="center" width="17%">Comments On</th>
                                <th class="titlerow" align="center" width="8%">Ip Address</th>
								<?php 
								if($this->session->userdata('user_level_id')==1){
								?>
									<th class="titlerow" align="center" width="5%">Publish</th>
								<?php }?>
								<th class="titlerow" align="center" width="5%">ID</th>	
							</tr>
							<?php
							$str = "";
							$i = 1;
							foreach($query as $rowcomments){
								if($i%2==0){$classname = 'evenrow';$selectedeven = 'selectedeven';}
								else{$classname = 'oddrow';$selectedeven = 'selectedodd';}
								$comments_id = $rowcomments->comments_id;
								$comments_user_name = $rowcomments->comments_user_name;
								$comments_user_email = $rowcomments->comments_user_email;
								$comments_description  = nl2br($rowcomments->comments_description );
								$client_ipaddress = $rowcomments->client_ipaddress;
								$comments_publish = $rowcomments->comments_publish;
								$table_name = $rowcomments->table_name;
								$table_id_name = $rowcomments->table_id_name;
								$table_id_val = $rowcomments->table_id_val;											
								$comments_on = $commonmodel->get_onefieldnamebyid($table_name, $table_id_name, $table_id_val, $table_name.'_name');
								$unpublished = str_replace('/index.php','',site_url("/")).'common/pagination/images/unpublished.png';
								$publishedimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/published.png';		
								if($comments_publish>0){
									$published = "<a href=\"".site_url("/")."admin/update_onetablefield/comments/comments_id/$comments_id/comments_publish/0/comments/comments_list\"><img src=\"$publishedimg\" border=\"0\" alt=\"Unpublished\"></a>";
								}
								else{
									$published = "<a href=\"".site_url("/")."admin/update_onetablefield/comments/comments_id/$comments_id/comments_publish/1/comments/comments_list\"><img src=\"$unpublished\" border=\"0\" alt=\"Published\"></a>";
								}
								$str .= "<tr>
									<td align=\"center\" class=\"$selectedeven\">$i</td>
									<td align=\"left\" class=\"$classname\">$comments_user_name</td>
									<td align=\"left\" class=\"$classname\" >$comments_user_email</td>
									<td align=\"left\" class=\"$classname\" >$comments_description </td>
									<td align=\"left\" class=\"$classname\" >$comments_on</td>
									<td align=\"center\" class=\"$classname\">$client_ipaddress</td>";
								if($this->session->userdata('user_level_id')==1){
									$str .= "<td align=\"center\" class=\"$classname\">$published</td>";
								}
								$str .= "<td align=\"center\" class=\"$classname\">$comments_id</td></tr>";
								if($i%10==0 && count($query)>$i){
									$str .= '</table></div>
									<div class="result">
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<th class="titlerow" align="center" width="5%">#</th>
											<th class="titlerow" align="left" nowrap="nowrap" width="15%">Name</th>
											<th class="titlerow" align="left" nowrap="nowrap" width="15%">Email</th>
											<th class="titlerow" align="center" width="30%">Comments</th>
											<th class="titlerow" align="center" width="17%">Comments On</th>
											<th class="titlerow" align="center" width="8%">Ip Address</th>';

											if($this->session->userdata('user_level_id')==1){											
												$str .= '<th class="titlerow" align="center" width="5%">Publish</th>';
											}
											$str .= '<th class="titlerow" align="center" width="5%">ID</th>															
										</tr>
									';
								}		
								$i++;
							}
							echo $str;
						?>
						</table>
						</div>
					</div>
			<?php
			}
			else{
				echo "<div class=red18bold>There are currently no $functionname found</div>";								}							
			?>
		</td>
	</tr>
</table>