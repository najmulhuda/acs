

<?php

if($this->session->userdata('first_name') !=''){ $first_nameval = $this->session->userdata('first_name');}

else{ $first_nameval = set_value('first_name');}

	

if($this->session->userdata('students_cell') !=''){ $phoneval = $this->session->userdata('students_cell');}

else{ $phoneval = set_value('phone');}



if($this->session->userdata('students_email') !=''){ $emailval = $this->session->userdata('students_email');}

else{ $emailval = set_value('email');}



if(isset($validation_code['value']) && $validation_code['value'] !=''){ $validation_codeval = $validation_code['value'];}

else{ $validation_codeval = set_value('validation_code');}



if($this->session->userdata('subject') !=''){ $subjectval = $this->session->userdata('subject');}

else{ $subjectval = set_value('subject');}



if($this->session->userdata('site_seo_title')){

	$site_name = $this->session->userdata('site_name');

	$site_seo_title = $this->session->userdata('site_seo_title');

	$site_seo_description = $this->session->userdata('site_seo_description');

	$site_seo_keywords = $this->session->userdata('site_seo_keywords');

	$site_email = $this->session->userdata('site_email');

	$site_address = $this->session->userdata('site_address');

	$site_telephone = $this->session->userdata('site_telephone');

	$site_mobile = $this->session->userdata('site_mobile');	

	$site_fax = $this->session->userdata('site_fax');	

	$site_email_logo_thumbimage = $this->session->userdata('site_email_logo_thumbimage');

}

else{

	if($this->Common_model->get_global_config()){

		$site_name = $this->session->userdata('site_name');

		$site_seo_title = $this->session->userdata('site_seo_title');

		$site_seo_description = $this->session->userdata('site_seo_description');

		$site_seo_keywords = $this->session->userdata('site_seo_keywords');

		$site_email = $this->session->userdata('site_email');

		$site_address = $this->session->userdata('site_address');

		$site_telephone = $this->session->userdata('site_telephone');

		$site_mobile = $this->session->userdata('site_mobile');	

		$site_fax = $this->session->userdata('site_fax');

		$site_email_logo_thumbimage = $this->session->userdata('site_email_logo_thumbimage');

	}

	else{

		$global_data = $this->Common_model->get_global_configdata();

		$site_name = $global_data['site_name'];

		$site_email = $global_data['site_email'];

		$site_address = $global_data['site_address'];

		$site_telephone = $global_data['site_telephone'];

		$site_mobile = $global_data['site_mobile'];	

		$site_fax = $global_data['site_fax'];		

		$site_email_logo_thumbimage = $global_data['site_email_logo_thumbimage'];

	}						

}

?>

<script type="text/javascript" language="javascript">

	function checkfontactfield(){

		var oField = document.frmcontact.first_name;

		var oElement = document.getElementById('err_first_name');

		oElement.innerHTML = "";

		if(oField.value == ""){

			oElement.innerHTML = "You are missing name.";

			oField.focus();

			return(false);

		}

		else if(oField.value.length<2){

			oElement.innerHTML = "Name should be more than 2 character";

			oField.focus();

			return(false);

		}

		

		var oField = document.frmcontact.phone;

		var oElement = document.getElementById('err_phone');

		oElement.innerHTML = "";

		if(oField.value != ""){

			if(checkmobileortelephone(oField.value)==false){

				oElement.innerHTML = "Phone is invalid.";

				oField.focus();

				return(false);

			}

			else if(oField.value.length<5){

				oElement.innerHTML = "Phone should be more than 5 character";

				oField.focus();

				return(false);

			}

		}

		

		var oField = document.frmcontact.email;

		var oElement = document.getElementById('err_email');

		oElement.innerHTML = "";

		if(oField.value == ""){

			oElement.innerHTML = "You are missing email.";

			oField.focus();

			return(false);

		}

		else if(emailcheck(oField.value)==false) {

			oElement.innerHTML = "You are missing valid email.";

			oField.focus();

			return(false);

		}

		

		var oField = document.frmcontact.subject;

		var oElement = document.getElementById('err_subject');

		oElement.innerHTML = "";

		if(oField.value == ""){

			oElement.innerHTML = "You are missing subject.";

			oField.focus();

			return(false);

		}

		else if(oField.value.length<5){

			oElement.innerHTML = "Subject should be more than 5 character";

			oField.focus();

			return(false);

		}

		var oField = document.frmcontact.comments;

		var oElement = document.getElementById('err_comments');

		oElement.innerHTML = "";

		if(oField.value == ""){

			oElement.innerHTML = "You are missing comments.";

			oField.focus();

			return(false);

		}

		else if(oField.value.length<10){

			oElement.innerHTML = "Comments should be more than 10 character";

			oField.focus();

			return(false);

		}		

		return(true);

	}



</script>

<h3 class="articleHead">

    Contact Us

</h3>

    

<table width="100%" border="0" cellspacing="0" cellpadding="0">		

    <tr>									

        <td width="100%" align="left">

            <?php

            $attributes = array('name' => 'frmcontact', 'id' => 'frmcontact','onsubmit'=>'return checkfontactfield();');

            echo form_open(site_url("/").'contact_us/send_mail', $attributes);

            ?>

                <table width="100%" border="0" cellspacing="4" cellpadding="0">										

                    <tr>

                        <td align="left" class="errormsg" colspan="2">

                            <?php 

                                echo validation_errors();

                                if(isset($err_message) && $err_message!=''){

                                    echo $err_message;

                                }

                            ?>

                        </td>

                    </tr>

                    <tr>

                        <td class="black12bold" width="20%" align="left">Name *:</td>

                        <td class="black12normal" width="80%" align="left">

                            <span class="inputbox300x25bg">

                                <?php 

                                $data = array('name'     => 'first_name',

                                              'id'       => 'first_name',

                                              'tabindex'       => '1',

                                              'value'    => $first_nameval,

                                              'maxlength'=> '100',

                                              'class'    => 'input300x25none'

                                            );															

                                echo form_input($data);

                                ?>

                            </span>

                            <span class="errormsg" id="err_first_name"></span>

                        </td>

                    </tr>

                    <tr>

                        <td width="20%" class="black12bold" align="left">Phone:</td>

                        <td width="70%" class="black12normal" align="left">

                            <span class="inputbox300x25bg">

                                <?php 

                                $data = array('name'     => 'phone',

                                              'id'       => 'phone',

                                              'tabindex'       => '2',

                                              'value'    => $phoneval,

                                              'maxlength'=> '20',

                                              'class'    => 'input300x25none'

                                            );															

                                echo form_input($data);

                                ?>

                            </span>

                            <span class="errormsg" id="err_phone"></span>

                        </td>

                    </tr>	

                    <tr>

                        <td width="20%" class="black12bold" align="left">Email *:</td>

                        <td width="70%" class="black12normal" align="left">

                            <span class="inputbox300x25bg">

                                <?php 

                                $data = array('name'     => 'email',

                                              'id'       => 'email',

                                              'tabindex'       => '3',

                                              'value'    => $emailval,

                                              'maxlength'=> '100',

                                              'size'    => '50',

                                              'class'    => 'input300x25none'

                                            );															

                                echo form_input($data);

                                ?>

                            </span>

                            <span class="errormsg" id="err_email"></span>

                        </td>

                    </tr>	

                    <tr>

                        <td width="20%" class="black12bold" align="left">Subject *:</td>

                        <td width="70%" class="black12normal" align="left">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                              <tr>

                                <td>

                                <span class="inputbox300x25bg">

                                <?php 

                                $data = array('name'     => 'subject',

                                              'id'       => 'subject',

                                              'tabindex'       => '4',

                                              'value'    => $subjectval,

                                              'maxlength'=> '100',

                                              'size'    => '50',

                                              'class'    => 'input300x25none'

                                            );															

                                echo form_input($data);

                                ?>

                            </span>

                            </td>

                              </tr>

                              <tr>

                                <td>

                                    <span class="errormsg" id="err_subject"></span></td>

                              </tr>

                            </table>

                        </td>

                    </tr>									

                    <tr>

                        <td width="20%" class="black12bold" align="left" valign="top">Message *</td>

                        <td width="70%" align="left" class="black12normal">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                              <tr>

                                <td>

                                    <?php 

                                    $data = array('name'     => 'comments',

                                                  'id'       => 'comments',

                                                  'value'    => set_value('comments'),

                                                  'tabindex'       => '5',

                                                  'rows'=> '5',

                                                  'cols'=> '40',

                                                  'class'    => 'textarea400x100css'

                                                );															

                                    echo form_textarea($data);

                                    ?>

                                </td>

                              </tr>

                              <tr>

                                <td><span class="errormsg" id="err_comments"></span></td>

                              </tr>

                            </table>

                        </td>

                    </tr>								

                    <tr>

                        <td align="left" class="black12bold"></td>

                        <td width="70%" align="left" class="black12normal">

                            <?php 							

                            $data = array( 'name'    => 'submit',

                                            'value'    => ' Send ',

                                            'tabindex'       => '6',

                                            'class'    => 'submitbutton');															

                            echo form_submit($data);

                            

                            $data = array( 'name'    => 'reset',

                                            'value'    => ' Reset ',

                                            'tabindex'       => '7',

                                            'class'    => 'submitbutton');															

                            echo '&nbsp;&nbsp;'.form_reset($data);

                            

                            ?>

                        </td>

                    </tr>

                </table>

            <?php echo form_close(); ?>

        </td>

    </tr>

    <tr>

        <td align="left" valign="top" style="padding-left:15px;">

            <table style="padding-top:20px;" border="0" cellspacing="0" cellpadding="2" width="90%">

                <tbody>

                    <tr>

                        <td  class="dottedborder greendeep16bold" align="left">

                            <?php echo $site_name;?>

                        </td>

                    </tr>													

                    <tr>

                        <td class="black12normal" align="left">

                            Address: <?php echo $site_address;?><br />

                            Telephone: <?php echo $site_telephone;?><br />

                            Mobile: <?php echo $site_mobile;?><br />

                            Email: <a href="mailto:<?php echo $site_email;?>" class="black12normal"><?php echo $site_email;?></a>

                            Fax: <?php echo $site_fax;?>

                        </td>

                    </tr>	

                </tbody>

            </table>

        </td>

    </tr>

    <tr>

        <td align="left" valign="top" style="padding-right:15px;">

        	<a class="readmore" href="<?php echo site_url("/")?>home">Back to Home</a>

        </td>

    </tr>

</table>