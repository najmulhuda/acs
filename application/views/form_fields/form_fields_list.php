<?php
$site_url = str_replace('/index.php','',site_url("/"));

if ($this->uri->segment(2) === FALSE){
    $segment2name = '';
}
else{
   $segment2name = $this->uri->segment(2);
}
$functionname = 'Form Fields';

$form_fields_totalrows = $commonmodel->getcount_all_resultsrows('form_fields', '', 'modified_date');
$form_fields_fatchingurl = site_url("/").'form_fields/fetching_form_fields_list/';
?>
<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $form_fields_totalrows;?>" />
<script type="text/javascript">
   	var j = jQuery.noConflict();
   	var form_fieldsfetchurl = '<?php echo $form_fields_fatchingurl;?>';
	var firstval = 1;
   function pageselectCallback(page_index, jq){
		var form_fields_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		var show_by_order=document.getElementById('show_by_order').value;
		var starting_val = parseInt(page_index*form_fields_per_page);
		var searching_field=document.getElementById('searching_field').value;
		
		j.post(form_fieldsfetchurl, {"show_by_order":show_by_order, "searching_field":searching_field, "starting_val": starting_val, "form_fields_per_page": form_fields_per_page},
		function(data) {
			j('#Searchresult').html(data);			
		});
		return true;
	}
	
	function getOptionsFromForm(){
		var opt = {callback: pageselectCallback};
		return opt;
	}
	function loadpagination(){
		var optInit = getOptionsFromForm(); //Create pagination element with options from form
		//alert(optInit);
		var totalrows_count = document.getElementById('totalrowscount').value;
		var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form
		j("#Pagination").pagination(parseInt(totalrows_count), optInit,data_per_page);
	}
	
	function seachingwithpagination(){
		var searching_field=document.getElementById('searching_field').value;
		j.post('<?php echo site_url("/").'form_fields/fetching_form_fields_listcountwithsearch/';?>', {"searching_field":searching_field},
		function(data) {
			//alert(data);
			document.getElementById('totalrowscount').value = data;	
			loadpagination();
		});
	}
	function show_by_orderlist(show_by_orderval){
		//alert(show_by_orderval);
		document.getElementById('show_by_order').value = show_by_orderval;
		loadpagination();        
	}
	j(document).ready(function(){
		loadpagination();
	});
	
</script>

<!--<script src="<?php //echo $site_url?>common/js/topPagination.js" type="text/javascript" language="javascript"></script>-->
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/toppagination.css" type="text/css" />
<script src="<?php echo $site_url?>common/pagination/toppagination.js" type="text/javascript" language="javascript"></script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td id="bluebgtd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                	<div style="width:540px; float:left; overflow:hidden;">
                    	<div id="Pagination" class="pagination"></div>
                    </div>
                   	<div style="width:175px; float:left; margin-top:2px;">
                        <div style="float:left; width:50px; line-height:22px;">Limits:</div>
                        <div style="float:left; width:125px; height:22px;">
                        <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20" selected="selected">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="70">70</option>
                            <option value="100">100</option>
                        </select>
                        <select id="show_by_order" class="searchselect80x20" name="show_by_order" onchange="seachingwithpagination();">
                            <option value="showing_order asc">Order Wise</option>
                            <option value="created_date desc">New First</option>
                            <option value="created_date asc">Old First</option>
                            <option value="admin_label asc">Label ASC</option>
                            <option value="admin_label desc">Label DESC</option>
                        </select>
                        </div>
                  	</div>
                    <div style="width:185px; float:left; margin-left:10px;">
                        <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" />
                        <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
                    </div>
                </td>
                <td align="right" width="200">
                	<a href="<?php echo site_url("/").'form_fields/form_fields_form';?>" class="white14bold">Add New <?php echo $functionname;?></a>                    
                </td>
              </tr>
            </table>			
		</td>
	</tr>
	<tr>
		<td align="center" class="errormessage">
            <?php 
			if ($this->uri->segment(3) === FALSE){
				$msg = '';
			}
			else{
				$msg = $this->uri->segment(3);
			}
			if(isset($msg) && $msg !='' ){
				if($msg=='add_success')
					echo "<div class=red18bold>$functionname added successfully</div>";
				elseif($msg=='update_success')
					echo "<div class=red18bold>$functionname updated successfully</div>";							
			}
			?>
		</td>
	</tr>
    <tr>
		<td align="center">
			<div id="Pagination"></div>
		</td>
	</tr>
	<tr>
		<td align="center" id="Searchresult">&nbsp;</td>
	</tr>
</table>