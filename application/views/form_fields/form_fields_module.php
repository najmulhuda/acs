<?php
$queryform_fields = $commonmodel->getallrowbysqlquery("select * from form_fields where form_fields_publish = 1 order by showing_order asc limit 0,2	");
if(count($queryform_fields)>0){
	foreach($queryform_fields as $form_fieldsrow){
		$form_fields_id = $form_fieldsrow->form_fields_id;
		$form_fields_name =$form_fieldsrow->form_fields_name;
		$form_fields_description = nl2br(substr(strip_tags($form_fieldsrow->form_fields_description),0,100));
		$form_fields_url = site_url("/").'form_fields/information/'.$form_fields_id;
		
		?>
		<div class="hedname"><a href="<?php echo $form_fields_url ;?>"><?php echo $form_fields_name; ?></a></div>
		
		<p class="oddrow"><?php echo $form_fields_description; ?></p>
		<?php
	}
}
?>
<a class="readmore" href="<?php echo site_url("/")?>form_fields">View All</a>
