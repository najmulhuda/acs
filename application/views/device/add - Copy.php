<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_addevice', 'onsubmit'=>'return check_add_device();');
 echo form_open(site_url("/").'device/add/', $userformattributes);
?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>



<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New Device

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'device';?>" class="white14bold">Show Device List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}

			?>

		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

				<tr>

					<td align="right" width="30%" class="black12bold">IP <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'device_ip',

									  'id'       => 'device_ip',

									  'tabindex'       => '1',

									  'value'    => '',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

				<tr>

					<td align="right" class="black12bold">Site Id <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'site_id',

									  'id'       => 'site_id',

									  'tabindex'       => '2',

									  'value'    => '',

									  'maxlength'=> '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												

				<tr>

					<td align="right" class="black12bold">Alarm <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'device_alarm',

									  'id'       => 'device_alarm',

									  'tabindex'       => '3',

									  'value'    => '',

									  'maxlength'=> '150',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_email" class="red11normal"></span>

					</td>

				</tr>												

				<tr>

					<td align="right" class="black12bold">Name:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'device_name',

									  'id'       => 'device_name',

									  'tabindex'       => '4',

									  'value'    => '',

									  'maxlength'=> '20',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_contactno" class="red11normal"></span>

					</td>

				</tr>												

			
				<tr>

					<td align="right" class="black12bold">Status:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'device_status',

									  'id'       => 'device_status',

									  'tabindex'       => '5',

									  'value'    => '',

									  'maxlength'=> '20',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_contactno" class="red11normal"></span>

					</td>

				</tr>
				
				
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>