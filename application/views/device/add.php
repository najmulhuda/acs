<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_addevice', 'onsubmit'=>'return check_add_device();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'device/edit/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'device/add/', $userformattributes); 
 }
 $sitedata_fetchurl = site_url("/").'site/sitelistbysubcentre/';
?>

<script type="text/javascript">
   var j = jQuery.noConflict();
   var sitedata_fetchurl = '<?php echo $sitedata_fetchurl;?>';
   
   function getSites(){
        var subcentreId = j('#sub_centre').val();
		
		j.post(sitedata_fetchurl, {"subcentreId":subcentreId},

		function(data) {
            j('#site_id').html("");
			j('#site_id').append(data);			

		});

		return true;

	}
</script>

<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   foreach($data as $row){
		   $id = $row->id;
		   $ip = $row->ip;
	       $siteid = $row->siteid;
		   $status = $row->status;
		   $versionno = $row->versionno;
		   $serialno = $row->serialno;
		   $subcentre = $row->subcentre;   
	   }	
	    
	}	
}else{
	$id = "";
    $ip = "";
	$siteid = "";
    $status = "";
    $versionno = "";
    $serialno = "";
    $subcentre = "";
}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New Device

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'device';?>" class="white14bold">Show Device List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">

				<tr>

					<td align="right" width="30%" class="black12bold">IP <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'device_ip',

									  'id'       => 'device_ip',

									  'tabindex'       => '1',

									  'value'    => $ip,

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
                <tr>
				    <td align="right" class="black12bold">Sub Centre <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					   echo "<select name='sub_centre' id='sub_centre' onclick='getSites()'>";
					    echo "<option value=''>--Select--</option>";
                        if(is_array($subcentredata) && count($subcentredata)>0) {
	                       foreach ($subcentredata as $row1) {
							 if($subcentre==$row1->id){
								 echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";
							 }else{
								 echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
							 }  
		                     
	                      }

                            
                        }	
					   echo "</select>";
					  ?>	
                     

					</td>				
				</tr>
				<tr>

					<td align="right" class="black12bold">Site Id <span class="red12bold">*</span>:</td>

					<td align="left">

						 <?php
						    if($mode=="edit"){
							echo "<select name='site_id' id='site_id'>";
                            if(is_array($sitedata) && count($sitedata)>0) {
	                           foreach ($sitedata as $row1) {
							     if($siteid==$row1->id){
								    echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";
							     }else{
							   	    echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
							     }  
		                     
	                          }
                            }	
					            echo "</select>";
					  	
						   }else{
								echo "<select name='site_id' id='site_id'></select>";
							}
					          
                         ?>	
                        <span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												

				<tr>

					<td align="right" class="black12bold">Status <span class="red12bold">*</span>:</td>

					<td align="left">

						<select name='device_status' id='device_status'>
						   <?php
						       if($status==0){
								   echo " <option value='0' selected>0</option>";
								   echo " <option value='1'>1</option>";
							   }else{
								   echo " <option value='0'>0</option>";
								   echo " <option value='1' selected>1</option>";
							   }
						   ?>
						  
						</select>

					</td>

				</tr>												

				<tr>

					<td align="right" class="black12bold">Version No.</td>

					<td align="left">

						<select name='version_no' id='version_no'>
						   <?php
						       if($versionno=="1.0"){
								   echo " <option value='1.0' selected>1.0</option>";
								   echo " <option value='2.0'>2.0</option>";
							   }else{
								   echo " <option value='1.0'>1.0</option>";
								   echo " <option value='2.0' selected>2.0</option>";
							   }
						   ?>
						  
						</select>

						
						<span id="errmsg_user_contactno" class="red11normal"></span>

					</td>

				</tr>												

			
				<tr>

					<td align="right" class="black12bold">Serial No.</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'serial_no',

									  'id'       => 'serial_no',

									  'tabindex'       => '5',

									  'value'    => $serialno,

									  'maxlength'=> '20',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_contactno" class="red11normal"></span>

					</td>

				</tr>
			   
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>