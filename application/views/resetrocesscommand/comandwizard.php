<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_setcommand', 'onsubmit'=>'return setcommand();');
 
 echo form_open(site_url("/").'resetprocesscommand', $userformattributes); 
 
 $sitedata_fetchurl = site_url("/").'site/sitelistbysubcentre/';
 $employeedata_fetchurl = site_url("/").'employee/emplyeeinfobyid/';
 
?>

<script type="text/javascript">
   var j = jQuery.noConflict();
   var sitedata_fetchurl = '<?php echo $sitedata_fetchurl;?>';
   var employeedata_fetchurl = '<?php echo  $employeedata_fetchurl;?>';
   function getSites(){
        var subcentreId = j('#sub_centre').val();
			
		j.post(sitedata_fetchurl, {"subcentreId":subcentreId},

		function(data) {
			
		
			
            j('#site_id').html("");
			j('#site_id').append(data);			

		});

		return true;

	}
	
	function getUserInfo(){
		
		 var employeeID = j('#employee_id').val();
		
		 j.post(employeedata_fetchurl, {"employeeID":employeeID},

		function(data){
			//alert(data);
			var obj = JSON.parse(data);
             j('#employee_name').val(obj.name);
			 j('#phone_no').val(obj.phoneno);
			 j('#card_number').val(obj.cardnumber);
			 j('#card_status').val(obj.cardstatus);
        });

		return true;
	}
	
</script>



<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Card Add or Delete For

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'cardprocesscommand/visitlist';?>" class="white14bold">Show Visit List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
                 <tr>
				    <td align="right" class="black12bold">Sub Centre <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					   echo "<select name='sub_centre' id='sub_centre' onclick='getSites()'>";
					    echo "<option value=''>--Select--</option>";
                        if(is_array($subcentredata) && count($subcentredata)>0) {
	                       foreach ($subcentredata as $row1) {
							 if($subcentre==$row1->id){
								 echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";
							 }else{
								 echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";
							 }  
		                     
	                      }

                            
                        }	
					   echo "</select>";
					  ?>	
                     

					</td>				
				</tr>				
				<tr>

					<td align="right" width="30%" class="black12bold">Employee Id <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'employee_id',

									  'id'       => 'employee_id',

									  'tabindex'       => '1',

									  'value'    =>'',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20',
									  
                                      'onblur'   => 'getUserInfo()',
									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
												
              <tr>

					<td align="right" width="30%" class="black12bold">Employee Name <span class="red12bold"></span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'employee_name',

									  'id'       => 'employee_name',

									  'tabindex'       => '1',

									  'value'    => '',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
               	
				<tr>

					<td align="right" width="30%" class="black12bold">Card Number<span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'card_number',

									  'id'       => 'card_number',

									  'tabindex'       => '2',

									  'value'    =>'',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20',
									  
                                      'onblur'   => 'getUserInfo()',
									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

               
				<tr>

					<td align="right" width="30%" class="black12bold">Status<span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'card_status',

									  'id'       => 'card_status',

									  'tabindex'       => '',

									  'value'    =>'',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20',
									  
                                      //'onblur'   => 'getUserInfo()',
									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>				

				<tr>

					<td align="right" class="black12bold">Phone No.</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'phone_no',

									  'id'       => 'phone_no',

									  'tabindex'       => '4',

									  'value'    =>  '',

									  'maxlength'=> '20',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_contactno" class="red11normal"></span>

					</td>

				</tr>												
			   											
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Card/Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>