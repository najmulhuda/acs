<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_region', 'onsubmit'=>'return check_add_region();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'region/edit/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'region/add/', $userformattributes); 
 }
 
?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>

<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   foreach($data as $row){
		   $id = $row->id;
		   $name = $row->name;
	       $description = $row->description;
		  
	   }	
	    
	}	
}else{
	$id="";
	$name = "";
	$description = "";

}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New Setting

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'region';?>" class="white14bold">Show Regions</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
			

		
				<tr>

					<td align="right" class="black12bold">Name <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'name',

									  'id'       => 'name',

									  'tabindex'       => '2',

									  'value'    => $name,

									  'maxlength'=> '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												

				<tr>

					<td align="right" width="30%" class="black12bold">Description <span class="red12bold"></span>:</td>

					<td align="left">

						<?php 

						$data = array(
                                  'name'        => 'description',
                                  'id'          => 'description',
                                  'value'       => $description,
                                  'rows'        => '5',
                                  'cols'        => '10',
                                  'style'       => 'width:50%',
                                );

                       echo form_textarea($data);

						?>


						<span id="errmsg_user_email" class="red11normal"></span>

					</td>

				</tr>												
				
			   
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>