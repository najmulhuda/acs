<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_changepassword', 'onsubmit'=>'return change_user_password();');
 echo form_open(site_url("/").'user/changepassword/', $userformattributes); 
 ?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>


<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Change Password

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'user';?>" class="white14bold">Show User List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => '',

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>

					<td align="right" width="30%" class="black12bold">Login Id <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'login_id',

									  'id'       => 'login_id',

									  'tabindex'       => '1',

									  'value'    => $username,

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

				<tr>
   
					<td align="right" width="30%" class="black12bold">Old Password <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'old_password',

									  'id'       => 'old_password',

									  'tabindex'       => '1',

									  'value'    => '',

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>

				<tr>

					<td align="right" class="black12bold">New Password <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'new_password',

									  'id'       => 'new_password',

									  'tabindex'       => '2',

									  'value'    => '',

									  'maxlength'=> '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_fullname" class="red11normal"></span>

					</td>

				</tr>												
			
			   <tr>
				    <td align="right" class="black12bold">Confirm New Password <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					  $data = array('name'     => 'confirm_new_password',

									  'id'       => 'confirm_new_password',

									  'tabindex'       => '2',

									  'value'    => '',

									  'maxlength'=> '250',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);
					  ?>	
                     

					</td>				
				</tr>
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>