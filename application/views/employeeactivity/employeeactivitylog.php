<?php
 $functionname = 'Employee Activities';
 $site_url = str_replace('/index.php','',site_url("/"));
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">

<script language="JavaScript" type="text/javascript">

	j=jQuery.noConflict();

	j(document).ready(function($) {

		initPagination();

	});

	 function pageselectCallback(page_index, jq){

		var new_content = j('#hiddenresult div.result:eq('+page_index+')').clone();

		j('#Searchresult').empty().append(new_content);

		return false;

	}

	function initPagination() {

		

		// count entries inside the hidden content

		var num_entries = j('#hiddenresult div.result tr').length-1;
       
		// Create content inside pagination element

		j("#Pagination").pagination(num_entries, {

			callback: pageselectCallback,

			items_per_page:20 // Show only one item per page

		});

	 }  
    
	
</script>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	All <?php echo $functionname; ?>

                </td>

                <td align="right">
                   <a href="<?php echo site_url("/").'subcentre/add';?>" class="white14bold"></a>
                </td>

              </tr>

            </table>			

		</td>

	</tr>
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	<tr>
     
		<td align="center">			

			<?php

			if(isset($employeedata) && count($employeedata)>0){
			 
			?>	

				<div style="width:99%; overflow:hidden;">				

					<div id="Searchresult">

						This content will be replaced when pagination inits.

					</div>

					<div style="padding:0 10px 0 20px;display:none" class="black12bold">Total <?php echo count($data);?> data or <?php echo ceil(count($data)/10);?> pages found </div>

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">

						<table width="100%" cellpadding="0" cellspacing="1" border="0">
                             <tr>
							 <td colspan="3">
							 <form action="employeeactivity" method="post">
							   <table width="100%" cellpadding="0" cellspacing="1" border="0">
							     <tr>
								
								 <td width="20%" style="text-align: left;font-weight:bold;">Employee Id :</td>
								 <td style="text-align:left;">
								      <?php 

						                 $data = array('name'     => 'employee_id',

									                   'id'       => 'employee_id',

									                   'tabindex'       => '1',

									                   'value'    => '',

									                   'maxlength' => '100',

									                   'size'    => '50',
													   
													   'class'    => 'input200x20'

									                );															

						                echo form_input($data);

						           ?>
								 </td>
								
								  <td style="text-align:left;">
								       <?php 

						                  $data = array( 'name'    => 'submit',

										                 'value'    => 'Search',

										                 'tabindex'       => '13',

									                      'class'    => 'submitbutton');															

						                           echo form_submit($data);
									  ?>			   
								  </td>
								 <td width="10%" style="text-align: right;font-weight:bold;"><a href="download/employeexls.php">Excel</a></td>
								  <td width="8%" style="text-align: right;font-weight:bold;"><a href="pdf_make/employeeactivity_pdf.php">Pdf</a></td>
								 </tr>
							
                               </table
							
							  </form>   
                           </td>	
                           <td></td>
                            <td></td>						   
							</tr>
							<tr style="background:#FFF;height:20px;"> <td colspan="5"></tr>
							<tr>

								<th class="titlerow" align="center" width="5%">#</th>

								<th class="titlerow" align="left" width="10%">Emp. Id</th>
								
								<th class="titlerow" align="left" width="53%">Emp. Name</th>

								<th class="titlerow" align="left" width="10%">Site Name</th>
								
                                <th class="titlerow" align="center" width="20%">Visit Date</th>																					
							</tr>

							<?php

							$str = "";

							$i = 1;

							foreach($employeedata as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;

								$employeeid = $row->employeeid;
                                $employeename = $row->employeename; 
								$sitename = $row->sitename;
                                $datetime = $row->datetime;
								
								$removeimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/deletesmall.png';

								$editimg = str_replace('/index.php','',site_url("/")).'common/pagination/images/edit.png';

								
								$str .= "<tr>

									<td align=\"center\" class=\"$classname\">$i</td>

									<td align=\"left\" class=\"$classname\">$employeeid</td>
                                     
									 <td align=\"left\" class=\"$classname\">$employeename</td>
									
									<td align=\"left\" class=\"$classname\">$sitename</td>
									 
									<td align=\"left\" class=\"$classname\">$datetime</td>";
								
									
								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				echo "<div class=red18bold>No record found</div>";					

			}								

			?>

		</td>

	</tr>

</table>