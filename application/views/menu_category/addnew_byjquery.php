<?php
$site_url = str_replace('/index.php','',site_url("/"));
?>
<script type="text/javascript" language="javascript">
	var j = jQuery.noConflict();
	//===========================For Menu Category insert ===================================//
	function show_menu_category_name_list(){
		j("#finalsaverow").fadeIn(100);
		j.post("<?php echo site_url("/");?>menu_category/show_menu_category_name_list/",{}, function(data){
			if(data !=''){
				j("#show_menu_category_name_list").html('<select name="menu_category_id" id="menu_category_id" onchange="show_parent_menu(this.value);" class="input200x20"><option value="0">Select Menu Category</option>'+data+'</select>');
				j("#addnew_menu_category_button").html("<a onclick=\"addnew_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Menu Category\" /></a>").fadeIn(100);
			}
			else{
				j("#show_menu_category_name_list").html('<select name="menu_category_id" id="menu_category_id" onchange="show_parent_menu(this.value);" class="input200x20"><option value="0">Select Menu Category</option></select>');
				j("#addnew_menu_category_button").html("<a onclick=\"addnew_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Menu Category\" /></a>").fadeIn(100);
			}
		});
	}	

	function addnew_menu_category_form(){
		j("#show_menu_category_name_list").fadeOut(100);
		j("#addnew_menu_category_button").fadeOut(100);
		j("#finalsaverow").fadeOut(100);
		j("#show_menu_category_name_list").html("<input name=\"menu_category_name\" id=\"menu_category_name\" class=\"input200x20\">").fadeIn(100);		
		j("#addnew_menu_category_button").html("<a onclick=\"savenew_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/savebutton.png\" alt=\"Save Menu Category\" /></a><a onclick=\"show_menu_category_name_list();\"><img src=\"<?php echo $site_url;?>common/images/cancelsave.png\" alt=\"Cancel Save Menu Category\" /></a>").fadeIn(100);
		 document.getElementById("menu_category_name").focus();
	}	

	function savenew_menu_category_form(){
		var menu_category_name = document.getElementById("menu_category_name");
		var elementmessage = document.getElementById("errmsg_menu_category_id");
		elementmessage.innerHTML = '';
		j('#menu_category_name').removeClass('error_input200x20').addClass('input200x20');
		if(menu_category_name.value==''){
			elementmessage.innerHTML = 'Menu Category name is mandatory';
			menu_category_name.focus();
			j('#menu_category_name').removeClass('input200x20').addClass('error_input200x20');
			return false;
		}
		if(menu_category_name.value !=''){
			j.post("<?php echo site_url("/");?>menu_category/save_menu_category_form/",{ menu_category_name:menu_category_name.value}, function(data){
				//alert(data);
				if(data !='ERROR'){
					j("#show_menu_category_name_list").fadeOut(100);
					j("#addnew_menu_category_button").fadeOut(100);
					j("#finalsaverow").fadeIn(100);					
					j("#show_menu_category_name_list").slideDown(500).html('<select name="menu_category_id" id="menu_category_id" onchange="show_parent_menu(this.value);" class="input200x20" onchange="show_menu_category_name_list(this.value);"><option value="0">Select Menu Category</option>'+data+'</select>');
					j("#addnew_menu_category_button").html("<a onclick=\"addnew_menu_category_form();\"><img src=\"<?php echo $site_url;?>common/images/addnewbutton.png\" alt=\"Add New Menu Category\" /></a>").fadeIn(100);
					
					show_parent_menu(document.getElementById("menu_category_id").value);
				}
				else{
					elementmessage.innerHTML = 'Error occured while saving menu_category name.';
					menu_category_name.focus();
				}
			});	
		}
	}
	
	function show_parent_menu(menu_category_id){
		j.post("<?php echo site_url("/");?>menu_category/show_parent_menu_listbyjquery/",{menu_category_id:menu_category_id}, function(data){
			if(data !=''){
				j("#parent_menu").html('<option value="0">Root</option>'+data);
			}
			else{
				j("#parent_menu").html('<option value="0">Root</option>');
			}
		});
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding:0px 5px 0px 0px;" width="200" valign="middle" align="left">
        <div id="show_menu_category_name_list">
        <select tabindex="4" name="menu_category_id" id="menu_category_id" onchange="show_parent_menu(this.value);" class="input200x20">
            <option value="0">Select Menu Category</option>
            <?php
            $conditionarray = array('menu_category_publish'=>1);
            echo $commonmodel->get_onetableallfieldvalueforselect('menu_category', 'menu_category_id', $menu_category_idval, 'menu_category_name', $conditionarray);
            ?>
        </select>
        </div>
    </td>
    <td width="50" style="padding:0px 5px 0px 0px;" align="center" valign="middle">
        <div id="addnew_menu_category_button" style="height:20px; width:80px;">
            <a onclick="addnew_menu_category_form();">
                <img src="<?php echo $site_url;?>common/images/addnewbutton.png" alt="Add New Menu Category" />
            </a>											
        </div>
    </td>
    <td><span id="errmsg_menu_category_id" class="red11normal"></span></td>
  </tr>
</table>