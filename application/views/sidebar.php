<div id="sideBar"><!-- Begin:sideBar -->	
	<div class="roundbox"><div class="roundboxIn"><div class="roundboxInner">		
		<div class="askQuestion"><!-- Begin:askQuestion -->			
			<h3>Get Your Free Consultation</h3>			
			<div class="innerEntry_0"><!-- Begin:innerEntry_0 -->			
				<?php $this->load->view('contact_us/quick_contact');?>							 
			</div><!-- End:innerEntry_0 -->			
			<span class="clear"><!-- Instead of overflow hidden --></span>			
		</div><!-- End:askQuestion -->		
		<span class="clear"><!-- Instead of overflow hidden --></span>			
	</div></div></div>    
    <div class="facebook_like">
    	<div class="facebookOuter">
         <div class="facebookInner">
            <div id="fb-root"></div>
				<script>
					(function(d, s, id) {
                  		var js, fjs = d.getElementsByTagName(s)[0];
                  		if (d.getElementById(id)) return;
                  		js = d.createElement(s); js.id = id;
                  		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=193726153980863";
                  		fjs.parentNode.insertBefore(js, fjs);
                	}(document, 'script', 'facebook-jssdk'));
               	</script>
          	<div class="fb-like" data-href="http://www.facebook.com/AcademyFitnessOfTexas" data-send="true" data-width="320" data-show-faces="true"></div>      
         </div>
        </div>
    </div>
    
    <div class="google_plus">
        <!-- Place this tag where you want the +1 button to render. -->
        <div class="g-plusone" data-annotation="inline" data-width="300"></div>
        
        <!-- Place this tag after the last +1 button tag. -->
        <script type="text/javascript">
          (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
        </script>
    </div>    
    <div class="google_plus">
    	<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>    
    <div class="socialLink"><!-- Begin:socialLink -->                
        <h3>follow us :</h3>        
        <ul>            
            <?php $this->load->view('common_front_module/front_social_media');?>            
        </ul>        
    </div><!-- End:socialLink -->	
</div><!-- End:sideBar -->