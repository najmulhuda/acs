<?php
 $site_url = str_replace('/index.php','',site_url("/"));
 $userformattributes = array('name' => 'frm_adsubcentre', 'onsubmit'=>'return check_add_subcentre();');
 
 if($mode=="edit"){
	$id=$this->uri->segment(3); 
	echo form_open(site_url("/").'subcentre/edit/'.$id, $userformattributes); 
 }else{
	echo form_open(site_url("/").'subcentre/add/', $userformattributes); 
 }
 
?>


<link rel="stylesheet" href="<?php echo $site_url?>common/calendar/calendar.css?random=20051112" media="screen" />

<script type="text/javascript" src="<?php echo $site_url?>common/calendar/birthcalendar.js?random=20051112"></script>

<?php 
if($mode=="edit"){
	if(isset($data) && count($data)>0){
	   foreach($data as $row){
		   $id = $row->id;
		   $name = $row->name;
		   $regionid = $row->regionid;
	       $address = $row->address;
		   
		}	
	    
	}	
}else{
	$id="";
	$name = "";
	$regionid="";
	$address = "";
}

?>

<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	<tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="50%">

                	Add New SubCentre

                </td>

                <td align="right">

                	<a href="<?php echo site_url("/").'subcentre';?>" class="white14bold">Show Sub Centre List</a>

                </td>

              </tr>

            </table>

			

		</td>

	</tr>

	<tr>

		<td align="center" class="errormessage">

            <?php 

			echo validation_errors();

			if(isset($err_message) && $err_message!=''){

				echo $err_message;

			}
            
			$data = array('type' => 'hidden', 'name' => 'u_id',

									          'id'   => 'u_id',

									          'tabindex'  => '',

									          'value'    => $id,

									          'maxlength'=> '100',

									          'size'    => '50',

									          'class'    => 'input200x20'

						  );															
			  
			    echo form_input($data);
			?>
            
		  
		</td>

	</tr>

    <tr>

		<td align="center">

			<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>

					<td align="right" width="30%" class="black12bold">Name <span class="red12bold">*</span>:</td>

					<td align="left">

						<?php 

						$data = array('name'     => 'subcentre_name',

									  'id'       => 'subcentre_name',

									  'tabindex'       => '1',

									  'value'    => $name,

									  'maxlength'=> '100',

									  'size'    => '50',

									  'class'    => 'input200x20'

									);															

						echo form_input($data);

						?>

						<span id="errmsg_user_name" class="red11normal"></span>

					</td>

				</tr>
				 <tr>
				    <td align="right" class="black12bold">Select Region <span class="red12bold">*</span>:</td>

					<td align="left">
                      
					  <?php
					   echo "<select name='region' id='region'>";
                        if(is_array($regiondata) && count($regiondata)>0) {
	                       foreach ($regiondata as $row1) {
							if($regionid==$row1->id){
							   echo "<option value='". $row1->id . "' selected>" . $row1->name . "</option>";	
							}else{
							   echo "<option value='". $row1->id . "'>" . $row1->name . "</option>";	
							}   
		                     
	                      }

                            
                        }	
					   echo "</select>";
					  ?>	
                     

					</td>				
				</tr>

				<tr>
   
					<td align="right" width="30%" class="black12bold">Address <span class="red12bold"></span>:</td>

					<td align="left">

						<?php 

						$data = array(
                                  'name'        => 'address',
                                  'id'          => 'address',
                                  'value'       => $address,
                                  'rows'        => '5',
                                  'cols'        => '10',
                                  'style'       => 'width:50%',
                                );

                       echo form_textarea($data);

						?>

						

					</td>

				</tr>

				
				<tr>

					<td align="right" class="black12bold">&nbsp;</td>

					<td align="left">

						<input type="hidden" name="action_type" id="action_type" value="" />

						<?php 

						$data = array( 'name'    => 'submit',

										'value'    => 'Add',

										'tabindex'       => '13',

									  'class'    => 'submitbutton');															

						echo form_submit($data);

						$data = array( 'name'    => 'reset',

										'value'    => ' Reset ',

										'tabindex'       => '14',

									  'class'    => 'submitbutton');															

						echo '&nbsp;&nbsp;'.form_reset($data);

						?>

					</td>

				</tr>

				<tr>

					<td align="right">&nbsp;</td>

					<td align="right">&nbsp;</td>

				</tr>						

			</table>

		</td>

	</tr>

</table>

<?php echo form_close(); ?>