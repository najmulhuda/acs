<?php
 $functionname = 'Visit';
 $site_url = str_replace('/index.php','',site_url("/"));
 
 $visitlist_fetchingurl = site_url("/").'processcommand/visitlistnextpage/';
 
?>

<script type="text/javascript" src="<?php echo $site_url?>common/pagination/paginationjs.js"></script>
<link rel="stylesheet" href="<?php echo $site_url?>common/pagination/paignationcss.css">

<input type="hidden" name="totalrowscount" id="totalrowscount" value="<?php echo $data['Count']; ?>" />
<script language="JavaScript" type="text/javascript">
var j = jQuery.noConflict();

   	var data_fieldsfetchurl = '<?php echo $visitlist_fetchingurl;?>';

	var firstval = 1;

   function pageselectCallback(page_index, jq){

		
		
		var diplay_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from

//		alert(diplay_per_page) ;
		
		var starting_val = parseInt(page_index * diplay_per_page);

		var searching_field=document.getElementById('searching_field').value;

		j.post(data_fieldsfetchurl, {"searching_field":searching_field, "page_index": page_index},

		function(data) {
			var htmlString = '<table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">'+
                            '<tr>'+
                                '<th class=\"titlerow\" align=\"left\" width=\"5%\">#</th>'+
								
								'<th class=\"titlerow\" align=\"left\" width=\"10%\">Employee Id</th>'+

								'<th class=\"titlerow\" align=\"left\" width=\"10%\">Name</th>'+

								'<th class=\"titlerow\" align=\"left\" width=\"10%\">Site Name</th>'+
								
								'<th class=\"titlerow\" align=\"left\" width=\"20%\">Visit Purpose</th>'+
								
								'<th class=\"titlerow\" align=\"left\" width=\"15%\">Visit Date</th>'+
								
								'<th class=\"titlerow\" align=\"left\" width=\"10%\">Status</th>'+
								
							'</tr>';
							for(var i = 0; i < data['Items'].length; i++)
							{
								var className = (i%2==0)?'evenrow':'oddrow';
								htmlString+='<tr>'+

									'<td align=\"left\" class=\"'+className+'\">'+(i+1)+'</td>'+
									
									'<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].employeeid+'</td>'+
									
                                     '<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].name+'</td>'+
									 
									'<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].sitename+'</td>'+
									
									'<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].purpose+'</td>'+
									
									'<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].visitdate+'</td>'+
									
									'<td align=\"left\" class=\"'+className+'\">'+data['Items'][i].status+'</td></tr>';
								
							}
			j('#Searchresult').html(htmlString);			

		}, "json");

		return true;

	}

	

	function getOptionsFromForm(){

		var opt = {callback: pageselectCallback, items_per_page:20};

		return opt;

	}

	function loadpagination(){

		var optInit = getOptionsFromForm(); //Create pagination element with options from form

		//alert(optInit);

		var totalrows_count = document.getElementById('totalrowscount').value;

		var data_per_page=document.getElementById('list_per_page').value; //Get number of elements per pagionation page from form

		j("#Pagination").pagination(totalrows_count, optInit);

	}

	

	function seachingwithpagination(){

		var searching_field=document.getElementById('searching_field').value;

		j.post('<?php echo site_url("/").'alarmlog/alarmloglist_count_with_search/';?>', {"searching_field":searching_field},

		function(data) {

			//alert(data);

			document.getElementById('totalrowscount').value = data;	

			loadpagination();

		});

	}

	function show_by_orderlist(show_by_orderval){

		//alert(show_by_orderval);

		document.getElementById('show_by_order').value = show_by_orderval;

		loadpagination();        

	}

	j(document).ready(function(){

		loadpagination();

	});
    
	
</script>


<table id="blueborder" width="98%" align="center" border="0" cellspacing="0" cellpadding="5">

	 <tr>

		<td id="bluebgtd">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td>

                	

                   	<div style="width:275px; float:left; margin-top:2px;">

                        <div style="float:left; width:140px; line-height:22px;">Display Per Page:</div>

                        <div style="float:left; width:125px; height:22px;">

                           <select id="list_per_page" class="searchselect40x20" name="list_per_page" onchange="seachingwithpagination();" disabled>

                               <option value="5">5</option>

                               <option value="10">10</option>

                               <option value="20" selected="selected">20</option>

                               <option value="30">30</option>

                               <option value="50">50</option>

                               <option value="70">70</option>

                               <option value="100">100</option>

                            </select>

                        

                        </div>

                  	</div>
                   
				    <div style="width:225px; float:right; margin-left:10px;">

                       <table style="width:100%">
					      <tr>
						      <td style="text-align:right;"> <input type="text" name="searching_field" id="searching_field" class="searchinput150x20" /></td>
							  <td>
							     <input type="button" name="searching_button" onclick="seachingwithpagination();" id="searching_button" class="searchbutton" />
							  </td>
						  </tr>
					   </table>                       
					  

                       

                    </div>     
                   

                </td>

                <td align="right" width="1">

                	               

                </td>

              </tr>

            </table>			

		</td>

	</tr>
	
	<tr>
	<td align="right">

                	<div id="Pagination">
					  
					</div>

                </td>

    </tr>
	<tr>
	    <td>
		     <?php 

			if(isset($msg) && $msg!=''){

				echo $msg;
                 
			}
			?>
		</td>
	</tr>
	<tr>
     
		<td align="center">			

			<?php

			if(isset($data) && count($data)>0){
			 
			?>	

				<div style="width:99%; overflow:hidden;">				

					<div id="Searchresult">

						This content will be replaced when pagination inits.

					</div>

					<div style="padding:0 10px 0 20px;display:none" class="black12bold">Total <?php echo count($data);?> data or <?php echo ceil(count($data)/10);?> pages found </div>

				</div>

				

				<div id="hiddenresult" style="display:none;">

					<div class="result">

						<table width="100%" cellpadding="0" cellspacing="1" border="0">
                           <tr>
						        <td colspan="7">
								    <table>
									    <tr>
										     <td>Date between</td>
								           <td align="left">
                      
						                        <?php 

						                            $datefrom = array('name'     => 'datefrom',

									                                  'id'       => 'datefrom',

									                                  'tabindex'       => '2',
															 
									                                  'maxlength'=> '250',

								                        	          'size'    => '50',

									                                  'class'    => 'input200x20'

								                             	);															

					 
					                                 echo form_input($datefrom);
					  

					                        	  ?>
                     
						                        

					                          </td>
                                              <td>to</td>
								              <td>
											         <?php 

						                                 $dateto = array('name'     => 'dateto',

									                                     'id'       => 'dateto',

									                                     'tabindex'       => '2',

									                                     'maxlength'=> '250',

								                        	             'size'    => '50',

									                                     'class'    => 'input200x20'

								                        	          );															

					 
					                                     echo form_input($dateto);
					  

					                        	      ?>
                       
											  </td>
											  <td>
											      <?php
											        	$submit = array( 'name'    => 'submit',

										                               'value'    => 'Search',

										                               'tabindex'       => '13',

									                                    'class'    => 'submitbutton');															

						                                 echo form_submit($submit);
						                          ?>
										     
											  </td>
										</tr>
									</table>
								</td>
						   </tr>
							<tr>

								<th class="titlerow" align="center" width="5%">#</th>

								<th class="titlerow" align="left" width="10%">Employee Id</th>

								<th class="titlerow" align="left" width="10%">Name</th>

								<th class="titlerow" align="left" width="10%">Site Name</th>
								
								<th class="titlerow" align="left" width="20%">Visit Purpose</th>

								<th class="titlerow" align="left" width="15%">Visiting Date</th>

                                <th class="titlerow" align="center" width="10%">Status</th>																					
							</tr>

							<?php

							$str = "";

							$i = 1;

							foreach($data as $row){

								
								if($i%2==0)	$classname = 'evenrow';

								else		$classname = 'oddrow';

								

								$id = $row->id;
                                $employeeid = $row->employeeid;
								$name = $row->name;
								$sitename = $row->sitename;
								$purpose =  $row->purpose;
                                $datetime = $row->visitdate;
								$status = $row->status;
								
								
								$str .= "<tr>

									<td align=\"center\" class=\"$classname\">$i</td>

									<td align=\"left\" class=\"$classname\">$employeeid </td>

									<td align=\"left\" class=\"$classname\">$name</td>

									<td align=\"left\" class=\"$classname\">$sitename</td>
									
									<td align=\"left\" class=\"$classname\">$purpose</td>
									
									
									<td align=\"left\" class=\"$classname\">$datetime</td>
									
									<td align=\"left\" class=\"$classname\">$status</td>";
								
								$str .= "</tr>";

								$i++;

							}

							echo $str;

						?>

						</table>

						</div>

					</div>

			<?php

			}

			else{

				echo "<div class=red18bold>No device found</div>";					

			}								

			?>

		</td>

	</tr>

</table>