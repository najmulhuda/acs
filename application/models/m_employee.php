<?php 

class M_Employee extends CI_Model{

	private $tableName = "tbl_employee";
	
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function save($data){
		$this->db->insert('tbl_employee', $data);
	}
	
	
	function getSelectedEmployee($subcentre/*,$status,$employeeid,$phoneno*/){
		$sql="";
		
		if($subcentre != -1){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE SubcentreId=". $subcentre ;
	    }
		
		/*if( !empty($sql) && $status != -1){
			$sql=$sql. " and State=". $status;
		}elseif(empty($sql) && $status != -1){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE State=". $status ;
		}
		
		
		
		if(!empty($sql) && $employeeid != ""){
			$sql=$sql. " and EmployeeId=". $employeeid;
		}elseif(empty($sql) && $employeeid != ""){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE EmployeeId=". $employeeid ;
		}
		
		if(!empty($sql) && $phoneno != ""){
			$sql=$sql. " and PhoneNo=". $phoneno;
		}elseif(empty($sql) && $phoneno != ""){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE PhoneNo=". $phoneno;
		}*/
		
		if(empty($sql)){
			$sql= "SELECT * FROM " . $this->tableName ;
		}
		
		$this->load->model('M_SubCentre');
		$list = array();

	  
		$query = $this->db->query($sql);
		foreach ($query->result() as $row)
		{
			$item = new M_Employee();
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->employeename = $row->EmployeeName;
			$item->cardnumber = $row->CardNumber;
			$item->cardstatus = $row->CardStatus;
			$item->state = $row->State;
			$item->subcentre = $this->M_SubCentre->getSubCentreName($row->SubCentreId);;
			$item->phoneno = $row->PhoneNo;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
	     $query->free_result(); // The $query result object will no longer be available	
	  	
		return $list;
		
	}
	
	function getAll()
	{
		$this->load->model('M_SubCentre');
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Employee();
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->employeename = $row->EmployeeName;
			$item->cardnumber = $row->CardNumber;
			$item->cardstatus = $row->CardStatus;
			$item->state = $row->State;
			$item->subcentre = $this->M_SubCentre->getSubCentreName($row->SubCentreId);;
			$item->phoneno = $row->PhoneNo;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getEmployeeById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Employee();
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->employeename = $row->EmployeeName;
			$item->cardnumber = $row->CardNumber;
			$item->cardstatus = $row->CardStatus;
			$item->state = $row->State;
			$item->subcentre = $this->M_SubCentre->getSubCentreName($row->SubCentreId);
			$item->phoneno = $row->PhoneNo;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getEmployeeNameByEmployeeId($employeeId){
		$employeeName = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE EmployeeId= ". $employeeId);
		
		foreach ($query->result() as $row){
			$employeeName = $row->EmployeeName;
		}
		
		return $employeeName;
		
	}
	function getEmployeeIdByEmployeeName($employeename){
		$employeeId = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE EmployeeName= ". $employeename);
		
		foreach ($query->result() as $row){
			$employeeId = $row->EmployeeId;
		}
		
		return $employeeId;
		
	}
	
	function getEmployeeIdByCardNumber($cardnumber){
		$employeeId = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE CardNumber= ". $cardnumber);
		
		foreach ($query->result() as $row){
			$employeeId = $row->EmployeeId;
		}
		
		return $employeeId;
	}
	function getEmployeeNameByCardNumber($cardnumber){
		$employeeName = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE CardNumber= ". $cardnumber);
		
		foreach ($query->result() as $row){
			$employeeName = $row->EmployeeName;
		}
		
		return $employeeName;
	}
	
	function getEmployeeCardNumberByEmployeeId($employeeId){
			$employeeCardNumber = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE EmployeeId= ". $employeeId);
		
		foreach ($query->result() as $row){
			$employeeCardNumber = $row->CardNumber;
		}
		
		return $employeeCardNumber;
	}
	
	function GetEmployeeIdById($id){
		$employeeId = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row){
			$employeeId = $row->EmployeeId;
		}
		
		return $employeeId;
		
	}
	
	function GetEmployeePhoneByEmployeeId($employeeId){
		$employeePhone = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE EmployeeId= ". $employeeId);
		
		foreach ($query->result() as $row){
			$employeePhone = $row->PhoneNo;
		}
		
		return $employeePhone;
		
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_employee', $data);
	}
	
	function delete($id){
		$this->db->where('Id', $id);
        $this->db->delete('tbl_employee');
	}
	
	function count_all(){
		return $this->db->count_all("tbl_employee");
	}
	
	function IsValidEmployeeId($employeeId){
		$sql="SELECT * FROM " . $this->tableName . " WHERE EmployeeId= ?";
		$query = $this->db->query($sql,array($employeeId));
		
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
		
	}
	
	function getEmployeeInfoByID($employeeId){
		
			
		$sql="SELECT * FROM " . $this->tableName . " WHERE EmployeeId= ?";
		$query = $this->db->query($sql,array($employeeId));
		$item = new M_Employee();
		foreach ($query->result() as $row)
		{
			
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->name = $row->EmployeeName;
			$item->cardnumber = $row->CardNumber;
			$item->state = $row->State;
			$item->subcentre = $this->M_SubCentre->getSubCentreName($row->SubCentreId);;
			$item->phoneno = $row->PhoneNo;
			$item->cardstatus = $row->CardStatus;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			
		}
		
		return $item;
	}
	
	function get_concatenate_value($data){
	  $concatenatedvalue = "";
	
    	foreach($data as $item){
		
			$concatenatedvalue=array("Id" => $item->id, "EmployeeId" => $item->employeeid, "EmployeeName" => $item->employeename, "CardNumber" => $item->cardnumber, "SubCentre" => $item->subcentre, "PhoneNo" => $item->phoneno, "State" =>  $item->state,"CardStatus" =>  $item->cardstatus);
			
		}
	 
	     return  $concatenatedvalue;
	}
		
}

?>