<?php 

class M_SubCentre extends CI_Model{

	private $tableName = "tbl_subcentre";
	
	function __construct()
	{
		$this->load->model('M_Region');
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_subcentre', $data);
	}
	
	
	function getAll()
	{
		$list1 = array();

		$query = $this->db->query("SELECT  `tbl_subcentre`.*, tbl_region.name as RegionName from tbl_subcentre INNER join tbl_region on tbl_subcentre.RegionId=tbl_region.Id");
		foreach ($query->result() as $row)
		{
			$item = new M_SubCentre();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->regionname = $row->RegionName;
			$item->address = $row->Address;
			
			array_push($list1,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list1;
	}
	
	function getSubcentreById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_SubCentre();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->regionid = $row->RegionId;
			$item->address = $row->Address;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getSubCentreName($id){
	   $name="";
	   $query = $this->db->query("SELECT Name FROM " . $this->tableName . " WHERE Id= ". $id);
	   
	   foreach ($query->result() as $row){
		   $name=$row->Name;
	   }
	   
	   return $name;
    }

	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_subcentre', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_user");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_subcentre');
	}
     
    function get_concatenate_value($data){
	  $concatenatedvalue = "";
	   
    	foreach($data as $item){
		
			$concatenatedvalue=array("Id" => $item->id, "RegionName" => $this->M_Region->getRegionNameById($item->regionid), "Name" => $item->name,"Address" => $item->address);
			
		}
	 
	     return  $concatenatedvalue;
	}	 
}

?>