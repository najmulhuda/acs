<?php 

class M_Region extends CI_Model{

	private $tableName = "tbl_region";
	
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function save($data){
		$this->db->insert('tbl_region', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Region();
			$item->id = $row->Id;
			$item->name = $row->name;
			$item->description = $row->description;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getRegionById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Region();
			$item->id = $row->Id;
			$item->name = $row->name;
			$item->description = $row->description;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getRegionNameById($id){
		$regionName = "";
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			
			$regionName = $row->name;
			
		}
		
		return $regionName;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_region', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_region");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_region');
	}	
	
	function get_concatenate_value($data){
	  $concatenatedvalue = "";
	
    	foreach($data as $item){
		
			$concatenatedvalue=array("Id" => $item->id, "name" => $item->name, "description" => $item->description);
			
		}
	 
	     return  $concatenatedvalue;
	}
}

?>