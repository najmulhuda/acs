<?php 

class M_Card extends CI_Model{

	private $tableName = "tbl_card";
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_card', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName);
		foreach ($query->result() as $row)
		{
			$item = new M_Card();
			$item->id = $row->Id;
			$item->cardnumber = $row->CardNumber;
			$item->status = $row->Status;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getCardById($id){
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE COALESCE(Id,0)= ". $id);
		if($query == true)
		{
		foreach ($query->result() as $row)
		{
			$item = new M_Card();
			$item->id = $row->Id;
			$item->cardnumber = $row->CardNumber;
			$item->status = $row->Status;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			return $item;
		}
		$query->free_result(); // The $query result object will no longer be available		
		}
		return null;
	}
	
	function getIdByCardNumber($cardNo){
		$sql = "SELECT Id FROM " . $this->tableName . " WHERE CardNumber = ?";
		$query = $this->db->query($sql, array($cardNo));
		foreach ($query->result() as $row)
		{
			return $row->Id;
		}
		return 0;
	}
	
	function update($data,$id)
	{
		$this->db->where('Id', $id);
        $this->db->update('tbl_card', $data);
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_card');
	}	
}

?>