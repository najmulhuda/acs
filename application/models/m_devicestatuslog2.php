<?php 
class M_DeviceStatusLog extends CI_Model{
	private $tableName = "tbl_devicestatuslog";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function get_books($limit, $start, $st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from devicestatusloglist where SiteName like '%$st%' limit " . $start . ", " . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function get_books_count($st = NULL)
    {
        if ($st == "NIL") $st = "";
        $sql = "select * from devicestatusloglist where SiteName like '%$st%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
  
}

?>