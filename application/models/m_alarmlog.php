<?php 

class M_AlarmLog extends CI_Model{

	private $tableName = "tbl_alarmlog";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	var $view = "";
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function getListByPage($pageIndex, $itemsByPage)
	{
		$listResult = array();
		$listResult['Items'] = array();
		
		//print_r ($show);
		$countSql = "SELECT  SiteName,SubCentreName,RegionName, Alarm, ATime,IP, Id as Serial FROM alarmloglist";
		$query = $this->db->query($countSql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
		//$sql = "SELECT  SiteName,SubcentreName,RegionName, Alarm, TIME(ATime) as ATime,IP, Id as Serial FROM alarmloglist".($pageIndex * $itemsByPage).", ".$itemsByPage;
				 
	   //$time = "SELECT DISTINCT tbl_alarmloghistroy.ATime as ATime FROM tbl_alarmloghistroy,tbl_alarmlog WHERE tbl_alarmlog.DeviceId = tbl_alarmloghistroy.DeviceId AND tbl_alarmloghistroy.ATime = (SELECT MIN(tbl_alarmloghistroy.ATime) FROM tbl_alarmloghistroy)";
		//$show = $this->db->query($time);
		//$show->free_result();
		$query = $this->db->query($countSql);
		
		foreach ($query->result() as $row){
			if($row->Alarm =='')
			{
				
			}
			else{
			$item = new M_AlarmLog();
			$item->id = $row->Serial;
			$item->sitename = $row->SiteName;
			$item->subcentrename = $row->SubcentreName;
			$item->regionname = $row->RegionName;
			$item->alarm = $row->Alarm;
			$item->atime = $row->ATime;
			$item->deviceip = $row->IP;
			array_push($listResult['Items'],$item);
			}
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;


	}
	
}

?>