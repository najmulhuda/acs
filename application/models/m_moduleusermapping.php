<?php 

class M_ModuleUserMapping extends CI_Model{

	private $tableName = "tbl_module";
	
	var $id = 0;
	var $loginid = "";
	var $password = "";
	var $employeeid = 0;
	var $modify_time;
	var $create_time;
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_modulemapping', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Name Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_ModuleUserMapping();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->inuptboxid = $row->Input_identifier_id;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getAllowedModules($userId){
		$list = array();

		$query = $this->db->query("SELECT * FROM tbl_modulemapping where UserId=". $userId);
		foreach ($query->result() as $row)
		{
			$item = new M_ModuleUserMapping();
			$item->id = $row->Id;
			$item->moduleid = $row->ModuleId;
			$item->userid = $row->UserId;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function DeleteRoles($userId){
		$this->db->where('UserId', $userId);
        $this->db->delete('tbl_modulemapping');
	}
	
	function getUserById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_User();
			$item->id = $row->Id;
			$item->loginid = $row->LoginId;
			$item->password = $row->Password;
			$item->employeeid = $row->EmployeeId;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_user', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_user");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_user');
	}	
}

?>