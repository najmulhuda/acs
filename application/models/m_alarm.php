<?php 
class M_Alarm extends CI_Model{
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_alarmname', $data);
	}
	
	function savealarmstatus($data){
		$this->db->insert('tbl_alarmstatus', $data);
	}
   function updatestatus($data,$id)
	{
		$this->db->where('Id', $id);
        $this->db->update('tbl_alarmstatus', $data);
	}
	
	function getAlarmList(){
		
		$list = array();

		$query = $this->db->query("SELECT tbl_alarmname.Id, tbl_alarmname.Name, tbl_alarmtype.Name as AlarmTypeName from tbl_alarmname inner join tbl_alarmtype on tbl_alarmname.alarmType=tbl_alarmtype.Id ");
		foreach ($query->result() as $row)
		{
			$item = new M_Alarm();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->alarmTypeName = $row->AlarmTypeName;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
		
	}
	
	function getAlarmType(){
		$list = array();

		$query = $this->db->query("SELECT Id, Name FROM tbl_alarmtype order by Id");
		foreach ($query->result() as $row)
		{
			$item = new M_Alarm();
			$item->id = $row->Id;
			$item->name = $row->Name;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getAlarmStatusList()
	{
		$list = array();

		$query = $this->db->query("SELECT tbl_alarmstatus.*, tbl_alarmname.Name,tbl_alarmname.alarmType,tbl_alarmtype.Name as alarmTypeName,tbl_alarmtype.Id as alarmTypeId from  tbl_alarmstatus inner join tbl_alarmname on tbl_alarmstatus.alarmId=tbl_alarmname.Id  inner join tbl_alarmtype on tbl_alarmtype.Id=tbl_alarmname.alarmType");
		foreach ($query->result() as $row)
		{
			if($row->alarmstatus=='High')
			{
				
			}
			else
			{
			$item = new M_Alarm();
			$item->id = $row->Id;
			$item->alarmid = $row->alarmId;
			$item->name = $row->Name;
			$item->alarmstatus = $row->alarmstatus;
			$item->alarmtypename = $row->alarmTypeName;
			$item->alarmtypeid = $row->alarmTypeId;
			$item->description = $row->description;
			array_push($list,$item);
			}
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getAlarmDetailsById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM tbl_alarmname WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Alarm();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->alarmtypeid = $row->alarmType;
			$item->description = $row->Description;
			array_push($list,$item);
			
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getAlarmByType($typeId){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM tbl_alarmname WHERE alarmType= ". $typeId);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Alarm();
			$item->id = $row->Id;
			$item->name = $row->Name;
			$item->alarmtypeid = $row->alarmType;
			$item->description = $row->Description;
			array_push($list,$item);
			
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_alarmname', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_alarmtext");
	}
	
	function deletedevicefunction($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_alarmstatus');
		//$query = $this->db->query("DELETE FROM tbl_alarmstatus WHERE Id= ". $id);
	}
  
}

?>