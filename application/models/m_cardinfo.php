<?php 

class M_Cardinfo extends CI_Model{

	private $tableName = "cardinfo";
	
	var $id = 0;
	var $ip = "";
	var $siteId = 1;
	var $status = 0;
	var $alarm = 0;
	var $modify_time;
	var $create_time;
	var $name = "";
	var $devstate = "";

	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('cardinfo', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Cardinfo();
			$item->id = $row->Id;
			$item->cardid = $row->CardId;
			$item->deviceip = $row->DeviceIp;
			$item->status = $row->Status;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getCardInfoById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Cardinfo();
			$item->id = $row->Id;
			$item->cardid = $row->CardId;
			$item->deviceip = $row->DeviceIp;
			$item->status = $row->Status;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('cardinfo', $data);
	}
	
	function count_all(){
		return $this->db->count_all("cardinfo");
	}
	
	function delete($data, $id)
	{
		$this->db->where('Id', $id);
        $this->db->update('cardinfo', $data);
	}	
}

?>