<?php 

class M_Schedule extends CI_Model{

	private $tableName = "tbl_schedule";
	
	var $id = 0;
	var $description = 1;
	var $time = 0;
	var $updatetime = 0;
	var $modify_time;
	var $cleardata="";
	var $readdata = "";
	var $updatecard = "";

	function __construct()
	{
		parent::__construct();
		
	}	
	
	function save($data){
		$this->db->insert('tbl_schedule', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Schedule();
			$item->id = $row->Id;
			$item->description = $row->Description;
			$item->time = $row->Time;
			$item->updatetime = $row->UpdateTime;
			$item->cleardata = $row->ClearData;
			$item->readdata = $row->ReadData;
			$item->updatecard = $row->UpdateCard;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getScheduleById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Schedule();
			$item->id = $row->Id;
			$item->description = $row->Description;
			$item->time = $row->Time;
			$item->updatetime = $row->UpdateTime;
			$item->cleardata = $row->ClearData;
			$item->readdata = $row->ReadData;
			$item->updatecard = $row->UpdateCard;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_schedule', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_schedule");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_schedule');
	}	
}

?>