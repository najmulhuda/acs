<?php 

class M_User extends CI_Model{

	private $tableName = "tbl_user";
	
	var $id = 0;
	var $loginid = "";
	var $password = "";
	var $employeeid = 0;
	var $modify_time;
	var $create_time;
	var $fname ="";
	var $phone="";
	var $email="";
	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_user', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_User();
			$item->id = $row->Id;
			$item->loginid = $row->LoginId;
			$item->password = $row->Password;
			$item->employeeid = $row->EmployeeId;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getUserById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_User();
			$item->id = $row->Id;
			$item->loginid = $row->LoginId;
			$item->password = $row->Password;
			$item->employeeid = $row->EmployeeId;
			$item->roleId = $row->RoleId;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_user', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_user");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_user');
	}

	function getRoles(){
		$list = array();
        $userRoleId=$this->getUserRoleId(get_loggedin_userid());
		if($userRoleId==1){
			$query = $this->db->query("SELECT * FROM tbl_role order by Id");
		}else{
			$query = $this->db->query("SELECT * FROM tbl_role where  Name<>'Admin' and  Name<>'Super Admin'");
		}
		  
		foreach ($query->result() as $row)
		{
			$item = new M_User();
			$item->id = $row->Id;
			$item->name = $row->Name;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getUserName($id){
		$userName="";
		$sql="SELECT * FROM " . $this->tableName . " WHERE Id= ?";
		
		$query = $this->db->query($sql,array($id));
		
		foreach($query->result() as $row){
			$userName=$row->LoginId;
		}
		
		return $userName;
		
	}
	function getUserId($id){
		$employeeId="";
		$sql="SELECT * FROM " . $this->tableName . " WHERE Id= ?";
		
		$query = $this->db->query($sql,array($id));
		
		foreach($query->result() as $row){
			$employeeId=$row->EmployeeId;
		}
		
		return $employeeId;
		
	}
	
	function getUserRoleId($id){
		$roleId="";
		$sql="SELECT * FROM " . $this->tableName . " WHERE Id= ?";
		
		$query = $this->db->query($sql,array($id));
		
		foreach($query->result() as $row){
			$roleId=$row->RoleId;
		}
		
		return $roleId;
		
	}
	
	function getUserRoleName($id){
		$roleName="";
		$sql="SELECT * FROM tbl_role WHERE Id= ?";
		
		$query = $this->db->query($sql,array($id));
		
		foreach($query->result() as $row){
			$roleName=$row->Name;
		}
		
		return $roleName;
		
	}
	
	function getUserIdByLoginName($loginId){
		$userId = "";
		$sql="SELECT * FROM tbl_user WHERE LoginId = ?";
		
		$query = $this->db->query($sql,array($loginId));
		
		foreach($query->result() as $row){
			$userId = $row->Id;
		}
		
		return $userId;
	}
	
	function IsOldPasswordMatched($id, $oldpass){
		$pass="";
		$sql="SELECT * FROM " . $this->tableName . " WHERE Id= ?";
		
		$query = $this->db->query($sql,array($id));
		
		foreach($query->result() as $row){
			$pass=$row->Password;
		}

		if(md5($oldpass)==$pass){
			return true;
		}else{
			return false;
		}
		
	}
	
	function get_concatenate_value($data){
	  $concatenatedvalue = "";
	
    	foreach($data as $item){
		
			$concatenatedvalue=array("Id" => $item->id, "LoginId" => $item->loginid, "Password" => $item->password, "EmployeeId" => $item->employeeid,"RoleName" => $this->getUserRoleName($item->roleId));
			
		}
	 
	     return  $concatenatedvalue;
	}
}

?>