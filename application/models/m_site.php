<?php 

class M_Site extends CI_Model{

	private $tableName = "tbl_site";
	
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function save($data){
		$this->db->insert('tbl_site', $data);
	}

	function getAll()
	{
		$this->load->model('M_SubCentre');
		$list = array();

		$query = $this->db->query("SELECT tbl_site.*, COALESCE(tbl_device.IP,'') as IP from tbl_site left join tbl_device on tbl_site.Id=tbl_device.SiteId ");
		foreach ($query->result() as $row)
		{
			$item = new M_Site();
			$item->id = $row->Id;
			$item->ip = $row->IP;
			$item->name = $row->Name;
			$item->subcentrename = $this->M_SubCentre->getSubCentreName($row->SubcentreId);
			$item->contactno = $row->ContactNo;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getSiteById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Site();
			$item->id = $row->Id;
			$item->ip ="";
			$item->name = $row->Name;
			$item->subcentre = $row->SubcentreId;
			$item->contactno = $row->ContactNo;
			$item->modifytime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function GetSiteNameById($siteId){
	     $sitename="";
    	 $query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $siteId);
		
		 foreach ($query->result() as $row)
		 {
			$sitename = $row->Name;
			
		 }
	    
		
		return $sitename;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_site', $data);
	}
	
	function delete($id){
		$this->db->where('Id', $id);
        $this->db->delete('tbl_site');
	}
	
	function count_all(){
		return $this->db->count_all("tbl_site");
	}
	
	function getSiteBySubcentre($subcentreId){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE SubcentreId= ". $subcentreId);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Site();
			$item->id = $row->Id;
			$item->name = $row->Name;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
		
}

?>