<?php 
class M_EmployeeActivity extends CI_Model{
	
	function __construct()
	{
		$this->load->model('M_Employee');
		$this->load->model('M_Site');
		$this->load->model('M_Device');
		parent::__construct();
	}	
	
		
	function getAll(){
		
		$list1 = array();

		$query = $this->db->query("SELECT * from tbl_attendancedata order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_EmployeeActivity();
			$item->id = $row->Id;
			$item->employeeid = $this->M_Employee->getEmployeeIdByCardNumber($row->CardId);
			//$item->employeename = $this->M_Employee->getEmployeeNameByCardNumber($row->CardId);
			$item->employeename = $this->M_Employee->getEmployeeNameByEmployeeId($item->employeeid);
			$siteId=$this->M_Device->GetSiteIdByDeviceId($row->DeviceId);
		    $item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
			//$item->sitename = $this->M_Site->GetSiteNameById($siteId);
			$item->datetime = $row->Ctime;
			
			array_push($list1,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list1;
		
	}
	
	function getSelectedEmployeeActivities($cardnumber){
		$sql="";
		
		
	    $sql= "SELECT * FROM tbl_attendancedata WHERE CardId=". $cardnumber . "  order by Id Desc" ;
	  
		$query = $this->db->query($sql);
		
		$list = array();
	if($query != FALSE){	
		foreach ($query->result() as $row){
			
			$item = new M_EmployeeActivity();
			$item->id = $row->Id;
			$item->employeeid = $this->M_Employee->getEmployeeIdByCardNumber($row->CardId);
			$item->employeename = $this->M_Employee->getEmployeeNameByEmployeeId($item->employeeid);
			$siteId=$this->M_Device->GetSiteIdByDeviceId($row->DeviceId);
			$item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
		    //$item->sitename = $this->M_Site->GetSiteNameById($siteId);
			$item->datetime = $row->Ctime;
			
			array_push($list,$item);
		}
	     $query->free_result();
	 }	
	     // The $query result object will no longer be available	
	  	
		 return $list;
		
	}
	
	
	
}

?>