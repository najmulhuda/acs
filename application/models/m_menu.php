<?php 

class M_Menu extends CI_Model{

	
	
	function __construct()
	{
		$this->load->model('M_User');
		
		parent::__construct();
	}	
	
	
	function getAllowedMenus($userId)
	{   
	    $list = array();
		$sql = "select * from view_allowedmenu where  UserId=". $userId;
		
        $query = $this->db->query($sql);
        if($this->db->affected_rows()>=1){
			foreach ($query->result() as $row)
		{
			$item = new M_Menu();
			$item->moduleid = $row->ModuleId;
			$item->name = $row->Name;
			$item->controlleractionurl = $row->ControllerActionUrl;
			$item->menutype = $row->MenuType;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
		
    }	
  }
 
   function getMenus(){
		 $list = array();
	   
         $roleId=$this->M_User->getUserRoleId(get_loggedin_userid());	
		 
		if($roleId == 1){
			$sql = "select * from tbl_module order by Id";
		}else{
			$sql = "select * from view_allowedmenu where UserId=".get_loggedin_userid()." order by ModuleId ASC";
		} 
	
        $query = $this->db->query($sql);
        if($this->db->affected_rows()>=1){
			foreach ($query->result() as $row)
		   {
		    	$item = new M_Menu();
		    	$item->id = $row->Id;
		    	$item->parentid = $row->ParentId;
	     		//$item->moduleid = $row->ModuleId;
		    	$item->name = $row->Name;
		    	$item->controlleractionurl = $row->ControllerActionUrl;
				$item->Input_identifier_id = $row->Input_identifier_id;

			    //$item->menutype = $row->MenuType;
			    array_push($list,$item);
	       }
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
   }
   }	
   
   function getChildMenus($parentId){
		 $list = array();
		 
		 $roleId=$this->M_User->getUserRoleId(get_loggedin_userid());	
		 if($roleId == 1){
			  $sql = "select * from tbl_module where ParentId=".$parentId." order by Id";
		 }else{
			  $sql = "select * from view_allowedmenu where ParentId=".$parentId." and UserId=".get_loggedin_userid();
		 }
		
		
        $query = $this->db->query($sql);
        if($this->db->affected_rows()>=1){
			foreach ($query->result() as $row)
		   {
		    	$item = new M_Menu();
		    	$item->id = $row->Id;
		    	$item->parentid = $row->ParentId;
	     		//$item->moduleid = $row->ModuleId;
		    	$item->name = $row->Name;
		    	$item->controlleractionurl = $row->ControllerActionUrl;
				$item->Input_identifier_id = $row->Input_identifier_id;
			    //$item->menutype = $row->MenuType;
			    array_push($list,$item);
	       }
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
   }
   }	
   
	function GetMenuTreeView(){
		
		
		
		 $site_url = str_replace('/index.php','',site_url("/"));
		 $output = " <div class='divshapefortreemenu'><div class='dTree'><li><a href='#'>Menu</a></li>";
          foreach ($this->getMenus() as $item) {
            if ($item->parentid==0)
            {
                $output .= "<li title=\"".$item->name."\">"."<a href=\"".$site_url.$item->controlleractionurl."\" >".$item->name. "</a>". "\n<ul>\n";
                $output .= $this->getAllChildren($item->id);
                $output .= "\n</ul>\n</li>";
            }
        }
        $output .= "</div></div>";
		return $output;
	}
	
	function getAllChildren($parent_id)
    {
        //Get all the nodes for particular ID
	    $site_url = str_replace('/index.php','',site_url("/"));
        $output = "";
		$id=$parent_id;
		
		$data = $this->getChildMenus($id);
	   
	 if(isset($data) && count($data)>0){
		foreach ($data as $child) {
            if ($child->parentid == $parent_id)
            {
                if((count($this->getChildMenus($child->id)))>0){
					$output .= "<li title=\"" . $child->name . "\">" . "<a href=\"".$site_url.$child->controlleractionurl."\">" . $child->name . "</a>" .  
				    "\n<ul>\n";
				}else{
					$output .= "<li title=\"" . $child->name . "\">" . "<a href=\"".$site_url.$child->controlleractionurl. "\">" . $child->name . "</a>" ."";
				}
				$output .= $this->getAllChildren($child->id);
				if((count($this->getChildMenus($child->id)))>0)
				{
					 $output .= "\n</ul>\n</li>"; 
				}else{
					$output .= "</li>"; 
				}
               
            }
        }
	 }	
        return $output;
    }
  
}

?>