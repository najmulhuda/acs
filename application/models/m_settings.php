<?php 

class M_Settings extends CI_Model{

	private $tableName = "tbl_settings";
	
	var $id = 0;
	var $loginid = "";
	var $password = "";
	var $employeeid = 0;
	var $modify_time;
	var $create_time;
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function save($data){
		$this->db->insert('tbl_settings', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Settings();
			$item->id = $row->Id;
			$item->field = $row->Field;
			$item->value = $row->Value;
			
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getSettingsById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Settings();
			$item->id = $row->Id;
			$item->field = $row->Field;
			$item->value = $row->Value;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_settings', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_settings");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_settings');
	}	
}

?>