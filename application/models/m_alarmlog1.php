<?php 

class M_AlarmLog extends CI_Model{

	private $tableName = "tbl_alarmlog";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function getListByPage($pageIndex, $itemsByPage)
	{
		$listResult = array();
		$listResult['Items'] = array();

		$countSql = "SELECT  SiteName,SubCentreName,RegionName, Alarm, ATime,IP, MAX(Id) as Serial FROM alarmloglist GROUP BY IP ORDER BY MAX(Id) DESC ";
		$query = $this->db->query($countSql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
		$sql = "SELECT  SiteName,SubcentreName,RegionName, Alarm, TIME(ATime) as ATime,IP, MAX(Id) as Serial FROM alarmloglist GROUP BY IP ORDER BY MAX(Id)
		         DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage;
				 
	   
		$query = $this->db->query($sql);
		
		foreach ($query->result() as $row){
			$item = new M_AlarmLog();
			$item->id = $row->Serial;
			$item->sitename = $row->SiteName;
			$item->subcentrename = $row->SubcentreName;
			$item->regionname = $row->RegionName;
			$item->alarm = $row->Alarm;
			$item->atime = $row->ATime;
			$item->deviceip = $row->IP;
			array_push($listResult['Items'],$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;


	}
	
}

?>