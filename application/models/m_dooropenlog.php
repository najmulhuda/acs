<?php 

class M_DoorOpenLog extends CI_Model{

	private $tableName = "tbl_processcommandinfo";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	
	function __construct()
	{
		$this->load->Model('M_Site');
		$this->load->Model('M_Employee');
		parent::__construct();
		
	}	
	
	function getListByPage($pageIndex, $itemsByPage,$searchedsiteId)
	{
		$listResult = array();
		$listResult['Items'] = array();

		$countSql = "SELECT  * from tbl_processcommandinfo order by Id DESC";
		$query = $this->db->query($countSql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
	   if ($searchedsiteId == -1){	
		$sql = "SELECT  * from tbl_processcommandinfo order by Id
		         DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage;
	   }else{
		  $sql = "SELECT  * from tbl_processcommandinfo where SiteId = " .$searchedsiteId. " order by Id
		         DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage; 
	   }		 
	   
		$query = $this->db->query($sql);
		
		foreach ($query->result() as $row){
			$item = new M_DoorOpenLog();
			$item->id = $row->Id;
			$item->user =$row->User;
			$item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
			$item->employeename = $this->M_Employee->getEmployeeNameByEmployeeId($row->EmployeeId);
			$item->employeeid = $row->EmployeeId;
			$item->phoneno = $this->M_Employee->GetEmployeePhoneByEmployeeId($row->EmployeeId);
			$item->status = $row->Status;
			$item->purpose = $row->Purpose;
			$item->cmd_time= $row->CommandDateTime;
			array_push($listResult['Items'],$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;


	}
	
   function getSelectedItemListByPage($pageIndex, $itemsByPage,$site,$datefrom,$dateto){
		
		$listResult = array();
		$listResult['Items'] = array();

		
		$sql="";
		
		if($site != -1){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE SiteId=". $site ;
	    }
		
		if( !empty($sql) && $datefrom != "" && $dateto != ""){
			$sql=$sql. " and CommandDateTime between ". $datefrom . " and ". $dateto;
		}elseif(empty($sql) && $datefrom != "" && $dateto != ""){
			$sql= "SELECT * FROM " . $this->tableName . " WHERE CommandDateTime between ". $datefrom . " and ". $dateto;
		}
		
		if(empty($sql)){
			$sql= "SELECT * FROM " . $this->tableName ;
		}
		
		
		
		$query = $this->db->query($sql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
		
		
        $sql=$sql." order by Id
		         DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage;
	
        echo $sql;	
	  
		$query = $this->db->query($sql);
		foreach ($query->result() as $row)
		{
			$item = new M_DoorOpenLog();
			$item->id = $row->Id;
			$item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
			$item->employeename = $this->M_Employee->getEmployeeNameByEmployeeId($row->EmployeeId);
			$item->employeeid = $row->EmployeeId;
			$item->phoneno = $this->M_Employee->GetEmployeePhoneByEmployeeId($row->EmployeeId);
			$item->status = $row->Status;
			$item->purpose = $row->Purpose;
			array_push($listResult['Items'],$item);
		}
	     $query->free_result(); // The $query result object will no longer be available	
	  	
		return $listResult;
		
	}
	
}

?>