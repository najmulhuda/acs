<?php 
class M_UserActivity extends CI_Model{
	
	function __construct()
	{
		$this->load->model('M_User');
		$this->load->Model('M_Employee');
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_useractivitylog', $data);
	}
	
	function getAll(){
		
		$list1 = array();

		$query = $this->db->query("SELECT * from tbl_useractivitylog");
		foreach ($query->result() as $row)
		{
			$item = new M_UserActivity();
			$item->id = $row->Id;
			//$item->tablename = $row->tableName;
            $item->emplyeeid = $this->M_User->getUserId($row->UserId);
			$item->description = $row->Description;
			
			$item->username = $this->M_User->getUserName($row->UserId);
			
			$item->datetime = $row->DateTime;
			
			array_push($list1,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list1;
		
	}
	
	function getSelectedUserActivities($userId){
		$list1 = array();

		//$query = $this->db->query("SELECT * from tbl_useractivitylog where UserId=(select Id from tbl_user where LoginId =$userId)");
		$query = $this->db->query("SELECT * from tbl_useractivitylog where UserId=".$userId);
		foreach ($query->result() as $row)
		{
			$item = new M_UserActivity();
			$item->id = $row->Id;
			//$item->tablename = $row->tableName;
			$item->emplyeeid = $this->M_User->getUserId($row->UserId);
			$item->description = $row->Description;
			$item->username = $this->M_User->getUserName($row->UserId);
			$item->datetime = $row->DateTime;
			
			array_push($list1,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list1;
	}
}

?>