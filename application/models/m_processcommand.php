<?php 

class M_ProcessCommand extends CI_Model{

	private $tableName = "tbl_processcommandinfo";
	
	function __construct()
	{
		$this->load->model('M_Site');
		$this->load->model('M_Employee');
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_processcommandinfo', $data);
		return $this->db->insert_id();
	}
	
	
	function getAll()
	{
		$listResult = array();
       
		$query = $this->db->query("SELECT * FROM " . $this->tableName);
		foreach ($query->result() as $row)
		{
			$item = new M_ProcessCommand(); //TODO
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->name = $this->M_Employee->getEmployeeNameByEmployeeId($row->EmployeeId);
			$item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
			$item->purpose = $row->Purpose;
			$item->visitdate = $row->CommandDateTime;
			$item->status = $row->Status;
			array_push($listResult,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;
	}
	
	function GetDoorOpenCommandSendFilePath(){
		$path = "";
		$query = $this->db->query("SELECT * FROM tbl_settings Where Field='conitious_service_data_path'");
		
		foreach ($query->result() as $row)
		{
			
			$path = $row->Value;
			
		}
		
		return $path;
	}
	
	function getListByPage($pageIndex, $itemsByPage)
	{
		$listResult = array();
		$listResult['Items'] = array();

		$countSql = "SELECT * from tbl_processcommandinfo order by Id DESC";
		$query = $this->db->query($countSql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
		$sql = "SELECT * from tbl_processcommandinfo order by Id DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage;
		$query = $this->db->query($sql);
		
		foreach ($query->result() as $row){
			$item = new M_ProcessCommand(); //TODO
			$item->id = $row->Id;
			$item->employeeid = $row->EmployeeId;
			$item->name = $this->M_Employee->getEmployeeNameByEmployeeId($row->EmployeeId);
			$item->sitename = $this->M_Site->GetSiteNameById($row->SiteId);
			$item->purpose = $row->Purpose;
			$item->visitdate = $row->CommandDateTime;
			$item->status = $row->Status;
			array_push($listResult['Items'],$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;


	}
	

}

?>