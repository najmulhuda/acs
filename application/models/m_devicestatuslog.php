<?php 
class M_DeviceStatusLog extends CI_Model{
	private $tableName = "tbl_devicestatuslog";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	
	function __construct()
	{
		parent::__construct();
		
	}	
	
	function getListByPage($pageIndex, $itemsByPage)
	{
		$listResult = array();
		$listResult['Items'] = array();

		$countSql = "SELECT SiteName,SubcentreName,RegionName, Status, DSTime as DSTime,IP, Id as Serial FROM devicestatusloglist";
		$query = $this->db->query($countSql);
		$listResult['Count'] = $query->num_rows();
		$query->free_result(); 
		
		//$sql = "SELECT  SiteName,SubcentreName,RegionName, Status, TIME(DSTime) as DSTime,IP, MAX(Id) as Serial FROM devicestatusloglist GROUP BY IP ORDER BY MAX(Id) DESC limit ".($pageIndex * $itemsByPage).", ".$itemsByPage;
				 
		//echo $sql;		 
		$query = $this->db->query($countSql);
		
		foreach ($query->result() as $row){
			if($row->Status =='')
			{
				
			}
			else
			{
			$item = new M_DeviceStatusLog();
			$item->id = $row->Serial;
			$item->sitename = $row->SiteName;
			$item->subcentrename = $row->SubcentreName;
			$item->regionname = $row->RegionName;
			$item->deviceip = $row->IP;
			$item->datetime = $row->DSTime;
			$item->status = $row->Status;
			
			array_push($listResult['Items'],$item);
			}
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $listResult;


	}
  
}

?>