<?php 

class M_ChangeLog extends CI_Model{

	private $tableName = "tbl_changelog";
	
	var $id = 0;
	var $deviceid = "";
	var $alarm = "";
	var $time = 0;
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_changelog', $data);
	}
	
	
	function getAll()
	{
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_ChangeLog();
			$item->id = $row->Id;
			$item->tablename = $row->TableName;
			$item->field = $row->Field;
			$item->time = $row->Time;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getAlarmLogById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_ChangeLog();
			$item->id = $row->Id;
			$item->tablename = $row->TableName;
			$item->field = $row->Field;
			$item->time = $row->Time;
			array_push($list,$item);
			
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_changelog', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_changelog");
	}
	
	function delete($id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_changelog');
	}	
}

?>