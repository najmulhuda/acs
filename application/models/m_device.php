<?php 

class M_Device extends CI_Model{

	private $tableName = "tbl_device";
	
	

	function __construct()
	{
		$this->load->Model('M_Site');
		parent::__construct();
	}	
	
	function save($data){
		$this->db->insert('tbl_device', $data);
	}
	
	
	function getAll()
	{
		$this->load->model('M_SubCentre');
		$list = array();

		$query = $this->db->query("SELECT * FROM " . $this->tableName . " order by Id Desc");
		foreach ($query->result() as $row)
		{
			$item = new M_Device();
			$item->id = $row->Id;
			$item->ip = $row->IP;
			$item->siteid = $this->M_Site->GetSiteNameById($row->SiteId);
			$item->status = $row->Status;
			$item->versionno = $row->VersionNo;
			$item->serialno = $row->SerialNo;
			$item->subcentre = $this->M_SubCentre->getSubCentreName($row->SubCentreId);
			$item->addedby = $row->AddedBy;
			$item->modifiedby = $row->ModifiedBy;
			$item->modifiedtime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	function getDeviceById($id){
		$list = array();
        
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $id);
		
		foreach ($query->result() as $row)
		{
			$item = new M_Device();
			$item->id = $row->Id;
			$item->ip = $row->IP;
			$item->siteid = $row->SiteId;
			$item->status = $row->Status;
			$item->versionno = $row->VersionNo;
			$item->serialno = $row->SerialNo;
			$item->subcentre = $row->SubCentreId;
			$item->addedby = $row->AddedBy;
			$item->modifiedby = $row->ModifiedBy;
			$item->modifiedtime = $row->ModifyTime;
			$item->createdate = $row->CreateDate;
			array_push($list,$item);
		}
		$query->free_result(); // The $query result object will no longer be available		
		return $list;
	}
	
	
	function GetSiteIP($siteId){
		$siteIP="";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE SiteId= ". $siteId);
		foreach ($query->result() as $row)
		{
			$siteIP = $row->IP;
	    }	
    
        return 	$siteIP;
	}
	function GetSiteIPsubcentre($subcentre){
		$siteIP="";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE SubCentreId= ". $subcentre);
		foreach ($query->result() as $row)
		{
			$siteIP = $row->IP;
	    }	
    
        return 	$siteIP;
	}
	
	function GetSiteIdByDeviceId($deviceId){
		$siteId=0;
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE  Id= ". $deviceId);
		foreach ($query->result() as $row)
		{
			$siteId = $row->SiteId;
	    }	
    
        return 	$siteId;
	}
	
	function GetDeviceIDBySiteId($siteId){
		$deviceId = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE SiteId= ". $siteId);
		foreach ($query->result() as $row)
		{
			$deviceId = $row->Id;
	    }	
    
        return 	$deviceId;
	}
	
	function getAllSubcentre(){
		$this->load->model('M_SubCentre');
		return $this->M_SubCentre->getAll();
	}
	
	
	function update($data,$id){
		$this->db->where('Id', $id);
        $this->db->update('tbl_device', $data);
	}
	
	function count_all(){
		return $this->db->count_all("tbl_device");
	}
	
	function delete($data, $id)
	{
		$this->db->where('Id', $id);
        $this->db->delete('tbl_device');
	}
    
	function get_concatenate_value($data){
	  $concatenatedvalue = "";
	
    	foreach($data as $item){
		
			$concatenatedvalue=array("Id" => $item->id, "IP" => $item->ip, "SiteId" => $item->siteid, "Status" => $item->status, "VersionNo" => $item->versionno, "SerialNo" => $item->serialno, "SubCentreId" =>  $item->subcentre);
			
		}
	 
	     return  $concatenatedvalue;
	}

function GetDeviceVersionByDeviceId($Id){
	$deviceVersion = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $Id);
		foreach ($query->result() as $row)
		{
			$deviceVersion = $row->VersionNo;
	    }	
    
        return 	$deviceVersion;
}

function GetDeviceIPById($Id){
	$IP = "";
		$query = $this->db->query("SELECT * FROM " . $this->tableName . " WHERE Id= ". $Id);
		foreach ($query->result() as $row)
		{
			$$IP = $row->IP;
	    }	
    
        return 	$IP;
}
	
}

?>