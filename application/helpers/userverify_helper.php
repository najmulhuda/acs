<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_logged_in() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $userid = get_session_object()->loginid;
	
    if ($userid == null) { return false; } else { return true; }
}

function can_edit() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $roleid = $CI->session->userdata['loggedinuser']['roleid'];
    if (isset($roleid) && ($roleid == 2 || $roleid == 1)) { return true; } else { return false; }
}

function can_admin() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $roleid = get_session_object()->roleid;
    if (isset($roleid) && $roleid == 1)  { return true; } else { return false; }
}

function getUserName(){
	// Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    return  get_session_object()->loginid;
}

function get_loggedin_userid(){
	// Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    return get_session_object()->id;
}

function get_session_object(){
	 $CI =& get_instance();
	 $logininfo=$CI->session->userdata('loggedinuser');
	 If(is_array($logininfo) && count($logininfo)>0)
     {
        return (object)$logininfo;
     }else{
         $logindata = array(
                              'id'  => '',
                              'loginid' => '',
                              'employeeid' => '',
							  'roleid' => '',
                          );
     
	     $logininfo=(object)$logindata;
		 return $logininfo;
	 }
	
}

?>