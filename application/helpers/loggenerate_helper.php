<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_edit_changetrack($old_data, $new_data,$table){
	if($old_data==$new_data){
		
		return $table." information browsed";
		
	}else{
		
		$diff = array_key_diff($old_data,$new_data);
		
		$str = "";
		
		if(!empty($diff)){
			 foreach ($diff as $key => $val){
				if($str == ""){
				   $str.=	$key." value changed from ".$diff[$key]." to ".	$new_data[$key];	
				}else{
				   $str.= ", ".$key." value changed from ".$diff[$key]." to ".	$new_data[$key];	
				} 
		     } 
		}
	
		return  $str;
	}
	
}


function array_key_diff($ar1, $ar2) { 

    $aSubtrahends = array_slice(func_get_args(),1); 
    foreach ($ar1 as $key => $val) 
        foreach ($aSubtrahends as $aSubtrahend) 
		     if (in_array($val, $aSubtrahend)) 
                 unset ($ar1[$key]); 
            
    return $ar1; 
}  

?>