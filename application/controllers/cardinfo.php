<?php
class Cardinfo extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Cardinfo');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}		
	}	

	function add()
	{
		if(!can_edit())
		{
			$view_data['content_view'] = "unauthorized";
			$this->load->view('template',$view_data);
			return;
		}
		$view_data = array();
		$view_data['content_view'] = "cardinfo/addcardinfo";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	    $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating CardId Field
            $this->form_validation->set_rules('card_id', 'Card Id', 'required');

            //Validating Device IP Field
            $this->form_validation->set_rules('device_ip', 'Device IP', 'required');

            //Validating Status Field
            $this->form_validation->set_rules('status', 'Status', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'CardId' => $this->input->post('card_id'),
                              'DeviceIp' => $this->input->post('device_ip'),
                              'Status' => $this->input->post('status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              
					      );
			
			$this->M_Cardinfo->save($data);  // Add Device
			$view_data['err_message'] = "Card Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		if(!can_edit())
		{
			$view_data['content_view'] = "unauthorized";
			$this->load->view('template',$view_data);
			return;
		}
		$view_data = array();
		$view_data['content_view'] = "cardinfo/addcardinfo";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	    $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'CardId' => $this->input->post('card_id'),
                              'DeviceIp' => $this->input->post('device_ip'),
                              'Status' => $this->input->post('status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
                          );
		  				 
			$this->M_Cardinfo->update($data,$id);  // Update Device 
            $view_data['content_view'] = "cardinfo/addcardinfo";
		    $view_data['data'] = $this->M_Cardinfo->getCardInfoById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Cardinfo->getCardInfoById($id);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$num_rows=$this->M_Cardinfo->count_all();
		
		$view_data = array();
		$view_data['data'] = $this->M_Cardinfo->getAll();
		$view_data['content_view'] = "cardinfo/cardinfolist";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	    $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		if(!can_edit())
		{
			$view_data['content_view'] = "unauthorized";
			$this->load->view('template',$view_data);
			return;
		}
		$data = array(    
                        'Status' => 4
				      );
		  				 
			$this->M_Cardinfo->delete($data,$id);  // Delete Device (Soft Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_Cardinfo->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	    $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "cardinfo/cardinfolist";
		$this->load->view('template',$view_data);
			
	}

}

?>