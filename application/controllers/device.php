<?php
class Device extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		
		$this->load->model('M_Device');
		$this->load->model('M_SubCentre');
		$this->load->model('M_Menu');
		$this->load->model('M_Site');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			
			redirect("login");
		}
		
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "device/add";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['sitedata'] = $this->M_Site->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating IP Field
            $this->form_validation->set_rules('device_ip', 'IP', 'required');

            //Validating Site Id Field
            $this->form_validation->set_rules('site_id', 'Site Id', 'required');

            //Validating Device Status Field
            $this->form_validation->set_rules('device_status', 'Status', 'required');

            //Version No
            $this->form_validation->set_rules('version_no', 'Device Version No.', 'required');

			//Serial No
            $this->form_validation->set_rules('serial_no', 'Serial No.', 'required');
			
			//Sub Centre
            $this->form_validation->set_rules('sub_centre', 'Subcentre Name.', 'required');
			
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$data = array(    
                              'IP' => $this->input->post('device_ip'),
                              'SiteId' => $this->input->post('site_id'),
                              'Status' => $this->input->post('device_status'),
							  'VersionNo' => $this->input->post('version_no'),
							  'SerialNo' => $this->input->post('serial_no'),
							  'SubCentreId' => $this->input->post('sub_centre'),
							  'AddedBy' => get_loggedin_userid(),
							  'ModifiedBy' => get_loggedin_userid(),
							  'ModifyTime' => $dt->format('Y-m-d H:i:s'),
							  'CreateDate' => $dt->format('Y-m-d H:i:s'),
                           );
			
			
			$this->M_Device->save($data);  // Add Device
			
			$data = array(
			              'tableName' => 'tbl_device', 
					      'Description' => 'Device '.$this->input->post('device_ip')." added", 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
			$view_data['err_message'] = "Device Added Successfully";
			$view_data['subcentredata'] = $this->M_Device->getAllSubcentre();
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			$this->load->view('template',$view_data);
		  }			
		}else{
		    
		    $view_data['subcentredata'] = $this->M_Device->getAllSubcentre();
            $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		$old_concatenated_value = "";
		$new_concatenated_value = "";
		$view_data = array();
		$view_data['content_view'] = "device/add";
		$view_data['subcentredata'] = $this->M_Device->getAllSubcentre();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['sitedata'] = $this->M_Site->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'IP' => $this->input->post('device_ip'),
                              'SiteId' => $this->input->post('site_id'),
                              'Status' => $this->input->post('device_status'),
							  'VersionNo' => $this->input->post('version_no'),
							  'SerialNo' => $this->input->post('serial_no'),
							  'SubCentreId' => $this->input->post('sub_centre'),
							  'ModifiedBy' => get_loggedin_userid(),
							  'ModifyTime' => $dt->format('Y-m-d H:i:s')
                          );
		  				 
			$this->M_Device->update($data,$id);
            $view_data['content_view'] = "device/add";
		    $view_data['data'] = $this->M_Device->getDeviceById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
			$new_concatenated_value = $this->M_Device->get_concatenate_value($view_data['data']);
			$old_concatenated_value = $this->session->userdata('old_concate_value');
			$changelog=get_edit_changetrack($old_concatenated_value,$new_concatenated_value,'tbl_device');
			$data = array(
			              'tableName' => 'tbl_device', 
					      'Description' => $changelog, 
                          'DateTime' => $dt->format('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Device->getDeviceById($id);
			$old_concatenated_value = $this->M_Device->get_concatenate_value($view_data['data']);
			$this->session->set_userdata('old_concate_value', $old_concatenated_value);
			$view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_Device->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "device/device_list";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		
		$data = array(    
                        'DevState' => 3
				      );
		  				 
	    $this->M_Device->delete($data,$id);  // Delete Device (Soft Delete)
		
		$data = array(
			              'tableName' => 'tbl_device', 
					      'Description' => 'Device '.$this->M_Device->GetDeviceIPById($id)." deleted", 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
		$this->M_UserActivity->save($data);
		
		$view_data = array();
		$view_data['data'] = $this->M_Device->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "device/device_list";
		$this->load->view('template',$view_data);
			
	}
	
	
}

?>