<?php
class SubCentre extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_SubCentre');
		$this->load->model('M_Region');
		$this->load->model('M_Menu');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "subcentre/addsubcentre";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['regiondata'] = $this->M_Region->getAll();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Subcentre Field
            $this->form_validation->set_rules('subcentre_name', 'Subcentre Name', 'required');

            //Validating Address Field
            $this->form_validation->set_rules('address', 'Address', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'RegionId' => $this->input->post('region'),
							  'Name' => $this->input->post('subcentre_name'),
                              'Address' => $this->input->post('address'),
                              'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              
					      );
			
			$this->M_SubCentre->save($data);  // Add Device
			$data = array(
			              'tableName' => 'tbl_subcentre', 
					      'Description' => 'Subcentre ('. $this->input->post('subcentre_name') . ') created under '. $this->M_Region->getRegionNameById($this->input->post('region')).' region', 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
			$this->M_UserActivity->save($data);
			$view_data['err_message'] = "Subcentre Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "subcentre/addsubcentre";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['regiondata'] = $this->M_Region->getAll();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'RegionId' => $this->input->post('region'),
							  'Name' => $this->input->post('subcentre_name'),
                              'Address' => $this->input->post('address'),
                              'ModifyTime' => date('Y-m-d H:i:s'),
							  
                          );
		  				 
			$this->M_SubCentre->update($data,$id);  // Update Device 
            $view_data['content_view'] = "subcentre/addsubcentre";
		    $view_data['data'] = $this->M_SubCentre->getSubcentreById($id);
			
			$new_concatenated_value = $this->M_SubCentre->get_concatenate_value($view_data['data']);
			$old_concatenated_value = $this->session->userdata('old_concate_value');
			$changelog=get_edit_changetrack($old_concatenated_value,$new_concatenated_value,'tbl_subcentre');
			$data = array(
			              'tableName' => 'tbl_subcentre', 
					      'Description' => $changelog, 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					    );
					
			$this->M_UserActivity->save($data);
			
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_SubCentre->getSubcentreById($id);
			$old_concatenated_value = $this->M_SubCentre->get_concatenate_value($view_data['data']);
			$this->session->set_userdata('old_concate_value', $old_concatenated_value);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$num_rows=$this->M_SubCentre->count_all();
		
		$view_data = array();
		$view_data['data'] = $this->M_SubCentre->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "subcentre/subcentrelist";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		
		$data = array(
			              'tableName' => 'tbl_subcentre', 
					      'Description' => 'Subcentre ('. $this->M_SubCentre->getSubCentreName($id) . ') deleted', 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
		
		$this->M_SubCentre->delete($id);  // Delete User (Delete)
		$this->M_UserActivity->save($data);
		
		$view_data = array();
		$view_data['data'] = $this->M_SubCentre->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "subcentre/subcentrelist";
		$this->load->view('template',$view_data);
			
	}

}

?>