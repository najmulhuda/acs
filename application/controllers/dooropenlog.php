<?php
class DoorOpenLog extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_DoorOpenLog');
		$this->load->model('M_Menu');
		$this->load->model('M_Site');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	
	
	private $itemsPerPage = 20;

	function index(){
		
		$view_data = array();
		$view_data['sitedata'] = $this->M_Site->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "dooropenlog/dooropenloglist";
	 if($_SERVER['REQUEST_METHOD'] == 'POST'){
	    $view_data['searchedsite'] = trim($this->input->post('site_name'));
	 }else{
		$view_data['searchedsite'] = -1; 
	 }
		
		
		$view_data['data'] = $this->M_DoorOpenLog->getListByPage(0, $this->itemsPerPage, $view_data['searchedsite']);
	   
		$this->load->view('template',$view_data);
	}
    
	function dooropenloglist(){
		$page_index = trim($this->input->post('page_index'));
		$searchedsite = trim($this->input->post('searching_field'));
	    $data = $this->M_DoorOpenLog->getListByPage($page_index, $this->itemsPerPage,$searchedsite);
	    echo json_encode($data);
		
    }
    
}

?>