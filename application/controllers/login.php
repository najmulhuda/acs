<?php
class Login extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Login');
		$this->load->model('M_User');
		$this->load->model('M_UserActivity');
		
	}	

	function check_user_login(){
		    $this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating LoginId Field
            $this->form_validation->set_rules('user_name', 'User Name', 'required');

            //Validating Password Field
            $this->form_validation->set_rules('user_password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
			      $this->load->view('login_view');
            } else { 
			     $view_data = array();
		         $view_data['data'] = $this->M_Login->getUserDetails($this->input->post('user_name'),$this->input->post('user_password'));
				 if(!$view_data['data']){
		           $view_data['content_view'] = 'login.php';
		           $this->load->view('template',$view_data);
		            return;
				 }else{
					 
				  foreach($view_data['data'] as $row){	
				    $userId = $row->id;
				    $logindata = array(
                              'id'  => $row->id,
                              'loginid' => $row->loginid,
                              'employeeid' => $row->employeeid,
							  'roleid' => $row->roleid,
                             );
					
					$this->session->set_userdata('loggedinuser', $logindata);
					$this->load->model('M_Menu');
					$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
					date_default_timezone_set("Asia/Dhaka");
					$data = array(
					      'Description' => 'Logged in as '.$this->M_User->getUserRoleName($userId), 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  $userId,						  
					);
					
					$this->M_UserActivity->save($data);
					
				  }

                  redirect("/");				  
					
					//$this->load->model('M_Home');
		           // $view_data['data'] = $this->M_Home->getAll();
					
		            //$view_data['content_view'] = "home/device_list";
		           // $this->load->view('template',$view_data);
					
		            
				 }
            }
	}
	
	function logout(){
		$this->session->sess_destroy();
		//$view_data['content_view'] = 'login';
		//$this->load->view('template',$view_data);
		redirect("login");
	}
	
	
	function index(){
		$view_data['content_view'] = 'login';
		$view_data['treemenu'] = '';
	    $this->load->view('template',$view_data);
	}
    
	

}

?>