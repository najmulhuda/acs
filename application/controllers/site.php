<?php
class Site extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		
		$this->load->model('M_Site');
		$this->load->model('M_SubCentre');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			
			redirect("login");
		}
		
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "site/addsite";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

          
            //Validating Site Id Field
            $this->form_validation->set_rules('site_name', 'Site Name', 'required');

            //Validating Device Status Field
            $this->form_validation->set_rules('sub_centre', 'Subcentre Name', 'required');

            //Version No
            $this->form_validation->set_rules('contact_no', 'Contact No.', 'required');

			
			
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              
                              'Name' => $this->input->post('site_name'),
                              'SubcentreId' => $this->input->post('sub_centre'),
							  'ContactNo' => $this->input->post('contact_no'),
							  'CreateDate' => date('Y-m-d H:i:s'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
						);
		
			$this->M_Site->save($data);  // Add Device
			$view_data['err_message'] = "Site Added Successfully";
			$view_data['subcentredata'] = $this->M_SubCentre->getAll();
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			$this->load->view('template',$view_data);
		  }			
		}else{
		    
		    $view_data['subcentredata'] = $this->M_SubCentre->getAll();
            $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "site/addsite";
		$view_data['subcentredata'] = $this->M_SubCentre->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                             
                              'Name' => $this->input->post('site_name'),
                              'SubcentreId' => $this->input->post('sub_centre'),
							  'ContactNo' => $this->input->post('contact_no'),
							  'CreateDate' => date('Y-m-d H:i:s'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  
                          );
		  				 
			$this->M_Site->update($data,$id);  // Update Device 
            $view_data['content_view'] = "site/addsite";
		    $view_data['data'] = $this->M_Site->getSiteById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Site->getSiteById($id);
			$view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_Site->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "site/site_list";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		
			    $this->M_Site->delete($id);  // Delete Device (Soft Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_Site->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "site/site_list";
		$this->load->view('template',$view_data);
			
	}
	
	function sitelistbysubcentre(){ // Used to populate site name dropdown based on subcentre
		$str = "<option value=''>--Select--</option>";
		$subcentreId = trim($this->input->post("subcentreId"));
		
		
		$data = $this->M_Site->getSiteBySubcentre($subcentreId);
		
		foreach($data as $row){
			$str = $str."<option value='".$row->id."'>".$row->name."</option>";
		}
		
		echo $str;
	}

}

?>