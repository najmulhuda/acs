<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DeviceStatusLog extends CI_Controller {
    function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_DeviceStatusLog');
		$this->load->model('M_Menu');
		$this->load->model('M_Site');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	
	
	private $itemsPerPage = 20;

	function index(){
		
		$view_data = array();
		//$view_data['data'] = $this->M_DeviceStatusLog->getListByPage(1, $this->itemsPerPage);
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "devicestatuslog/devicestatuslog_list";
	 if($_SERVER['REQUEST_METHOD'] == 'POST'){
	    $view_data['searchedsite'] = trim($this->input->post('site_name'));
	 }else{
		$view_data['searchedsite'] = -1; 
	 }
		
		
		$view_data['data'] = $this->M_DeviceStatusLog->getListByPage(0, $this->itemsPerPage, $view_data['searchedsite']);
	   
		$this->load->view('template',$view_data);
	}
    
	function devicestatusloglist(){
		$page_index = trim($this->input->post('page_index'));
		$searchedsite = trim($this->input->post('searching_field'));
	    $data = $this->M_DeviceStatusLog->getListByPage($page_index, $this->itemsPerPage,$searchedsite);
	    echo json_encode($data);
		
    }
	
}

?>

