<?php
class AlarmLog extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_AlarmLog');
		$this->load->model('M_Menu');
		
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	
	
	private $itemsPerPage = 20;

	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_AlarmLog->getListByPage(0, $this->itemsPerPage);
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "alarmlog/alarmloglist";
		$this->load->view('template',$view_data);
	}
    
	function alarmloglist(){
		$page_index = trim($this->input->post('page_index'));
		$data= $this->M_AlarmLog->getListByPage($page_index, $this->itemsPerPage);
		echo json_encode($data);
		
    }
    
}

?>