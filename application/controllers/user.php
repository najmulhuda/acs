<?php
class User extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Menu');
		$this->load->model('M_Employee');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function add()
	{
	  
		$view_data = array();
		$view_data['content_view'] = "user/adduser";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		
		
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['userroledata'] = $this->M_User->getRoles();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating LoginId Field
            $this->form_validation->set_rules('login_id', 'Login Id', 'required');
			$this->form_validation->set_rules('fname', 'Full name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');

            //Validating Password Field
            $this->form_validation->set_rules('password', 'Password', 'required');

            //Validating Status Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			
			if($this->M_Employee->IsValidEmployeeId($this->input->post('employee_id'))){
				$data = array(    
                              'LoginId' => $this->input->post('login_id'),
							  'FullName' =>$this->input->post('fname'),
							  'Phone' => $this->input->post('phone'),
							  'Email' =>$this->input->post('email'),
                              'Password' => MD5($this->input->post('password')),
                              'EmployeeId' => $this->input->post('employee_id'),
							  'RoleId' => $this->input->post('user_role'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              );
			
			$this->M_User->save($data);  // Add Device
			
			$data = array(
			              'tableName' => 'tbl_user', 
					      'Description' => 'User ('. $this->input->post('login_id') . ') created with role '.$this->M_User->getUserRoleName($this->input->post('user_role')) , 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
			$this->M_UserActivity->save($data);
			
			
			$view_data['err_message'] = "User Added Successfully";
			$this->load->view('template',$view_data);
			
			}else{
				
			$view_data['err_message'] = "Employee not found for given Id.";
			$this->load->view('template',$view_data);
			}
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	function updateadd()
	{
	  
		$view_data = array();
		$view_data['content_view'] = "user/updateadduser";
		$view_data['data'] = "";
		$view_data['mode'] = "updateadd";
		
		
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['userroledata'] = $this->M_User->getRoles();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating LoginId Field
            $this->form_validation->set_rules('login_id', 'Login Id', 'required');
			
			//Validating full name Field
			$this->form_validation->set_rules('fname', 'Full Name', 'required');
			//Validating phone name Field
            $this->form_validation->set_rules('phone', 'Phone No', 'required');
			//Validating email name Field
			$this->form_validation->set_rules('email', 'Email', 'required');
				
            //Validating Password Field
            $this->form_validation->set_rules('password', 'Password', 'required');

            //Validating Status Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			
			if($this->M_Employee->IsValidEmployeeId($this->input->post('employee_id'))){
				$data = array(    
                              'LoginId' => $this->input->post('login_id'),
							  'FullName' => $this->input->post('fname'),
							  'Phone' => $this->input->post('phone'),
							  'Email' => $this->input->post('email'),
                              'Password' => MD5($this->input->post('password')),
                              'EmployeeId' => $this->input->post('employee_id'),
							  'RoleId' => $this->input->post('user_role'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              );
			
			$this->M_User->save($data);  // Add Device
			
			$data = array(
			              'tableName' => 'tbl_user', 
					      'Description' => 'User ('. $this->input->post('login_id') . ') created with role '.$this->M_User->getUserRoleName($this->input->post('user_role')) , 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
			$this->M_UserActivity->save($data);
			
			
			$view_data['err_message'] = "User Added Successfully";
			$this->load->view('template',$view_data);
			
			}else{
				
			$view_data['err_message'] = "Employee not found for given Id.";
			$this->load->view('template',$view_data);
			}
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "user/adduser";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['userroledata'] = $this->M_User->getRoles();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'LoginId' => $this->input->post('login_id'),
                              'EmployeeId' => $this->input->post('employee_id'),
							  'RoleId' => $this->input->post('user_role'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                          );
		  				 
			$this->M_User->update($data,$id);  // Update Device 
            $view_data['content_view'] = "user/adduser";
		    $view_data['data'] = $this->M_User->getUserById($id);
			
			$new_concatenated_value = $this->M_User->get_concatenate_value($view_data['data']);
			$old_concatenated_value = $this->session->userdata('old_concate_value');
			$changelog=get_edit_changetrack($old_concatenated_value,$new_concatenated_value,'tbl_user');
			$data = array(
			              'tableName' => 'tbl_user', 
					      'Description' => $changelog, 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_User->getUserById($id);
			$old_concatenated_value = $this->M_User->get_concatenate_value($view_data['data']);
		
			$this->session->set_userdata('old_concate_value', $old_concatenated_value);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	function changepassword(){
		  $view_data['content_view'] = "user/changepassword";
		  $view_data['username']=$this->M_User->getUserName(get_loggedin_userid());
		  $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		  
		  if($_SERVER['REQUEST_METHOD'] == 'POST'){
			  
			  if($this->M_User->IsOldPasswordMatched(get_loggedin_userid(), $this->input->post('old_password'))){
			  if($this->input->post('new_password') == $this->input->post('confirm_new_password')){
			  $data = array(    
                              'LoginId' => $this->input->post('login_id'),
                              'Password' => md5($this->input->post('new_password')),
                              'ModifyTime' => date('Y-m-d H:i:s'),
							 );
		  				 
			$this->M_User->update($data,get_loggedin_userid());  // Update Device 
            $view_data['data'] = $this->M_User->getUserById(get_loggedin_userid());
			$view_data['mode'] = "changepassword";
			$view_data['err_message'] = "Password Changed Successful";
		    $this->load->view('template',$view_data);
		   }else{
			   $view_data['err_message'] = "New password and confirm new password did not match.";
		       $this->load->view('template',$view_data);  
		   }
	       }else{
				
				$view_data['err_message'] = "Old password is not correct";
		        $this->load->view('template',$view_data);  
		   
		   }
		}else{
			
			$this->load->view('template',$view_data);
		}
		  
     }
	
	function index(){
		
		$num_rows=$this->M_User->count_all();
		
		$view_data = array();
		$view_data['data'] = $this->M_User->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "user/userlist";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		
		 // Delete User (Delete)
		$data = array(
			              'tableName' => 'tbl_user', 
					      'Description' => 'User ('. $this->M_User->getUserName($id) . ') deleted', 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_User->delete($id); 
			$this->M_UserActivity->save($data);
			
		$view_data = array();
		$view_data['data'] = $this->M_User->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "user/userlist";
		$this->load->view('template',$view_data);
			
	}
	
	

}

?>