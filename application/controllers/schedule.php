<?php
class Schedule extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Schedule');
		$this->load->model('M_Menu');
		$this->load->library('calendar');
		if(!is_logged_in()){
			redirect("login");
		}
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "schedule/addschedule";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating description Field
            $this->form_validation->set_rules('description', 'Description', 'required');

            //Validating time Field
            $this->form_validation->set_rules('datetimepicker', 'Time', 'required');

			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			
			//$IsUpdatetime = $this->input->post('updatetime');
			//$this->input->post('cleardata');
			//$this->input->post('readdata');
			//$this->input->post('updatecard');
			
			$data = array(    
                              'Description' => $this->input->post('description'),
                              'Time' => $this->input->post('datetimepicker'),
                              'UpdateTime' => $this->input->post('updatetime'),
							  'ClearData' => $this->input->post('cleardata'),
							  'ReadData' => $this->input->post('readdata'),
							  'UpdateCard' => $this->input->post('updatecard'),
                              
                             );
			
			$this->M_Schedule->save($data);  // Add Device
			$view_data['err_message'] = "Schedule Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "schedule/addschedule";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'Description' => $this->input->post('description'),
                              'Time' => $this->input->post('datetimepicker'),
                              'UpdateTime' => $this->input->post('updatetime'),
							  'ClearData' => $this->input->post('cleardata'),
							  'ReadData' => $this->input->post('readdata'),
							  'UpdateCard' => $this->input->post('updatecard'),
                          );
		  				 
			$this->M_Schedule->update($data,$id);  // Update Device 
            $view_data['content_view'] = "schedule/addschedule";
		    $view_data['data'] = $this->M_Schedule->getScheduleById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Schedule->getScheduleById($id);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_Schedule->getAll();
		$view_data['content_view'] = "schedule/schedulelist";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
				 
		$this->M_Schedule->delete($id);  // Delete Device (Soft Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_Schedule->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "schedule/schedulelist";
		$this->load->view('template',$view_data);
			
	}

}

?>