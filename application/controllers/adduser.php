<?php
class AddUser extends CI_Controller {
	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Menu');
		$this->load->model('M_Employee');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function add()
	{
	  
		$view_data = array();
		$view_data['content_view'] = "user/adduser";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		
		
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['userroledata'] = $this->M_User->getRoles();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating LoginId Field
            $this->form_validation->set_rules('login_id', 'Login Id', 'required');

            //Validating Password Field
            $this->form_validation->set_rules('password', 'Password', 'required');

            //Validating Status Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			
			if($this->M_Employee->IsValidEmployeeId($this->input->post('employee_id'))){
				$data = array(    
                              'LoginId' => $this->input->post('login_id'),
                              'Password' => MD5($this->input->post('password')),
                              'EmployeeId' => $this->input->post('employee_id'),
							  'RoleId' => $this->input->post('user_role'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              );
			
			$this->M_User->save($data);  // Add Device
			
			$data = array(
			              'tableName' => 'tbl_user', 
					      'Description' => 'User ('. $this->input->post('login_id') . ') created with role '.$this->M_User->getUserRoleName($this->input->post('user_role')) , 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
			$this->M_UserActivity->save($data);
			
			
			$view_data['err_message'] = "User Added Successfully";
			$this->load->view('template',$view_data);
			
			}else{
				
			$view_data['err_message'] = "Employee not found for given Id.";
			$this->load->view('template',$view_data);
			}
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
}
?>