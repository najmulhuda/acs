<?php
class EmployeeActivity extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		
		$this->load->model('M_EmployeeActivity');
		$this->load->model('M_Menu');
		$this->load->model('M_Employee');
		
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	
	function index(){
		
		$view_data = array();
		$view_data['content_view'] = "employeeactivity/employeeactivitylog";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
		   
		       $employeeId = $this->input->post('employee_id');
			
			   $employeeCardNumber = $this->M_Employee->getEmployeeCardNumberByEmployeeId($employeeId);
			
		       $view_data['employeedata'] = $this->M_EmployeeActivity->getSelectedEmployeeActivities($employeeCardNumber);
			 
			   $this->load->view('template',$view_data);
	    }else{
			
			
			$view_data['employeedata'] = $this->M_EmployeeActivity->getAll();
			
			$this->load->view('template',$view_data);
		
		}
	}
    
	

}

?>