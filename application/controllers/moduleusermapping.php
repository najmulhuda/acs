<?php
class ModuleUserMapping extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_ModuleUserMapping');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	
	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_ModuleUserMapping->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid()); 
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "moduleusermapping/moduleusermappingview";
		$this->load->view('template',$view_data);
	}
    
	function assignrole($id){
       
	   $view_data = array();
	   $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid()); 
	   $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	  if($_SERVER['REQUEST_METHOD'] == 'POST'){
		  $this->M_ModuleUserMapping->DeleteRoles($id);
		  $data = $this->input->post('inputcheckbox');
		  foreach( $data as $key ){
			  $chkdata=array(
			            'ModuleId' => $key,
						 'UserId'  => $id,
			  );
			  $this->M_ModuleUserMapping->save($chkdata);
		  }
		  
		  $view_data['data'] = $this->M_ModuleUserMapping->getAll();
		  $view_data['allowedmodule'] = $this->M_ModuleUserMapping->getAllowedModules($id);
		  $view_data['content_view'] = "moduleusermapping/moduleusermappingview";
	      $this->load->view('template',$view_data);
		 		  
	  }	else{
		$view_data['mode']= "edit";
		$view_data['data'] = $this->M_ModuleUserMapping->getAll();
		$view_data['allowedmodule'] = $this->M_ModuleUserMapping->getAllowedModules($id);
		$view_data['content_view'] = "moduleusermapping/moduleusermappingview";
	    $this->load->view('template',$view_data);
	  
	  }
	}
	
	
}

?>