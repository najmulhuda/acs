<?php
class Card extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Card');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "card/addcard";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating IP Field
            $this->form_validation->set_rules('card_id', 'Card Id', 'required');

            //Validating Email Field
            //$this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

            //Validating Mobile no. Field
            //$this->form_validation->set_rules('ctime', 'CTime', 'required');

            //Alarm
            //$this->form_validation->set_rules('site_id', 'Site Id', 'required');

			
			
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else {  
		    //Form input value capture
			$data = array(    
                              'CardNumber' => $this->input->post('card_id'),
                              'Status' => $this->input->post('card_status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                         );
			
			$this->M_Card->save($data);  // Add Card
			$view_data['data'] = "Card added to the list.";
			$view_data['err_message'] = "Card Added Successfully";
			$this->load->view('template',$view_data);
		}			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
	}
	
	function view($id)
	{
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "card/addcard";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'CardNumber' => $this->input->post('card_id'),                             
                              'Status' => $this->input->post('card_status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							
                         );			 
			$this->M_Card->update($data,$id);  // Update Device 
            $view_data['content_view'] = "card/addcard";
		    $view_data['data'] = $this->M_Card->getCardById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);			
		}else{
		    $view_data['data'] = $this->M_Card->getCardById($id);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		$view_data = array();
		$view_data['data'] = $this->M_Card->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "card/cardlist";
		$this->load->view('template',$view_data);
	}
	
	function delete($id){
		
	    $this->M_Card->delete($id);  // Delete Card
		$view_data = array();
		$view_data['data'] = $this->M_Card->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "card/cardlist";
		$this->load->view('template',$view_data);
	}
}
?>