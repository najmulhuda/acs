<?php
class Employee extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Employee');
		$this->load->model('M_SubCentre');
		$this->load->model('M_Menu');
		$this->load->model('M_Card');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "employee/addemployee";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Emplyee Id Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

            //Validating Card Id Field
            //$this->form_validation->set_rules('card_id', 'Card Id', 'required');

            //Validating State Field
            $this->form_validation->set_rules('employee_status', 'State', 'required');

            //Validating sub_centre Field
            $this->form_validation->set_rules('sub_centre', 'Sub Centre', 'required');
            
			//Validating sub_centre Field
            $this->form_validation->set_rules('phone_no', 'Phone No', 'required');

			
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			
			$data = array(    
                              'EmployeeId' => $this->input->post('employee_id'),
							  'EmployeeName' => $this->input->post('employee_name'),
                              'CardNumber' => $this->input->post('card_id'),
                              'State' => $this->input->post('employee_status'),
							  'SubCentreId' => $this->input->post('sub_centre'),
							  'PhoneNo' => $this->input->post('phone_no'),
							  'CardStatus' => $this->input->post('card_status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
							  'CreateDate' => date('Y-m-d H:i:s'),
                              
						 );
			
			$this->M_Employee->save($data);  // Add Device
			
				$data = array(
			              'tableName' => 'tbl_employee', 
					      'Description' => 'Employee('.$this->input->post('employee_id').") added", 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
			 $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			$view_data['err_message'] = "Employee Added Successfully";
			$this->load->view('template',$view_data);
			
			}
		}			
		else{
		   
		    $view_data['subcentredata'] = $this->M_SubCentre->getAll();
			 $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			$this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id){
		
		$view_data = array();
		$view_data['content_view'] = "employee/addemployee";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			//Validating Emplyee Id Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

            //Validating Card Id Field
            //$this->form_validation->set_rules('card_id', 'Card Id', 'required');

            //Validating State Field
            $this->form_validation->set_rules('employee_status', 'State', 'required');

            //Validating sub_centre Field
            $this->form_validation->set_rules('sub_centre', 'Sub Centre', 'required');
            
			//Validating sub_centre Field
            $this->form_validation->set_rules('phone_no', 'Phone No', 'required');
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
			   
			   $data = array(    
                              'EmployeeId' => $this->input->post('employee_id'),
							  'EmployeeName' => $this->input->post('employee_name'),
                              'CardNumber' => $this->input->post('card_id'),
                              'State' => $this->input->post('employee_status'),
							  'SubCentreId' => $this->input->post('sub_centre'),
							  'PhoneNo' => $this->input->post('phone_no'),
							  'CardStatus' => $this->input->post('card_status'),
							  'ModifyTime' => date('Y-m-d H:i:s'),
                          );
		  				 
			$this->M_Employee->update($data,$id);  // Update Device 
			
            $view_data['content_view'] = "employee/addemployee";
		    $view_data['data'] = $this->M_Employee->getEmployeeById($id);
			
			$new_concatenated_value = $this->M_Employee->get_concatenate_value($view_data['data']);
			$old_concatenated_value = $this->session->userdata('old_concate_value');
			$changelog=get_edit_changetrack($old_concatenated_value,$new_concatenated_value,'tbl_employee');
			$data = array(
			              'tableName' => 'tbl_employee', 
					      'Description' => $changelog, 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
			$view_data['subcentredata'] = $this->M_SubCentre->getAll();
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}
		
		}else{
		    $view_data['data'] = $this->M_Employee->getEmployeeById($id);
			
			$old_concatenated_value = $this->M_Employee->get_concatenate_value($view_data['data']);
			$this->session->set_userdata('old_concate_value', $old_concatenated_value);
			
			$view_data['subcentredata'] = $this->M_SubCentre->getAll();
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$num_rows=$this->M_Employee->count_all();
		$view_data = array();
		$view_data['content_view'] = "employee/employee_list";
		
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
	   
	      if($_SERVER['REQUEST_METHOD'] == 'POST'){
		   
		     $subcentreId = $this->input->post('sub_centre');
			 $status= $this->input->post('employee_status');
			 $emid = $this->input->post('employee_id');
			 $phone = $this->input->post('phone_no');  
			   
		       $view_data['employeedata'] = $this->M_Employee->getSelectedEmployee($subcentreId,$status,$emid,$phone);
			   $view_data['subcentredata'] = $this->M_SubCentre->getAll();
			   $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			   
			   $desc= "Employee list searched";
			   $desc .= $this->input->post('employee_id')!= "" ? ' by Employee Id('.$this->input->post('employee_id').')' : "";
			   if($this->input->post('employee_id') != "")
			     $desc .= $this->input->post('phone_no')!= "" ? ', by Phone('.$this->input->post('phone_no').')' : "";
			   else{
				   $desc .= $this->input->post('phone_no')!= "" ? ' by Phone('.$this->input->post('phone_no').')' : "";
			   }
			   
			   $data = array(
			              'tableName' => 'tbl_employee', 
					      'Description' => $desc,
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			$this->M_UserActivity->save($data);
			
			   
		       $this->load->view('template',$view_data);
	    }else{
		     
			 $view_data['employeedata'] = $this->M_Employee->getAll();
			 $view_data['subcentredata'] = $this->M_SubCentre->getAll();
			 $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			 $this->load->view('template',$view_data);
	    }
		
		
	}
	
	function search_employee(){
		$view_data = array();
		$view_data['content_view'] = "employee/employee_list";
		
		 if($_SERVER['REQUEST_METHOD'] == 'POST'){
		   
		     $subcentreId = $this->input->post('sub_centre');
			/* $status= $this->input->post('employee_status');
			 $emid = $this->input->post('employee_id');
			 $phone = $this->input->post('phone_no');*/  
			   
		    $view_data['employeedata'] = $this->M_Employee->getSelectedEmployee($subcentreId/*,$status,$emid,$phone*/);
			$view_data['subcentredata'] = $this->M_SubCentre->getAll();
		    $this->load->view('template',$view_data);
	   
	   }
	}
	
	function delete($id){
	 
			$this->M_Employee->delete($id);  // Delete Employee (Soft Delete)
		
		     $view_data = array();
		    
		     $view_data['employeedata'] = $this->M_Employee->getAll();
			 $view_data['subcentredata'] = $this->M_SubCentre->getAll();
			 $view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			  $view_data['content_view'] = "employee/employee_list";
			 $this->load->view('template',$view_data);
			
	}
	
	function emplyeeinfobyid(){
		$empployeeId = trim($this->input->post('employeeID'));
		$data= $this->M_Employee->getEmployeeInfoByID($empployeeId);
		echo json_encode($data);
	}

}

?>