<?php
class Region extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Region');
		$this->load->model('M_Menu');
		$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function add()
	{
		
		$view_data = array();
		$view_data['content_view'] = "region/addregion";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Field Id Field
            $this->form_validation->set_rules('name', 'Region Name', 'required');

            //Validating Text Value
            //$this->form_validation->set_rules('description', 'Region Description', 'required');
           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'Name' => $this->input->post('name'),
                              'Description' => $this->input->post('description'),
                              
						      
					      );
			
			$this->M_Region->save($data);  // Add Settings
			
			$data = array(
			              'tableName' => 'tbl_region', 
					      'Description' => 'Region ('. $this->input->post('name') . ') created', 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
			$this->M_UserActivity->save($data);
			
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
			$view_data['err_message'] = "Region Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id){
		
		$view_data = array();
		$view_data['content_view'] = "region/addregion";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'name' => $this->input->post('name'),
                              'description' => $this->input->post('description'),
                          );
		  				 
			$this->M_Region->update($data,$id);  // Update Device 
            $view_data['content_view'] = "region/addregion";
		    $view_data['data'] = $this->M_Region->getRegionById($id);
			
			$new_concatenated_value = $this->M_Region->get_concatenate_value($view_data['data']);
			$old_concatenated_value = $this->session->userdata('old_concate_value');
			$changelog=get_edit_changetrack($old_concatenated_value,$new_concatenated_value,'tbl_region');
			$data = array(
			              'tableName' => 'tbl_region', 
					      'Description' => $changelog, 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					    );
					
			$this->M_UserActivity->save($data);
			
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Region->getRegionById($id);
			
			$old_concatenated_value = $this->M_Region->get_concatenate_value($view_data['data']);
			$this->session->set_userdata('old_concate_value', $old_concatenated_value);
			
			$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$num_rows=$this->M_Region->count_all();
		
		$view_data = array();
		$view_data['data'] = $this->M_Region->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		
		$view_data['content_view'] = "region/regionlist";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		  				 
		
		$data = array(
			              'tableName' => 'tbl_region', 
					      'Description' => 'Region ('. $this->M_Region->getRegionNameById($id) . ') deleted', 
                          'DateTime' => date('Y-m-d H:i:s'),	
                          'UserId' =>  get_loggedin_userid(),						  
					);
					
			
		
		
		$this->M_Region->delete($id);  // Delete User (Delete)
		$this->M_UserActivity->save($data);
		
		$view_data = array();
		$view_data['data'] = $this->M_Region->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		
		$view_data['content_view'] = "region/regionlist";
		$this->load->view('template',$view_data);
			
	}

}

?>