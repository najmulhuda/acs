<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DeviceActivity extends CI_Controller {



	function __construct()

	{
		date_default_timezone_set("Asia/Dhaka");

		parent::__construct();

		$this->load->model('M_DeviceActivity');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}
	}

    private $itemsPerPage = 20;

	function index(){
		$view_data = array();
		//$view_data['data'] = $this->M_DeviceStatusLog->getListByPage(1, $this->itemsPerPage);
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
	    $view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$config['base_url'] = site_url('devicestatuslog/index');
        $config['total_rows'] = $this->db->count_all('acs');
        $config['per_page'] = "100";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"]/$config["per_page"];
        $config["num_links"] = floor($choice);

        // integrate bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '�';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '�';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
       //$this->pagination->initialize($config);

        $view_data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        // get books list
        $view_data['booklist'] = $this-> M_DeviceActivity->get_books($config["per_page"], $view_data['page'], NULL);
        
       // $data['pagination'] = $this->pagination->create_links();
       $view_data['content_view'] = "deviceactivity/deviceactivitylist";
        // load view
        $this->load->view('template',$view_data);
	}
	function search()
    {
		$view_data = array();
		//$view_data['data'] = $this->M_DeviceStatusLog->getListByPage(1, $this->itemsPerPage);
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$config['base_url'] = site_url('devicestatuslog/index');
        // get search string
        $search = ($this->input->post("book_name"))? $this->input->post("book_name") : "NIL";

        $search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

        // pagination settings
        $config = array();
        $config['base_url'] = site_url("devicestatuslog/search/$search");
        $config['total_rows'] = $this-> M_DeviceActivity->get_books_count($search);
        $config['per_page'] = "100";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"]/$config["per_page"];
        $config["num_links"] = floor($choice);

        // integrate bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
       // $this->pagination->initialize($config);

        $view_data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        // get books list
        $view_data['booklist'] = $this-> M_DeviceActivity->get_books($config['per_page'], $view_data['page'], $search);

        //$data['pagination'] = $this->pagination->create_links();
       $view_data['content_view'] = "deviceactivity/deviceactivitylist";
        //load view
       $this->load->view('template',$view_data);
    }
		
    
	/*function deviceactivitylist(){
		$page_index = trim($this->input->post('page_index'));
		$data= $this->M_DeviceActivity->getListByPage($page_index, $this->itemsPerPage);
		echo json_encode($data);
		
    }*/

	function select_by_date_range() {
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$date1 = $this->input->post('date_from');
		$date2 = $this->input->post('date_to');
		$name = $this->input->post('name');
		/*$data = array(
		'date1' => $date1,
		'date2' => $date2
		);*/
		
		$result = $this->M_DeviceActivity->show_data_by_date_range($date1,$date2,$name);
		if ($result != false) {
		$view_data['result_display'] = $result;
		} else {
		$data['result_display'] = "No record found !";
		}
	
		$view_data['content_view'] = "deviceactivity/date_search";
		$this->load->view('template', $view_data);

}
	
	
}

?>

