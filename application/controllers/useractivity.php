<?php
class UserActivity extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		
		$this->load->model('M_UserActivity');
		$this->load->model('M_Menu');
		$this->load->model('M_User');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	
	function index(){
		
		
		$view_data = array();
		
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "useractivity/useractivitylog";
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
		       $loginId = $this->input->post('login_id');
			   $userId = $this->M_User->getUserIdByLoginName($loginId);
			   $view_data['useractivitydata'] = $this->M_UserActivity->getSelectedUserActivities($userId);
			   $this->load->view('template',$view_data);
	    }else{
			
		    $view_data['useractivitydata'] = $this->M_UserActivity->getAll();
	    	$this->load->view('template',$view_data);
			
		}
	}
    
	

}

?>