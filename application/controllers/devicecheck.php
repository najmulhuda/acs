<?php
class DeviceCheck extends CI_Controller {
	
		function __construct()
	{
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Menu');
		//$this->load->model('M_Employee');
		//$this->load->model('M_UserActivity');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}
    function add()
	{
	  
		$view_data = array();
		$view_data['content_view'] = "maintenance/read";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		
		
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['userroledata'] = $this->M_User->getRoles();
        $this->load->view('template',$view_data);

		
		
	}	
}
?>