<?php
class ProcessCommand extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_ProcessCommand');
		$this->load->model('M_SubCentre');
		$this->load->model('M_Menu');
		$this->load->model('M_Site');
		$this->load->model('M_Device');
		if(!is_logged_in()){
			
			redirect("login");
		}
		
	}	
	
	function index(){
		$view_data = array();
		$view_data['content_view'] = "processcommand/comandwizard";
		$view_data['subcentredata'] = $this->M_SubCentre->getAll();
		$view_data['sitedata'] = $this->M_Site->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating IP Field
            $this->form_validation->set_rules('sub_centre', 'Subcentre', 'required');

            //Validating Site Id Field
            $this->form_validation->set_rules('site_id', 'Site Id', 'required');

            //Validating Device Status Field
            $this->form_validation->set_rules('employee_id', 'Employee Id', 'required');

         
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
	         $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			 $data = array(    
                              'User' => getUserName(),
							  'EmployeeId' => $this->input->post('employee_id'),
							  'SubCentreId' => $this->input->post('sub_centre'),
                              'SiteId' => $this->input->post('site_id'),
							  'Purpose' => $this->input->post('purpose'),
                              'CommandType' => 'DOOR_OPEN',
							  'CommandBy' => get_loggedin_userid(),
							  
							  'Status' => 'Pending',
                         );
		  	
			$datatoprocess = array(
			    'IP' => $this->M_Device->GetSiteIP($this->input->post('site_id')),
			    'ProcessCommand' => 'DOOR_OPEN',
			);	
			
			 $this->load->helper('file');
			 $text = '';
			 $siteIP = $this->M_Device->GetSiteIP($this->input->post('site_id'));
			 $deviceId = $this->M_Device->GetDeviceIDBySiteId($this->input->post('site_id'));
			 $commandId =  $this->M_ProcessCommand->save($data);  
			 $fileName = $siteIP."@DOOR_OPEN@". $deviceId."@".$commandId;
			 
			 $folderPath=$this->M_ProcessCommand->GetDoorOpenCommandSendFilePath();
			 
             if ( ! write_file($folderPath. $fileName,  $text))
             {
			    $view_data['err_message'] = "Command Send Failure.";
             }
             else
             {   $view_data['err_message'] = "Command Send Successfully.";
               
		   	 }
			
			
			
			

           
		    $this->load->view('template',$view_data);
	      }
		}else{
			$this->load->view('template',$view_data);
		}
		
	 }
	 
	 private $itemsPerPage = 20;
	 
	 
	 function visitlist(){
		$view_data = array();
		$view_data['data'] = $this->M_ProcessCommand->getListByPage(1, $this->itemsPerPage);
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "processcommand/visitlist";
		$this->load->view('template',$view_data);
	 }
	 
	 function visitlistnextpage(){
		$page_index = trim($this->input->post('page_index'));
		$data= $this->M_ProcessCommand->getListByPage($page_index, $this->itemsPerPage);
		echo json_encode($data);
	 }
    
	
}

?>