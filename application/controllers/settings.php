<?php
class Settings extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Settings');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function add(){
		
		$view_data = array();
		$view_data['content_view'] = "settings/addsettings";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Field Id Field
            $this->form_validation->set_rules('field', 'Field', 'required');

            //Validating Text Value
            $this->form_validation->set_rules('value', 'Value', 'required');
           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'Field' => $this->input->post('field'),
                              'Value' => $this->input->post('value'),
                              
						      
					      );
			
			$this->M_Settings->save($data);  // Add Settings
			$view_data['err_message'] = "Settings Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id){
		
		$view_data = array();
		$view_data['content_view'] = "settings/addsettings";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'Field' => $this->input->post('field'),
                              'Value' => $this->input->post('value'),
                          );
		  				 
			$this->M_Settings->update($data,$id);  // Update Device 
            $view_data['content_view'] = "settings/addsettings";
		    $view_data['data'] = $this->M_Settings->getSettingsById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Settings->getSettingsById($id);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$num_rows=$this->M_Settings->count_all();
		
		$view_data = array();
		$view_data['data'] = $this->M_Settings->getAll();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "settings/settingslist";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		  				 
		$this->M_Settings->delete($id);  // Delete User (Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_Settings->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "settings/settingslist";
		$this->load->view('template',$view_data);
			
	}

}

?>