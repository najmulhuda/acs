<?php
class ChangeLog extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_ChangeLog');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}		
	}	

	function add()
	{
		

		$view_data = array();
		$view_data['content_view'] = "changelog/addchangelog";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Alarmtext Id Field
            $this->form_validation->set_rules('device_id', 'Device Id', 'required');

            //Validating Text Field
            $this->form_validation->set_rules('alarm', 'Alarm', 'required');

            //Validating Description Field
            $this->form_validation->set_rules('time', 'Time', 'required');

           
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'DeviceId' => $this->input->post('device_id'),
                              'Alarm' => $this->input->post('alarm'),
                              'Time' => $this->input->post('time'),
						      
					      );
			
			$this->M_AlarmText->save($data);  // Add Device
			$view_data['err_message'] = "AlarmText Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function edit($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "alarmlog/addalarmlog";
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'DeviceId' => $this->input->post('device_id'),
                              'Alarm' => $this->input->post('alarm'),
                              'Time' => $this->input->post('time'),
							 
							  
                          );
		  				 
			$this->M_AlarmLog->update($data,$id);  // Update Device 
            $view_data['content_view'] = "alarmlog/addalarmlog";
		    $view_data['data'] = $this->M_AlarmLog->getAlarmLogById($id);
		    $view_data['mode'] = "edit";
			$view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_AlarmLog->getAlarmLogById($id);
		    $view_data['mode'] = "edit";
		    $this->load->view('template',$view_data);
		}
	}

	
	function index(){
		
		$view_data = array();
		$view_data['data'] = $this->M_ChangeLog->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "changelog/changeloglist";
		$this->load->view('template',$view_data);
	}
    
	function delete($id){
		  				 
		$this->M_AlarmText->delete($id);  // Delete User (Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_AlarmText->getAll();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "alarmtext/alarmtextlist";
		$this->load->view('template',$view_data);
			
	}

}

?>