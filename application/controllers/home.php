<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {

	function __construct()
	{		date_default_timezone_set("Asia/Dhaka");
		parent::__construct();
		$this->load->model('M_Home');		$this->load->model('M_Menu');		if(!is_logged_in()){			redirect("login");		}
	}

	function index(){		$view_data = array();		$view_data['data'] = $this->M_Home->getAll();		//		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();		$view_data['content_view'] = "home/device_list";		$this->load->view('template',$view_data);
	}	
}
?>