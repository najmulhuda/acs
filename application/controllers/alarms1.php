<?php
class Alarms extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Alarm');
		$this->load->model('M_Menu');
		if(!is_logged_in()){
			redirect("login");
		}
		
	}	

	function alarmlist(){
		$view_data = array();
		$view_data['data'] = $this->M_Alarm->getAlarmList();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "alarms/alarmnamelist";
		$this->load->view('template',$view_data);
	}
	
	function alarmstatuslist(){
		$view_data = array();
		$view_data['data'] = $this->M_Alarm->getAlarmStatusList();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['content_view'] = "alarms/alarmdetailslist";
		$this->load->view('template',$view_data);
	}
	
	function addalarmstatus(){
		$view_data = array();
		$view_data['content_view'] = "alarms/addalarmstatus";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['alarmtypedata']=$this->M_Alarm->getAlarmType();
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_name', 'Alarm Name', 'required');
            
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_type', 'Alarm Type', 'required');
			
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_status', 'Alarm Status', 'required');
			
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('description', 'Description', 'required');
         
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'alarmId' => $this->input->post('alarm_name'),
							  'alarmstatus' => $this->input->post('alarm_status'),
                              'Description' => $this->input->post('description'),
						 );
			
			$this->M_Alarm->savealarmstatus($data);  // Add Device
			$view_data['err_message'] = "Alarm Status Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function editalarmstatus($id){
		$view_data = array();
		$view_data['content_view'] = "alarms/addalarmstatus";
		$view_data['data'] = "";
		$view_data['mode'] = "edit";
		$view_data['alarmtypedata']=$this->M_Alarm->getAlarmType();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_name', 'Alarm Name', 'required');
            
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_type', 'Alarm Type', 'required');
			
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_status', 'Alarm Status', 'required');
			
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('description', 'Description', 'required');
         
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'alarmId' => $this->input->post('alarm_name'),
							  'alarmstatus' => $this->input->post('alarm_status'),
                              'description' => $this->input->post('description'),
						 );
			
			$this->M_Alarm->updatestatus($data,$id);  // Add Device
			$view_data['err_message'] = "Update Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
	}
	
	function alarmbytype(){
		$str = "<option value=''>--Select--</option>";
		$alarmtypeid = trim($this->input->post("alarm_TypeId"));
		$data = $this->M_Alarm->getAlarmByType($alarmtypeid);
		
		foreach($data as $row){
			$str = $str."<option value='".$row->id."'>".$row->name."</option>";
		}
		
		echo $str;
	}
	
	function addalarm()
	{
		
        $view_data = array();
		$view_data['content_view'] = "alarms/addalarmname";
		$view_data['data'] = "";
		$view_data['mode'] = "add";
		$view_data['alarmtypedata']=$this->M_Alarm->getAlarmType();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$this->load->library('form_validation');
			$this->load->helper('date');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            //Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_name', 'Alarm Name', 'required');
            
			//Validating Alarmtext Id Field
            $this->form_validation->set_rules('alarm_type', 'Alarm Type', 'required');
         
			if ($this->form_validation->run() == FALSE) {
			      $this->load->view('template',$view_data);
            } else { 
		    //Form input value capture
			$data = array(    
                              'Name' => $this->input->post('alarm_name'),
                              'Description' => $this->input->post('description'),
							  'alarmType' => $this->input->post('alarm_type'),
		                 );
			
			$this->M_Alarm->save($data);  // Add Device
			$view_data['err_message'] = "Alarm Name Added Successfully";
			$this->load->view('template',$view_data);
		  }			
		}else{
		   
		    $this->load->view('template',$view_data);
		}
		
		
	}
	
	function editalarm($id)
	{
		
		$view_data = array();
		$view_data['content_view'] = "alarms/addalarmname";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['mode'] = "edit";
		$view_data['alarmtypedata']=$this->M_Alarm->getAlarmType();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'Name' => $this->input->post('alarm_name'),
                              'Description' => $this->input->post('description'),
							  'alarmType' => $this->input->post('alarm_type'),
					     );
		  				 
			$this->M_Alarm->update($data,$id);  // Update Device 
            $view_data['data'] = $this->M_Alarm->getAlarmDetailsById($id);
		    $view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Alarm->getAlarmDetailsById($id);
		    $this->load->view('template',$view_data);
		}
	}

	////////////////////
		function editalarmtast($id)
	   {
		
		$view_data = array();
		$view_data['content_view'] = "alarms/addalarmstatus";
		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['menu']=$this->M_Menu->getAllowedMenus(get_loggedin_userid());
		$view_data['mode'] = "edit";
		$view_data['alarmtypedata']=$this->M_Alarm->getAlarmType();
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = array(    
                              'alarmId' => $this->input->post('alarm_name'),
							  'alarmstatus' => $this->input->post('alarm_status'),
                              'Description' => $this->input->post('description'),
					     );
		  				 
			$this->M_Alarm->updatestatus($data,$id);  // Update Device 
            $view_data['data'] = $this->M_Alarm->getAlarmDetailsById($id);
		    $view_data['err_message'] = "Update Successful";
		    $this->load->view('template',$view_data);
			
		}else{
		    $view_data['data'] = $this->M_Alarm->getAlarmDetailsById($id);
		    $this->load->view('template',$view_data);
		}
	}
	
	
	function index(){
		
	}
    
	function deletedevicefunction($id){
		 				 
		$this->M_Alarm->deletedevicefunction($id);  // Delete User (Delete)
		
		$view_data = array();
		$view_data['data'] = $this->M_Alarm->getAlarmStatusList();

		$view_data['treemenu'] = $this->M_Menu->GetMenuTreeView();
		$view_data['content_view'] = "alarms/alarmdetailslist";
		$this->load->view('template',$view_data);	
	}

}

?>